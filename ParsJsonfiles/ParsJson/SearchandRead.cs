﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace ParsJson
{
    static class SearchandRead
    {
        static public void Search(string Path, List<ParseItem> parseItems, List<ParseFileResult> result, string exc, string lang)
        {
            if (!Directory.Exists(Path))
                return;

            string[] dirs = Directory.GetDirectories(Path);
            foreach (string dir in dirs)
            {

                DirectoryInfo dirInfo = new DirectoryInfo(dir);
                if (dirInfo.Name == "i18n")
                {
                    string[] files = Directory.GetFiles(dir);
                    foreach (var file in files)
                    {
                        if (file != exc)
                        {
                            FileRead(file, parseItems);
                            FileInfo fileInf = new FileInfo(file);
                            if (fileInf.Name.Substring(0, fileInf.Name.Length - 3) == lang || lang == "")
                            {
                                result.Add(new ParseFileResult()
                                {
                                    parseItem = new List<ParseItem>(parseItems),
                                    fullpath = file,
                                    language = fileInf.Name.Substring(0, fileInf.Name.Length - 3)
                                });
                            }
                            parseItems.Clear();
                        }
                    }
                }
                else
                {
                    Search(dir, parseItems, result, exc, lang);
                }
            }
        }

        public static void FileRead(string path, List<ParseItem> parseItems)
        {
            string json = "{\n";
            using (StreamReader sr = new StreamReader(path, Encoding.Default))
            {
                string line;
                int NumberofLines = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    NumberofLines++;
                    if (NumberofLines > 3)
                        json += "\n" + line;
                }
            }
            JObject obj = JObject.Parse(json.Substring(0, json.LastIndexOf('}')).Replace("`", "'"));
            ParseItem.Parse("", obj.Properties(), path, parseItems);
        }
    }
}
