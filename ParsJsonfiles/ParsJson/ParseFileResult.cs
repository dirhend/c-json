﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
namespace ParsJson
{
    public class ParseFileResult
    {
        public List<ParseItem> parseItem;
        public string language;
        public string fullpath;
        static public void Itemsadd(List<ParseItem> parseItems, JToken prop, string propertyName)
        {
            parseItems.Add(new ParseItem()
            {
                Alias = propertyName,
                key = Convert.ToString(prop)
            });
        }
        static public void addResult(List<ParseFileResult> result)
        {
            using (File.Create($@"netcoreapp2.2\..\..\..\..\..\..\allresult.txt")) ;
            using (StreamWriter sw = new StreamWriter($@"netcoreapp2.2\..\..\..\..\..\..\allresult.txt", false, Encoding.Default))
            {
                foreach (var keyValue in result)
                {
                    sw.WriteLine(keyValue.language + " \n " + keyValue.fullpath + "\n ");
                    foreach (var item in keyValue.parseItem)
                    {
                        sw.WriteLine(item.Alias + " - " + item.key + "\n");
                    }
                }
            }
        }
    }
}
