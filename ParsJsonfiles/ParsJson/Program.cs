﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace ParsJson
{


    public class Program
    {
        static void Main(string[] args)
        {
            List<ParseFileResult> result = new List<ParseFileResult>();
            List<ParseItem> parseItems = new List<ParseItem>();
            Console.WriteLine("Введите путь файла");
            string Path = Console.ReadLine();
            Console.WriteLine("Файлы исключения");
            string exc = Console.ReadLine();
            Console.WriteLine("язык ru/en");
            string lang = Console.ReadLine();
            SearchandRead.Search(Path, parseItems, result, exc, lang);
            ParseFileResult.addResult(result);
        }
    }
}