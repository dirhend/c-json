﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
namespace ParsJson
{
    public class ParseItem
    {
        public string key;
        public string Alias;
        static public void Parse(string parentKey, IEnumerable<JProperty> jProperties, string newPath, List<ParseItem> parseItems)
        {
            foreach (var prop in jProperties)
            {
                var propertyName = parentKey == "" ? prop.Name : $"{parentKey}.{prop.Name}";
                switch (prop.Value.Type)
                {
                    case JTokenType.Array:
                        var array = JArray.Parse(prop.Value.ToString());
                        ParseArray(propertyName, array, newPath, parseItems);
                        break;
                    case JTokenType.Object:
                        var subObj = JObject.Parse(prop.Value.ToString());
                        Parse(propertyName, subObj.Properties(), newPath, parseItems);
                        break;
                    default:
                        ParseFileResult.Itemsadd(parseItems, prop.Value, propertyName);
                        break;
                }
            }
        }
        static public void ParseArray(string propertyName, JArray array, string newPath, List<ParseItem> parseItems)
        {
            int k = 0;
            foreach (var token in array)
            {
                k++;
                if (token.Type == JTokenType.Array || token.Type == JTokenType.Object)
                {
                    var subObj1 = JObject.Parse(token.ToString());
                    Parse(propertyName, subObj1.Properties(), newPath, parseItems);
                }
                else
                {
                    ParseFileResult.Itemsadd(parseItems, token, $"{propertyName}.{k}");
                }
            }

        }
    }
}