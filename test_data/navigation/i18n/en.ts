export const locale = {
  lang: 'en',
  data: {
    NAV: {
      COMMON: 'Common',
      ACCREPLENISHMENT: {
        TITLE: 'Balance'
      },
      FLOWOFFUNDS: {
        TITLE: 'Flow of funds'
      },
      MAIN: 'Main',
      SALON: {
        TITLE: 'Salons'
      },
      ORDER: {
        TITLE: 'Orders'
        // 'BADGE': '15'
      },
      SUPPORT: {
        TITLE: 'Поддержка'
      },
      RATES: {
        TITLE: 'Rates'
      },
      ORDERSYNCLIST: {
        TITLE: 'History'
      },
      STATE_INFO: {
        TITLE: 'Состояния заказов'
      },
      ABOUTSERVICE: {
        TITLE: 'Об услуге'
      },
      SRVLIST: {
        TITLE: 'Services'
      },
      BONUSES: {
        TITLE: 'Bonuses'
      },
      CALLING: {
        TITLE: 'Callings'
      },

      CUTTINGCLIENTS: {
        TITLE: 'Clients'
      },
      CUTTINGORDER: {
        TITLE: 'Orders'
      },
      CUTTINGORDERARCHIVE: {
        TITLE: 'Архив заказов'
      },
      CUTTINGPAYMENTS: {
        TITLE: 'Платежи'
      },
      CUTTINGSETTINGS: {
        TITLE: 'Settings'
      },
      CUTTINGREPORTS: {
        TITLE: 'Отчеты',
        CUTTING_INFO: 'Информация по раскрою',
        CLIENTS_TURNOVER: 'Оборот по клиентам'
      },
      CUTTINGORDERQUESTION: {
        TITLE: 'Обращения'
      },
      SERVICE: 'Services',
      ORDEREXCHANGE: {
        TITLE: 'Order exchange'
      },
      MATERIALBASE: {
        TITLE: 'Updating price list prices'
      },
      PRICELIST: {
        TITLE: 'Price list',
        PRICELISTS: 'Price lists',
        SALONS: 'Salons'
      },
      STOCK: {
        TITLE: 'Склады'
      },
      SALON_SETTINGS: {
        TITLE: 'Settings'
      },
      CUTTING: {
        TITLE: 'Order processing cutting'
      },
      MANAGERS: {
        TITLE: 'Managers'
      },
      YOUTUBE: {
        TITLE: 'Видеоролики'
      },

      INFO: 'Information',
      ABOUT: {
        TITLE: 'About'
      },
      CHANGELOG: {
        TITLE: 'Изменения'
      }
    }
  }
};
