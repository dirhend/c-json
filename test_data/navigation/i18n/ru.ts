export const locale = {
  lang: 'ru',
  data: {
    NAV: {
      COMMON: 'Общее',
      ACCREPLENISHMENT: {
        TITLE: 'Баланс'
      },
      FLOWOFFUNDS: {
        TITLE: 'Движение редств'
      },

      SALON: {
        TITLE: 'Салоны'
      },
      SUPPORT: {
        TITLE: 'Поддержка'
      },
      ORDER: {
        TITLE: 'Заказы'
        // 'BADGE': '15'
      },
      RATES: {
        TITLE: 'Тарифы'
      },
      ORDERSYNCLIST: {
        TITLE: 'История'
      },
      STATE_INFO: {
        TITLE: 'Состояния заказов'
      },
      ABOUTSERVICE: {
        TITLE: 'Об услуге'
      },

      SRVLIST: {
        TITLE: 'Список услуг'
      },
      BONUSES: {
        TITLE: 'Бонусы'
      },
      CALLING: {
        TITLE: 'Обращения'
      },

      CUTTINGCLIENTS: {
        TITLE: 'Клиенты'
      },
      CUTTINGORDER: {
        TITLE: 'Заказы'
      },
      CUTTINGORDERARCHIVE: {
        TITLE: 'Архив заказов'
      },
      CUTTINGPAYMENTS: {
        TITLE: 'Платежи'
      },
      CUTTINGSETTINGS: {
        TITLE: 'Настройки'
      },
      CUTTINGREPORTS: {
        TITLE: 'Отчеты',
        CUTTING_INFO: 'Информация по раскрою',
        CLIENTS_TURNOVER: 'Оборот по клиентам'
      },
      CUTTINGORDERQUESTION: {
        TITLE: 'Обращения'
      },
      SERVICE: 'Услуги',
      ORDEREXCHANGE: {
        TITLE: 'Синхронизация заказов салона'
      },
      SALONREPORTLIST: {
        TITLE: 'Отчеты'
      },
      MATERIALBASE: {
        TITLE: 'Обновление цен прайс-листа'
      },
      PRICELIST: {
        TITLE: 'Актуализация данных в салоне',
        PRICELISTS: 'Прайс-листы',
        SALONS: 'Салоны',
      },
      STOCK: {
        TITLE: 'Склады'
      },
      SALON_SETTINGS: {
        TITLE: 'Настройки'
      },
      CUTTING: {
        TITLE: 'Обработка заказов раскроя'
      },
      MANAGERS: {
        TITLE: 'Менеджеры'
      },
      YOUTUBE: {
        TITLE: 'Видеоролики'
      },

      INFO: 'Справка',
      ABOUT: {
        TITLE: 'О сервисе'
      },
      CHANGELOG: {
        TITLE: 'Изменения'
      }
    }
  }
};
