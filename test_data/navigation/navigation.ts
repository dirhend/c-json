import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
  {
    'id'      : 'common',
    'title'   : 'Общее',
    'translate': 'NAV.COMMON',
    'hidden'   : false,
    'type'    : 'group',
    'children': [
      {
        'id'   : 'account_replenishment',
        'hidden'   : false,
        'title': 'Баланс',
        'translate': 'NAV.ACCREPLENISHMENT.TITLE',
        'type' : 'item',
        'icon' : 'account_balance_wallet',
        'url'  : '/client/account-replenishment'
      },
      {
        'id'   : 'points',
        'hidden'   : false,
        'title': 'Бонусы',
        'translate': 'NAV.BONUSES.TITLE',
        'type' : 'item',
        'icon' : 'format_bold',
        'url'  : '/points-info'
      },
      {
        'id'   : 'services_list',
        'hidden'   : false,
        'title': 'Список услуг',
        'translate': 'NAV.SRVLIST.TITLE',
        'type' : 'item',
        'icon' : 'extension',
        'url': '/service/list'
      }
    ]
  },
  {
    'id'       : 'services',
    'title'    : 'Услуги',
    'translate': 'NAV.SERVICE',
    'hidden'   : false,
    'type'     : 'group',
    'children' : [
      {
        'id'   : 'order_exchange',
        'hidden'   : false,
        'title': 'Синхронизация заказов салона',
        'translate': 'NAV.ORDEREXCHANGE.TITLE',
        'type' : 'item',
        'icon' : 'business_center',
        'url'  : '/order/list'
      },
      {
        'id'   : 'cutting',
        'hidden'   : false,
        'title': 'Обработка заказов раскроя',
        'translate': 'NAV.CUTTING.TITLE',
        'type' : 'item',
        'icon' : 'content_cut',
        'url'  : '/cutting/order/list'
      },
      {
        'id'   : 'price-list',
        'hidden'   : false,
        'title': 'Актуализация данных в салоне',
        'translate': 'NAV.PRICELIST.TITLE',
        'type' : 'item',
        'icon' : 'format_list_bulleted',
        'url'  : '/price-list/list'
      }
    ]
  },
  {
    'id'      : 'order_exchange_detail',
    'title'   : 'Синхронизация заказов салона',
    'translate': 'NAV.ORDEREXCHANGE.TITLE',
    'type'    : 'group',
    'hidden': true,
    'children': [
      {
        'id'   : 'tariffs',
        'hidden'   : false,
        'title': 'Тарифы',
        'translate': 'NAV.RATES.TITLE',
        'type' : 'item',
        'icon' : 'card_travel',
        'url'  : '/service/1/tariffs'
      },
      {
        'id'   : 'salons',
        'hidden'   : false,
        'title': 'Салоны',
        'translate': 'NAV.SALON.TITLE',
        'type' : 'item',
        'icon' : 'work',
        'url'  : '/order/salon/list'
      },
      {
        'id'   : 'orders',
        'hidden'   : false,
        'title': 'Заказы',
        'translate': 'NAV.ORDER.TITLE',
        'type' : 'item',
        'icon' : 'shopping_basket',
        'url'  : '/order/list'
      },
      {
        'id'   : 'order-sync-list',
        'hidden'   : false,
        'title': 'История',
        'translate': 'NAV.ORDERSYNCLIST.TITLE',
        'type' : 'item',
        'icon' : 'history',
        'url'  : '/order-sync-list'
      },
      {
        'id'   : 'state-info-list',
        'hidden'   : false,
        'title': 'Состояния заказов',
        'translate': 'NAV.STATE_INFO.TITLE',
        'type' : 'item',
        'icon' : 'change_history',
        'url'  : '/state-info-list'
      },
      {
        'id'   : 'questions',
        'hidden'   : false,
        'title': 'Обращения',
        'translate': 'NAV.CALLING.TITLE',
        'type' : 'item',
        'icon' : 'question_answer',
        'url'  : '/question'
      },
      {
        'id'   : 'about-service',
        'hidden'   : false,
        'title': 'Об услуге',
        'translate': 'NAV.ABOUTSERVICE.TITLE',
        'type' : 'item',
        'icon' : 'info_outline',
        'url'  : '/services/1/about'
      }
    ]
  },
  {
    'id'      : 'cutting_detail',
    'title'   : 'Обработка заказов раскроя',
    'translate': 'NAV.CUTTING.TITLE',
    'type'    : 'group',
    'hidden': true,
    'children': [
      {
        'id'   : 'tariffs',
        'hidden'   : false,
        'title': 'Тарифы',
        'translate': 'NAV.RATES.TITLE',
        'type' : 'item',
        'icon' : 'card_travel',
        'url'  : '/service/4/tariffs'
      },
      {
        'id'   : 'managers',
        'hidden'   : false,
        'title': 'Менеджеры',
        'translate': 'NAV.MANAGERS.TITLE',
        'type' : 'item',
        'icon' : 'supervisor_account',
        'url'  : '/cutting/manager/list'
      },
      {
        'id'   : 'cutting-clients',
        'hidden'   : false,
        'title': 'Клиенты',
        'translate': 'NAV.CUTTINGCLIENTS.TITLE',
        'type' : 'item',
        'icon' : 'supervised_user_circle',
        'url'  : '/cutting/client/list'
      },
      {
        'id'   : 'orders',
        'hidden'   : false,
        'title': 'Заказы',
        'translate': 'NAV.CUTTINGORDER.TITLE',
        'type' : 'item',
        'icon' : 'shopping_basket',
        'url'  : '/cutting/order/list'
      },
      {
        'id'   : 'cutting-order-questions',
        'hidden'   : false,
        'title': 'Обращения',
        'translate': 'NAV.CUTTINGORDERQUESTION.TITLE',
        'type' : 'item',
        'icon' : 'question_answer',
        'url'  : '/cutting/question/list',
        'badge': {
          'hidden'   : true,
          'title'    : '0',
          'bg'       : '#09d261',
          'fg'       : '#FFFFFF'
        }
      },
      {
        'id'   : 'cutting-settings',
        'hidden'   : false,
        'title': 'Настройки',
        'translate': 'NAV.CUTTINGSETTINGS.TITLE',
        'type' : 'item',
        'icon' : 'settings',
        'url'  : '/cutting/settings'
      },
      {
        'id'      : 'reports',
        'title'   : 'Отчеты',
        'translate': 'NAV.CUTTINGREPORTS.TITLE',
        'type'    : 'group',
        'hidden': false,
        'children': [
          {
            'id'   : 'cutting-info',
            'hidden'   : false,
            'title': 'Информация по раскрою',
            'translate': 'NAV.CUTTINGREPORTS.CUTTING_INFO',
            'type' : 'item',
            'icon' : 'timeline',
            'url'  : '/cutting/info'
          },
          {
            'id'   : 'cutting-clients-turnover',
            'hidden'   : false,
            'title': 'Оборот по клиентам',
            'translate': 'NAV.CUTTINGREPORTS.CLIENTS_TURNOVER',
            'type' : 'item',
            'icon' : 'account_circle',
            'url'  : '/cutting/clients-turnover'
          },
          {
            'id'   : 'cutting-payments',
            'hidden'   : false,
            'title': 'Платежи',
            'translate': 'NAV.CUTTINGPAYMENTS.TITLE',
            'type' : 'item',
            'icon' : 'payment',
            'url'  : '/cutting/payment-list'
          }
        ]
      },
      {
        'id'   : 'about-service',
        'hidden'   : false,
        'title': 'Об услуге',
        'translate': 'NAV.ABOUTSERVICE.TITLE',
        'type' : 'item',
        'icon' : 'info_outline',
        'url'  : '/services/4/about'
      }
    ]
  },
  {
    'id'      : 'price-lists_detail',
    'title'   : 'Актуализация данных в салоне',
    'translate': 'NAV.PRICELIST.TITLE',
    'type'    : 'group',
    'hidden': true,
    'children': [
      {
        'id'   : 'tariffs',
        'hidden'   : false,
        'title': 'Тарифы',
        'translate': 'NAV.RATES.TITLE',
        'type' : 'item',
        'icon' : 'card_travel',
        'url'  : '/service/3/tariffs'
      },
      {
        'id'   : 'salons',
        'hidden'   : false,
        'title': 'Салоны',
        'translate': 'NAV.PRICELIST.SALONS',
        'type' : 'item',
        'icon' : 'work',
        'url'  : '/price/salon/list'
      },
      {
        'id'   : 'price-lists',
        'hidden': false,
        'title': 'Прайс-листы',
        'translate': 'NAV.PRICELIST.PRICELISTS',
        'type' : 'item',
        'icon' : 'list',
        'url'  : '/price-list/list'
      },
      {
        'id'   : 'stocks',
        'hidden'   : false,
        'title': 'Склады',
        'translate': 'NAV.STOCK.TITLE',
        'type' : 'item',
        'icon' : 'store',
        'url'  : '/stocks'
      },
      {
        'id'   : 'salon-report-list',
        'hidden'   : false,
        'title': 'Отчеты',
        'translate': 'NAV.SALONREPORTLIST.TITLE',
        'type' : 'item',
        'icon' : 'list_alt',
        'url'  : '/salon-report/list'
      },
      {
        'id'   : 'salon-settings',
        'hidden'   : false,
        'title': 'Настройки',
        'translate': 'NAV.SALON_SETTINGS.TITLE',
        'type' : 'item',
        'icon' : 'settings',
        'url'  : '/salon/settings'
      },
      {
        'id'   : 'about-service',
        'hidden'   : false,
        'title': 'Об услуге',
        'translate': 'NAV.ABOUTSERVICE.TITLE',
        'type' : 'item',
        'icon' : 'info_outline',
        'url'  : '/services/3/about'
      }
    ]
  },
  {
    'id'      : 'info',
    'title'   : 'Справка',
    'translate': 'NAV.INFO',
    'hidden'   : false,
    'type'    : 'group',
    'children': [
      {
        'id'   : 'support_list',
        'hidden'   : false,
        'title': 'Поддержка',
        'translate': 'NAV.SUPPORT.TITLE',
        'type' : 'item',
        'icon' : 'help',
        'url': '/support'
      },
      {
        'id'   : 'youtube',
        'hidden'   : false,
        'title': 'Видеоролики',
        'translate': 'NAV.YOUTUBE.TITLE',
        'type' : 'item',
        'icon' : 'video_library',
        function: () => {
          window.open('https://www.youtube.com/playlist?list=PL8VDoOOy-T1ZiU0uBzIBlFKa8_PVw_rAw', '_blank');
        }
      },

      {
        'id'   : 'changelog',
        'hidden'   : false,
        'title': 'Изменения',
        'translate': 'NAV.CHANGELOG.TITLE',
        'type' : 'item',
        'icon' : 'update',
        'url'  : '/changelog'
      },

      {
        'id'   : 'about',
        'hidden'   : false,
        'title': 'О сервисе',
        'translate': 'NAV.ABOUT.TITLE',
        'type' : 'item',
        'icon' : 'info_outline',
        'url'  : '/about-application'
      }

    ]
  },
];
