import 'hammerjs';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AboutAppModule } from '@cloud/main/content/about-app/about-app.module';
import { ServiceListService } from '@cloud/main/content/common/service-list/service-list.service';
import { CuttingServiceModule } from '@cloud/main/content/cutting-service/cutting-service.module';
import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxLocalizedNumbers } from 'ngx-localized-numbers';
import { NgxMaskModule } from 'ngx-mask';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { fuseConfig } from './fuse-config';
import { AboutServiceModule } from './main/content/about-service/about-service.module';
import { ChangeLogModule } from './main/content/changelog/changelog.module';
import { TokenInterceptor } from './main/content/client/authentication/token-manager/token-manager.interceptor';
import { TokenManagerService } from './main/content/client/authentication/token-manager/token-manager.service';
import { ClientModule } from './main/content/client/client.module';
import { CloudCommonModule } from './main/content/common/cloud-common.module';
import { OrderSyncServiceModule } from './main/content/order-sync-service/order-sync-service.module';
import { PriceListUpdateServiceModule } from './main/content/price-list-update-service/price-list-update-service.module';
import { SupportModule } from './main/content/support/support.module';
import { TariffModule } from './main/content/tariff/tariff.module';
import { FuseMainModule } from './main/main.module';
import { FinanceManagerService } from './main/shared/services/finance-manager.service';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot(),
    FuseMainModule,
    ClientModule,
    TariffModule,
    AboutAppModule,
    AboutServiceModule,
    OrderSyncServiceModule,
    CuttingServiceModule,
    PriceListUpdateServiceModule,
    SupportModule,
    CloudCommonModule,
    ChangeLogModule,
    AppRoutingModule,


    // Fuse Main and Shared modules
    FuseModule.forRoot(fuseConfig),
    FuseSharedModule,
    NgxMaskModule.forRoot(),
    NgxLocalizedNumbers.forRoot()
  ],
  providers: [
    FinanceManagerService,
    ServiceListService,
    TokenManagerService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
