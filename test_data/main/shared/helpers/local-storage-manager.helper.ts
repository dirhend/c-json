import { CuttingClientIdentification, UserIdentification } from 'cloud-shared-lib';

export class LocalStorageManager {
  static get client(): UserIdentification {
    const LSClient: string | null = localStorage.getItem('sp_client');

    if (LSClient == null) {
      return undefined;
    } else {
      const clientIdent: UserIdentification = JSON.parse(LSClient);

      return clientIdent;
    }
  }

  static set client(value: UserIdentification) {
    localStorage.setItem('sp_client', JSON.stringify(value));
  }

  static set refreshToken(refreshToken: string) {
    const user = LocalStorageManager.client;
    if (user && user.token) {
      user.token.refresh = refreshToken;
      LocalStorageManager.client = user;
    }
  }

  static get refreshToken() {
    const user = LocalStorageManager.client;
    if (user && user.token) {
      return user.token.refresh;
    }
    return null;
  }

  static get jwtToken() {
    const user = LocalStorageManager.client;
    if (user && user.token) {
      return user.token;
    }
    return null;
  }

  static get accessToken() {
    const user = LocalStorageManager.client;
    if (user && user.token) {
      return user.token.access;
    }
    return null;
  }

  static set accessToken(accessToken: string) {
    const user = LocalStorageManager.client;
    if (user && user.token) {
      user.token.access = accessToken;
      LocalStorageManager.client = user;
    }
  }

  static get displayOrderColumns(): string[] {
    const LSColumns: string | null = localStorage.getItem('order_columns');

    if (LSColumns == null) {
      return [];
    } else {
      const columns: string[] = JSON.parse(LSColumns);

      return columns;
    }
  }

  static set cuttingClient(value: CuttingClientIdentification) {
    localStorage.setItem('cm_client', JSON.stringify(value));
  }

  static set displayOrderColumns(value: string[]) {
    localStorage.setItem('order_columns', JSON.stringify(value));
  }

  static get cuttingClientRegData(): number {
    const userId: string | null = localStorage.getItem('cm_reg_data');
    if (userId) {
      return Number(userId);
    } else {
      return 0;
    }
  }
  static set cuttingClientRegData(value: number) {
    localStorage.setItem('cm_reg_data', value.toString());
  }

  static get orderListFilter(): number[] | undefined {
    const statusIds: string | null = localStorage.getItem('cm_order_list_filter');
    if (statusIds) {
      return JSON.parse(statusIds);
    } else {
      return undefined;
    }
  }
  static set orderListFilter(val: number[]) {
    localStorage.setItem('cm_order_list_filter', JSON.stringify(val));
  }

  static clearUser() {
    localStorage.removeItem('sp_client');
  }
}
