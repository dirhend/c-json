import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TariffSizeUnitOfMeasurementPipe } from './pipes/tariff-size-unit-of-measurement.pipe';
import { OrderSyncLogDescPipe } from '@cloud/main/shared/pipes/order-sync-log-desc.pipe';
import { FileSizePipe } from './pipes/file-size.pipe';
import { DisableControlDirective } from './directives/disable-control.directive';
import { CuttingClientTypePipe } from './pipes/cutting-client-type.pipe';
import { ServiceNamePipe } from './pipes/service-name.pipe';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    TariffSizeUnitOfMeasurementPipe,
    OrderSyncLogDescPipe,
    FileSizePipe,
    DisableControlDirective,
    CuttingClientTypePipe,
    ServiceNamePipe,
  ],
  exports: [
    TariffSizeUnitOfMeasurementPipe,
    OrderSyncLogDescPipe,
    FileSizePipe,
    DisableControlDirective,
    CuttingClientTypePipe,
  ]
})
export class SharedModule { }
