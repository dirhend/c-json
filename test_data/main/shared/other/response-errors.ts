export enum ManagerErrors {
    PasswordExists,
    EmailExists,
    PhoneExists,
}

export enum UserErrors {
    Existed,
    ResendedMailForActivate,
    InvalidData,
}
