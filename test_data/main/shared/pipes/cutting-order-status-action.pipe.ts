import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { OrderStatusAction } from 'cloud-shared-lib';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
  name: 'cuttingOrderStatusAction'
})
export class CuttingOrderStatusActionPipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
    private translationLoader: FuseTranslationLoaderService,
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: OrderStatusAction): any {
    switch (value) {
      case OrderStatusAction.Create:
        return this.translate.get('PIPE.CUTTINGORDERSTATUSACTION.CREATE');

      case OrderStatusAction.CuttingCompleted:
        return this.translate.get('PIPE.CUTTINGORDERSTATUSACTION.CUTTING');

      case OrderStatusAction.Ordering:
        return this.translate.get('PIPE.CUTTINGORDERSTATUSACTION.ORDERING');

      case OrderStatusAction.Confirm:
        return this.translate.get('PIPE.CUTTINGORDERSTATUSACTION.CONFIRM');

      case OrderStatusAction.Paid:
        return this.translate.get('PIPE.CUTTINGORDERSTATUSACTION.PAID');

      case OrderStatusAction.Completed:
        return this.translate.get('PIPE.CUTTINGORDERSTATUSACTION.COMPLETED');

      case OrderStatusAction.Verified:
        return this.translate.get('PIPE.CUTTINGORDERSTATUSACTION.VERIFIED');
    }
  }

}
