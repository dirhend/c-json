import { Pipe, PipeTransform } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

import { ServiceIdEnum } from 'cloud-shared-lib';
import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
  name: 'serviceName'
})
export class ServiceNamePipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
    private translationLoader: FuseTranslationLoaderService,
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: number): Observable<string> {
    switch (value) {
      case ServiceIdEnum.Cutting:
        return this.translate.get('PIPE.SERVICENAME.CUTTING');

      case ServiceIdEnum.OrderExchange:
        return this.translate.get('PIPE.SERVICENAME.ORDEREXCHANGE');

      case ServiceIdEnum.PriceListUpdate:
        return this.translate.get('PIPE.SERVICENAME.PRICELISTUPDATE');
    }
  }

}
