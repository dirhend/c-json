import { Pipe, PipeTransform } from '@angular/core';
import { TariffType } from 'cloud-shared-lib';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
  name: 'tariffSizeUnitOfMeasurement'
})
export class TariffSizeUnitOfMeasurementPipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
    private translationLoader: FuseTranslationLoaderService,
  ) 
  {
    this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: TariffType): Observable<string> {
    switch (value) {
      case TariffType.Count:
        return this.translate.get('PIPE.TARIFFSIZEUNITOFMEASUREMENT.COUNT');

      case TariffType.Time:
        return this.translate.get('PIPE.TARIFFSIZEUNITOFMEASUREMENT.DAYS');
    
      case TariffType.DiskSize:
        return this.translate.get('PIPE.TARIFFSIZEUNITOFMEASUREMENT.DISKSIZE');

      // default:
      //   return '';
    }
  }
}
