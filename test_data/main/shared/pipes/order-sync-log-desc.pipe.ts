import { Pipe, PipeTransform } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';

import { OrderSyncLogType } from 'cloud-shared-lib';
import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
    name: 'orderSyncLogDesc'
})
export class OrderSyncLogDescPipe implements PipeTransform {

    constructor(
        private translate: TranslateService,
        private translationLoader: FuseTranslationLoaderService,
    ) {
        this.translationLoader.loadTranslations(english, russian);
    }

    transform(value: OrderSyncLogType): Observable<string> {
        if (value === null || value === undefined) {
            return this.translate.get('PIPE.ORDERSYNCLOGDESCRIPTION.DEFAULT');
        }

        switch (value) {
            case OrderSyncLogType.Loaded: {
                return this.translate.get('PIPE.ORDERSYNCLOGDESCRIPTION.LOADED');
            }

            case OrderSyncLogType.Unloaded: {
                return this.translate.get('PIPE.ORDERSYNCLOGDESCRIPTION.UNLOADED');
            }

            case OrderSyncLogType.Downloaded: {
                return this.translate.get('PIPE.ORDERSYNCLOGDESCRIPTION.DOWNLOADED');
            }

            default: {
                return this.translate.get('PIPE.ORDERSYNCLOGDESCRIPTION.DEFAULT');
            }
        }
    }

}
