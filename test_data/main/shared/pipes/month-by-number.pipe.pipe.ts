import { Pipe, PipeTransform } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';


@Pipe({
  name: 'monthByNumber'
})
export class MonthByNumberPipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
    private translationLoader: FuseTranslationLoaderService,
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: number): any {

    if (value < 1 || value > 12) {
      return this.translate.get('PIPE.MONTHBYNUMBER.NONE');
    }

    return this.translate.get(`PIPE.MONTHBYNUMBER.${value}`);

  }

}
