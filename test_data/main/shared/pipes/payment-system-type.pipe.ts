import { Pipe, PipeTransform } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { CloudPaymentSysType } from 'cloud-shared-lib';
import { Observable } from 'rxjs/Observable';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
  name: 'paymentSystemType'
})
export class PaymentSystemTypePipe implements PipeTransform {
  constructor(
    private translate: TranslateService, //
    private translationLoader: FuseTranslationLoaderService
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: CloudPaymentSysType): Observable<string> {
    switch (value) {
      case CloudPaymentSysType.Certificate:
        return this.translate.get('PIPE.PAYMENTSYSTYPE.CERTIFICATE');

      case CloudPaymentSysType.Psb:
        return this.translate.get('PIPE.PAYMENTSYSTYPE.CARD');

      case CloudPaymentSysType.PayPal:
        return this.translate.get('PIPE.PAYMENTSYSTYPE.PAYPAL');

      case CloudPaymentSysType.Invoice:
        return this.translate.get('PIPE.PAYMENTSYSTYPE.INVOICE');

      case CloudPaymentSysType.Refund:
        return this.translate.get('PIPE.PAYMENTSYSTYPE.REFUND');

      default:
        return this.translate.get('PIPE.PAYMENTSYSTYPE.DEFAULT');
    }
  }
}
