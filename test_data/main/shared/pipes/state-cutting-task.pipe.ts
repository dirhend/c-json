import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Observable } from 'rxjs/Observable';
import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { StateCuttingTask } from 'cloud-shared-lib';

@Pipe({
  name: 'stateCuttingTask'
})
export class StateCuttingTaskPipe implements PipeTransform {

  constructor(
      private translate: TranslateService,
      private translationLoader: FuseTranslationLoaderService,
  ) {
      this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: StateCuttingTask): Observable<string> {
    switch (value) {
        case StateCuttingTask.Error:
            return this.translate.get('PIPE.STATECUTTINGTASK.ERROR');

        case StateCuttingTask.Success:
            return this.translate.get('PIPE.STATECUTTINGTASK.SUCCESS');

        case StateCuttingTask.Wait:
            return this.translate.get('PIPE.STATECUTTINGTASK.WAIT');

        default:
            return this.translate.get('PIPE.STATECUTTINGTASK.DEFAULT');
    }
  }

}
