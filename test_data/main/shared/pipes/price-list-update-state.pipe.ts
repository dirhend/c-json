import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Observable } from 'rxjs/Observable';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
  name: 'priceListState'
})
export class PriceListStatePipe implements PipeTransform {

  constructor(
      private translate: TranslateService,
      private translationLoader: FuseTranslationLoaderService,
  ) {
      this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: boolean): Observable<string> {
    if (value)
        return this.translate.get('PIPE.PRICELISTSTATE.ISUPDATED');
    else
        return this.translate.get('PIPE.PRICELISTSTATE.ISNOTUPDATED');
  }

}
