import { Pipe, PipeTransform } from '@angular/core';
import { FinancialFlowStatus } from 'cloud-shared-lib';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Observable } from 'rxjs/Observable';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
  name: 'financialFlowStatus'
})
export class FinancialFlowStatusPipe implements PipeTransform {

  constructor(
      private translate: TranslateService,
      private translationLoader: FuseTranslationLoaderService,
  ) {
      this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: FinancialFlowStatus): Observable<string> {
    switch (value) {
      case FinancialFlowStatus.Processed:
      return this.translate.get('PIPE.FINANCIALFLOWSTATUS.WAITCONFIRM');
      
      case FinancialFlowStatus.Success:
      return this.translate.get('PIPE.FINANCIALFLOWSTATUS.PAID');

      case FinancialFlowStatus.Cancel:
      return this.translate.get('PIPE.FINANCIALFLOWSTATUS.CANCELED');
    
      default:
        return this.translate.get('PIPE.FINANCIALFLOWSTATUS.DEFAULT');
    }
  }

}
