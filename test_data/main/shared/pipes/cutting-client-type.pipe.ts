import { Pipe, PipeTransform } from '@angular/core';
import { CuttingClientType } from 'cloud-shared-lib';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Observable } from 'rxjs/Observable';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
  name: 'cuttingClientType'
})
export class CuttingClientTypePipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
    private translationLoader: FuseTranslationLoaderService,
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: CuttingClientType): Observable<string> {

    switch (value) {
      case CuttingClientType.PhysicalPerson:
        return this.translate.get('PIPE.CUTTINGCLIENTTYPE.PHYSICAL');

      case CuttingClientType.JuridicalPerson:
        return this.translate.get('PIPE.CUTTINGCLIENTTYPE.JURIDICAL');
    }

    return null;
  }

}
