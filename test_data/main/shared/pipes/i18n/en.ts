export const locale = {
    lang: 'en',
    data: {
        'PIPE': {
            'TARIFFSIZEUNITOFMEASUREMENT': {
                'COUNT': 'Шт',
                'DAYS': 'Дней',
                'DISKSIZE': 'MB'
            },
            'SALONIDENT': {
                'KEY': 'Ключ',
                'LOGIN': 'Логин',
                'DEFAULT': 'Тип идентификации'
            },
            'SALONIDENTTYPE': {
                'KEY': 'Базис-ключ',
                'LOGIN': 'Логин Базис Онлайн',
                'DEFAULT': 'Неизвестный тип'
            },
            'PAYMENTSYSTYPE': {
                'CERTIFICATE': 'Сертификат',
                'CARD': 'Карта',
                'PAYPAL': 'PayPal',
                'INVOICE': 'Оплата счета',
                'REFUND': 'Возврат средств',
                'DEFAULT': ''
            },
            'ORDERSYNCLOGDESCRIPTION': {
                'LOADED': 'Заказ отправлен в БАЗИС-Облако',
                'UNLOADED': 'Заказ получен из БАЗИС-Облако',
                'DOWNLOADED': 'Скачан на диск',
                'DEFAULT': ''
            },
            'FINANCIALFLOWSTATUS': {
                'WAITCONFIRM': 'Не подтвержден',
                'PAID': 'Выполнен',
                'CANCELED': 'Отменен',
                'DEFAULT': ''
            },
            'CUTTINGCLIENTTYPE': {
                'PHYSICAL': 'Физическое лицо',
                'JURIDICAL': 'Юридическое лицо'
            },
            'CUTTINGORDERSTATUSACTION': {
                'CREATE': 'Создан',
                'CUTTING': 'Произведен раскрой',
                'ORDERING': 'Оформлен',
                'CONFIRM': 'Принят',
                'COMPLETED': 'Выполнен',
                'PAID': 'Оплачен',
                'VERIFIED': 'Проверен администратором'
            },
            'PRICELISTSTATE': {
                'ISUPDATED': 'Обновлен',
                'ISNOTUPDATED': 'Не обновлен'
            },
            'STATECUTTINGTASK': {
                'ERROR': 'Не выполнен',
                'SUCCESS': 'Выполнен',
                'WAIT': 'В процессе',
                'DEFAULT': ''
            },
            'TIME': {
                'SECONDS': '{{ value }} сек.',
                'MINUTES': '{{ value }} мин.',
                'DEFAULT': ''
            },
            'STATECUTTINGSMS': {
                'WAITTRANSFEROPERATOR': 'Ожидает передачи оператору',
                'BADIDPARAM': 'Не корректный ID',
                'DELIVERED': 'Доставлено',
                'EXPIRED': 'Просрочено',
                'NODELIVERED': 'Не доставлено',
                'NOTFOUNDID': 'Указанный ID не найден',
                'TRANSFERTOOPERATOR': 'Передано оператору связи',
                'DEFAULT': 'Неизвестно'
            },
            'SERVICENAME': {
                'CUTTING': 'Обработка заказов раскроя',
                'ORDEREXCHANGE': 'Синхронизация заказов салона',
                'PRICELISTUPDATE': 'Актуализация данных в салоне'
            },
            'MONTHBYNUMBER': {
                'NONE': '',
                1: 'Январь',
                2: 'Февраль',
                3: 'Март',
                4: 'Апрель',
                5: 'Май',
                6: 'Июнь',
                7: 'Июль',
                8: 'Август',
                9: 'Октябрь',
                10: 'Сентябрь',
                11: 'Ноябрь',
                12: 'Декабрь',
            }
        }
    }
};
