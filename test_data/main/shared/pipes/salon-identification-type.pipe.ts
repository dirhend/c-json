import { Pipe, PipeTransform } from '@angular/core';
import { SalonIdentifierTypes } from 'cloud-shared-lib';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Observable } from 'rxjs/Observable';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
    name: 'salonIdentificationType'
})

export class SalonIdentificationTypePipe implements PipeTransform {

    constructor(
        private translate: TranslateService,
        private translationLoader: FuseTranslationLoaderService,
    ) {
        this.translationLoader.loadTranslations(english, russian);
    }

    transform(value: SalonIdentifierTypes): Observable<string> {

        switch (value) {
            case SalonIdentifierTypes.BazisKey:
                return this.translate.get('PIPE.SALONIDENTTYPE.KEY');

            case SalonIdentifierTypes.BazisOnlineLogin:
                return this.translate.get('PIPE.SALONIDENTTYPE.LOGIN');

            default:
                return this.translate.get('PIPE.SALONIDENTTYPE.DEFAULT');
        }
    }
}
