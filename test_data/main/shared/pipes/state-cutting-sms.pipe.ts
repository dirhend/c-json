import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { SmsSendingState } from 'cloud-shared-lib';
import { Observable } from 'rxjs';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Pipe({
  name: 'stateCuttingSms'
})
export class StateCuttingSmsPipe implements PipeTransform {

  constructor(
    private translate: TranslateService,
    private translationLoader: FuseTranslationLoaderService,
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: SmsSendingState): Observable<string> {
    switch (value) {
      case SmsSendingState.WaitTransferOperator:
        return this.translate.get('PIPE.STATECUTTINGSMS.WAITTRANSFEROPERATOR');

      case SmsSendingState.BadIdParam:
        return this.translate.get('PIPE.STATECUTTINGSMS.BADIDPARAM');

      case SmsSendingState.Delivered:
        return this.translate.get('PIPE.STATECUTTINGSMS.DELIVERED');

      case SmsSendingState.Expired:
        return this.translate.get('PIPE.STATECUTTINGSMS.EXPIRED');

      case SmsSendingState.NoDelivered:
        return this.translate.get('PIPE.STATECUTTINGSMS.NODELIVERED');

      case SmsSendingState.NotFoundId:
        return this.translate.get('PIPE.STATECUTTINGSMS.NOTFOUNDID');

      case SmsSendingState.TransferToOperator:
        return this.translate.get('PIPE.STATECUTTINGSMS.TRANSFERTOOPERATOR');

      default:
        return this.translate.get('PIPE.STATECUTTINGSMS.DEFAULT');
    }
  }

}
