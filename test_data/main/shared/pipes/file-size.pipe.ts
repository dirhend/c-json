import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileSize'
})
export class FileSizePipe implements PipeTransform {

  transform(bytes: number = 0 ): string {
    const sizes = ['Б', 'КБ', 'МБ', 'ГБ', 'ТБ'];

    if (bytes === 0) { return 'n/a'; }

    const i = Math.floor(Math.log(bytes) / Math.log(1024));

    if (i === 0) { return bytes + ' ' + sizes[i]; }
    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
  }

}
