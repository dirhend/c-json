import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Observable } from 'rxjs/Observable';
import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { StateCuttingTask } from 'cloud-shared-lib';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  constructor(
      private translate: TranslateService,
      private translationLoader: FuseTranslationLoaderService,
  ) {
      this.translationLoader.loadTranslations(english, russian);
  }

  transform(value: number): Observable<string> {
    let seconds: number = value / 1000;
    seconds = Math.round(seconds);

    if (seconds === 0)
        return this.translate.get('PIPE.TIME.DEFAULT');

    if (seconds < 60)
        return this.translate.get('PIPE.TIME.SECONDS', { value: seconds.toString() });
    else
        return this.translate.get('PIPE.TIME.MINUTES', { value: Math.round((seconds / 60)).toString() });
  }

}
