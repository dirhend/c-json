import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiLinks } from '@cloud/app.api-links';
import { customResponse, ICustomResponce, Stock, Salon } from 'cloud-shared-lib'


export interface IStocksResponce {
  stocksDTO: Stock[];
  totalCount: number;
}

export interface IStockSalonsResponce {
  salonsDTO: Salon[];
  totalCount: number;
}

@Injectable()
export class StockService {

  constructor(
    private http: HttpClient
  ) { }

  getStocks(pageIndex: number, pageSize: number, filterText: string): Observable<ICustomResponce<IStocksResponce>> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText);

    return this.http.get<IStocksResponce>(ApiLinks.stocks, { params: paramsHttp }).pipe(customResponse<IStocksResponce>());
  }

  getSalonsForStock(stockId: number, pageIndex: number, pageSize: number, filterText: string): Observable<ICustomResponce<IStockSalonsResponce>> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText);

    return this.http.get<IStockSalonsResponce>(ApiLinks.stockSalons(stockId), { params: paramsHttp }).pipe(customResponse<IStockSalonsResponce>());
  }

  deleteStock(stockId: number)
  {
    return this.http.delete(ApiLinks.stock(stockId));
  }

  getStock(stockId: number): Observable<Stock>
  {
    return this.http.get<Stock>(ApiLinks.stock(stockId));
  }

  deleteSalonFromStock(stockId: number, salonId: number)
  {
    const paramsHttp: HttpParams = new HttpParams()
      .set('salonId', salonId.toString());

    return this.http.delete(ApiLinks.deleteSalonFromStock(stockId), { params: paramsHttp });
  }

  getSalonsNotAdded(pageIndex: number, pageSize: number, filterText: string): Observable<ICustomResponce<IStockSalonsResponce>> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText);

    return this.http.get<IStockSalonsResponce>(ApiLinks.stockSalonsNotAdded, { params: paramsHttp }).pipe(customResponse<IStockSalonsResponce>());
  }

  postSalonForStock(stockId: number, selectedSalons: number[])
  {
    return this.http.post(ApiLinks.addSalonToStock(stockId), selectedSalons);
  }



}
