import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiLinks } from '@cloud/app.api-links';
import { customResponse, CuttingClientTurnover, ICustomResponce } from 'cloud-shared-lib';
import { Observable } from 'rxjs';

export interface ICuttingClientTurnoverResponse {
  turnovers: CuttingClientTurnover[];
  totalCount: number;
}

@Injectable()
export class CuttingReportsService {
  constructor(private http: HttpClient) {}

  getClientTurnover(
    pageIndex: number,
    pageSize: number,
    filter: string,
    fromDate: Date | null,
    toDate: Date | null
  ): Observable<ICustomResponce<ICuttingClientTurnoverResponse>> {
    let paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filter', filter);

    if (fromDate && toDate) {
      paramsHttp = paramsHttp
        .set('fromDate', fromDate.toISOString())
        .set('toDate', toDate.toISOString());
    }

    return this.http
      .get(ApiLinks.cuttingClientTurnover, { params: paramsHttp })
      .pipe(customResponse<ICuttingClientTurnoverResponse>());
  }
}
