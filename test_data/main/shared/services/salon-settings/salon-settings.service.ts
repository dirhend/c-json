import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiLinks } from '@cloud/app.api-links';
import { StockSettings } from 'cloud-shared-lib';



@Injectable()
export class SalonSettingsService {
  

  constructor(private http: HttpClient) {}

  getStockSettings(): Observable<StockSettings>
  {
    return this.http.get<StockSettings>(ApiLinks.stockSettings);
  }

  postStockSettings(settings: StockSettings): Observable<any>
  {
    return this.http.post(ApiLinks.stockSettings, settings);
  }

}
