import { Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { switchMap, tap, map } from 'rxjs/operators';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogsService {

  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

  constructor(
    public dialog: MatDialog,
    private translate: TranslateService
  ) { }

  confirm(messageTranslateKey: string, value?: { [key: string]: string }): Observable<boolean> {
    this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
      disableClose: false
    });

    return this.translate.get(messageTranslateKey, value).pipe(
      tap((result: string) => (this.confirmDialogRef.componentInstance.confirmMessage = result)),
      switchMap<string, boolean>(result => this.confirmDialogRef.afterClosed()),
      tap(result => (this.confirmDialogRef = null))
    );
  }

}
