import { Injectable, OnDestroy } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { ApiLinks } from '@cloud/app.api-links';
import { LocalStorageManager } from '../../helpers/local-storage-manager.helper';


@Injectable({
  providedIn: 'root'
})
export class NotificationService implements OnDestroy {

  public connection: HubConnection;

  public readonly newValueHubEvent = 'GetCuttingQuestionsCount';
  public readonly updateQuestionsCount = 'UpdateCuttingQuestionsCountAsync';
  public readonly connectToGroup = 'ConnectToGroup';

  constructor() {
  }

  ngOnDestroy(): void {
    this.disconnect();
  }

  connect(/*token: string*/): Promise<void> {
    const user = LocalStorageManager.client;

    if (!user) {
      return Promise.reject(false);
    }

    if (!this.connection) {
      this.connection = new HubConnectionBuilder()
        .withUrl(ApiLinks.hubNotification)// + '?token=' + token)
        .build();

      return new Promise((resolve, reject) => {
        this.connection.start()
          .then(() => {
            console.log('Notify connect');

            this.connection
              .invoke(this.connectToGroup, user.id)
              .then(() => {
                console.log('Group connected ^_^');
                resolve();
              })
              .catch(() => {
                console.log('Group connect error -_-');
                reject();
              });
          })
          .catch(err => {
            console.error(err);
            reject();
          });
      });
    }

    return Promise.resolve();
  }

  disconnect() {
    if (this.connection) {
      this.connection.stop();
      this.connection = undefined;
    }
  }

  public updateCuttingQuestionsCount(): Promise<void> {
    const user = LocalStorageManager.client;

    if (!user) {
      return Promise.reject(false);
    }

    return new Promise<void>((resolve, reject) => {
      this.connection
        .invoke(this.updateQuestionsCount, user.id)
        .then(() => {
          console.log(this.updateQuestionsCount + ' ^_^');
          resolve();
        })
        .catch(() => {
          console.log(this.updateQuestionsCount + ' error -_-');
          reject();
        });
    });
  }

}
