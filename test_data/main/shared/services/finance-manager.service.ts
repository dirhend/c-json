import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ApiLinks } from '../../../app.api-links';
import { Balance } from 'cloud-shared-lib';
import { TariffsData } from 'cloud-shared-lib';

@Injectable()
export class FinanceManagerService {

  onBalanceChanged: BehaviorSubject<any> = new BehaviorSubject({});

  private readonly urlBalance = ApiLinks.balance;

  constructor(
    private http: HttpClient
  ) { }

  getBalance(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.urlBalance)
        .subscribe((response: Balance) => {
          this.onBalanceChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

  getTariffsData(serviceId: number): Promise<TariffsData> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.tariffsData(serviceId))
        .subscribe((response: TariffsData) => {
          resolve(response);
        }, reject);
    });
  }

  recalculateTariffs(serviceId: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.recalculateTariffsData(serviceId))
        .subscribe((response: TariffsData) => {
          resolve(response);
        }, reject);
    });
  }

}
