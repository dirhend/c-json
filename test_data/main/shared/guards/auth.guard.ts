import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';

import { UserIdentification } from 'cloud-shared-lib';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private fuseConfig: FuseConfigService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.authorized()) {
            return true;
        } else {
            this.router.navigate(['/client/auth/login']);

            this.fuseConfig.setConfig({
                layout: {
                    navigation: 'none',
                    toolbar: 'none',
                    footer: 'none'
                }
            });
        }

        return false;
    }

    authorized(): boolean {
        const LSClient: string | null = localStorage.getItem('sp_client');

        if (LSClient == null) {
            return false;
        }
        else {
            const clientIdent: UserIdentification = JSON.parse(LSClient);

            return clientIdent.token.refresh !== '';
        }
    }
}
