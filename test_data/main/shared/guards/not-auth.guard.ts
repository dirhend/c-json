import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { UserIdentification } from 'cloud-shared-lib';

@Injectable()
export class NotAuthGuard implements CanActivate {
    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.authorized()) {
            return true;
        }

        this.router.navigate(['/']);
        return false;
    }

    authorized(): boolean {
        const LSClient: string | null = localStorage.getItem('sp_client');

        if (LSClient == null) {
            return false;
        }
        else {
            const clientIdent: UserIdentification = JSON.parse(LSClient);

            return clientIdent.token.refresh !== '';
        }
    }
}
