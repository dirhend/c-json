export const locale = {
    lang: 'en',
    data: {
        'TOOLBAR': {
            'PROFILE': 'My Profile',
            'LOGOUT': 'Logout',
            'ACCREPLENISHMENT': 'Balance',
            'RECALCTARIFFS': 'Пересчитать',
            'TYPE': {
                '2': '{{ size }}',
                '3': '{{ size }}',
                '4': '{{ size }} МБ'
            },
            'SNACKBAR': {
                'TARIFFDATACECALCULATED': 'Данные по тарифам пересчитаны',
                'TARIFFDATACECALCULATEFAIL': 'Не удалось пересчитать данные по тарифам'
            }
        }
    }
};
