export const locale = {
    lang: 'ru',
    data: {
        'TOOLBAR': {
            'PROFILE': 'Профиль',
            'LOGOUT': 'Выйти',
            'ACCREPLENISHMENT': 'Баланс',
            'RECALCTARIFFS': 'Пересчитать',
            'TYPE': {
                '2': '{{ size }}',
                '3': '{{ size }}',
                '4': '{{ size }} МБ'
            },
            'SNACKBAR': {
                'TARIFFDATACECALCULATED': 'Данные по тарифам пересчитаны',
                'TARIFFDATACECALCULATEFAIL': 'Не удалось пересчитать данные по тарифам'
            }
        }
    }
};
