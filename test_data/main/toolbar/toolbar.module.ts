import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatProgressBarModule,
    MatToolbarModule,
    MatTooltipModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { FuseToolbarComponent } from '@cloud/main/toolbar/toolbar.component';
import { FuseSearchBarModule, FuseShortcutsModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [FuseToolbarComponent],
  imports: [
    RouterModule,

    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatProgressBarModule,
    MatToolbarModule,
    MatTooltipModule,

    FuseSharedModule,
    FuseSearchBarModule,
    FuseShortcutsModule,

    TranslateModule
  ],
  exports: [FuseToolbarComponent]
})
export class FuseToolbarModule {}
