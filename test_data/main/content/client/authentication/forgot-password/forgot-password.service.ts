import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { ApiLinks } from '../../../../../app.api-links';

export enum RecoveryPassErrorEnum
{
    InvalidEmail,
    ManagerForbid,
    UserNotFound,
}

@Injectable()
export class ForgotPasswordService {
    private readonly urlPasswordRecovery: string = ApiLinks.passwordRecovery;

    constructor (private http: Http) { }

    sendEmailForRecovery(email: string): Observable<any> {

        const searchParams: URLSearchParams = new URLSearchParams();
        searchParams.append('email', email);

        return this.http.get(this.urlPasswordRecovery, 
            { search: searchParams })
            .catch(this.handleError);
    }

    private  handleError (error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);

        if (error instanceof Response) {
            return Observable.throw(error);
        }
        else {
            return Observable.throw(errMsg);
        }
    }
}
