export const locale = {
    lang: 'ru',
    data: {
        'FORGOTPASS': {
            'TITLE': 'ВОССТАНОВИТЬ ПАРОЛЬ',
            'SENDMESS': 'ОТПРАВИТЬ ССЫЛКУ',
            'TOLOGIN': 'Вернуться на страницу авторизации',
            'FORM': {
                'EMAIL': 'Email',
                'ERROR': {
                    'EMAILEMPTY': 'Email не задан',
                    'EMAILINVALID': 'Пожалуйста введите действительный Email'
                }
            },
            'SNACKBAR': {
                'EMAILNOTFOUND': 'Email не найден',
                'DATAINVALID': 'Введены некорректные данные',
                'MANAGERFORBID': 'Для восстановления пароля, обратитесь к администратору'
            }          
        }
    }
};
