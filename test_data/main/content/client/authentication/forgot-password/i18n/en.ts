export const locale = {
    lang: 'en',
    data: {
        'FORGOTPASS': {
            'TITLE': 'RECOVER YOUR PASSWORD',
            'SENDMESS': 'SEND RESET LINK',
            'TOLOGIN': 'Go back to login',
            'FORM': {
                'EMAIL': 'Email',
                'ERROR': {
                    'EMAILEMPTY': 'Email is required',
                    'EMAILINVALID': 'Please enter a valid email address'
                }
            },
            'SNACKBAR': {
                'EMAILNOTFOUND': 'E-mail not found',
                'DATAINVALID': 'Data is invalid',
                'MANAGERFORBID': 'Для восстановления пароля, обратитесь к администратору'
            }           
        }
    }
};
