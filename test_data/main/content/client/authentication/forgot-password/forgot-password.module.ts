import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NotAuthGuard } from '../../../../shared/guards/not-auth.guard';
import { FuseSharedModule } from '@fuse/shared.module';

import { FuseForgotPasswordComponent } from './forgot-password.component';
import { ForgotPasswordService } from './forgot-password.service';
import { TranslateModule } from '@ngx-translate/core';
import { MatFormFieldModule, MatIconModule, MatInputModule, MatButtonModule } from '@angular/material';
import { ForResetPasswordComponent } from './for-reset-password/for-reset-password.component';

const routes = [
    {
        path     : 'client/auth/forgot-password',
        component: FuseForgotPasswordComponent,
        canActivate: [NotAuthGuard]
    },
    {
        path     : 'client/auth/forgot-password/confirm-email',
        component: ForResetPasswordComponent,
        canActivate: [NotAuthGuard]
    }
];

@NgModule({
    declarations: [
        FuseForgotPasswordComponent,
        ForResetPasswordComponent
    ],
    imports     : [
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatButtonModule,

        FuseSharedModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        ForgotPasswordService,
        NotAuthGuard
    ]
})

export class ForgotPasswordModule
{

}
