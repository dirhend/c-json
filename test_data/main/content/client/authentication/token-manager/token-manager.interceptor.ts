import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/concatMap';
import 'rxjs/add/observable/concat';
import 'rxjs';
import {
    HttpInterceptor,
    HttpEvent,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse
} from '@angular/common/http';

import { TokenManagerService } from './token-manager.service';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private injector: Injector) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const auth = this.injector.get(TokenManagerService);

        const gg =
            auth
                // Get the latest token from the auth service.
                .token
                // Map the token to a request with the right header set.
                .map(token => {

                    if (request.method === 'POST' || request.method === 'PUT') {
                        request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
                    }

                    return request.clone({ headers: request.headers.set('Authorization', `Bearer ${token}`) });
                })
                // Execute the request on the server.
                .concatMap(authReq => next.handle(authReq))
                // Catch the 401 and handle it by refreshing the token and restarting the chain
                // (where a new subscription to this.auth.token will get the latest token).
                .catch((err, restart) => {
                    // If the request is unauthorized, try refreshing the token before restarting.
                    if (err instanceof HttpErrorResponse && err.status === 401) {
                        return Observable.concat(auth.refreshToken, restart);
                    }
                    throw err;
                });
        return gg;
    }
}
