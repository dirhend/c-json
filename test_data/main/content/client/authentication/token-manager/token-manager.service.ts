import 'rxjs/add/observable/defer';
import 'rxjs/add/operator/ignoreElements';
import 'rxjs/add/operator/isEmpty';
import 'rxjs/add/operator/shareReplay';
import 'rxjs/add/operator/take';

import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Router } from '@angular/router';
import { LocalStorageManager } from '@cloud/main/shared/helpers/local-storage-manager.helper';
import { BehaviorSubject, Observable } from 'rxjs';

import { ApiLinks } from '../../../../../app.api-links';

// import { NotificationService } from '@cloud/main/shared/services/notification/notification.service';

@Injectable()
export class TokenManagerService {

    // Subject tracks the current token, or is null if no token is currently
    // available (e.g. refresh pending).
    public subject = new BehaviorSubject<string | null>(null);

    readonly refreshToken: Observable<any>;
    readonly token: Observable<string>;

    private readonly urlUpdateAccessToken: string = ApiLinks.updateAccessToken;

    constructor(private http: Http,
        private router: Router,
        // private notificationService: NotificationService
    ) {

        // refreshToken, when subscribed, gets the new token from the backend,
        // and then completes without values.
        this.refreshToken = Observable.defer(() => {
            // Defer allows us to easily execute some action when the Observable
            // is subscribed. Here, we set the current token to `null` until the
            // refresh operation is complete. This ensures no requests will be
            // sent with a known bad token.
            this.subject.next(null);

            return this
                // Next, we refresh the token from the server.
                .updateAccessToken()
                // Set it as the active token and set token to local storage
                .do(token => {
                    LocalStorageManager.accessToken = token;

                    return this.subject.next(token);
                },
                    err => {
                        this.logout();
                    })
                // Drop the value, ensuring this Observable only completes when
                // done and doesn't emit.
                .ignoreElements()
                // Finally, share the Observable so we don't attempt multiple
                // refreshes at once.
                .shareReplay();
        });

        // token, when subscribed, returns the latest token.
        this.token = this
            // Read the subject (stream of tokens).
            .subject
            // Filter out the `null` ones. This part ensure we wait for the next
            // good token.
            .filter(token => token !== null)
            // Take the next good token.
            .take(1);

        // There's no current token to start, so refresh to start with. Optionally,
        // we could set token up to refresh on the first subscription.
        //   this.refreshToken.subscribe();

        // Set token from local storage if it is first page loading
        if (this.subject.isEmpty) {
            this.subject.next(LocalStorageManager.accessToken);
        }
    }



    updateAccessToken(): Observable<string> {
        const contentHeaders: Headers = new Headers();
        contentHeaders.append('Accept', 'application/json');
        contentHeaders.append('Content-Type', 'application/json');

        return this.http.post(this.urlUpdateAccessToken, LocalStorageManager.jwtToken, { headers: contentHeaders })
            .map((response: Response) => response.json())
            .map(json => String(json))
            .catch(this.handleError);
    }


    private clearUserInfo() {
        localStorage.removeItem('sp_client');
        this.router.navigateByUrl('/client/auth/login');
        this.subject.next('');
        // this.notificationService.disconnect();
    }

    logout() {
        // const jwtToken = LocalStorageManager.jwtToken;

        // if (jwtToken !== null) {
        //     this.http.post(ApiLinks.logout, jwtToken)
        //         .subscribe(
        //             () => {
        //                 this.clearUserInfo();
        //             },
        //             (error) => {
        //                 this.clearUserInfo();
        //             }
        //         );
        // }
        // else {
        this.clearUserInfo();
        // }
    }

    private handleError(error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);

        if (error instanceof Response) {
            return Observable.throw(error);
        }
        else {
            return Observable.throw(errMsg);
        }
    }
}
