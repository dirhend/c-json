import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivateAccountComponent } from './activate-account.component';
import { NotAuthGuard } from '../../../../shared/guards/not-auth.guard';

const routes: Routes = [
  {
    path: 'client/auth/activate-account',
    component: ActivateAccountComponent,
    canActivate: [NotAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [NotAuthGuard]
})
export class ActivateAccountRoutingModule { }
