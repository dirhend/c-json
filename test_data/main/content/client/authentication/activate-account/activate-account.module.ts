import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivateAccountRoutingModule } from './activate-account-routing.module';
import { ActivateAccountComponent } from './activate-account.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    MatIconModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    ActivateAccountRoutingModule
  ],
  declarations: [ActivateAccountComponent]
})
export class ActivateAccountModule { }
