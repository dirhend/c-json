export const locale = {
    lang: 'en',
    data: {
        'ACTIVACCOUNT': {
            'TITLE': 'Account is activated!',
            'MESSAGE': `
                <p>You have successfully registered in the service 
                    <a href="/">BAZIS-Cloud</a>.</p>
                <p>
                    Go to the link "Go to login page" and enter the data that you specified during registration.
                </p>`,
            'TOLOGIN': 'Go to login page'
        }
    }
};
