import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseLockComponent } from './lock.component';
import { NotAuthGuard } from '../../../../shared/guards/not-auth.guard';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'client/auth/lock',
        component: FuseLockComponent,
        canActivate: [NotAuthGuard]
    }
];

@NgModule({
    declarations: [
        FuseLockComponent
    ],
    imports     : [
        MatIconModule,

        FuseSharedModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})

export class LockModule
{

}
