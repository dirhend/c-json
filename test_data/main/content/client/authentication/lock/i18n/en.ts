export const locale = {
    lang: 'en',
    data: {
        'ACCOUNTLOCK': {
            'TITLE': 'YOUR ACCOUNT IS LOCKED',
            'DESCRIPTION': 'Your account has not been activated or was blocked by the administrator.',
            'TOLOGIN': 'Go to the login page'            
        }
    }
};
