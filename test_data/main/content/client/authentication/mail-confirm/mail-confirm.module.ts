import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseMailConfirmComponent } from './mail-confirm.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'client/auth/mail-confirm',
        component: FuseMailConfirmComponent
    }
];

@NgModule({
    declarations: [
        FuseMailConfirmComponent
    ],
    imports     : [
        MatIconModule,

        FuseSharedModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ]
})

export class MailConfirmModule
{

}
