export const locale = {
    lang: 'en',
    data: {
        'CONFIRMMESS': {
            'TITLE': 'Confirm your email address!',
            'MESSAGE': 'To your e-mail ({{value}}) was sent email to activate account.',
            'NOTE': 'Check your mailbox and click the link provided in the email to activate your account.',
            'TOLOGIN': 'Go back to login page'
        }
    }
};
