import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ApiLinks } from '../../../../../app.api-links';

@Injectable()
export class ResetPasswordService {
    private urlCheckRecoveryKey: string = ApiLinks.checkPasswordRecoveryKey;
    private urlSetNewPassword: string = ApiLinks.setNewPassword;

    constructor (private http: Http) { }

    checkKeyForResetPass(key: string): Observable<any> {
        const searchParams: URLSearchParams = new URLSearchParams();
        searchParams.append('key', key);

        return this.http.get(this.urlCheckRecoveryKey, 
            { search: searchParams });
    }

    setNewPassword(password: string, keyRecovery: string): Observable<any> {
        const contentHeaders: Headers = new Headers();
        contentHeaders.append('Accept', 'application/json');
        contentHeaders.append('Content-Type', 'application/json');

        return this.http.post(this.urlSetNewPassword, 
            { key: keyRecovery, password: password }, 
            { headers: contentHeaders });
    }
}
