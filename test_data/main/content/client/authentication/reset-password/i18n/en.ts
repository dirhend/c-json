export const locale = {
  lang: 'en',
  data: {
    RESETPASS: {
      TITLE: 'RESET YOUR PASSWORD',
      RESETPASS: 'RESET MY PASSWORD',
      TOLOGIN: 'Go back to login',
      FORM: {
        PASSWORD: 'New password',
        ERROR: {
          PASSWORDEMPTY: 'Password is required',
          PASSWORD_IS_SHORT: 'Минимальная длина - {{ minlength }}'
        }
      },
      SNACKBAR: {
        CHANGEFAIL: 'Failed to change password'
      }
    }
  }
};
