export const locale = {
  lang: 'ru',
  data: {
    RESETPASS: {
      TITLE: 'СБРОС ПАРОЛЯ',
      RESETPASS: 'СБРОСИТЬ ПАРОЛЬ',
      TOLOGIN: 'Вернуться на страницу авторизации',
      FORM: {
        PASSWORD: 'Новый пароль',
        ERROR: {
          PASSWORDEMPTY: 'Пароль не задан',
          PASSWORD_IS_SHORT: 'Минимальная длина - {{ minlength }}'
        }
      },
      SNACKBAR: {
        CHANGEFAIL: 'Не удалось сменить пароль'
      }
    }
  }
};
