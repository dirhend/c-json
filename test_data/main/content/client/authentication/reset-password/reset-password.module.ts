import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseResetPasswordComponent } from './reset-password.component';
import { ResetPasswordService } from './reset-password.service';
import { NotAuthGuard } from '../../../../shared/guards/not-auth.guard';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatFormFieldModule, MatButtonModule, MatInputModule, MatIconModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';

const routes = [
    {
        path     : 'client/auth/reset-password',
        component: FuseResetPasswordComponent,
        canActivate: [NotAuthGuard]
    }
];

@NgModule({
    declarations: [
        FuseResetPasswordComponent
    ],
    imports     : [
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,

        FuseSharedModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        ResetPasswordService,
        NotAuthGuard
    ]
})

export class ResetPasswordModule
{

}
