export const locale = {
    lang: 'en',
    data: {
        'LOGIN': {
            'TITLE': 'LOGIN TO YOUR ACCOUNT',
            'LOGIN': 'LOGIN',
            'NOACCOUNT': "Don't have an account?",
            'SIGNUP': 'Create an account',
            'FORGOTPASS': 'Forgot Password?',
            'FORM': {
                'EMAIL': 'Email',
                'PASSWORD': 'Password',
                'ERROR': {
                    'PASSWORDEMPTY': 'Password is required',
                    'EMAILEMPTY': 'Email is required',
                    'EMAILINVALID': 'Please enter a valid email address'
                }
            },
            "SNACKBAR": {
                "DATAINCORRECT": "Username or password is incorrect"
            }
        }
    }
};
