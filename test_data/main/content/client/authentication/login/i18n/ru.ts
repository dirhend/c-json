export const locale = {
    lang: 'ru',
    data: {
        'LOGIN': {
            'TITLE': 'ВХОД',
            'LOGIN': 'ВОЙТИ',
            'NOACCOUNT': 'Еще нет аккаунта?',
            'SIGNUP': 'Создать аккаунт',
            'FORGOTPASS': 'Забыли пароль?',
            'FORM': {
                'EMAIL': 'Email',
                'PASSWORD': 'Пароль',
                'ERROR': {
                    'PASSWORDEMPTY': 'Пароль не задан',
                    'EMAILEMPTY': 'Email не задан',
                    'EMAILINVALID': 'Пожалуйста введите действительный Email'
                }
            },
            "SNACKBAR": {
                "DATAINCORRECT": "Не верно указан логин или пароль"
            }
        }
    }
};
