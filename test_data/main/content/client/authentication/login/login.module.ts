import { NgModule } from '@angular/core';
import {
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatProgressSpinnerModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { NotAuthGuard } from '../../../../shared/guards/not-auth.guard';
import { TokenManagerService } from '../token-manager/token-manager.service';
import { FuseLoginComponent } from './login.component';
import { LoginService } from './login.service';

const routes = [
    {
        path: 'client/auth/login',
        component: FuseLoginComponent,
        canActivate: [NotAuthGuard]
    }
];

@NgModule({
    declarations: [
        FuseLoginComponent
    ],
    imports: [
        MatMenuModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatButtonModule,
        MatProgressSpinnerModule,

        FuseSharedModule,
        TranslateModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        TokenManagerService,
        LoginService,
        NotAuthGuard
    ]
})

export class LoginModule {

}
