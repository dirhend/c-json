import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ApiLinks } from '../../../../../app.api-links';
import { UserIdentification } from 'cloud-shared-lib';

@Injectable()
export class LoginService {
    constructor(private http: Http) { }

    private urlLogin: string = ApiLinks.login;

    login(email: string, password: string): Observable<UserIdentification> {

        const contentHeaders: Headers = new Headers();
        contentHeaders.append('Accept', 'application/json');
        contentHeaders.append('Content-Type', 'application/json');


        return this.http.post(this.urlLogin, { email: email, password: password }, { headers: contentHeaders })
            .map((response: Response) => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);

        if (error instanceof Response) {
            return Observable.throw(error);
        }
        else {
            return Observable.throw(errMsg);
        }
    }
}
