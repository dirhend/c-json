import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangeEmailByKeyComponent } from './change-email-by-key.component';
import { AuthGuard } from '../../../../shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'client/auth/email-change',
    component: ChangeEmailByKeyComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class ChangeEmailByKeyRoutingModule { }
