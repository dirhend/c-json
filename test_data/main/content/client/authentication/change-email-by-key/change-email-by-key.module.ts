import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangeEmailByKeyRoutingModule } from './change-email-by-key-routing.module';
import { ChangeEmailByKeyComponent } from './change-email-by-key.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material';

@NgModule({
  imports: [
    MatIconModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    ChangeEmailByKeyRoutingModule
  ],
  declarations: [ChangeEmailByKeyComponent]
})
export class ChangeEmailByKeyModule { }
