export const locale = {
    lang: 'en',
    data: {
        'CHANGEEMAILBYKEY': {
            'TITLE': 'Email changed',
            'MESSAGE': 'Your account has been successfully linked to the new Email',
            'TOSTARTPAGE': 'Back to the start page'
        }
    }
};
