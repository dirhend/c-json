export const locale = {
    lang: 'ru',
    data: {
        'CHANGEEMAILBYKEY': {
            'TITLE': 'Email изменен',
            'MESSAGE': 'Ваш аккаунт был успешно привязан к новому Email',
            'TOSTARTPAGE': 'Вернуться на стартовую страницу'
        }
    }
};
