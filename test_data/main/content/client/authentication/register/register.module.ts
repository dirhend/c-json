import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseRegisterComponent } from './register.component';
import { RegisterService } from './register.service';
import { NotAuthGuard } from '../../../../shared/guards/not-auth.guard';
import { FuseSharedModule } from '@fuse/shared.module';
import { MatMenuModule, MatFormFieldModule, MatIconModule, MatCheckboxModule, MatInputModule, MatButtonModule, MatProgressSpinnerModule, MatSelectModule } from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { NgxMaskModule } from 'ngx-mask';

const routes = [
    {
        path     : 'client/auth/register',
        component: FuseRegisterComponent,
        canActivate: [NotAuthGuard]
    }
];

@NgModule({
    declarations: [
        FuseRegisterComponent
    ],
    imports     : [
        MatMenuModule,
        MatFormFieldModule,
        MatIconModule,
        MatCheckboxModule,
        MatInputModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatSelectModule,

        FuseSharedModule,
        NgxMaskModule.forChild(),
        TranslateModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        RegisterService,
        NotAuthGuard
    ]
})

export class RegisterModule
{

}
