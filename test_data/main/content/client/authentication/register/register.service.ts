import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { URLSearchParams, Headers } from '@angular/http';
import { ApiLinks } from '../../../../../app.api-links';
import { User } from 'cloud-shared-lib';

@Injectable()
export class RegisterService {
    constructor(private http: Http) { }

    register(client: User): Observable<Number> {

        const contentHeaders: Headers = new Headers();
        contentHeaders.append('Accept', 'application/json');
        contentHeaders.append('Content-Type', 'application/json');

        return this.http.post(ApiLinks.users, client, { headers: contentHeaders })
            .map((response: Response) => response.json())
            .map((res: Response) => Number(res))
            .catch(this.handleError);
    }

    activateClientByKey(keyActivate: string): Observable<any> {
        const searchParams: URLSearchParams = new URLSearchParams();
        searchParams.append('key', keyActivate);

        return this.http.get(ApiLinks.activateClientKey, { search: searchParams });
    }

    private handleError(error: any) {
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);

        if (error instanceof Response) {
            return Observable.throw(error);
        }
        else {
            return Observable.throw(errMsg);
        }
    }
}

export enum UserRegistrStatusError
{
    Existed,
    ResendedMailForActivate,
    InvalidData
}
