export const locale = {
  lang: 'ru',
  data: {
    REGISTER: {
      TITLE: 'РЕГИСТРАЦИЯ',
      CREATEACCOUNT: 'СОЗДАТЬ АККАУНТ',
      HAVEACCOUNT: 'Уже зарегестрированы?',
      LOGIN: 'Войти',
      AGREEMENT: {
        IACCEPT: 'Я принимаю',
        AGREEMENT: 'соглашение'
      },
      FORM: {
        COMPANY: 'Название фирмы',
        SURNAME: 'Фамилия',
        NAME: 'Имя',
        PATRONYMIC: 'Отчество',
        PHONE: 'Телефон',
        EMAIL: 'Email',
        PASSWORD: 'Пароль',
        COUNTRY: 'Страна',
        ERROR: {
          PASSWORDEMPTY: 'Пароль не задан',
          EMAILEMPTY: 'Email не задан',
          EMAILINVALID: 'Пожалуйста введите действительный Email',
          NAMEEMPTY: 'Имя не задано',
          SURNAMEEMPTY: 'Фамилия не задана',
          COUNTRYEMPTY: 'Страна не выбрана',
          PASSWORD_IS_SHORT: 'Минимальная длина - {{ minlength }}'
        }
      },
      SNACKBAR: {
        EMAILEXIST: 'Такой Email уже зарегестрирован',
        INVALIDDATA: 'Данные введены некорректно'
      }
    }
  }
};
