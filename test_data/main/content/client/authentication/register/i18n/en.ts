export const locale = {
  lang: 'en',
  data: {
    REGISTER: {
      TITLE: 'CREATE AN ACCOUNT',
      CREATEACCOUNT: 'CREATE AN ACCOUNT',
      HAVEACCOUNT: 'Already have an account?',
      LOGIN: 'Login',
      AGREEMENT: {
        IACCEPT: 'I read and accept',
        AGREEMENT: 'terms and conditions'
      },
      FORM: {
        COMPANY: 'Company name',
        SURNAME: 'Surname',
        NAME: 'Name',
        PATRONYMIC: 'Patronymic',
        PHONE: 'Mobile number',
        EMAIL: 'Email',
        PASSWORD: 'Password',
        COUNTRY: 'Страна',
        ERROR: {
          PASSWORDEMPTY: 'Password is required',
          EMAILEMPTY: 'Email is required',
          EMAILINVALID: 'Please enter a valid email address',
          NAMEEMPTY: 'Name is required',
          SURNAMEEMPTY: 'Surname is required',
          COUNTRYEMPTY: 'Страна не выбрана',
          PASSWORD_IS_SHORT: 'Минимальная длина - {{ minlength }}'
        }
      },
      SNACKBAR: {
        EMAILEXIST: 'This Email Has already been registered',
        INVALIDDATA: 'Data is invalid'
      }
    }
  }
};
