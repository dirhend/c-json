import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComingSoonComponent } from './coming-soon.component';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'coming-soon',
    component: ComingSoonComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class ComingSoonRoutingModule { }
