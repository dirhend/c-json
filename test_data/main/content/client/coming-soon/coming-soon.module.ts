import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComingSoonRoutingModule } from './coming-soon-routing.module';
import { ComingSoonComponent } from './coming-soon.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    ComingSoonRoutingModule,
    
    FuseSharedModule,
    TranslateModule,
  ],
  declarations: [ComingSoonComponent]
})
export class ComingSoonModule { }
