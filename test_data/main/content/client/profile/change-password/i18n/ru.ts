export const locale = {
    lang: 'ru',
    data: {
        'CHANGEPASS': {
            'TITLE': 'Изменение пароля',
            'SAVE': 'СОХРАНИТЬ',
            'FORM': {
                'OLDPASS': 'Старый пароль',
                'NEWPASS': 'Новый пароль',                  
                'ERROR': {
                }
            },
            'SNACKBAR': {
                'SUCCCHANGE': 'Изменения сохранены',
                'ERRCHANGE': 'Не удалось сохранить изменения'
            }
        }
    }
};
