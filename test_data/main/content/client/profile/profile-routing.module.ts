import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { ProfileService } from './profile.service';
import { AuthGuard } from '../../../shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'client/profile',
    component: ProfileComponent,
    resolve: {
      profile: ProfileService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
