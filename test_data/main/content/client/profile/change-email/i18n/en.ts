export const locale = {
    lang: 'en',
    data: {
        'CHANGEEMAIL': {
            'TITLE': 'Change Email',
            'CHANGE': 'CHANGE',
            'FORM': {
                'NEWEMAIL': 'New Email',                  
                'ERROR': {
                }
            },
            'SNACKBAR': {
                'EMAILSENDED': 'Sent a message asking for confirmation Email',
                'ERREMAILSEND': 'Failed to apply changes'
            }
        }
    }
};
