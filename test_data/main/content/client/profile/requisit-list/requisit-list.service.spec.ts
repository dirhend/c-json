import { TestBed, inject } from '@angular/core/testing';

import { RequisitListService } from './requisit-list.service';

describe('RequisitListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequisitListService]
    });
  });

  it('should be created', inject([RequisitListService], (service: RequisitListService) => {
    expect(service).toBeTruthy();
  }));
});
