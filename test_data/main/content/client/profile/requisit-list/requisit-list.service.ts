import { HttpBackend, HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import { UserRequisit } from 'cloud-shared-lib';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class RequisitListService {
  requisits: UserRequisit[];
  onRequisitsChanged: BehaviorSubject<any> = new BehaviorSubject({});
  requisitsTotalCount = 0;
  defaultRequisitId = 0;

  private readonly urlRequisits: string = ApiLinks.requisits;

  private httpLocal: HttpClient;

  constructor(private handler: HttpBackend, private http: HttpClient) {
    this.httpLocal = new HttpClient(handler);
  }

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([this.getRequisitsList()]).then(() => {
        resolve();
      }, reject);
    });
  }

  getRequisitsList(): Promise<any> {
    const paramsHttp: HttpParams = new HttpParams();

    return new Promise((resolve, reject) => {
      this.http
        .get(this.urlRequisits, { params: paramsHttp })
        .map((res: { totalCount: number; requestsDto: UserRequisit[]; defaultId: number }) => {
          this.requisitsTotalCount = res.totalCount;
          this.defaultRequisitId = res.defaultId;
          return res.requestsDto;
        })
        .subscribe((response: any) => {
          this.requisits = response;
          this.onRequisitsChanged.next(this.requisits);
          resolve(response);
        }, reject);
    });
  }

  addRequisit(item: UserRequisit): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.urlRequisits, item).subscribe((response: UserRequisit) => {
        this.requisits.push(response);
        this.onRequisitsChanged.next(this.requisits);

        this.requisitsTotalCount++;

        resolve(response);
      }, reject);
    });
  }

  editRequisit(item: UserRequisit): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.urlRequisits}/${item.id}`, item).subscribe(() => {
        this.updateRequisit(item);
        this.onRequisitsChanged.next(this.requisits);

        resolve();
      }, reject);
    });
  }

  deleteRequisit(item: UserRequisit): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.urlRequisits}/${item.id}`).subscribe(() => {
        this.requisits.splice(this.requisits.findIndex(s => s.id === item.id), 1);
        this.onRequisitsChanged.next(this.requisits);

        this.requisitsTotalCount--;

        resolve();
      }, reject);
    });
  }

  setDefaultRequest(item: UserRequisit): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.urlRequisits}/${item.id}/SetDefault`, {}).subscribe(() => {
        this.defaultRequisitId = item.id;
        this.onRequisitsChanged.next(this.requisits);

        resolve();
      }, reject);
    });
  }

  private updateRequisit(item: UserRequisit) {
    const requisit = this.requisits.find(s => s.id === item.id);

    requisit.name = item.name;
    requisit.inn = item.inn;
    requisit.officialAddress = item.officialAddress;
    requisit.postAddress = item.postAddress;
  }
}
