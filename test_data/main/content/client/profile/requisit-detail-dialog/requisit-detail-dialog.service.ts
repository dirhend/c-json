import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DadataBankInfo } from 'cloud-shared-lib';
import { ApiLinks } from '@cloud/app.api-links';
import { Observable } from 'rxjs';


@Injectable()
export class RequisitDetailService {

    constructor(private httpClient: HttpClient) {        
    }

    getFirmInfoByInn(inn: string): Observable<any> {
        const url = ApiLinks.clientByInn + inn;
        return this.httpClient.get(url, {});
    }

    getBankInfoByBik(bik: string): Observable<DadataBankInfo> {
        const url = ApiLinks.bankByBik + bik;
        return this.httpClient.get<DadataBankInfo>(url, {});
    }
}
