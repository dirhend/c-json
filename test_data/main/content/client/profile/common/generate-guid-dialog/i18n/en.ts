export const locale = {
    lang: 'en',
    data: {
        'GENGUIDDIALOG': {
            'TITLE': 'Генерация нового ключа доступа',
            'WARNING': 'После генерации нового ключа доступа подключеныйе салоны станут недоступны. Для подтверждения личности необходимо ввести пароль учетной записи.',
            'GENERATE': 'СГЕНЕРИРОВАТЬ',
            'CANCEL': 'ОТМЕНА',
            'PASSWORD': {
                'TITLE': 'Пароль',
                'ERROR': {
                    'REQUIRED': 'Поле не должно быть пустым',
                    'MAXLEN': 'Максимальная длина - 100 символов'
                }
            },
            'SNACKBAR': {
                'NEWGUIDGENERATED': 'The Ключ доступа generated',
                'NEWGUIDGENERATEFAIL': 'Wrong password'
            }
        }
    }
};
