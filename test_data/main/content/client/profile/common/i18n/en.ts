export const locale = {
    lang: 'en',
    data: {
        'COMMON': {
            'TITLE': 'Common',
            'SAVE': 'SAVE',
            'FORM': {
                'GENERATEGUID': 'Сгенерировать новый ключ',
                'GUID': 'Ключ доступа',
                'PUBLIC_GUID': 'Публичный ключ API',
                'COPYGUIDTOCLIPBOARD': 'Copy to clipboard',
                'COMPANY': 'Company name',
                'SURNAME': 'Surname',
                'NAME': 'Name',
                'PATRONYMIC': 'Patronymic',
                'PHONE': 'Mobile number',
                'COUNTRY': 'Страна',
                'CURRENCY': 'Валюта',
                'NOTIFY_IN_SUPPORT': 'Уведомлять при ответе на обращения',
                'ERROR': {
                    'NAMEEMPTY': 'Name is required',
                    'SURNAMEEMPTY': 'Surname is required',
                    'COUNTRYEMPTY': 'Страна не выбрана',
                    'CURRENCYEMPTY': 'Валюта не выбрана'
                }
            },
            'SNACKBAR': {
                'SUCCSAVE': 'Changes are saved',
                'ERRSAVE': 'Failed to save changes',
                'COPYIDTOCLIPBOARD': 'Key copied to the clipboard',
                'NEWGUIDGENERATED': 'Ключ доступа сгенерирован',
                'NEWGUIDGENERATEFAIL': 'Неверный пароль'
            }
        }
    }
};
