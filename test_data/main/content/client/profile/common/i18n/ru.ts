export const locale = {
    lang: 'ru',
    data: {
        'COMMON': {
            'TITLE': 'Общее',
            'SAVE': 'СОХРАНИТЬ',
            'FORM': {
                'GENERATEGUID': 'Сгенерировать новый ключ',
                'GUID': 'Ключ доступа',
                'PUBLIC_GUID': 'Публичный ключ API',
                'COPYGUIDTOCLIPBOARD': 'Копировать',
                'COMPANY': 'Название фирмы',
                'SURNAME': 'Фамилия',
                'NAME': 'Имя',
                'PATRONYMIC': 'Отчество',
                'PHONE': 'Телефон',
                'COUNTRY': 'Страна',
                'CURRENCY': 'Валюта',
                'NOTIFY_IN_SUPPORT': 'Уведомлять при ответе на обращения',
                'ERROR': {
                    'NAMEEMPTY': 'Имя не задано',
                    'SURNAMEEMPTY': 'Фамилия не задана',
                    'COUNTRYEMPTY': 'Страна не выбрана',
                    'CURRENCYEMPTY': 'Валюта не выбрана'
                }
            },
            'SNACKBAR': {
                'SUCCSAVE': 'Изменения сохранены',
                'ERRSAVE': 'Не удалось сохранить изменения',
                'COPIEDTOCLIPBOARD': 'Ключ скопирован в буфер обмена',
                'NEWGUIDGENERATED': 'Ключ доступа сгенерирован',
                'NEWGUIDGENERATEFAIL': 'Неверный пароль'

            }
        }
    }
};
