import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatRadioModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import {
  RequisitDetailService,
} from '@cloud/main/content/client/profile/requisit-detail-dialog/requisit-detail-dialog.service';
import { RequisitListService } from '@cloud/main/content/client/profile/requisit-list/requisit-list.service';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxMaskModule } from 'ngx-mask';

import { ChangeEmailComponent } from './change-email/change-email.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CommonComponent } from './common/common.component';
import { GenerateGuidDialogComponent } from './common/generate-guid-dialog/generate-guid-dialog.component';
import { EmployeeDetailDialogComponent } from './employee-detail-dialog/employee-detail-dialog.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ProfileService } from './profile.service';
import { RequisitDetailDialogComponent } from './requisit-detail-dialog/requisit-detail-dialog.component';
import { RequisitListComponent } from './requisit-list/requisit-list.component';

@NgModule({
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    CdkTableModule,
    MatDialogModule,
    MatTooltipModule,
    MatMenuModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTabsModule,
    MatToolbarModule,
    MatSelectModule,

    CommonModule,
    NgxMaskModule.forChild(),
    FuseSharedModule,
    TranslateModule,
    ProfileRoutingModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    ProfileComponent,
    CommonComponent,
    ChangePasswordComponent,
    ChangeEmailComponent,
    GenerateGuidDialogComponent,
    RequisitListComponent,
    RequisitDetailDialogComponent,
    EmployeeDetailDialogComponent
  ],
  providers: [ProfileService, RequisitListService, RequisitDetailService],
  entryComponents: [GenerateGuidDialogComponent, RequisitDetailDialogComponent, EmployeeDetailDialogComponent]
})
export class ProfileModule {}
