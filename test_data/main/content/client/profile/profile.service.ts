import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpBackend, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { ApiLinks } from '../../../../app.api-links';
import { UserIdentification } from 'cloud-shared-lib';
import { User } from 'cloud-shared-lib';
import { LocalStorageManager } from '@cloud/main/shared/helpers/local-storage-manager.helper';

@Injectable()
export class ProfileService {

  private readonly urlAvatar = ApiLinks.avatar;
  private readonly urlChangePassword = ApiLinks.changePassword;
  private readonly urlSendChangeEmailMess = ApiLinks.sendChangeEmailMess;
  private readonly urlSetNewEmail = ApiLinks.setNewEmail;
  private readonly urlNewGUID = ApiLinks.getNewGUID;


  client: User;
  onClientChanged: BehaviorSubject<any> = new BehaviorSubject({});

  private httpLocal: HttpClient;

  constructor(
    private handler: HttpBackend,
    private http: HttpClient,
  ) {
    this.httpLocal = new HttpClient(handler);
  }

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getClient()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getClient(): Promise<any> {
    return new Promise((resolve, reject) => {
      const clientLS = this.clientLS;
      if (!clientLS) {
        reject();
      }

      this.http.get<User>(ApiLinks.users + '/' + clientLS.id)
        .subscribe((response: any) => {
          this.client = response;

          this.onClientChanged.next(this.client);
          resolve(response);
        }, reject);
    });
  }

  saveClient(client: User): Promise<any> {
    return new Promise((resolve, reject) => {
      const clientLS = this.clientLS;
      if (!clientLS) {
        reject();
      }

      this.http.put<User>(ApiLinks.users + '/' + clientLS.id, client)
        .subscribe(() => {
          this.client = client;
          this.onClientChanged.next(this.client);
          resolve(client);
        }, reject);
    });
  }

  changePassword(oldPassword: string, newPassword: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const clientLS = this.clientLS;
      if (!clientLS) {
        reject();
      }

      this.http.post(this.urlChangePassword, { oldPassword: oldPassword, newPassword: newPassword })
        .subscribe(() => {
          resolve();
        }, reject);
    });
  }

  changeLanguage(langCode: string): void {
    const paramsHttp: HttpParams = new HttpParams()
      .set('langCode', langCode);

    this.http.post(`${ApiLinks.users}/change-language`, undefined, { params: paramsHttp })
      .subscribe(() => {

      });
  }

  sendChangeEmailMess(newEmail: string) {
    return new Promise((resolve, reject) => {
      this.http.post(this.urlSendChangeEmailMess, { newEmail: newEmail })
        .subscribe(() => {
          resolve();
        }, reject);
    });
  }

  changeEmail(key: string): Promise<any> {
    const httpParams: HttpParams = new HttpParams()
      .set('key', key);

    return new Promise((resolve, reject) => {
      const clientLS = this.clientLS;
      if (!clientLS) {
        reject();
      }

      this.http.post(this.urlSetNewEmail, undefined, { params: httpParams })
        .subscribe(() => {
          resolve();
        }, reject);
    });
  }

  getNewGUID(pass: string): Promise<any> {
    const httpParams: HttpParams = new HttpParams()
      .set('password', pass);

    return new Promise((resolve, reject) => {
      this.http.get(this.urlNewGUID, { params: httpParams })
        .subscribe((response: string) => {
          this.client.guid = response;
          this.onClientChanged.next(this.client);
          resolve(response);
        }, reject);
    });
  }

  uploadAvatar(file: any): Promise<any> {

    return new Promise((resolve, reject) => {
      this.http.post(this.urlAvatar, file)
        .subscribe((avatarName: string) => {
          resolve(avatarName);
        }, reject);
    });
  }

  deleteAvatar() {
    return new Promise((resolve, reject) => {
      this.http.delete(this.urlAvatar)
        .subscribe(() => {
          resolve();
        }, reject);
    });
  }

  getNewPublicGUID(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.getNewPublicGUID)
        .subscribe((response: string) => {
          this.client.publicGuid = response;
          this.onClientChanged.next(this.client);
          resolve(response);
        }, reject);
    });
  }

  private get clientLS(): UserIdentification {
    return LocalStorageManager.client;
  }
}
