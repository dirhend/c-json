import { NgModule } from '@angular/core';

import { LoginModule } from './authentication/login/login.module';
import { RegisterModule } from './authentication/register/register.module';
import { ForgotPasswordModule } from './authentication/forgot-password/forgot-password.module';
import { MailConfirmModule } from './authentication/mail-confirm/mail-confirm.module';
import { ResetPasswordModule } from './authentication/reset-password/reset-password.module';
import { LockModule } from './authentication/lock/lock.module';
import { ChangeEmailByKeyModule } from './authentication/change-email-by-key/change-email-by-key.module';
import { ActivateAccountModule } from './authentication/activate-account/activate-account.module';
import { ProfileModule } from './profile/profile.module';
import { ComingSoonModule } from './coming-soon/coming-soon.module';

@NgModule({
    imports: [
        // Auth
        LoginModule,  
        RegisterModule,
        ForgotPasswordModule,
        MailConfirmModule,
        ResetPasswordModule,
        LockModule,
        ChangeEmailByKeyModule,
        ActivateAccountModule,
        ComingSoonModule,
        
        ProfileModule
    ],
    declarations: []
})
export class ClientModule
{
}
