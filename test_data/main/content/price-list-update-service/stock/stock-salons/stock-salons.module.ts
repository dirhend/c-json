import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatRippleModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import { FuseConfirmDialogModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { StockService } from '@cloud/main/shared/services/stock/stock.service';
import { StockSalonsComponent } from './stock-salons.component';
import { RouterModule } from '@angular/router';
import { AddSalonDialogComponent } from './add-salon-dialog/add-salon-dialog.component';
import { SharedModule } from '@cloud/main/shared/shared.module';

@NgModule({
  imports: [
    MatIconModule,
    MatTableModule,
    CdkTableModule,
    RouterModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatRippleModule,
    MatMenuModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatListModule,

    CommonModule,
    FuseConfirmDialogModule,
    FuseSharedModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [StockSalonsComponent, AddSalonDialogComponent],
  exports: [],
  providers: [StockService],
  entryComponents: [AddSalonDialogComponent]
})
export class StockSalonsModule {}
