export const locale = {
    lang: 'ru',
    data: {
        'STOCK_SALONS': {
            'TITLE': 'Склад {{ value }}',
            'GUID': 'Идентификатор: {{ value }}',
            'SEARCH': 'Поиск',
            'ADD_SALON': 'Добавить салоны',
            'TABLE': {
                'NAME': 'Название',
                'ARTICLE': 'Идентификатор салона',
                'DATE_CREATE': 'Дата создания',
                'IDENTIFIER': 'Тип авторизации',
            },
            'SNACKBAR': {
                'SALON_DELETED': 'Салон удален',
                'SALON_DELETE': 'Удалить салон {{value}}?',
                'SALON_DELETED_ERROR': 'Ошибка удаления салона',
                'SALONS_ADDED': 'Салоны добавлены',
                'SALONS_ADD_FAIL': 'Ошибка добавления салонов'
            },
            'TOOLTIP': {
                'DELETE': 'Удалить'
            }
        }
    }
};
