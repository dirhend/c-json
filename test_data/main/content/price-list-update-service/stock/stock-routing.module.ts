import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { StockListComponent } from './stock-list/stock-list.component';
import { StockSalonsComponent } from './stock-salons/stock-salons.component';

const routes: Routes = [
  {
    path: 'stocks',
    component: StockListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'stocks/:id',
    component: StockSalonsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class StockRoutingModule { }
