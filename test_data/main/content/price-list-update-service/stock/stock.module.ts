import { NgModule } from '@angular/core';
import { StockRoutingModule } from './stock-routing.module';
import { StockListModule } from './stock-list/stock-list.module';
import { StockSalonsModule } from './stock-salons/stock-salons.module';

@NgModule({
  imports: [
    StockRoutingModule,
    StockListModule,
    StockSalonsModule
  ]
})
export class StockModule { }
