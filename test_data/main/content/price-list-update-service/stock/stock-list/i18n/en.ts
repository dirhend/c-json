export const locale = {
    lang: 'en',
    data: {
        'STOCK_LIST': {
            'TITLE': 'Список складов',
            'SEARCH': 'Поиск',
            'TABLE': {
                'NAME': 'Наименование',
                'DATE_CREATE': 'Дата создания',
                'DATE_LAST_UPDATE': 'Дата последнего обновления',
                'PHONE': 'Телефон',
                'ADDRESS': 'Адрес',
            },
            'SNACKBAR': {
                'STOCK_DELETED': 'Склад удален',
                'STOCK_DELETE': 'Удалить склад {{value}}?',
                'STOCK_DELETED_ERROR': 'Ошибка удаления склада'
            }
        }
    }
};
