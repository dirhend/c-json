import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatRippleModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
  MatProgressSpinnerModule,
} from '@angular/material';
import { FuseConfirmDialogModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { StockListComponent } from './stock-list.component';
import { StockService } from '@cloud/main/shared/services/stock/stock.service';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@cloud/main/shared/shared.module';

@NgModule({
  imports: [
    MatIconModule,
    MatTableModule,
    CdkTableModule,
    RouterModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatRippleModule,
    MatMenuModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatListModule,
    MatProgressSpinnerModule,

    CommonModule,
    FuseConfirmDialogModule,
    FuseSharedModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [StockListComponent],
  exports: [],
  providers: [StockService],
  entryComponents: []
})
export class StockListModule {}
