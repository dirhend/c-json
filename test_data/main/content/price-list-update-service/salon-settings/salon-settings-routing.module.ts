import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { SalonSettingsComponent } from './salon-settings.component';

const routes: Routes = [
  {
    path: 'salon/settings',
    component: SalonSettingsComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class SalonSettingsRoutingModule { }
