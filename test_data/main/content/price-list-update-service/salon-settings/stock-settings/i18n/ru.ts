export const locale = {
    lang: 'ru',
    data: {
        'STOCK_SETTINGS': {
            'FIND_PRODUCTS': {
                'TITLE': 'Сопостовлять товары по',
                'ARTICLE': 'артикулу',
                'NAME': 'наименованию',
                'ARTICLE_AND_NAME': 'артикулу и наименованию'
            }
        }
    }
};
