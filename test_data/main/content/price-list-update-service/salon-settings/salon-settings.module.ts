import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatRippleModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatRadioModule,
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { SalonSettingsComponent } from './salon-settings.component';
import { SalonSettingsRoutingModule } from './salon-settings-routing.module';
import { StockSettingsComponent } from './stock-settings/stock-settings.component';
import { FormsModule } from '@angular/forms';
import { SalonSettingsService } from '@cloud/main/shared/services/salon-settings/salon-settings.service';

@NgModule({
  imports: [
    MatIconModule,
    MatTableModule,
    CdkTableModule,
    RouterModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatRippleModule,
    MatMenuModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    FormsModule,
    MatRadioModule,
    SalonSettingsRoutingModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [SalonSettingsComponent, StockSettingsComponent],
  exports: [],
  providers: [SalonSettingsService],
  entryComponents: []
})
export class SalonSettingsModule {}
