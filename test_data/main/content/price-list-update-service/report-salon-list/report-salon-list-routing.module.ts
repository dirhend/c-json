import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportSalonListComponent } from './report-salon-list.component';
import { ReportSalonListService } from './report-salon-list.service';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'salon-report/list',
    component: ReportSalonListComponent,
    resolve: {
      data: ReportSalonListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class ReportSalonListRoutingModule { }
