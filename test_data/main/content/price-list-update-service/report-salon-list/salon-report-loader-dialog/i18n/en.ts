export const locale = {
    lang: 'en',
    data: {
        'SALONREPORTLOADER': {
            'TITLE': 'Загрузка отчетов',
            'UPLOAD': 'ДОБАВИТЬ ФАЙЛЫ',
            'SAVE': 'ЗАГРУЗИТЬ',
            'CANCEL': 'ОТМЕНА',
            'TABLE': {
                'NAME': 'Название файла',
                'NEWNAME': 'Название',
                'FILESIZE': 'Размер'
            },
            'SNACKBAR': {                
                'REPORTSUPLOADED': 'Файл(ы) загружен(ы)',
                'UPLOADFAIL': 'Не зудалось загрузить файл(ы)',
            }
        }
    }
};