import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { SalonReport } from 'cloud-shared-lib';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';

@Injectable()
export class ReportSalonListService {

  onSalonReportsChanged: BehaviorSubject<SalonReport[]> = new BehaviorSubject([]);

  constructor(
    private http: HttpClient
  ) { }

  /**
  * The Academy App Main Resolver
  *
  * @param {ActivatedRouteSnapshot} route
  * @param {RouterStateSnapshot} state
  * @returns {Observable<any> | Promise<any> | any}
  */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getSalonReports(''),
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getSalonReports(filterText: string): Promise<SalonReport[]> {

    const paramsHttp: HttpParams = new HttpParams()
      .set('filterText', filterText);

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.salonReport, { params: paramsHttp })
        .subscribe((response: SalonReport[]) => {
          this.onSalonReportsChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

  downloadReport(reportId: number): Promise<Blob> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.salonReportDownload(reportId), { responseType: 'blob' })
        .subscribe((response: Blob) => {
          resolve(response);
        }, reject);
    });
  }

  uploadReport(files: File[]): Promise<void> {
    const input = new FormData();
    files.forEach((f: File) => {
      input.append(f.name, f);
    });

    return new Promise((resolve, reject) => {      

      this.http.post(ApiLinks.salonReportUpload, input)
        .subscribe((response: SalonReport[]) => {

          const newReportList: SalonReport[] = [...this.onSalonReportsChanged.value, ...response];

          this.onSalonReportsChanged.next(newReportList);

          resolve();
        }, reject);
    });
  }

  deleteReport(reportId: number) {
    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.salonReport + '/' + reportId)
        .subscribe(() => {

          const salonReportIndex = this.onSalonReportsChanged.value.findIndex(r => +r.id === +reportId);
          this.onSalonReportsChanged.value.splice(salonReportIndex, 1);

          this.onSalonReportsChanged.next(this.onSalonReportsChanged.value);
          resolve();
        }, reject);
    });
  }
}
