export const locale = {
    lang: 'ru',
    data: {
        'SALONREPORTS': {
            'TITLE': 'Отчеты',
            'SEARCH': 'Поиск',
            'UPLOADREPORT': 'Загрузить отчет',
            'DELETEREPORTCONFIRM': 'Удалить отчет {{ value }}?',
            'TABLE': {
                'DATEUPLOAD': 'Дата загрузки',
                'NAME': 'Название'
            },
            'SNACKBAR': {
                'DOWNLOADFAIL': 'Не удалось загрузить файл',
                'REPORTDELETED': 'Отчет удален',
                'REPORTDELETEFAIL': 'Не удалось удалить отчет'
            }
        }
    }
};
