import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportSalonListRoutingModule } from './report-salon-list-routing.module';
import { ReportSalonListComponent } from './report-salon-list.component';
import { ReportSalonListService } from './report-salon-list.service';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatPaginatorModule, MatSortModule, MatTableModule, MatInputModule, MatIconModule, MatFormFieldModule, MatButtonModule,
  MatDialogModule, MatToolbarModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { SalonReportLoaderDialogComponent } from './salon-report-loader-dialog/salon-report-loader-dialog.component';

@NgModule({
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    CdkTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatDialogModule,
    MatToolbarModule,

    CommonModule,
    SharedModule,
    FuseSharedModule,
    TranslateModule,
    ReportSalonListRoutingModule
  ],
  declarations: [
    ReportSalonListComponent,
    SalonReportLoaderDialogComponent
  ],
  providers: [ReportSalonListService],
  entryComponents: [SalonReportLoaderDialogComponent]
})
export class ReportSalonListModule { }
