import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalonListModule } from '../order-sync-service/salon-list/salon-list.module';
import { PriceListsModule } from './price-lists/price-lists.module';
import { ReportSalonListModule } from './report-salon-list/report-salon-list.module';
import { SalonSettingsModule } from './salon-settings/salon-settings.module';
import { StockModule } from './stock/stock.module';

@NgModule({
  imports: [
    CommonModule,
    SalonListModule,
    PriceListsModule,
    ReportSalonListModule,
    SalonSettingsModule,
    StockModule
  ]
})
export class PriceListUpdateServiceModule { }
