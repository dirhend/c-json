import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../../shared/guards/auth.guard';
import { PriceListDetailComponent } from './price-list-detail.component';
import { PriceListDetailService } from './price-list-detail.service';

const routes: Routes = [
  {
    path: 'price-list/detail/:id',
    component: PriceListDetailComponent,
    resolve: {
      data: PriceListDetailService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class PriceListDetailRoutingModule { }
