import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '../../../../app.api-links';
import { PriceList } from 'cloud-shared-lib';
import { SalonConfiguration } from 'cloud-shared-lib';
import { ConfigurationPriceList } from 'cloud-shared-lib';
import { Salon, SalonFilter } from 'cloud-shared-lib';
import { PriceListUpdateHistory } from 'cloud-shared-lib';

@Injectable()
export class PriceListDetailService {
  salons: Salon[] = [];
  onSalonsChanged: BehaviorSubject<any> = new BehaviorSubject({});
  salonsTotalCount = 0;
  
  priceList: PriceList = new PriceList();
  onPriceListChanged: BehaviorSubject<any> = new BehaviorSubject({});
  priceListId = 0;
  
  salonConfigurations: SalonConfiguration[] = [];
  onSalonConfigurationChanged: BehaviorSubject<any> = new BehaviorSubject({});

  configurationsPriceList: ConfigurationPriceList[] = [];
  onConfigurationsPriceListChanged: BehaviorSubject<any> = new BehaviorSubject({});

  priceListUpdateHistories: PriceListUpdateHistory[] = [];
  priceListUpdateHistoriesTotalCount = 0;
  onPriceListUpdateHistoriesChanged: BehaviorSubject<any> = new BehaviorSubject({});

  constructor(
    private http: HttpClient
  ) {
  }

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.priceListId = route.params.id;
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getPriceList(),
        this.getSalonConfigurations(0, 10, SalonFilter.All, ''),
        this.getConfigurations()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getConfigurations(): Promise<ConfigurationPriceList[]> {  
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.configurationsForPriceList(this.priceListId))
        .subscribe((response: any) => {
          this.configurationsPriceList = response;
          this.onConfigurationsPriceListChanged.next(this.configurationsPriceList);
          resolve(response);
        }, reject);
    });
  }

  getSalonConfigurations(pageIndex: number, pageSize: number, filterSalon: SalonFilter = SalonFilter.All, filterText: string = ''): Promise<SalonConfiguration[]> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterSalon', filterSalon.toString())
      .set('filterText', filterText);
    
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.salonsForPriceList(this.priceListId), { params: paramsHttp })
        .map((res: {
          totalCount: number,
          salonConfsDTO: SalonConfiguration[]
        }) => {
          this.salonsTotalCount = res.totalCount;
          return res.salonConfsDTO;
        })
        .subscribe((response: any) => {
          this.salonConfigurations = response;
          this.onSalonConfigurationChanged.next(this.salonConfigurations);
          resolve(response);
        }, reject);
    });
  }

  deleteSalonFromPriceList(salonId: number): Promise<void>
  {
    const paramsHttp: HttpParams = new HttpParams()
      .set('salonId', salonId.toString());

    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.deleteConfiguration(this.priceListId), { params: paramsHttp })
        .subscribe((response: any) => {
          const itemIndex = this.salonConfigurations.findIndex((s) => s.salon.id === salonId);
          if (itemIndex !== -1) {
            this.salonConfigurations.splice(itemIndex, 1);
          }
          
            this.onSalonConfigurationChanged.next(this.salonConfigurations);  
          resolve(response);
        }, reject);
    });
  }

  putSalonConfigurations(salonId: number, configurationId: number): Promise<void> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('salonId', salonId.toString())
      .set('configurationId', configurationId.toString());

    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.editConfiguration(this.priceListId), null, { params: paramsHttp })
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  getPriceList(): Promise<PriceList> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.priceList(this.priceListId))
        .subscribe((response: any) => {
          this.priceList = response;
          this.onPriceListChanged.next(this.priceList);
          resolve(response);
        }, reject);
    });
  }

  postSaveSelectedSalons(selectedSalons: number[]): Promise<void>
  {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.salonAdd(this.priceListId), selectedSalons)
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  getSalonsNotAdded(pageIndex: number, pageSize: number, filterText: string): Promise<Salon[]> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText);

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.salonsNotAddedForPriceList(this.priceListId), { params: paramsHttp })
        .map((res: {
          totalCount: number,
          salonsDTO: Salon[]
        }) => {
          this.salonsTotalCount = res.totalCount;
          return res.salonsDTO;
        })
        .subscribe((response: Salon[]) => {
          this.salons = response;
          this.onSalonsChanged.next(this.salons);
          resolve(response);
        }, reject);
    });
  }

  getHistoryUpdatePriceList(pageIndex: number, pageSize: number, salonId: number): Promise<PriceListUpdateHistory[]> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.historiesUpdatePriceLists(this.priceListId, salonId), { params: paramsHttp })
        .map((res: {
          totalCount: number,
          histories: PriceListUpdateHistory[]
        }) => {
          this.priceListUpdateHistoriesTotalCount = res.totalCount;
          return res.histories;
        })
        .subscribe((response: PriceListUpdateHistory[]) => {
          this.priceListUpdateHistories = response;
          this.onPriceListUpdateHistoriesChanged.next(this.priceListUpdateHistories);
          resolve(response);
        }, reject);
    });
  }

}
