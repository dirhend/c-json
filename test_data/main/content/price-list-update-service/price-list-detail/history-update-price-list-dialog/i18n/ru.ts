export const locale = {
    lang: 'ru',
    data: {
        'HISTORY_UPDATE_PRICE_LIST': {
            'TITLE': 'История обновления прайс-листа',
            'CLOSE': 'ЗАКРЫТЬ',
            'DEFAULT': 'По умолчанию',
            'TABLE': {
                'DATE_UPDATE': 'Дата обновления',
                'CONFIGURATION': 'Конфигурация',
                'DATE_UPLOAD_PRICE_LIST': 'Версия прайс-листа от'
            },
        }
    }
};