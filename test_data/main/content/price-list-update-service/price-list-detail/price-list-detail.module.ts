import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatIconModule, MatTabsModule, MatTableModule, MatSortModule, MatButtonModule, MatSelectModule, MatDialogModule, MatToolbarModule, MatPaginatorModule,
  MatProgressSpinnerModule, MatCheckboxModule, MatTooltipModule, MatFormFieldModule, MatInputModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { SharedModule } from '../../../shared/shared.module';
import { ServiceListService } from '@cloud/main/content/common/service-list/service-list.service';
import { PriceListDetailRoutingModule } from './price-list-detail-routing.module';
import { PriceListDetailComponent } from './price-list-detail.component';
import { PriceListDetailService } from './price-list-detail.service';
import { AddSalonDialogComponent } from './add-salon-dialog/add-salon-dialog.component';
import { PriceListStatePipe } from '@cloud/main/shared/pipes/price-list-update-state.pipe';
import { HistoryUpdatePriceListDialogComponent } from './history-update-price-list-dialog/history-update-price-list-dialog.component';

@NgModule({
  imports: [
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    CdkTableModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatToolbarModule,
    MatSelectModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    PriceListDetailRoutingModule,
    SharedModule
  ],
  declarations: [
    PriceListDetailComponent,
    AddSalonDialogComponent,
    HistoryUpdatePriceListDialogComponent,
    PriceListStatePipe
  ],
  entryComponents: [AddSalonDialogComponent, HistoryUpdatePriceListDialogComponent],
  providers: [
    PriceListDetailService,
    ServiceListService
  ]
})
export class PriceListDetailModule { }
