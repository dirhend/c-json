import { TestBed, inject } from '@angular/core/testing';

import { PriceListDetailService } from './price-list-detail.service';

describe('PriceListDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PriceListDetailService]
    });
  });

  it('should be created', inject([PriceListDetailService], (service: PriceListDetailService) => {
    expect(service).toBeTruthy();
  }));
});
