export const locale = {
    lang: 'en',
    data: {
        'ADDSALON': {
            'TITLE': 'Добавление салонов',
            'OK': 'OK',
            'CANCEL': 'ОТМЕНА',
            'SEARCH': 'Поиск',
            'TABLE': {
                'NAME': 'Название',
                'ARTICLE': 'Идентификатор салона',
                'CREATIONDATE': 'Дата создания',
                'IDENTIFIER': 'Тип авторизации',
                'CONFIGURATION': 'Конфигурация'
            },
        }
    }
};