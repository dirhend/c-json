export const locale = {
    lang: 'en',
    data: {
        'PRICELISTDETAIL': {
            'HEADER': {
                'PRICELIST': 'Price-list {{ value }}',
                'PRICELIST_GUID': 'Идентификатор: {{ value }}',
                'SEARCH': 'Поиск'
            },
            'FILTER': {
                'TITLE': 'Салоны',
                'ALL': 'Все',
                'UPDATED': 'Обновлены',
                'NOT_UPDATED': 'Не обновлены'
            },
            'DEFAULT': 'По умолчанию',
            'TABLE': {
                'NAME': 'Название',
                'ARTICLE': 'Идентификатор салона',
                'CREATIONDATE': 'Дата создания',
                'IDENTIFIER': 'Тип авторизации',
                'CONFIGURATION': 'Конфигурация',
                'STATE': 'Состояние'
            },
            'TOOLTIP': {
                'DELETE': 'Удалить',
                'SHOWHISTORY': 'Просмотр истории обновлений'
            },
            'ADDSALON': 'Добавить салон',
            'SNACKBAR': {
                'SALONDELETED': 'Салон удален',
                'SALONDELETE': 'Удалить салон {{value}}?',
            }
        }
    }
};
