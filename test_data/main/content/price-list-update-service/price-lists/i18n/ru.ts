export const locale = {
    lang: 'ru',
    data: {
        'PRICELISTS': {
            'TITLE': 'Прайс-листы',   
            'SEARCH': 'Поиск',
            'TABLE': {
                'NAME': 'Название',
                'DATECREATE': 'Дата создания',
                'SIZE': 'Общий размер',
                'STATE': 'Состояние',
                'STATELOADED': 'Загружен',
                'STATEUNLOADED': 'Не загружен',
                'GUID': 'Уникальный идентификатор',
                'DATE_LAST_UPDATE': 'Дата последнего обновления',
                'UPDATED_COUNT': 'Обновлены'
            },
            'SNACKBAR': {
                'PRICELISTDELETED': 'Прайс-лист удален',
                'PRICELISTDELETE': 'Удалить прайс-лист {{value}}?',
                'PRICELISTDELETEDERROR': 'Ошибка удаления прайс-листа'
            }
        }
    }
};
