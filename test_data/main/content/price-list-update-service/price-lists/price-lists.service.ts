import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {  Observable, BehaviorSubject } from 'rxjs';
import { ApiLinks } from '../../../../app.api-links';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PriceList } from 'cloud-shared-lib';

@Injectable()
export class PriceListsService {
  priceLists: PriceList[];
  onPriceListsChanged: BehaviorSubject<any> = new BehaviorSubject({});
  priceListsTotalCount = 0;

  filtersOptions = {
    pageIndex: 0,
    pageSize: 10,
    searchText: ''
  };

  constructor(
    private http: HttpClient
  ) { }

  /**
  * Resolve
  * @param {ActivatedRouteSnapshot} route
  * @param {RouterStateSnapshot} state
  * @returns {Observable<any> | Promise<any> | any}
  */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getPriceLists()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getPriceLists(): Promise<any>
  {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', this.filtersOptions.pageIndex.toString())
      .set('pageSize', this.filtersOptions.pageSize.toString())
      .set('filterText', this.filtersOptions.searchText);

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.priceLists, { params: paramsHttp })
        .map((res: {
          totalCount: number,
          priceListsDTO: PriceList[]
        }) => {
          this.priceListsTotalCount = res.totalCount;
          return res.priceListsDTO;
        })
        .subscribe((response: any) => {
          this.priceLists = response;
          this.onPriceListsChanged.next(this.priceLists);
          resolve(response);
        }, reject);
    });
  }

  deletePriceList(priceListId: number): Promise<void>
  {
    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.priceList(priceListId))
        .subscribe((response: any) => {
          const itemIndex = this.priceLists.findIndex((p) => p.id === priceListId);
          if (itemIndex !== -1) {
            this.priceLists.splice(itemIndex, 1);
          }
          
            this.onPriceListsChanged.next(this.priceLists);  
          resolve(response);
        }, reject);
    });
  }


}
