import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../../shared/guards/auth.guard';
import { PriceListsComponent } from './price-lists.component';
import { PriceListsService } from './price-lists.service';

const routes: Routes = [
  {
    path: 'price-list/list',
    component: PriceListsComponent,
    resolve: {
      data: PriceListsService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class PriceListsRoutingModule { }
