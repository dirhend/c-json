import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatRippleModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material';
import { FuseConfirmDialogModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { FileSizePipe } from '../../../shared/pipes/file-size.pipe';
import { SharedModule } from '../../../shared/shared.module';
import { PriceListDetailModule } from '../price-list-detail/price-list-detail.module';
import { PriceListsRoutingModule } from './price-lists-routing.module';
import { PriceListsComponent } from './price-lists.component';
import { PriceListsService } from './price-lists.service';

@NgModule({
  imports: [
    MatIconModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatRippleModule,
    MatMenuModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatListModule,

    CommonModule,
    FuseConfirmDialogModule,
    FuseSharedModule,
    TranslateModule,
    PriceListsRoutingModule,
    PriceListDetailModule,
    SharedModule
  ],
  declarations: [PriceListsComponent],
  exports: [FileSizePipe],
  providers: [PriceListsService],
  entryComponents: []
})
export class PriceListsModule {}
