export const locale = {
    lang: 'en',
    data: {
        'DESCRIPTION': 'Description',
        'POINTSINFO': {
            'TITLE': 'Bonuses',
            'SLOGAN': 'Bonus programа',
            'WHY_IS_PROFIT': 'Why is it cost-efficient?',
            'DESCRIPTION': {
                'MAIN_TEXT': `Now you get bonuses for each payment. Bonus number depends on payment amount and specified according to the table herebelow. 
				When you buy time, first your real money will be used, and then your saved bonuses`,
                'ONEBOBUS_ONERUB': 'One bonus is equal to one ruble.',
                'PART_OR_FULL_SUM': 'You can pay for the full purchase sum or only its part.',
                'DO_NOT_BURN': 'The bonuses never burn and always available for payment.',
                'HOW_TO_USE': 'You can pay for BAZIS - Cloud service with bonuses.'
            } ,
            'BONUS_FROM_SUM_PAYMENT': ' Bonus number from payment sum',
            'TABLE': {
                'AMOUNT': 'Payment sum',
                'COUNT': 'Bonus number',
                'LITTLE': 'Fewer {{ value }} RUB',
                'BIG': 'More {{ value }} RUB',
                'FROMTO': 'From {{ from }} To {{ to }} RUB'
            }
        }
    }
};
