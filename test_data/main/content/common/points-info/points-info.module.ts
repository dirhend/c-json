import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PointsInfoRoutingModule } from './points-info-routing.module';
import { PointsInfoComponent } from './points-info.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material';

@NgModule({
  imports: [
    MatIconModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    PointsInfoRoutingModule
  ],
  declarations: [PointsInfoComponent]
})
export class PointsInfoModule { }
