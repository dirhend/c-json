import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs/Observable';

import { Service } from 'cloud-shared-lib';

@Injectable()
export class ServiceListService {

  private readonly urlServices: string = ApiLinks.services;

  onServicesChanged: BehaviorSubject<any> = new BehaviorSubject({});

  constructor(private http: HttpClient) {
  }

  /**
  * The Academy App Main Resolver
  *
  * @param {ActivatedRouteSnapshot} route
  * @param {RouterStateSnapshot} state
  * @returns {Observable<any> | Promise<any> | any}
  */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {

      Promise.all([
        this.getServices()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getServices(): Promise<any> {
    const httpParams: HttpParams = new HttpParams()
      .set('ignoreBloked', 'true');

    return new Promise((resolve, reject) => {
      this.http.get(this.urlServices, { params: httpParams })
        .subscribe((response: Service[]) => {
          this.onServicesChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

}
