export const locale = {
    lang: 'en',
    data: {
        'SERVICES': {
            'TITLE': 'BAZIS-Cloud',
            'DESC': 'Кабинет управления услугами, связывающие клиентов системы БАЗИС',
            'SERVICE': {
                'UPTO': 'up to',
                'UNLIMITEDNONE': '–',
                'NAME': {
                    1: 'Order swap',
                    2: 'Updating price list prices',
                    3: 'Price list',
                    4: 'Cutting'
                },
                'QUANTITY': 
                {
                    '1': 'шт',
                    '2': 'шт',
                    '3': 'MB',
                    '4': 'шт',  
                },
                'RATES': 'TARIFFS',
                'MORE': 'MORE',
                'COMINGSOON': 'COMING SOON'
            }
        }
    }
};
