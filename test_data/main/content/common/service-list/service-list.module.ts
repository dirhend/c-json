import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServiceListRoutingModule } from './service-list-routing.module';
import { ServiceListComponent } from './service-list.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ServiceListService } from './service-list.service';
import { MatSidenavModule, MatSelectModule, MatInputModule, MatIconModule, MatFormFieldModule, MatButtonModule } from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatSidenavModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    ServiceListRoutingModule
  ],
  declarations: [
    ServiceListComponent,
  ],
  providers: [
    ServiceListService
  ]
})
export class ServiceListModule { }
