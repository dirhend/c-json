import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ServiceListComponent } from './service-list.component';
import { ServiceListService } from './service-list.service';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'service/list',
    component: ServiceListComponent,
    resolve: {
      data: ServiceListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class ServiceListRoutingModule { }
