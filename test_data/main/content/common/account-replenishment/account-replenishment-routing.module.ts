import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../../shared/guards/auth.guard';
import { AccountReplenishmentComponent } from './account-replenishment.component';
import { AccountReplenishmentService } from './account-replenishment.service';

const routes: Routes = [
  {
    path: 'client/account-replenishment',
    component: AccountReplenishmentComponent,
    resolve: {
      profile: AccountReplenishmentService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [AuthGuard],
  exports: [RouterModule]
})
export class AccountReplenishmentRoutingModule { }
