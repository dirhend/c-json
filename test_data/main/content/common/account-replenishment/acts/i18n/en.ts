export const locale = {
    lang: 'en',
    data: {
        'ACTLIST': {
            'DOWNLOADING': 'Загрузка файла',
            'TABLE': {
                'DATE': 'Дата создания',
                'AMOUNT': 'Сумма',
                'MONTH': 'Месяц',
                'YEAR': 'Год',
                'REQUISIT': 'Реквизиты'
            },
            'SNACKBAR' : {
                'DOWNLOADFAIL': 'Не удалось скачать документ'
            }
        }
    }
};
