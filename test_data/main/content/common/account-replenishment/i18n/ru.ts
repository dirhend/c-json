export const locale = {
    lang: 'ru',
    data: {
        'ACCREPLENISHMENT': {
            'TITLE': 'Баланс',
            'PAYMENTSHIST': 'История платежей',
            'PURCHASEHIST': 'История покупок',
            'INVOICES': 'Счета на оплату',
            'ACTS': 'Акты',
            'REFILL': {
                'TITLE': 'Пополнить баланс',
                'TOOLTIP': {
                    'CARD': 'Банковская карта',
                    'PAYPAL': 'PayPal',
                    'CERTIFICATE': 'Сертификат',
                    'INVOICE': 'Выставить счет на оплату'
                }
            }
        }
    }
};
