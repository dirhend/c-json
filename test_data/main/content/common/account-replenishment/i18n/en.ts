export const locale = {
    lang: 'en',
    data: {
        'ACCREPLENISHMENT': {
            'TITLE': 'Balance',
            'PAYMENTSHIST': 'Payment history',
            'PURCHASEHIST': 'Purchase history',
            'INVOICES': 'Invoices',
            'ACTS': 'Акты',
            'REFILL': {
                'TITLE': 'Top up your balance',
                'TOOLTIP': {
                    'CARD': 'Plastic card',
                    'PAYPAL': 'PayPal',
                    'CERTIFICATE': 'Certificate',
                    'INVOICE': 'Get invoice on pay'
                }
            }
        }
    }
};
