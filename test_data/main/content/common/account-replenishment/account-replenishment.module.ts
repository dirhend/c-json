import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  InvoiceCreateDialogComponent,
} from '@cloud/main/content/common/account-replenishment/invoice-create-dialog/invoice-create-dialog.component';
import { MonthByNumberPipe } from '@cloud/main/shared/pipes/month-by-number.pipe.pipe';
import { ServiceNamePipe } from '@cloud/main/shared/pipes/service-name.pipe';
import { NgxLocalizedNumbers } from 'ngx-localized-numbers';

import { FinancialFlowStatusPipe } from '../../../shared/pipes/financial-flow-status.pipe';
import { PaymentSystemTypePipe } from '../../../shared/pipes/payment-system-type.pipe';
import { SharedModule } from '../../../shared/shared.module';
import { AccountReplenishmentRoutingModule } from './account-replenishment-routing.module';
import { AccountReplenishmentComponent } from './account-replenishment.component';
import { AccountReplenishmentService } from './account-replenishment.service';
import { ActsComponent } from './acts/acts.component';
import { CertificatePayDialogComponent } from './certificate-pay-dialog/certificate-pay-dialog.component';
import { InvoiceListComponent } from './invoice-list/invoice-list.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { PaypalPayDialogComponent } from './paypal-pay-dialog/paypal-pay-dialog.component';
import { PsbPayDialogComponent } from './psb-pay-dialog/psb-pay-dialog.component';
import { PurchaseHistoryComponent } from './purchase-history/purchase-history.component';

@NgModule({
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSortModule,
    MatTableModule,
    CdkTableModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    MatTabsModule,
    MatTooltipModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    AccountReplenishmentRoutingModule,
    SharedModule,
    NgxLocalizedNumbers,
  ],
  declarations: [
    AccountReplenishmentComponent,
    PaymentHistoryComponent,
    PurchaseHistoryComponent,
    PaypalPayDialogComponent,
    PsbPayDialogComponent,
    CertificatePayDialogComponent,
    PaymentSystemTypePipe,
    FinancialFlowStatusPipe,
    InvoiceCreateDialogComponent,
    InvoiceListComponent,
    ActsComponent,
    MonthByNumberPipe,
  ],
  providers: [
    AccountReplenishmentService,
    ServiceNamePipe,
  ],
  entryComponents: [
    PsbPayDialogComponent,
    PaypalPayDialogComponent,
    CertificatePayDialogComponent,
    InvoiceCreateDialogComponent
  ]
})
export class AccountReplenishmentModule { }
