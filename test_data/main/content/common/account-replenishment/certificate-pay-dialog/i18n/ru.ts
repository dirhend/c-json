export const locale = {
    lang: 'ru',
    data: {
        'CERTIFICATEPAY': {
            'TITLE': 'Активация сертификата',
            'ACTIVATE': 'АКТИВИРОВАТЬ',
            'CANCEL': 'ОТМЕНА',
            'DESC': 'Для активации сертификата введите его код и нажмите на конпку "Активировать".',
            'CERTIFICATECODE': {
                'PLACEHOLDER': 'Код сертификата',
                'ERROR': {
                    'REQUIRED': 'Поле не должно быть пустым',
                    'LENGTH': 'длина кода - 23 символа'
                }
            },
            'SNACKBAR': {
                'ACTIVATED': 'Сертификат активирован',
                'ACTIVATEFAIL': {
                    'CERTNOTFOUND': 'Сертификат с данным кодом не найден',
                    'INVALIDCODE': 'Код сертификата введен не корректно',
                    'CERTEXPIRED': 'Срок действия сертификата истек',
                    'CERTBEENACTIVATED': 'Данный сертификат уже был активирован',
                    'CERTACTIVATEFAIL': 'Не удалось активировать сертификат'
                }
            }
        }
    }
};
