import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ApiLinks } from '../../../../app.api-links';
import { Invoice } from 'cloud-shared-lib';
import { PaymentData } from 'cloud-shared-lib';
import { PaymentHistory } from 'cloud-shared-lib';
import { PaymentResponse } from 'cloud-shared-lib';
import { PurchaseHistory } from 'cloud-shared-lib';
import { Act } from 'cloud-shared-lib';

export interface ActListResponse {
  totalCount: number;
  actsDto: Act[];
}

@Injectable()
export class AccountReplenishmentService {

  paymentHistory: PaymentHistory[];
  purchaseHistory: PurchaseHistory[];

  onPaymentHistoryChanged: BehaviorSubject<any> = new BehaviorSubject({});
  onPurchaseHistoryChanged: BehaviorSubject<any> = new BehaviorSubject({});
  onActListChanged: BehaviorSubject<ActListResponse> = new BehaviorSubject(null);

  invoiceList: Invoice[];
  invoiceTotalCount = 0;
  onInvoiceChanged: BehaviorSubject<any> = new BehaviorSubject({});

  private readonly urlPSBPay = ApiLinks.psbPay;
  private readonly urlPayPalPay = ApiLinks.payPalPay;
  private readonly urlPaymentHistory = ApiLinks.paymentHistory;
  private readonly urlPurchaseHistory = ApiLinks.purchaseHistory;
  private readonly urlActivateCertificate = ApiLinks.payCertificate;
  private readonly urlInvoices = ApiLinks.invoices;

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getPaymentHistory(),
        this.getPurchaseHistory(),
        this.getInvoiceList(0, 16),
        this.getActList(0, 10),
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getPSBPaymentParams(data: PaymentData): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<PaymentResponse>(this.urlPSBPay, data)
        .subscribe((response: PaymentResponse) => {
          resolve(response);
        }, reject);
    });
  }

  getPayPalPaymentParams(data: PaymentData): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post<PaymentResponse>(this.urlPayPalPay, data)
        .subscribe((response: PaymentResponse) => {
          resolve(response);
        }, reject);
    });
  }

  getPaymentHistory(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<PaymentHistory[]>(this.urlPaymentHistory)
        .subscribe((response: PaymentHistory[]) => {
          this.paymentHistory = response;
          this.onPaymentHistoryChanged.next(this.paymentHistory);
          resolve(response);
        }, reject);
    });
  }

  getPurchaseHistory(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get<PurchaseHistory[]>(this.urlPurchaseHistory)
        .subscribe((response: PurchaseHistory[]) => {
          this.purchaseHistory = response;
          this.onPurchaseHistoryChanged.next(this.purchaseHistory);
          resolve(response);
        }, reject);
    });
  }

  activateCertificate(code: string) {
    const params: HttpParams = new HttpParams()
      .set('code', code);

    return new Promise((resolve, reject) => {
      this.http.post(this.urlActivateCertificate, undefined, { params: params })
        .subscribe(() => {
          resolve();
        }, reject);
    });
  }

  stopPurchaseHistory(id: number) {
    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.stopPurchaseHistory(id), undefined)
        .subscribe(() => {
          resolve();
        }, reject);
    });
  }


  getInvoiceList(pageIndex: number, pageSize: number): Promise<any> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString());

    return new Promise((resolve, reject) => {
      this.http.get(this.urlInvoices, { params: paramsHttp })
        .map((res: {
          totalCount: number,
          invoicesDto: Invoice[]
        }) => {
          this.invoiceTotalCount = res.totalCount;
          return res.invoicesDto;
        })
        .subscribe((response: any) => {
          this.invoiceList = response;
          this.onInvoiceChanged.next(this.invoiceList);
          resolve(response);
        }, reject);
    });
  }

  createInvoice(invoice: Invoice) {
    return new Promise((resolve, reject) => {
      this.http.post(this.urlInvoices, invoice, {})
        .subscribe(() => {
          resolve();
        }, reject);
    });
  }

  downloadInvoice(invoiceId: number): Promise<Blob> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.invoiceFile + invoiceId.toString(), { responseType: 'blob' })
        .subscribe((response) => {
          resolve(response);
        }, reject);
    });
  }

  getActList(pageIndex: number, pageSize: number): Promise<ActListResponse> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.act, { params: paramsHttp })
        .subscribe((response: ActListResponse) => {
          this.onActListChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

  downloadAct(actId: number) {
    return new Promise((resolve, reject) => {
      this.http.get(`${ApiLinks.generateActDocument(actId)}`, { responseType: 'blob' })
        .subscribe((response) => {
          resolve(response);
        }, reject);
    });
  }

}
