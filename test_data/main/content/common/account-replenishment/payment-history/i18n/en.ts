export const locale = {
    lang: 'en',
    data: {
        'PAYMENTHIST': {
            'TABLE': {
                'DATE': 'Дата',
                'MONEY': 'Сумма',
                'POINTS': 'Начислено баллов',
                'PAYMENTSYSTYPE': 'Платежная система',
                'PAYMENTSYSSTATUS': 'Статус',
                'CHEQUE': 'Чек'
            }
        }
    }
};
