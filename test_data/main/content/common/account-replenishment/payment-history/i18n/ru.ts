export const locale = {
    lang: 'ru',
    data: {
        'PAYMENTHIST': {
            'TABLE': {
                'DATE': 'Дата',
                'MONEY': 'Сумма',
                'POINTS': 'Начислено баллов',
                'PAYMENTSYSTYPE': 'Платежная система',
                'PAYMENTSYSSTATUS': 'Статус',
                'CHEQUE': 'Чек'
            }
        }
    }
};
