import { TestBed, inject } from '@angular/core/testing';

import { AccountReplenishmentService } from './account-replenishment.service';

describe('AccountReplenishmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountReplenishmentService]
    });
  });

  it('should be created', inject([AccountReplenishmentService], (service: AccountReplenishmentService) => {
    expect(service).toBeTruthy();
  }));
});
