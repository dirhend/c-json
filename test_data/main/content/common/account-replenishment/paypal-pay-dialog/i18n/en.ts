export const locale = {
    lang: 'en',
    data: {            
        'PAYPALPAY': {
            'TITLE': 'Replenishment by PayPal',
            'AMOUNT': {
                'PLACEHOLDER': 'Amount',
                'RUB': 'RUB',
                'ERROR': {
                    'REQUIRED': 'You must enter a value',
                    'MAXLEN': 'Maximum value is 99999',
                    'PATTERN': 'The field should contain only digits',
                    'MIN': 'We need more gold!'
                }
            },
            'PAY': 'REFILL',
            'BANKDESC': 'Balance of BAZIS-Cloud',
            'CANCEL': 'CANCEL',
            'PAYPALREFILLDESCRIPTION': "Enter the amount you want to deposit into your Bazis Cloud account and click \"Refill\". You will be redirected to the website of PayPal payment system for processing."
        }    
    }
};
