export const locale = {
    lang: 'en',
    data: {
        'INVOICELIST': {
            'DOWNLOADING': 'Загрузка файла',
            'TABLE': {
                'ID': 'Номер',
                'DATE': 'Дата',
                'MONEY': 'Сумма',
                'REQUISIT': 'Реквизиты',
                'DATE_PAYMENT': 'Дата оплаты',
                'NUMBER_PREFIX': 'BOS'
            },
            'SNACKBAR' : {
                'INVOICEDOWNLOADFAIL': 'Не удалось скачать счет для оплаты'
            }
        }
    }
};
