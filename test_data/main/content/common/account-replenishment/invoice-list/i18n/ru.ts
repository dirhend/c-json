export const locale = {
    lang: 'ru',
    data: {
        'INVOICELIST': {
            'DOWNLOADING': 'Загрузка файла',
            'TABLE': {
                'ID': 'Номер',
                'DATE': 'Дата',
                'MONEY': 'Сумма',
                'REQUISIT': 'Реквизиты',
                'DATE_PAYMENT': 'Дата оплаты',
                'NUMBER_PREFIX': 'БОС'
            },
            'SNACKBAR' : {
                'INVOICEDOWNLOADFAIL': 'Не удалось скачать счет для оплаты'
            }
        }
    }
};
