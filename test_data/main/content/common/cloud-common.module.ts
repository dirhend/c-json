import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountReplenishmentModule } from './account-replenishment/account-replenishment.module';
import { PointsInfoModule } from './points-info/points-info.module';
import { ServiceListModule } from './service-list/service-list.module';

@NgModule({
  imports: [
    CommonModule,
    AccountReplenishmentModule,
    PointsInfoModule,
    ServiceListModule,
  ],
  declarations: []
})
export class CloudCommonModule { }
