import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TariffComponent } from './tariff.component';
import { TariffService } from './tariff.service';
import { AuthGuard } from '../../shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'service/:id/tariffs',
    component: TariffComponent,
    resolve: {
      data: TariffService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class TariffRoutingModule { }
