import { Injectable } from '@angular/core';
import { ApiLinks } from '../../../app.api-links';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TariffService {

  private urlTariffs = 'Set in resolve';

  onTariffsChanged: BehaviorSubject<any> = new BehaviorSubject({});

  constructor(private http: HttpClient) {
  }

  /**
  * The Academy App Main Resolver
  *
  * @param {ActivatedRouteSnapshot} route
  * @param {RouterStateSnapshot} state
  * @returns {Observable<any> | Promise<any> | any}
  */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    this.urlTariffs = ApiLinks.tariffs(route.params.id);

    return new Promise((resolve, reject) => {

      Promise.all([
        this.getTariffs()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getTariffs(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.urlTariffs)
        .subscribe((response: any) => {
          this.onTariffsChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

  purchaseTariff(idTariff: number, optionIds: number[]): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.purchaseTariff(idTariff), optionIds)
        .subscribe((data: { money: number, points: number }) => {
          resolve(data);
        }, reject);
    });
  }
}
