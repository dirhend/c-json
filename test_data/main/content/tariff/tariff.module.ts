import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatDividerModule,
  MatIconModule,
  MatToolbarModule,
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../shared/shared.module';
import { PurchaseTariffDialogComponent } from './purchase-tariff-dialog/purchase-tariff-dialog.component';
import { TariffRoutingModule } from './tariff-routing.module';
import { TariffComponent } from './tariff.component';
import { TariffService } from './tariff.service';

@NgModule({
  imports: [
    MatButtonModule,
    MatDividerModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    MatCheckboxModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    TariffRoutingModule,
    SharedModule
  ],
  declarations: [
    TariffComponent, //
    PurchaseTariffDialogComponent
  ],
  providers: [TariffService],
  entryComponents: [PurchaseTariffDialogComponent]
})
export class TariffModule {}
