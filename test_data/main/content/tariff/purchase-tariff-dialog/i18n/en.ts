export const locale = {
  lang: 'en',
  data: {
    PURCHASERATE: {
      TITLE: 'Proof of purchase',
      RATENAME: 'Rate name',
      RATEPRICE: 'Rate price',
      PURCHASE: 'BUY',
      CANCEL: 'CANCEL',
      ACCEPT:
        'согласен с <a href="http://bazissoft.ru/servicebazis/bazis-cloud/contract-offer-bazis-cloud" target="_blank">договором оферты</a>',
      LITTLEMONEY: 'There is not enough money to buy this tariff',
      TOREFILL: {
        TEXT: 'To refill the balance, go to',
        LINK: 'link'
      },
      OPTIONS: {
        TITLE: 'Дополнительные опции',
        ADDITIONAL_PROCESSING: 'Дополнительная обработка',
        HOLES: 'Отверстия',
        BAZIS_MODELS: 'Загрузка БАЗИС-моделей',
        IMPORT: 'Импорт'
      }
    }
  }
};
