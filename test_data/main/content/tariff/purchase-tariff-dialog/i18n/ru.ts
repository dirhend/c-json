export const locale = {
  lang: 'ru',
  data: {
    PURCHASERATE: {
      TITLE: 'Подтверждение покупки',
      RATENAME: 'Тариф',
      RATEPRICE: 'Стоимость',
      PURCHASE: 'КУПИТЬ',
      CANCEL: 'ОТМЕНА',
      ACCEPT:
        'согласен с <a href="http://bazissoft.ru/servicebazis/bazis-cloud/contract-offer-bazis-cloud" target="_blank">договором оферты</a>',
      LITTLEMONEY: 'Не достаточно средств для покупки данного тарифа',
      TOREFILL: {
        TEXT: 'Для пополнения баланса перейдите по',
        LINK: 'ссылке'
      },
      OPTIONS: {
        TITLE: 'Дополнительные опции',
        ADDITIONAL_PROCESSING: 'Дополнительная обработка',
        HOLES: 'Отверстия',
        BAZIS_MODELS: 'Загрузка БАЗИС-моделей',
        IMPORT: 'Импорт'
      }
    }
  }
};
