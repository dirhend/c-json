export const locale = {
    lang: 'ru',
    data: {
        'TARIFF': {
            'COUNTSIZE': 'ЗА {{ size }} ЗАКАЗОВ',
            'TIMESIZE': 'НА {{ size }} ДНЕЙ',
            'PURCHASE': 'КУПИТЬ',
            'FROM_PRICE': 'от',
            'TYPE': {
                '0': 'ЛИМИТНЫЙ',
                '1': 'БЕЗЛИМИТНЫЙ',
                '2': '{{ size }} МБ'
            },
            'SNACKBAR': {
                'PURCHSUCC': 'Тариф куплен',
                'PURCHFAIL': 'Не удалось купить тариф',
                'PURCHCONFLICT': 'Невозможно купить тариф, отличающийся от текущего',
                'PURCHSIZEERROR': 'Невозможно купить тариф. Общий объем прайс-листов превышает ограничения тарифного плана'
            }
        }
    }
};
