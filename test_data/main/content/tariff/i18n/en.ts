export const locale = {
    lang: 'en',
    data: {
        'TARIFF': {
            'COUNTSIZE': 'FOR {{ size }} ORDERS',
            'TIMESIZE': '{{ size }} DAYS',
            'PURCHASE': 'BUY',
            'FROM_PRICE': 'от',
            'TYPE': {
                '0': 'LIMITED',
                '1': 'UNLIMITED',
                '2': '{{ size }} MB'
            },
            'SNACKBAR': {
                'PURCHSUCC': 'Тариф куплен',
                'PURCHFAIL': 'Не удалось купить тариф',
                'PURCHCONFLICT': 'Невозможно купить тариф, отличающийся от текущего',
                'PURCHSIZEERROR': 'Невозможно купить тариф. Общий объем прайс-листов превышает ограничения тарифного плана'
            }
        }
    }
};
