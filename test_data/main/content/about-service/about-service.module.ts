import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutServiceRoutingModule } from './about-service-routing.module';
import { AboutServiceComponent } from './about-service.component';
import { TranslateModule } from '@ngx-translate/core';
import { FuseSharedModule } from '@fuse/shared.module';

@NgModule({
  imports: [
    CommonModule,
    AboutServiceRoutingModule,
    TranslateModule,
    FuseSharedModule
  ],
  declarations: [AboutServiceComponent]
})
export class AboutServiceModule { }
