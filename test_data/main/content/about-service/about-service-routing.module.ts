import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../shared/guards/auth.guard';
import { AboutServiceComponent } from './about-service.component';

const routes: Routes = [
  {
    path: 'services/:id/about',
    component: AboutServiceComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AboutServiceRoutingModule { }
