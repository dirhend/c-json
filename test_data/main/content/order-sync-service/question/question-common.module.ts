import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatIconModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatToolbarModule,
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { ChatModule } from './chat/chat.module';
import { EmployeeSalonInfoDialogComponent } from './employee-salon-info-dialog/employee-salon-info-dialog.component';
import { QuestionModule } from './questions/question.module';

@NgModule({
  imports: [
    MatIconModule, //
    MatListModule,
    MatToolbarModule,
    MatButtonModule,
    MatProgressSpinnerModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    QuestionModule,
    ChatModule
  ],
  declarations: [
    EmployeeSalonInfoDialogComponent //
  ],
  entryComponents: [
    EmployeeSalonInfoDialogComponent //
  ]
})
export class QuestionCommonModule {}
