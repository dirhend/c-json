import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatChipsModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { CKEditorModule } from 'ngx-ckeditor';

import { ChatRoutingModule } from './chat-routing.module';
import { ChatComponent } from './chat.component';
import { ChatService } from './chat.service';
import { MessageComponent } from './message/message.component';

@NgModule({
  imports: [
    CommonModule,
    ChatRoutingModule,

    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    MatChipsModule,

    FuseSharedModule,
    TranslateModule,
    CKEditorModule,
    SharedModule,
  ],
  declarations: [
    ChatComponent,
    MessageComponent,
  ],
  providers: [
    ChatService
  ],
})
export class ChatModule { }
