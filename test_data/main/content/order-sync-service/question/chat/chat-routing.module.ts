import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './chat.component';
import { ChatService } from './chat.service';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'question/:id/detail',
    component: ChatComponent,
    resolve: {
      data: ChatService
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'question/:key/:id/detail',
    component: ChatComponent,
    resolve: {
      data: ChatService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class ChatRoutingModule { }
