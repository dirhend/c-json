import { HttpBackend, HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import { Attachment } from 'cloud-shared-lib';
import { Message } from 'cloud-shared-lib';
import { Question } from 'cloud-shared-lib';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ChatService {


  isEmployeeUser: boolean;
  private _key = '';
  get key(): string {
    return this._key;
  }
  set key(managerKey: string) {
    this._key = managerKey;
    this.httpClient = this._key ? new HttpClient(this.handler) : this.http;
  }

  private readonly urlQuestions = ApiLinks.question;
  private readonly urlMessage = ApiLinks.message;

  onQuestionChanged = new BehaviorSubject<any>(null);
  onMessagesChanged = new BehaviorSubject<any>(null);
  messagesTotalCount = 0;
  httpClient: HttpClient;

  constructor(private http: HttpClient, private handler: HttpBackend) {
    this.httpClient = this.http;
  }

  /**
   * The Chat App Main Resolver
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    this.key = route.params.key || '';

    return new Promise((resolve, reject) => {
      Promise.all([
        this.getQuestion(route.params.id),
        this.getMessages(route.params.id, false)
      ]).then(() => {
        resolve();
      },
        reject
      );
    });
  }

  getQuestion(questionId: number) {
    const url = this.key ? ApiLinks.getQuestionByKey(questionId) : ApiLinks.getQuestion(questionId);
    let httpParams = new HttpParams();
    if (this.key) {
      httpParams = httpParams.set('key', this.key);
    }

    return new Promise((resolve, reject) => {
      this.httpClient.get(url, { params: httpParams })
        .map((res: {
          isEmployeeUser: boolean,
          questionDTO: Question
        }) => {
          this.isEmployeeUser = res.isEmployeeUser;
          return res.questionDTO;
        })
        .subscribe((response: Question) => {
          this.onQuestionChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

  getMessages(questionId: number, all: boolean = false) {
    return new Promise((resolve, reject) => {
      const url = this.key ? ApiLinks.messageManager(questionId) : ApiLinks.message(questionId);

      let httpParams = new HttpParams();
      if (this.key) {
        httpParams = httpParams.set('key', this.key);
      }
      if (all) {
        httpParams = httpParams.set('all', 'true');
      }

      this.httpClient.get(url, { params: httpParams })
        .map((response: {
          messagesDTO: Message[],
          totalCount: number
        }) => {
          this.messagesTotalCount = response.totalCount;
          return response.messagesDTO;
        })
        .subscribe((response: Message[]) => {

          this.onMessagesChanged.next(response);

          resolve(response);
        }, reject);
    });
  }

  saveMessage(questionId: number, message: Message) {
    return new Promise((resolve, reject) => {
      const url = this.key ? ApiLinks.messageManager(questionId) : ApiLinks.message(questionId);

      let httpParams = new HttpParams();
      if (this.key) {
        httpParams = httpParams.set('key', this.key);
      }

      this.httpClient.post(url, message, { params: httpParams })
        .subscribe((response: Message) => {
          resolve(response);
        }, reject);
    });
  }

  uploadFiles(files: File[], questionId: number, messageId: number) {
    const input = new FormData();
    files.forEach((f: File) => {
      input.append(f.name, f);
    });

    return new Promise((resolve, reject) => {

      this.httpClient.post(ApiLinks.messageFiles(questionId, messageId), input)
        .subscribe((attachments: Attachment[]) => {
          resolve(attachments);
        }, reject);
    });
  }

  getAttachmentFile(messageId: number, attachmentId: number): Promise<Blob> {
    return new Promise((resolve, reject) => {
      this.httpClient.get(ApiLinks.getMessageFiles(this.onQuestionChanged.value.id, messageId, attachmentId), { responseType: 'blob' })
        .subscribe((response) => {
          resolve(response);
        }, reject);
    });
  }

  closeQuestion(): Promise<any> {
    let httpParams = new HttpParams();
    if (this.key) {
      httpParams = httpParams.set('key', this.key);
    }

    return new Promise((resolve, reject) => {
      this.httpClient.post(ApiLinks.closeQuestion(this.onQuestionChanged.value.id), undefined, { params: httpParams })
        .subscribe((response) => {

          this.onQuestionChanged.value.closed = true;
          this.onQuestionChanged.next(this.onQuestionChanged.value);

          resolve(response);
        }, reject);
    });
  }

  restoreQuestion(): Promise<any> {
    let httpParams = new HttpParams();
    if (this.key) {
      httpParams = httpParams.set('key', this.key);
    }

    return new Promise((resolve, reject) => {
      this.httpClient.post(ApiLinks.restoreQuestion(this.onQuestionChanged.value.id), undefined, { params: httpParams })
        .subscribe((response) => {

          this.onQuestionChanged.value.closed = false;
          this.onQuestionChanged.next(this.onQuestionChanged.value);

          resolve(response);
        }, reject);
    });
  }
}
