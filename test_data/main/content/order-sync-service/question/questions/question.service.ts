import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpBackend } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Question } from 'cloud-shared-lib';
import { ApiLinks } from '@cloud/app.api-links';
import { EmployeeSalon } from 'cloud-shared-lib';
import { Message } from 'cloud-shared-lib';
import { LogResponsibleQuestion } from 'cloud-shared-lib';

@Injectable()
export class QuestionService {
  orderGuid = '';

  private _managerKey = '';
  get managerKey(): string {
    return this._managerKey;
  }
  set managerKey(managerKey: string) {
    this._managerKey = managerKey;
    this.httpClient = this._managerKey ? new HttpClient(this.handler) : this.http;
  }

  onQuestionsChanged = new BehaviorSubject<any>(null);

  questionsTotalCount: number;
  employeeInfo: EmployeeSalon;
  isEmployeeUser: boolean;
  httpClient: HttpClient;

  constructor(
    private http: HttpClient, //
    private handler: HttpBackend
  ) {
    this.httpClient = http;
  }

  getQuestions(pageIndex: number, pageSize: number, filterText: string, statusFilter: QuestionStatusFilter, responsibleEmployeeId: number = 0) {
    let paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText)
      .set('statusFilter', statusFilter.toString());

    if (responsibleEmployeeId !== 0) {
      paramsHttp = paramsHttp.set('responsibleEmployeeId', responsibleEmployeeId.toString());
    }

    return new Promise((resolve, reject) => {
      this.httpClient
        .get(ApiLinks.question, { params: paramsHttp })
        .map((res: { totalCount: number; questionsDTO: Question[] }) => {
          this.questionsTotalCount = res.totalCount;
          return res.questionsDTO;
        })
        .subscribe((response: Question[]) => {
          this.onQuestionsChanged.next(response);

          resolve(response);
        }, reject);
    });
  }

  getQuestionsManager(pageIndex: number, pageSize: number, statusFilter: QuestionStatusFilter, responsibleEmployeeId: number = 0) {
    let paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('key', this.managerKey)
      .set('statusFilter', statusFilter.toString());

    if (responsibleEmployeeId !== 0) {
      paramsHttp = paramsHttp.set('responsibleEmployeeId', responsibleEmployeeId.toString());
    }

    return new Promise((resolve, reject) => {
      this.httpClient
        .get(ApiLinks.questionManager, { params: paramsHttp })
        .map((res: { totalCount: number; questionsDTO: Question[] }) => {
          this.questionsTotalCount = res.totalCount;
          return res.questionsDTO;
        })
        .subscribe((response: Question[]) => {
          this.onQuestionsChanged.next(response);

          resolve(response);
        }, reject);
    });
  }

  getEmployeeInfoByKey() {
    const paramsHttp: HttpParams = new HttpParams().set('key', this.managerKey);

    return new Promise((resolve, reject) => {
      this.httpClient
        .get(ApiLinks.employeeInfoByKey, { params: paramsHttp })
        .map((res: { employeeDTO: EmployeeSalon; isEmployeeUser: boolean }) => {
          this.isEmployeeUser = res.isEmployeeUser;
          this.employeeInfo = res.employeeDTO;
          return res.employeeDTO;
        })
        .subscribe((response: EmployeeSalon) => {
          this.onQuestionsChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

  getEmployeesUser() {
    return new Promise((resolve, reject) => {
      this.httpClient.get(ApiLinks.employeesUser).subscribe((response: EmployeeSalon[]) => {
        resolve(response);
      }, reject);
    });
  }

  getLogsResponsible(questionId: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(ApiLinks.getLogsResponsible(questionId)).subscribe((response: LogResponsibleQuestion[]) => {
        resolve(response);
      }, reject);
    });
  }

  getEmployeesUserByKey() {
    const paramsHttp: HttpParams = new HttpParams().set('key', this.managerKey);

    return new Promise((resolve, reject) => {
      this.httpClient.get(ApiLinks.employeesUserByKey, { params: paramsHttp }).subscribe((response: EmployeeSalon[]) => {
        resolve(response);
      }, reject);
    });
  }

  postSetResponsibleUserForQuestion(questionId: number, employeeId: number) {
    const paramsHttp: HttpParams = new HttpParams().set('employeeId', employeeId.toString());

    return new Promise((resolve, reject) => {
      this.httpClient.post(ApiLinks.setResponsibleUserForQuestion(questionId), null, { params: paramsHttp }).subscribe((response: { questionDTO: Question }) => {
        resolve(response);
      }, reject);
    });
  }

  postSetResponsibleUserForQuestionByKey(questionId: number, employeeId: number) {
    const paramsHttp: HttpParams = new HttpParams().set('employeeId', employeeId.toString()).set('key', this.managerKey);

    return new Promise((resolve, reject) => {
      this.httpClient.post(ApiLinks.setResponsibleUserForQuestionByKey(questionId), null, { params: paramsHttp }).subscribe((response: { questionDTO: Question }) => {
        resolve(response);
      }, reject);
    });
  }

  createQuestion(question: Question): Promise<any> {
    const paramsHttp: HttpParams = new HttpParams().set('key', this.managerKey);

    return new Promise((resolve, reject) => {
      this.httpClient.post(ApiLinks.question, question, { params: paramsHttp }).subscribe((response: { questionDTO: Question; messageDTO: Message }) => {
        this.onQuestionsChanged.value.push(response.questionDTO);
        this.onQuestionsChanged.next(this.onQuestionsChanged.value);

        resolve(response);
      }, reject);
    });
  }

  getEmployeeSalon(questionId: number): Promise<any> {
    const url = this.managerKey ? ApiLinks.employeeSalonByKey(questionId) : ApiLinks.employeeSalon(questionId);

    let httpParams: HttpParams = new HttpParams();
    if (this.managerKey) {
      httpParams = httpParams.set('key', this.managerKey);
    }

    return new Promise((resolve, reject) => {
      this.httpClient.get(url, { params: httpParams }).subscribe((response: EmployeeSalon) => {
        resolve(response);
      }, reject);
    });
  }
}

export enum QuestionStatusFilter {
  All,
  Opened,
  Closed
}
