export const locale = {
    lang: 'ru',
    data: {
        'NEWCALLING': {
            'TITLE': 'Новое обращение',
            'CREATE': 'СОЗДАТЬ',
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'TEXT': 'Описание',
                'SUBJECT': 'Тема',
                'ERROR': {
                    'SUBJECTEMPTY': 'Заголовок не должен быть пустым',
                    'TEXTEMPTY': 'Описание не может быть пустым'
                }
            }
        }
    }
};
