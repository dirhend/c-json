import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionListManagerComponent } from './question-list-manager/question-list-manager.component';
import { QuestionListResolver } from './question-list/question-list.resolver';
import { QuestionListManagerResolver } from './question-list-manager/question-list-manager.resolver';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { QuestionListComponent } from './question-list/question-list.component';

const routes: Routes = [
  {
    path: 'question',
    component: QuestionListComponent,
    children: [],
    resolve: {
      data: QuestionListResolver
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'question/:key',
    component: QuestionListManagerComponent,
    children: [],
    resolve: {
      data: QuestionListManagerResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class QuestionRoutingModule { }
