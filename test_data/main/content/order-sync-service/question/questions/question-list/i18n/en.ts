export const locale = {
    lang: 'ru',
    data: {
        'CALLINGLIST': {
            'TITLE': 'Обращения',
            'SEARCH': 'Поиск',
            'STATUSFILTER': {
                'ALL': 'Все',
                'OPENED': 'Открытые',
                'CLOSED': 'Закрытые'
            },
            'RESPONSIBLEEMPLOYEE': 'Ответственный',
            'ALL': 'Все',
            'TABLE': {
                'ID': '№',
                'CREATIONDATE': 'Создан',
                'SUBJECT': 'Заголовок',
                'DATELASTANSWER': 'Обновлен',
                'CALLINGUSERNAME': 'Автор',
                'SALONNAME': 'Салон',
                'RESPONSIBLEEMPLOYEE': 'Ответственный',
                'ORDERNAME': '№ Заказа',
                'MENU':
                {
                    'APPOINTRESPONSIBLE': 'Назначить ответственного',
                    'LOGSRESPONSIBLEQUESTION': 'Лог изменения ответственного'
                }
            }
        }
    }
};
