import { TestBed, inject } from '@angular/core/testing';
import { QuestionListResolver } from './question-list.resolver';


describe('QuestionListResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestionListResolver]
    });
  });

  it('should be created', inject([QuestionListResolver], (service: QuestionListResolver) => {
    expect(service).toBeTruthy();
  }));
});
