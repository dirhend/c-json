import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { QuestionService, QuestionStatusFilter } from '../question.service';

@Injectable()
export class QuestionListResolver {

  constructor(
    private questionService: QuestionService
  ) { }

  /**
   * The Chat App Main Resolver
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.questionService.getQuestions(0, 10, '', QuestionStatusFilter.Opened)
      ]).then(() => {
        resolve();
      },
        reject
      );
    });
  }

}
