import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QuestionRoutingModule } from './question-routing.module';
import { QuestionService } from './question.service';
import {
  MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatTableModule, MatPaginatorModule, MatToolbarModule, MatRippleModule, MatListModule,
  MatSelectModule, MatChipsModule, MatMenuModule, MatProgressSpinnerModule
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { QuestionListComponent } from './question-list/question-list.component';
import { TranslateModule } from '@ngx-translate/core';
import { CdkTableModule } from '@angular/cdk/table';
import { QuestionListManagerComponent } from './question-list-manager/question-list-manager.component';
import { QuestionListManagerResolver } from './question-list-manager/question-list-manager.resolver';
import { QuestionListResolver } from './question-list/question-list.resolver';
import { NewQuestionDialogComponent } from './new-question-dialog/new-question-dialog.component';
import { AppointResponsibleDialogComponent } from './appoint-responsible-dialog/appoint-responsible-dialog.component';
import { LogsResponsibleQuestionDialogComponent } from './logs-responsible-question-dialog/logs-responsible-question-dialog.component';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { ChatService } from '../chat/chat.service';

@NgModule({
  imports: [
    CommonModule,
    QuestionRoutingModule,

    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatRippleModule,
    MatListModule,
    MatSelectModule,
    MatChipsModule,
    MatMenuModule,
    MatProgressSpinnerModule,

    FuseSharedModule,
    TranslateModule,
    SharedModule,
  ],
  declarations: [
    QuestionListComponent,
    QuestionListManagerComponent,
    NewQuestionDialogComponent,
    AppointResponsibleDialogComponent,
    LogsResponsibleQuestionDialogComponent,
  ],
  providers: [
    QuestionService,
    QuestionListManagerResolver,
    QuestionListResolver,
    ChatService,
  ],
  entryComponents: [
    NewQuestionDialogComponent,
    AppointResponsibleDialogComponent,
    LogsResponsibleQuestionDialogComponent,
  ]
})
export class QuestionModule { }
