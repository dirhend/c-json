import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { QuestionService, QuestionStatusFilter } from '../question.service';

@Injectable()
export class QuestionListManagerResolver {
  constructor(private questionService: QuestionService) {}

  /**
   * The Chat App Main Resolver
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.questionService.managerKey = route.params.key;
    this.questionService.orderGuid = route.queryParams.orderGuid;

    return new Promise((resolve, reject) => {
      Promise.all([this.questionService.getEmployeeInfoByKey()])
        .then(() => {
          this.questionService.getQuestionsManager(0, 10, QuestionStatusFilter.Opened, this.questionService.isEmployeeUser ? this.questionService.employeeInfo.id : 0).then(() => {
            resolve();
          });
        }, reject)
        .catch(() => {
          console.error('pizderc');
        });
    });
  }
}
