import { TestBed, inject } from '@angular/core/testing';

import { QuestionListManagerResolver } from './question-list-manager.resolver';

describe('QuestionListManagerResolverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestionListManagerResolver]
    });
  });

  it('should be created', inject([QuestionListManagerResolver], (service: QuestionListManagerResolver) => {
    expect(service).toBeTruthy();
  }));
});
