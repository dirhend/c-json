export const locale = {
    lang: 'ru',
    data: {
        'CALLINGLISTMANAGER': {
            'TITLE': 'Обращения',
            'ADDCALLING': 'Создать обращение',
            'RESPONSIBLEEMPLOYEE': 'Сотрудник',
            'ALL': 'Все',
            'TABLE': {
                'ID': '№',
                'CREATIONDATE': 'Создан',
                'SUBJECT': 'Заголовок',
                'DATELASTANSWER': 'Обновлен',
                'CALLINGUSERNAME': 'Автор',
                'RESPONSIBLEEMPLOYEE': 'Ответственный',
                'APPOINTRESPONSIBLE': 'Назначить ответственного',
                'ORDERNAME': '№ Заказа'
            },
            'SNACKBAR': {
                'CALLINGADDED': 'Обращение создано',
                'CALLINGADDFAIL': 'Не удалось создать обращение',
                'ERRORPAID': 'Для загрузки файлов необходимо иметь активный тариф сервиса синхронизация заказов салона',
                'ERRORUPLOAD': 'Ошибка загрузки файлов'
            },
            'STATUSFILTER': {
                'ALL': 'Все',
                'OPENED': 'Открытые',
                'CLOSED': 'Закрытые'
            }
        }
    }
};
