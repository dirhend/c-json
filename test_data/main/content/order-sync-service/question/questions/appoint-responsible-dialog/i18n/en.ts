export const locale = {
    lang: 'ru',
    data: {
        'APPOINTRESPONSIBLEDIALOG': {
            'TABLE':
            {
                'NAME': 'Имя',
                'PHONENUMBER': 'Номер телефона',
                'EMAIL': 'E-mail'
            },
            'TITLE': 'Список сотрудников',
            'OK': 'ОК',
            'CANCEL': 'Отмена'
        }
    }
};
