export const locale = {
    lang: 'ru',
    data: {
        'LOGSRESPONSIBLEQUESTION': {
            'TABLE':
            {
                'EMPLOYEENAME': 'Назначил',
                'RESPONSIBLEEMPLOYEENAME': 'Ответственный',
                'DATE': 'Дата'
            },
            'TITLE': 'Лог изменения ответственного за обращение',
            'CLOSE': 'Закрыть'
        }
    }
};
