export const locale = {
    lang: 'ru',
    data: {
        'SYNCLOG': {
            'TABLE': {
                'DATE': 'Дата',
                'DESC': 'Описание'
            },
            'SNACKBAR': {
                'DOWNLOADFAIL': 'Не удалось скачать заказ',
                'DOWNLOADTARIFFNONE': 'Необходимо оплатить заказы'
            },
            'DOWNLOADWARNINGPAY': 'При скачивании заказа будет списан 1 заказ из текущего тарифа'
        }
    }
};
