import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../../shared/guards/auth.guard';
import { OrderDetailService } from './order-detail.service';
import { OrderDetailComponent } from './order-detail.component';

const routes: Routes = [
  {
    path: 'order/detail/:id',
    component: OrderDetailComponent,
    resolve: {
      data: OrderDetailService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class OrderDetailRoutingModule { }
