export const locale = {
    lang: 'ru',
    data: {
        'STATUSLOG': {
            'TABLE': {
                'DATE': 'Дата',
                'NAME': 'Состояние',
                'SYNCRECIPIENT': 'Синхр. с администратором',
                'SYNCSENDER': 'Синхр. с салоном',
                'SYNCSTATS': {
                    'SYNCWAIT': 'Не синхронизирован'
                }
            }
        }
    }
};
