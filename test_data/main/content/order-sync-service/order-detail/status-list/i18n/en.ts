export const locale = {
    lang: 'en',
    data: {
        'STATUSLOG': {
            'TABLE': {
                'DATE': 'Date',
                'NAME': 'Status name',
                'SYNCRECIPIENT': 'Синхр. с администратором',
                'SYNCSENDER': 'Синхр. с салоном',
                'SYNCSTATS': {
                    'SYNCWAIT': 'Не синхронизирован'
                }
            }
        }
    }
};
