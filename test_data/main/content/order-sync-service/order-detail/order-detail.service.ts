import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { OrderStatusLog, OrderSyncLog, SalonOrder } from 'cloud-shared-lib';
import { BehaviorSubject, Observable } from 'rxjs';

import { ApiLinks } from '../../../../app.api-links';

@Injectable()
export class OrderDetailService {
  order: SalonOrder;
  syncLog: OrderSyncLog[];
  statusLog: OrderStatusLog[];
  onOrderChanged: BehaviorSubject<any> = new BehaviorSubject({});
  onSyncLogChanged: BehaviorSubject<any> = new BehaviorSubject({});
  onStatusLogChanged: BehaviorSubject<any> = new BehaviorSubject({});
  statusLogTotalCount = 0;
  syncLogTotalCount = 0;

  private urlOrdeerSyncLog = 'Set in resolve';
  private urlOrder = 'Set in resolve';
  private urlOrderStatusLog = 'Set in resolve';
  private readonly urlDownloadOrder: string = ApiLinks.downloadOrder;

  constructor(private http: HttpClient) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.urlOrdeerSyncLog = ApiLinks.orderSyncLog(route.params.id);
    this.urlOrder = ApiLinks.orders + '/' + route.params.id;
    this.urlOrderStatusLog = ApiLinks.orderStatusLog(route.params.id);

    return new Promise((resolve, reject) => {
      Promise.all([this.getOrder(), this.getOrderSyncLog(), this.getOrderStatusLog()]).then(() => {
        resolve();
      }, reject);
    });
  }

  getOrderSyncLog(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.urlOrdeerSyncLog)
        .map((res: { totalCount: number; logSyncOrderDTO: OrderSyncLog[] }) => {
          this.syncLogTotalCount = res.totalCount;
          return res.logSyncOrderDTO;
        })
        .subscribe((response: any) => {
          this.syncLog = response;
          this.onSyncLogChanged.next(this.syncLog);
          resolve(response);
        }, reject);
    });
  }

  getOrderStatusLog(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .get(this.urlOrderStatusLog)
        .map((res: { totalCount: number; statusesDTO: OrderStatusLog[] }) => {
          this.statusLogTotalCount = res.totalCount;
          return res.statusesDTO;
        })
        .subscribe((response: any) => {
          this.statusLog = response;
          this.onStatusLogChanged.next(this.statusLog);
          resolve(response);
        }, reject);
    });
  }

  getOrder(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get(this.urlOrder).subscribe((response: any) => {
        this.order = response;
        this.onOrderChanged.next(this.order);
        resolve(response);
      }, reject);
    });
  }

  getOrderArchive(syncId: number): Promise<Blob> {
    const httpParams: HttpParams = new HttpParams().set('syncId', syncId.toString());

    return new Promise((resolve, reject) => {
      this.http.get(this.urlDownloadOrder, { params: httpParams, responseType: 'blob' }).subscribe(response => {
        resolve(response);
      }, reject);
    });
  }

  // getOrder(): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     this.http.get('api/e-commerce-orders/' + this.routeParams.id)
  //       .subscribe((response: any) => {
  //         this.order = response;
  //         this.onOrderChanged.next(this.order);
  //         resolve(response);
  //       }, reject);
  //   });
  // }
}
