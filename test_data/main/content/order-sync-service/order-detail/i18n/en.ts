export const locale = {
    lang: 'en',
    data: {
        'ORDERDETAIL': {
            'HEADER': {
                'ORDER': 'Order {{ value }}',
                'ORDERFROM': 'from {{ value }}'
            },
            'TABLABEL': {
                'COMMON': 'Order Details',
                'SYNCLOG': 'Sync Log',
                'STATUSLOG': 'Status log'
            },
            'COMMON': {
                'TABLE': {
                    'NAME': 'Name',
                    'DATECREATED': 'Creation date',
                    'SALONNAME': 'Salon name',
                    'STATUS': 'Status',
                    'NOTE': 'Note',
                    'SALONIDENTIFIER': 'Тип авторизации'
                }
            }
        }
    }
};
