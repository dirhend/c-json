export const locale = {
    lang: 'ru',
    data: {
        'ORDERDETAIL': {
            'HEADER': {
                'ORDER': 'Заказ {{ value }}',
                'ORDERFROM': 'из {{ value }}'
            },
            'TABLABEL': {
                'COMMON': 'Детали заказа',
                'SYNCLOG': 'История синхронизаций',
                'STATUSLOG': 'История состояний'
            },
            'COMMON': {
                'TABLE': {
                    'NAME': 'Название',
                    'DATECREATED': 'Дата создания',
                    'SALONNAME': 'Салон',
                    'STATUS': 'Статус',
                    'NOTE': 'Описание',
                    'SALONIDENTIFIER': 'Тип авторизации'
                }
            }
        }
    }
};
