import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
} from '@angular/material';
import { ServiceListService } from '@cloud/main/content/common/service-list/service-list.service';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../../shared/shared.module';
import { OrderDetailRoutingModule } from './order-detail-routing.module';
import { OrderDetailComponent } from './order-detail.component';
import { OrderDetailService } from './order-detail.service';
import { StatusListComponent } from './status-list/status-list.component';
import { SyncListComponent } from './sync-list/sync-list.component';

@NgModule({
  imports: [
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    CdkTableModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    OrderDetailRoutingModule,
    SharedModule
  ],
  declarations: [OrderDetailComponent, StatusListComponent, SyncListComponent],
  providers: [OrderDetailService, ServiceListService]
})
export class OrderDetailModule {}
