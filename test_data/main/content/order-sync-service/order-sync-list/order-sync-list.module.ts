import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderSyncListRoutingModule } from './order-sync-list-routing.module';
import { OrderSyncListComponent } from './order-sync-list.component';
import { OrderSyncListService } from './order-sync-list.service';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatTableModule, MatSortModule, MatPaginatorModule } from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
  imports: [
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    CdkTableModule,
    MatSortModule,
    MatPaginatorModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    OrderSyncListRoutingModule,
    SharedModule,
  ],
  declarations: [
    OrderSyncListComponent,
  ],
  providers: [
    OrderSyncListService
  ]
})
export class OrderSyncListModule { }
