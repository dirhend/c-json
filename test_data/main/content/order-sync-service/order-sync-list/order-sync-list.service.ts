import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiLinks } from '../../../../app.api-links';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { OrderSyncLogType, OrderSyncLog } from 'cloud-shared-lib';

@Injectable()
export class OrderSyncListService {

  onSyncListChanged: BehaviorSubject<any> = new BehaviorSubject({});
  syncLogTotalCount = 0;

  private readonly urlSyncList: string = ApiLinks.orderCommonSyncLog;

  constructor(
    private http: HttpClient
  ) { }

  /**
  * Resolve
  * @param {ActivatedRouteSnapshot} route
  * @param {RouterStateSnapshot} state
  * @returns {Observable<any> | Promise<any> | any}
  */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getOrderSyncList(0, 10)
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getOrderSyncList(pageIndex: number, pageSize: number, filterText: string = '', filterType: OrderSyncLogType = OrderSyncLogType.All): Promise<any> {
    
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText)
      .set('filterType', filterType.toString());

    return new Promise((resolve, reject) => {
      this.http.get(this.urlSyncList, { params: paramsHttp })
        .map((res: {
          totalCount: number,
          logSyncOrderDTO: OrderSyncLog[]
        }) => {
          this.syncLogTotalCount = res.totalCount;
          return res.logSyncOrderDTO;
        })
        .subscribe((response: any) => {
          this.onSyncListChanged.next(response);
          resolve(response);
        }, reject);
    });
  }
}
