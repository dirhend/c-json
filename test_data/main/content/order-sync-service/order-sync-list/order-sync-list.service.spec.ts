import { TestBed, inject } from '@angular/core/testing';

import { OrderSyncListService } from './order-sync-list.service';

describe('OrderSyncListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderSyncListService]
    });
  });

  it('should be created', inject([OrderSyncListService], (service: OrderSyncListService) => {
    expect(service).toBeTruthy();
  }));
});
