import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderSyncListComponent } from './order-sync-list.component';
import { OrderSyncListService } from './order-sync-list.service';
import { AuthGuard } from '../../../shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'order-sync-list',
    component: OrderSyncListComponent,
    resolve: {
      data: OrderSyncListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderSyncListRoutingModule { }
