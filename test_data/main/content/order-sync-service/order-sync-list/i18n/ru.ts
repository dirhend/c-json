export const locale = {
    lang: 'ru',
    data: {
        'ORDERSYNCLOG': {
            'TITLE': 'История',
            'SEARCH': 'Поиск',
            'FILTERTYPE': {
                'TITLE': 'Тип',
                'ALL': 'Все',
                'LOADED': 'Отправлен в БАЗИС-Облако',
                'UNLOADED': 'Получен из БАЗИС-Облако',
                'DOWNLOADED': 'Скачан на диск'
            },
            'TABLE': {
                'DATE': 'Дата',
                'ORDERNAME': 'Заказ',
                'SALONNAME': 'Салон',
                'DESC': 'Описание'
            }
        }
    }
};
