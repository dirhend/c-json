import { TestBed, inject } from '@angular/core/testing';
import { StateInfoListService } from './state-info-list.service';


describe('StateInfoListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StateInfoListService]
    });
  });

  it('should be created', inject([StateInfoListService], (service: StateInfoListService) => {
    expect(service).toBeTruthy();
  }));
});
