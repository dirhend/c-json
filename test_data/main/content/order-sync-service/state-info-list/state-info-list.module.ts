import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatCheckboxModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MAT_DATE_LOCALE, SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';

import { SharedModule } from '../../../shared/shared.module';
import { StateInfoListRoutingModule } from './state-info-list-routing.module';
import { StateInfoListComponent } from './state-info-list.component';
import { StateInfoListService } from './state-info-list.service';

@NgModule({
  imports: [
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    CdkTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDatepickerModule,
    SatDatepickerModule,
    SatNativeDateModule,
    // MatModule

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    StateInfoListRoutingModule,
    SharedModule,
  ],
  declarations: [
    StateInfoListComponent,
  ],
  providers: [
    StateInfoListService,
    {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
  ]
})
export class StateInfoListModule { }
