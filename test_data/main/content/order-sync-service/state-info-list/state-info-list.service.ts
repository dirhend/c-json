import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { StateInfo, StateInfoFilter } from 'cloud-shared-lib';
import { BehaviorSubject, Observable } from 'rxjs';

import { ApiLinks } from '../../../../app.api-links';

@Injectable()
export class StateInfoListService {
  onStateListChanged: BehaviorSubject<StateInfo[]> = new BehaviorSubject([]);
  stateListTotalCount = 0;

  constructor(private http: HttpClient) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<StateInfo[]> | Promise<StateInfo[]> | StateInfo[]}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<StateInfo[]> | Promise<StateInfo[]> | StateInfo[] {
    return new Promise((resolve, reject) => {
      Promise.all([this.getStateList(0, 10)]).then(() => {
        resolve();
      }, reject);
    });
  }

  saveStateInfo(state: StateInfo): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .put(ApiLinks.stateInfoCheck(state.id), null)
        .subscribe((response: any) => {
          this.onStateListChanged.next(this.onStateListChanged.value);
          resolve(response);
        }, reject);
    });
  }

  getStateItems(): Promise<string[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.stateItems).subscribe((response: string[]) => {
        resolve(response);
      }, reject);
    });
  }

  getStateList(
    pageIndex: number,
    pageSize: number,
    filterText: string = '',
    filter: StateInfoFilter = StateInfoFilter.UnChecked,
    state: string = '',
    dateBegin: Date = null,
    dateEnd: Date = null
  ): Promise<StateInfo[]> {
    let paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText)
      .set('filter', filter.toString())
      .set('state', state);

    if (dateBegin && dateEnd) {
      paramsHttp = paramsHttp
        .set('dateBegin', dateBegin.toISOString())
        .set('dateEnd', dateEnd.toISOString());
    }

    return new Promise((resolve, reject) => {
      this.http
        .get(ApiLinks.stateInfo, { params: paramsHttp })
        .map((res: { totalCount: number; stateListDTO: StateInfo[] }) => {
          this.stateListTotalCount = res.totalCount;
          return res.stateListDTO;
        })
        .subscribe((response: StateInfo[]) => {
          this.onStateListChanged.next(response);
          resolve(response);
        }, reject);
    });
  }
}
