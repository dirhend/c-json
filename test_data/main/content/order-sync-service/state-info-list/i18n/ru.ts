export const locale = {
    lang: 'ru',
    data: {
        'STATE_INFO': {
            'TITLE': 'Состояния заказов',
            'SEARCH': 'Поиск',
            'FILTERTYPE': {
                'TITLE': 'Тип',
                'ALL': 'Все',
                'CHECKED': 'Обработанные',
                'UNCHECKED': 'Необработанные'
            },
            'TABLE': {
                'NAME': 'Наименование',
                'DATE': 'Дата изменения статуса',
                'ORDER_NAME': 'Заказ',
                'ORDER_DATE': 'Дата принятия заказа',
                'SALON_NAME': 'Салон',
                'CHECK': 'Обработан'
            },
            'ALL': 'Все',
            'STATE_SELECT': 'Состояние',
            'DATE': 'Дата'
        }
    }
};
