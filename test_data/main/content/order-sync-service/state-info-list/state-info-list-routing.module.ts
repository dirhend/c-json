import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../../shared/guards/auth.guard';
import { StateInfoListComponent } from './state-info-list.component';
import { StateInfoListService } from './state-info-list.service';

const routes: Routes = [
  {
    path: 'state-info-list',
    component: StateInfoListComponent,
    resolve: {
      data: StateInfoListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StateInfoListRoutingModule { }
