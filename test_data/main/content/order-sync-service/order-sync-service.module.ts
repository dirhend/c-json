import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalonListModule } from './salon-list/salon-list.module';
import { OrderListModule } from './order-list/order-list.module';
import { OrderDetailModule } from './order-detail/order-detail.module';
import { OrderSyncListModule } from './order-sync-list/order-sync-list.module';
import { QuestionCommonModule } from './question/question-common.module';
import { StateInfoListModule } from './state-info-list/state-info-list.module';

@NgModule({
  imports: [
    CommonModule,
    SalonListModule,
    OrderListModule,
    OrderDetailModule,
    OrderSyncListModule,
    QuestionCommonModule,
    StateInfoListModule
  ]
})
export class OrderSyncServiceModule { }
