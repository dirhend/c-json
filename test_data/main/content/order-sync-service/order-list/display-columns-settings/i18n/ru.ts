export const locale = {
    lang: 'ru',
    data: {
        'DISPLAYCOLSSETTINGS': {
            'TITLE': 'Настройка таблицы',
            'SAVE': 'СОХРАНИТЬ',
            'CANCEL': 'ОТМЕНА',
            'TABLE': {
                'COLS': {
                    'name': 'Название', 
                    'dateCreated': 'Дата создания', 
                    'salonName': 'Салон', 
                    'salonIdentifier': 'Тип авторизации', 
                    'statusName': 'Статус', 
                    'note': 'Описание', 
                    'syncSender': 'Синхр. с салоном', 
                    'syncRecipient': 'Синхр. с администратором'
                }
            }
        }
    }
};
