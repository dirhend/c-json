export const locale = {
    lang: 'en',
    data: {
        'ORDER': {
            'TITLE': 'Orders',
            'SEARCH': 'Search',
            'SETTINGS': {
                'DISPLAYCOLSSETTINGS': 'Настройка колонок'
            },
            'FILTER': {
                'TITLE': 'Заказы',
                'ALL': 'Все',
                'NOT_IN_ARCHIVE': 'В работе',
                'IN_ARCHIVE': 'В архиве'
            },
            'TABLE': {
                'NAME': 'Name',
                'SALONNAME': 'Salon',
                'CREATIONDATE': 'Creation date',
                'STATUS': 'Status',
                'NOTE': 'Note',
                'SALONIDENT': 'Тип авторизации',
                'SYNCRECIPIENT': 'Sync administrator',
                'SYNCSENDER': 'Sync with salon',
                'RESETSYNCSTATUS': 'Сбросить статус синхронизации',
                'RESETSYNCSTATUSCONFIRM': 'Сбросить статус синхронизации с Администратором для заказа "{{ orderName }}"',
                'SYNCSTATS': {
                    'SYNCSUCC': 'Synced',
                    'SYNCWAIT': 'Not in sync'
                },
                'MENU': {
                    'IN_ARCHIVE': 'Переместить в архив',
                    'NOT_IN_ARCHIVE': 'Восстановить из архива',
                    'DELETE': 'Удалить',
                }
            },
            'SNACKBAR': {
                'UNLOCKORDER': 'Разблокировать заказ {{value}}?',
                'LOCKORDER': 'Заблокировать заказ {{value}}?',
                'LOCKEDORDER': 'Заказ заблокирован',
                'UNLOCKEDORDER': 'Заказ разблокирован',
                'RESETSYNCSTATUSFAIL': 'Не удалось сбросить статус синхронизации',
                'RESETSYNCSTATUSSUCCESS': 'Статус синхронизации изменен',
                'IN_ARCHIVE': 'Переместить заказ {{value}} в архив? <br /> При перемещении заказа в архив сохранится только последняя копия заказа',
                'FROM_ARCHIVE': 'Восстановить заказ  {{value}} из архива?',
                'IN_ARCHIVED': 'Заказ перемещен в архив',
                'FROM_ARCHIVED': 'Заказ восстановлен из архива',
                'DELETE_ORDER': 'Удалить заказ {{value}}?',
                'DELETED_ORDER': 'Заказ удален',
                'DELETED_ORDER_ERROR': 'Ошибка удаления заказа',
            }
        }
    }
};
