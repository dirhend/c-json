export const locale = {
    lang: 'ru',
    data: {
        'ORDER': {
            'TITLE': 'Заказы',
            'SEARCH': 'Поиск',
            'SETTINGS': {
                'DISPLAYCOLSSETTINGS': 'Настройка колонок'
            },
            'FILTER': {
                'TITLE': 'Заказы',
                'ALL': 'Все',
                'NOT_IN_ARCHIVE': 'Активные',
                'IN_ARCHIVE': 'В архиве'
            },
            'TABLE': {
                'NAME': 'Название',
                'SALONNAME': 'Салон',
                'CREATIONDATE': 'Дата создания',
                'STATUS': 'Статус',
                'NOTE': 'Описание',
                'SALONIDENT': 'Тип авторизации',
                'SYNCRECIPIENT': 'Синхр. с администратором',
                'SYNCSENDER': 'Синхр. с салоном',
                'RESETSYNCSTATUS': 'Сбросить статус синхронизации',
                'RESETSYNCSTATUSCONFIRM': 'Сбросить статус синхронизации с Администратором для заказа "{{ orderName }}"',
                'SYNCSTATS': {
                    'SYNCSUCC': 'Синхронизирован',
                    'SYNCWAIT': 'Не синхронизирован'
                },
                'MENU': {
                    'IN_ARCHIVE': 'Переместить в архив',
                    'NOT_IN_ARCHIVE': 'Восстановить из архива',
                    'DELETE': 'Удалить',
                }
            },
            'SNACKBAR': {
                'UNLOCKORDER': 'Разблокировать заказ {{value}}?',
                'LOCKORDER': 'Заблокировать заказ {{value}}?',
                'LOCKEDORDER': 'Заказ заблокирован',
                'UNLOCKEDORDER': 'Заказ разблокирован',
                'RESETSYNCSTATUSFAIL': 'Не удалось сбросить статус синхронизации',
                'RESETSYNCSTATUSSUCCESS': 'Статус синхронизации изменен',
                'IN_ARCHIVE': 'Переместить заказ {{value}} в архив? <br /> При перемещении заказа в архив сохранится только последняя копия заказа',
                'FROM_ARCHIVE': 'Восстановить заказ  {{value}} из архива?',
                'IN_ARCHIVED': 'Заказ перемещен в архив',
                'FROM_ARCHIVED': 'Заказ восстановлен из архива',
                'DELETE_ORDER': 'Удалить заказ {{value}}?',
                'DELETED_ORDER': 'Заказ удален',
                'DELETED_ORDER_ERROR': 'Ошибка удаления заказа',
            }
        }
    }
};
