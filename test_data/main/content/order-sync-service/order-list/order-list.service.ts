import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiLinks } from '../../../../app.api-links';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SalonOrder } from 'cloud-shared-lib';

export enum OrderFilter {
  All,
  NotInArchive,
  InArchive
}

export class FiltersOptions {
  pageIndex = 0;
  pageSize = 10;
  searchText = '';
  filter: OrderFilter = OrderFilter.NotInArchive;
}

@Injectable()
export class OrderListService {
  orders: SalonOrder[];
  onOrdersChanged: BehaviorSubject<any> = new BehaviorSubject({});
  ordersTotalCount = 0;

  filtersOptions: FiltersOptions = new FiltersOptions();

  private readonly urlOrder: string = ApiLinks.orders;
  private readonly urlOrderUnlock: string = ApiLinks.orderUnlock;
  private readonly urlOrderLock: string = ApiLinks.orderLock;

  constructor(private http: HttpClient) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([this.getOrderList()]).then(() => {
        resolve();
      }, reject);
    });
  }

  getOrderList(): Promise<any> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', this.filtersOptions.pageIndex.toString())
      .set('pageSize', this.filtersOptions.pageSize.toString())
      .set('filterText', this.filtersOptions.searchText)
      .set('filter', this.filtersOptions.filter.toString());

    return new Promise((resolve, reject) => {
      this.http
        .get(this.urlOrder, { params: paramsHttp })
        .map((res: { totalCount: number; ordersDTO: SalonOrder[] }) => {
          this.ordersTotalCount = res.totalCount;
          return res.ordersDTO;
        })
        .subscribe((response: any) => {
          this.orders = response;
          this.onOrdersChanged.next(this.orders);
          resolve(response);
        }, reject);
    });
  }

  toggleBlockOrder(item: SalonOrder): Promise<any> {
    const paramsHttp: HttpParams = new HttpParams().set('orderId', item.id.toString());

    const url = item.blocked ? this.urlOrderUnlock : this.urlOrderLock;

    return new Promise((resolve, reject) => {
      this.http.get(url, { params: paramsHttp }).subscribe(() => {
        this.orders.find(o => o.id === item.id).blocked = !item.blocked;
        this.onOrdersChanged.next(this.orders);
        resolve();
      }, reject);
    });
  }

  archiveOrder(orderId: number, inArchive: boolean): Promise<any> {
    const paramsHttp: HttpParams = new HttpParams().set('inArchive', inArchive.toString());

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.archiveOrder(orderId), null, { params: paramsHttp }).subscribe(() => {
        resolve();
      }, reject);
    });
  }

  deleteOrder(orderId: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.order(orderId)).subscribe(() => {
        resolve();
      }, reject);
    });
  }

  resetSyncStatus(item: SalonOrder): Promise<any> {
    const urlResetOrderSyncStatus = ApiLinks.resetOrderSyncStatus(item.id);

    return new Promise((resolve, reject) => {
      this.http.post(urlResetOrderSyncStatus, undefined).subscribe(() => {
        this.orders.find(o => o.id === item.id).syncRecipient = false;
        this.onOrdersChanged.next(this.orders);
        resolve();
      }, reject);
    });
  }
}
