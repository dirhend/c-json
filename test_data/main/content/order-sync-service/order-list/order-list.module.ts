import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderListComponent } from './order-list.component';
import { OrderListService } from './order-list.service';
import { OrderListRoutingModule } from './order-list-routing.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import {
  MatIconModule, MatTableModule, MatPaginatorModule, MatFormFieldModule, MatSelectModule, MatInputModule, MatSortModule, MatButtonModule, MatDialogModule,
  MatRippleModule, MatMenuModule, MatToolbarModule, MatCheckboxModule, MatListModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { FuseConfirmDialogModule } from '@fuse/components';
import { DisplayColumnsSettingsComponent } from './display-columns-settings/display-columns-settings.component';

@NgModule({
  imports: [
    MatIconModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatRippleModule,
    MatMenuModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatListModule,

    CommonModule,
    FuseConfirmDialogModule,
    FuseSharedModule,
    TranslateModule,
    OrderListRoutingModule,
  ],
  declarations: [
    OrderListComponent,
    DisplayColumnsSettingsComponent,
  ],
  providers: [
    OrderListService
  ],
  entryComponents: [
    DisplayColumnsSettingsComponent,
  ]
})
export class OrderListModule { }
