import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../../../shared/guards/auth.guard';
import { OrderListComponent } from './order-list.component';
import { OrderListService } from './order-list.service';

const routes: Routes = [
  {
    path: 'order/list',
    component: OrderListComponent,
    resolve: {
      data: OrderListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class OrderListRoutingModule { }
