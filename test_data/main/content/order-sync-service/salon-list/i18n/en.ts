export const locale = {
    lang: 'en',
    data: {
        'SALON': {
            'TITLE': 'Salons',
            'SEARCH': 'Search',
            'ADDSALON': 'ADD SALON',
            'FROMXML': 'Import from XML',
            'TABLE': {
                'NAME': 'Name',
                'ARTICLE': 'Article',
                'CREATIONDATE': 'Creation date',
                'IDENTIFIER': 'Тип авторизации',
                'STATE': 'Состояние',
                'STATELOADED': 'Обновлен',
                'STATEUNLOADED': 'Не обновлен',
                'EDIT': 'Редактировать',
                'BLOCK': 'Заблокировать',
                'UNBLOCK': 'Разблокировать',
                'DELETE': 'Удалить',
                'FORCED_UPDATED_PRICE_LIST': 'Обновление прайс-листов'
            },
            'FILTER': {
                'TITLE': 'Салоны',
                'ALL': 'Все',
                'UPDATED': 'Обновлены',
                'NOT_UPDATED': 'Не обновлены'
            },
            'SNACKBAR': {
                'SALONADDED': 'Салон добавлен',
                'SALONADDFAIL': 'Не удалось добавить салон',
                'SALONEDITED': 'Салон изменен',
                'SALONDELETED': 'Салон удален',
                'SALONDELETE': 'Удалить салон {{value}}?',
                'LOCKSALON': 'Заблокировать салон {{value}}?',
                'UNLOCKSALON': 'Разблокировать салон {{value}}?',
                'LOCKEDSALON': 'Салон заблокирован',
                'UNLOCKEDSALON': 'Салон разблокирован',
                'SALONSIMPORTED': 'Salons imported',
                'SALONSIMPORTFAIL': 'Could not import salons',
                'SALONHASORDERS': 'Невозможно удалить салон. У салона имеются синхронизированые заказы',
                'EXISTS_PRICE_LISTS': 'Невозможно удалить салон. У салона подключена услуга обновления прайс-листов',
                'SALONEXIST': 'Салон с таким идентификатором уже существует',
                'FORCED_UPDATED_PRICE_LIST': 'Запустить команду принудительного обновления всех прайс-листов для салона {{value}}?',
                'FORCED_UPDATED_SUCCESS': 'Принудительное обновление всех прайс-листов запущено',
                'FORCED_UPDATED_ERROR': 'Ошибка запуска команды обновления всех прайс-листов',
                'EXISTS_STOCK': 'Невозможно удалить салон. У салона назначены склады'
            }
        }
    }
};
