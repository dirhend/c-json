import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalonListComponent } from './salon-list.component';
import { SalonListService } from './salon-list.service';
import { AuthGuard } from '../../../shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'order/salon/list',
    component: SalonListComponent,
    resolve: {
      data: SalonListService
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'price/salon/list',
    component: SalonListComponent,
    resolve: {
      data: SalonListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class SalonListRoutingModule { }
