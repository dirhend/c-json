import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpBackend, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { Salon } from 'cloud-shared-lib';
import { ApiLinks } from '../../../../app.api-links';

@Injectable()
export class SalonListService {
  salons: Salon[];
  onSalonsChanged: BehaviorSubject<any> = new BehaviorSubject({});
  salonsTotalCount = 0;
  isRoutePriceList: boolean = false;

  private readonly urlSalon: string = ApiLinks.salons;
  private readonly urlSalonLock: string = ApiLinks.salonLock;
  private readonly urlSalonUnlock: string = ApiLinks.salonUnlock;
  private readonly urlFiasSuggest: string = ApiLinks.fiasSuggest;

  private httpLocal: HttpClient;

  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.httpLocal = new HttpClient(handler);
  }

  /**
  * Resolve
  * @param {ActivatedRouteSnapshot} route
  * @param {RouterStateSnapshot} state
  * @returns {Observable<any> | Promise<any> | any}
  */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    
    this.isRoutePriceList = route.url[0].path === 'price';
    return new Promise((resolve, reject) => {

      Promise.all([
        this.getSalonList(0, 10, '', 0)
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getSalonList(pageIndex: number, pageSize: number, filterText: string, filterSalon: number): Promise<any> {
    let paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText)
      .set('filterSalon', filterSalon.toString());

    return new Promise((resolve, reject) => {
      this.http.get(this.urlSalon, { params: paramsHttp })
        .map((res: {
          totalCount: number,
          salonsDTO: Salon[]
        }) => {
          this.salonsTotalCount = res.totalCount;
          return res.salonsDTO;
        })
        .subscribe((response: any) => {
          this.salons = response;
          this.onSalonsChanged.next(this.salons);
          resolve(response);
        }, reject);
    });
  }

  addSalon(item: Salon): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.urlSalon, item)
        .subscribe((response: Salon) => {
          this.salons.push(response);
          this.onSalonsChanged.next(this.salons);

          this.salonsTotalCount++;

          resolve(response);
        }, reject);
    });
  }

  editSalon(item: Salon): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.urlSalon}/${item.id}`, item)
        .subscribe(() => {
          this.updateSalon(item);
          this.onSalonsChanged.next(this.salons);

          resolve();
        }, reject);
    });
  }

  deleteSalon(item: Salon): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.urlSalon}/${item.id}`)
        .subscribe(() => {
          this.salons.splice(this.salons.findIndex(s => s.id === item.id), 1);
          this.onSalonsChanged.next(this.salons);

          this.salonsTotalCount--;

          resolve();
        }, reject);
    });
  }

  toggleBlockSalon(item: Salon, success?: Function) {
    const paramsHttp: HttpParams = new HttpParams()
      .set('id', item.id.toString());

    const url = item.blocked ? this.urlSalonUnlock : this.urlSalonLock;

    this.http.get(url, { params: paramsHttp })
      .subscribe(() => {
        this.salons.find(s => s.id === item.id).blocked = !item.blocked;

        this.onSalonsChanged.next(this.salons);

        if (success) { success(); }
      });
  }

  forcedUpdatedPriceLists(salonId: number) {
    return new Promise((resolve, reject) => {

      this.http.post(ApiLinks.forcedUpdatePriceLists(salonId), undefined)
        .subscribe(() => {
          resolve();
        }, reject);
    });
  }


  private updateSalon(item: Salon) {
    const salon = this.salons.find(s => s.id === item.id);

    salon.name = item.name;
    salon.article = item.article;
    salon.identifier = item.identifier;
    salon.identifierType = item.identifierType;
    salon.blocked = item.blocked;
  }

  getAddress(settlementName: string): Promise<any> {
    const contentHeaders: HttpHeaders = new HttpHeaders()
      .set('Accept', 'application/json')
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Token 8e2d19a16b3ff9b15091275305c365822a886c14');

    return new Promise((resolve, reject) => {
      this.httpLocal.post(this.urlFiasSuggest, {
        query: settlementName,
        count: 1,
        from_bound: { value: 'city' },
        to_bound: { value: 'settlement' }
      }, { headers: contentHeaders })
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }
}
