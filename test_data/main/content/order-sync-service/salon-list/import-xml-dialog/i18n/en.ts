export const locale = {
    lang: 'en',
    data: {
        'IMPORTXMLDIALOG': {
            'TITLE': 'Upload from XML',
            'UPLOAD': 'Upload',
            'EXAMPLE': 'File example'
        }
    }
};
