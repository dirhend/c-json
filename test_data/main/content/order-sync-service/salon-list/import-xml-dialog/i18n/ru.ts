export const locale = {
    lang: 'ru',
    data: {
        'IMPORTXMLDIALOG': {
            'TITLE': 'Загрузка из XML',
            'UPLOAD': 'Загрузить',
            'EXAMPLE': 'Пример файла'
        }
    }
};
