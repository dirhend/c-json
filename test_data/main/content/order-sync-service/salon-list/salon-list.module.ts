import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalonListComponent } from './salon-list.component';
import { SalonDetailDialogComponent } from './salon-detail-dialog/salon-detail-dialog.component';
import { SalonListService } from './salon-list.service';
import { SalonIdentificationTypePipe } from '../../../shared/pipes/salon-identification-type.pipe';
import { SalonListRoutingModule } from './salon-list-routing.module';
import { SalonIdentificationPipe } from '../../../shared/pipes/salon-identification.pipe';
import { ImportXmlDialogComponent } from './import-xml-dialog/import-xml-dialog.component';
import { FuseSharedModule } from '@fuse/shared.module';
import {
  MatMenuModule, MatIconModule, MatFormFieldModule, MatTableModule, MatPaginatorModule, MatSelectModule, MatToolbarModule, MatDialogModule, MatSnackBarModule,
  MatInputModule, MatSortModule, MatButtonModule, MatTooltipModule
} from '@angular/material';
import { TranslateModule } from '@ngx-translate/core';
import { CdkTableModule } from '@angular/cdk/table';
import { FuseHighlightModule, FuseConfirmDialogModule } from '@fuse/components';
import { SharedModule } from '@cloud/main/shared/shared.module';

@NgModule({
  imports: [
    MatIconModule,
    MatFormFieldModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatToolbarModule,
    FuseHighlightModule,
    MatDialogModule,
    MatSnackBarModule,
    MatInputModule,
    MatSortModule,
    MatButtonModule,
    MatTooltipModule,
    SharedModule,
    CommonModule,
    MatMenuModule,
    FuseSharedModule,
    FuseConfirmDialogModule,
    TranslateModule,
    SalonListRoutingModule
  ],
  declarations: [
    SalonListComponent,
    SalonDetailDialogComponent,
    SalonIdentificationTypePipe,
    SalonIdentificationPipe,
    ImportXmlDialogComponent
  ],
  providers: [
    SalonListService
  ],
  entryComponents: [
    SalonDetailDialogComponent,
    ImportXmlDialogComponent
  ]
})
export class SalonListModule { }
