export const locale = {
    lang: 'ru',
    data: {
        'SALONMODAL': {
            'TITLE': 'Карточка салона',
            'OK': 'ОК',
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'NAME': 'Название',
                'ARTICLE': 'Идентификатор салона',
                'IDENTTYPE': 'Тип идентификатора',
                'CITY': 'Город',
                'COUNTRY': 'Страна',
                'ERROR': {
                    'NAMEEMPTY': 'Название не задано',
                    'ARTICLEEMPTY': 'Идентификатор салона не задан',
                    'IDENTIFIEREMPTY': 'Идентификатор не задан',
                    'CITYEMPTY': 'Город не задан',
                    'COUNTRYEMPTY': 'Страна не задана'
                }
            }
        }
    }
};
