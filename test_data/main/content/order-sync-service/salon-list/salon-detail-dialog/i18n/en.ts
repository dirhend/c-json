export const locale = {
    lang: 'en',
    data: {
        'SALONMODAL': {
            'TITLE': 'Salon Card',
            'OK': 'OK',
            'CANCEL': 'CANCEL',
            'FORM': {
                'NAME': 'Name',
                'ARTICLE': 'Article',
                'IDENTTYPE': 'Identifier type',
                'CITY': 'City',
                'COUNTRY': 'Страна',
                'ERROR': {
                    'NAMEEMPTY': 'Name is required',
                    'ARTICLEEMPTY': 'Article is required',
                    'IDENTIFIEREMPTY': 'Identifier is requiredн',
                    'CITYEMPTY': 'City is required',
                    'COUNTRYEMPTY': 'Страна не задана'
                }
            }
        }
    }
};
