import { TestBed, inject } from '@angular/core/testing';

import { SalonListService } from './salon-list.service';

describe('SalonListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalonListService]
    });
  });

  it('should be created', inject([SalonListService], (service: SalonListService) => {
    expect(service).toBeTruthy();
  }));
});
