import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { ManagerListService } from './manager-list.service';
import { ManagerListComponent } from './manager-list.component';

const routes: Routes = [
  {
    path: 'cutting/manager/list',
    component: ManagerListComponent,
    resolve: {
      data: ManagerListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class ManagerListRoutingModule { }
