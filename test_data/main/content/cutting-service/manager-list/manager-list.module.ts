import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagerListRoutingModule } from './manager-list-routing.module';
import { ManagerListComponent } from './manager-list.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ManagerListService } from './manager-list.service';
import {
  MatButtonModule, MatFormFieldModule, MatIconModule, MatInputModule, MatTableModule, MatSortModule, MatPaginatorModule, MatDialogModule,
  MatToolbarModule, MatCheckboxModule, MatRippleModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { AddManagerDialogComponent } from './add-manager-dialog/add-manager-dialog.component';

@NgModule({
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    CdkTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatDialogModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatRippleModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    ManagerListRoutingModule,
  ],
  declarations: [ManagerListComponent, AddManagerDialogComponent],
  providers: [ManagerListService],
  entryComponents: [AddManagerDialogComponent]
})
export class ManagerListModule { }
