import { TestBed, inject } from '@angular/core/testing';

import { ManagerListService } from './manager-list.service';

describe('ManagerListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ManagerListService]
    });
  });

  it('should be created', inject([ManagerListService], (service: ManagerListService) => {
    expect(service).toBeTruthy();
  }));
});
