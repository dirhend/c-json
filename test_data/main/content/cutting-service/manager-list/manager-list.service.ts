import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from 'cloud-shared-lib';
import { ApiLinks } from '../../../../app.api-links';
import { UserPermissiion, PermissionList } from 'cloud-shared-lib';

export interface IManagerListResponse {
  managersDTO: UserPermissiion[];
  totalCount: number;
}

@Injectable()
export class ManagerListService {

  onManagersChanged: BehaviorSubject<IManagerListResponse> = new BehaviorSubject({ managersDTO: [], totalCount: 0 });

  constructor(
    private http: HttpClient
  ) { }

  /**
  * The Academy App Main Resolver
  *
  * @param {ActivatedRouteSnapshot} route
  * @param {RouterStateSnapshot} state
  * @returns {Observable<any> | Promise<any> | any}
  */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getManagers(0, 10, ''),
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getManagers(pageIndex: number, pageSize: number, filterText: string): Promise<IManagerListResponse> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText);

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.manager, { params: paramsHttp })
        .subscribe((response: IManagerListResponse) => {
          this.onManagersChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

  addManager(manager: User) {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.manager, manager)
        .subscribe((response: UserPermissiion) => {

          this.onManagersChanged.value.totalCount++;
          this.onManagersChanged.value.managersDTO.unshift(response);
          this.onManagersChanged.next(this.onManagersChanged.value);

          resolve();
        }, reject);
    });
  }

  saveManagerPermissions(managerId: number, permissions: PermissionList): Promise<void> {

    return new Promise((resolve, reject) => {
      this.http.put(`${ApiLinks.manager}/${managerId}/permissions`, permissions)
        .subscribe(() => {

          const managerIndex = this.onManagersChanged.value.managersDTO.findIndex(m => +m.user.id === +managerId);
          this.onManagersChanged.value.managersDTO[managerIndex].permissions = permissions;
          this.onManagersChanged.next(this.onManagersChanged.value);

          resolve();
        }, reject);
    });
  }

  setManagerBlock(managerId: number, value: boolean) {
    const paramsHttp: HttpParams = new HttpParams()
      .set('blocked', value ? 'true' : 'false');

    return new Promise((resolve, reject) => {
      this.http.post(`${ApiLinks.manager}/${managerId}/block`, undefined, { params: paramsHttp })
        .subscribe(() => {

          const managerIndex = this.onManagersChanged.value.managersDTO.findIndex(m => +m.user.id === +managerId);
          this.onManagersChanged.value.managersDTO[managerIndex].user.blocked = value;
          this.onManagersChanged.next(this.onManagersChanged.value);

          resolve();
        }, reject);
    });
  }

  saveManager(manager: User): any {
    return new Promise((resolve, reject) => {
      this.http.put(`${ApiLinks.manager}/${manager.id}`, manager)
        .subscribe(() => {

          const managerIndex = this.onManagersChanged.value.managersDTO.findIndex(m => +m.user.id === +manager.id);

          const man: User = this.onManagersChanged.value.managersDTO[managerIndex].user;
          man.email = manager.email;
          man.phone = manager.phone;
          man.personSurname = manager.personSurname;
          man.personName = manager.personName;
          man.personPatronymic = manager.personPatronymic;
          man.password = '';

          this.onManagersChanged.value.managersDTO[managerIndex].user = man;
          this.onManagersChanged.next(this.onManagersChanged.value);

          resolve();
        }, reject);
    });
  }

  deleteManager(managerId: number) {
    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.manager + '/' + managerId)
        .subscribe(() => {

          const managerIndex = this.onManagersChanged.value.managersDTO.findIndex(m => +m.user.id === +managerId);

          this.onManagersChanged.value.managersDTO.splice(managerIndex, 1);
          this.onManagersChanged.value.totalCount--;

          this.onManagersChanged.next(this.onManagersChanged.value);

          resolve();
        }, reject);
    });
  }
}
