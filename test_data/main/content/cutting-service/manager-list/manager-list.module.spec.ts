import { ManagerListModule } from './manager-list.module';

describe('ManagerListModule', () => {
  let managerListModule: ManagerListModule;

  beforeEach(() => {
    managerListModule = new ManagerListModule();
  });

  it('should create an instance', () => {
    expect(managerListModule).toBeTruthy();
  });
});
