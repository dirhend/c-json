export const locale = {
  lang: 'ru',
  data: {
    ADDMANAGERDIALOG: {
      TITLE: 'Карточка менеджера',
      CREATE: 'СОЗДАТЬ',
      OK: 'OK',
      CANCEL: 'ОТМЕНА',
      FORM: {
        SURNAME: 'Фамилия',
        NAME: 'Имя',
        PATRONYMIC: 'Отчество',
        PHONE: 'Телефон',
        EMAIL: 'E-mail',
        PASSWORD: 'Пароль',
        ERROR: {
          SURNAMEEMPTY: 'Поле Фамилия не должно быть пустым',
          NAMEEMPTY: 'Поле Имя не должно быть пустым',
          EMAILEMPTY: 'Поле E-mail не должно быть пустым',
          EMAILINVALID: 'Некорректный E-mail',
          PASSWORDEMPTY: 'Поле Пароль не должно быть пустым',
          PASSWORD_IS_SHORT: 'Минимальная длина - {{ minlength }}'
        }
      },
      SNACKBAR: {
        MANAGEREDITED: 'Изменения сохранены',
        MANAGEREDITFAIL: 'Не удалось сохранить изменения',
        MANAGERADDED: 'Пользователь добавлен',
        EMAIL_EXIST: 'Пользователь с таким e-mail уже существует',
        INVALID_DATA: 'Введены некорректные данные',
        PASSWORD_EXISTS: 'Ненадежный пароль. Введите другой',
        PHONE_EXISTS: 'Пользователь с таким номером телефона уже существует'
      }
    }
  }
};
