export const locale = {
  lang: 'en',
  data: {
    MANAGERS: {
      TITLE: 'Менеджеры и права доступа',
      SEARCH: 'Поиск',
      ADDMANAGER: 'ДОБАВИТЬ',
      TABLE: {
        DATECREATION: 'Дата создания',
        FULLNAME: 'ФИО',
        COMPANY: 'Фирма',
        EMAIL: 'Email',
        PHONE: 'Телефон',
        FINANCEPERM: 'Финансы',
        CUTSETTINGSPERM: 'Настройки раскроя',
        MANAGERSETTINGSPERM: 'Менеджеры',
        CLIENTDELETING: 'Удаление клиентов',
        APIKEY: 'Ключ доступа',
        DISCOUNT: 'Скидки'
      },
      DELETE_CONFIRM: 'Удалить менеджера "{{ name }}"?',
      SNACKBAR: {
        BLOCKED: 'Пользователь заблокирован',
        BLOCKFAIL: 'Не удалось заблокированть пользователя',
        UNBLOCKED: 'Пользователь разблокирован',
        UNBLOCKFAIL: 'Не удалось разблокированть пользователя',
        DELETED: 'Менеджер удален',
        DELETE_FAIL: 'Не удалось удалить менеджера',
        MANAGER_HAVE_ORDERS: 'Невозможно удалить менеджера т. к. он назначен ответственным за заказ(ы)'
      }
    }
  }
};
