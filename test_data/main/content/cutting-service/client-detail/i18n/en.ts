export const locale = {
  lang: 'en',
  data: {
    CUTTINGCLIENTDETAIL: {
      TITLE: '{{ name }}',
      NEW_CLIENT_TITLE: 'Новый клиент',
      CREATE_CLIENT: 'СОЗДАТЬ',
      TABS: {
        COMMON: {
          TITLE: 'Общее'
        },
        SETTINGS: {
          TITLE: 'Настройки',
          CANDOWNLOADCUTTINGMAPS: 'Разрешить скачивать карты раскроя',
          SHOWCUTTINGMAPS: 'Отображение карт раскроя',
          DEFAULT: 'По умолчанию',
          YES: 'Да',
          NO: 'Нет',
          GROUP: {
            TITLE: 'Категория скидок',
            NONE: 'Нет'
          },
          PERSONAL_DISCOUNT: {
            TITLE: 'Персональная скидка',
            WARNING: 'При установке, к заказам клиента будет применяться Персональная скидка',
            ERROR: {
              EMPTY: 'Поле не должно быть пустым',
              RANGE: 'Число должно находиться в диапазоне от {{ min }} до {{ max }}'
            }
          },
          EDIT_PANEL_POSITION: {
            AUTO: 'Автоматическая',
            CUSTOM: 'Ручная',
            TITLE: 'Расстановка позиции панелей'
          }
        }
      },
      SNACKBAR: {
        CLIENT_CREATED: 'Клиент создан',
        EMAIL_EXISTS: 'Такой email уже зарегестрирован',
        PHONE_EXISTS: 'Такой номер телефона уже зарегестрирован',
        CLIENT_CREATE_FAIL: 'Не удалось зарегистрировать клиента',
        EMPTY_PASS_AND_EMAIL: 'Необходимо ввести email или телефон'
      }
    }
  }
};
