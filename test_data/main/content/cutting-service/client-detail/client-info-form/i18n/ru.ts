export const locale = {
    lang: 'ru',
    data: {
        'CUTTING_CLIENT_INFO_FORM': {
            'FORM': {
                'COMPANYNAME': 'Название фирмы',
                'DATECREATION': 'Дата регистрации',
                'EMAIL': 'Email',
                'NAME': 'Имя',
                'PHONE': 'Телефон',
                'TYPE': 'Тип',
                'SURNAME': 'Фамилия',
                'PATRONYMIC': 'Отчество',
                'PASSWORD': 'Пароль',
                'PASSWORD_PLACEHOLDER': 'Без изменений',
                'REMARK': 'Примечание',
                'ACTIVATED': 'Активирован',
                'ERROR': {
                    'EMPTY': 'Поле не должно быть пустым',
                    'EMAILINVALID': 'Некорректный Email',
                    'MAX_LENGTH': 'Максимальная длина - {{ maxlength }}',
                    'MIN_LENGTH': 'Минимальная длина - {{ minlength }}',
                }
            }
        }
    }
};
