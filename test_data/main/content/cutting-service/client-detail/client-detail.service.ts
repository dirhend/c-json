import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Params, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import { CuttingClient, CuttingClientGroup, CuttingClientOptions } from 'cloud-shared-lib';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class ClientDetailService {
  onCuttingClientChanged = new BehaviorSubject<CuttingClient>(null);
  onCuttingClientOptionsChanged = new BehaviorSubject<CuttingClientOptions>(null);
  clientId = 0;
  routeParams: Params;
  isNewClient = false;

  constructor(private http: HttpClient) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.routeParams = route.params;
    this.isNewClient = this.routeParams.id === 'new' ? true : false;

    return new Promise((resolve, reject) => {
      this.clientId = route.params.id;

      Promise.all([this.getClient(), this.getClientOptions()]).then(() => {
        resolve();
      }, reject);
    });
  }

  getClient(): Promise<CuttingClient> {
    if (this.isNewClient) {
      return new Promise((resolve, reject) => {
        const newClient = new CuttingClient();
        this.onCuttingClientChanged.next(newClient);
        resolve(newClient);
      });
    }

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingClient + '/' + this.clientId).subscribe((response: CuttingClient) => {
        this.onCuttingClientChanged.next(response);

        resolve(response);
      }, reject);
    });
  }

  saveClient(client: CuttingClient): Promise<CuttingClient> {
    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.cuttingClient + '/' + this.clientId, client).subscribe((response: CuttingClient) => {
        this.onCuttingClientChanged.next(response);

        resolve(response);
      }, reject);
    });
  }

  getClientOptions(): Promise<CuttingClientOptions> {
    if (this.isNewClient) {
      return new Promise((resolve, reject) => {
        resolve(undefined);
      });
    }

    return new Promise((resolve, reject) => {
      this.http.get<CuttingClientOptions>(ApiLinks.cuttingClientOptions(this.clientId)).subscribe((response: CuttingClientOptions) => {
        this.onCuttingClientOptionsChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  saveClientOptions(options: CuttingClientOptions): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.put<CuttingClientOptions>(ApiLinks.cuttingClientOptions(this.clientId), options).subscribe(() => {
        this.onCuttingClientOptionsChanged.next(options);
        resolve();
      }, reject);
    });
  }

  addClient(client: CuttingClient): Promise<CuttingClient> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingClient, client).subscribe((response: CuttingClient) => {
        this.onCuttingClientChanged.next(response);

        resolve(response);
      }, reject);
    });
  }

  getCuttingClientGroups(): Observable<CuttingClientGroup[]> {
    return this.http.get<CuttingClientGroup[]>(ApiLinks.cuttingClientGroup);
  }

  setCuttingClientGroup(client: CuttingClient, groupId: number): Observable<void> {
    const paramsHttp: HttpParams = new HttpParams() //
      .set('groupId', groupId.toString());

    return this.http.put<void>(ApiLinks.cuttingClientSetGroup(client.id), undefined, { params: paramsHttp });
  }
}
