import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { ClientDetailService } from './client-detail.service';
import { ClientDetailComponent } from './client-detail.component';

const routes: Routes = [
  {
    path: 'cutting/client/detail/:id',
    component: ClientDetailComponent,
    resolve: {
      profile: ClientDetailService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class ClientDetailRoutingModule { }
