import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatTabsModule,
  MatTooltipModule,
} from '@angular/material';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { CloudSharedLibModule } from 'cloud-shared-lib';
import { CustomFormsModule } from 'ngx-custom-validators';

import { environment } from '../../../../../environments/environment';
import { ClientDetailRoutingModule } from './client-detail-routing.module';
import { ClientDetailComponent } from './client-detail.component';
import { ClientDetailService } from './client-detail.service';
import { ClientInfoFormComponent } from './client-info-form/client-info-form.component';

@NgModule({
  imports: [
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatListModule,
    MatTooltipModule,

    FormsModule,
    CustomFormsModule,
    CloudSharedLibModule.forRoot(environment),

    CommonModule,
    FuseSharedModule,
    SharedModule,
    TranslateModule,
    ClientDetailRoutingModule
  ],
  declarations: [ClientDetailComponent, ClientInfoFormComponent],
  providers: [ClientDetailService]
})
export class ClientDetailModule {}
