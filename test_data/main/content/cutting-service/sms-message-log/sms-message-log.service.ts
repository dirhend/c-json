import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { SmsMessageLog } from 'cloud-shared-lib';
import { ApiLinks } from '@cloud/app.api-links';

export interface SmsMessagesLogResponse {
  totalCount: number;
  log: SmsMessageLog[];
}

@Injectable()
export class SmsMessageLogService {

  onSmsMessagesLogChanged: BehaviorSubject<SmsMessagesLogResponse> = new BehaviorSubject(null);

  constructor(
    private http: HttpClient,
  ) { }

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getSmsMessageLog(0, 10),
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getSmsMessageLog(pageIndex: number, pageSize: number): Promise<SmsMessagesLogResponse> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.smsMessageLog, { params: paramsHttp })
        .subscribe((response: SmsMessagesLogResponse) => {
          this.onSmsMessagesLogChanged.next(response);

          resolve(response);
        }, reject);
    });
  }
}
