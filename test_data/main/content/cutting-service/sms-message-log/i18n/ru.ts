export const locale = {
    lang: 'ru',
    data: {
        'CUTTINGSMSMESSAGELOG': {
            'TITLE': 'История отправки SMS',  
            'TABLE' : {
                'DATE': 'Дата',
                'CLIENTNAME': 'Клиент',
                'STATUS': 'Статус',
                'TEXT': 'Текст'
            }          
        }
    }
};
