export const locale = {
    lang: 'en',
    data: {
        'CUTTINGSMSMESSAGELOG': {
            'TITLE': 'История отправки SMS',   
            'TABLE' : {
                'DATE': 'Дата',
                'CLIENTNAME': 'Клиент',
                'STATUS': 'Статус',
                'TEXT': 'Текст'
            }            
        }
    }
};
