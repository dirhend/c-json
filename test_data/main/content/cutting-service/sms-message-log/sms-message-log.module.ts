import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SmsMessageLogRoutingModule } from './sms-message-log-routing.module';
import { SmsMessageLogComponent } from './sms-message-log.component';
import { SmsMessageLogService } from './sms-message-log.service';
import {
  MatIconModule, MatButtonModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSortModule, MatTableModule, MatRippleModule,
  MatDialogModule, MatToolbarModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { StateCuttingSmsPipe } from '@cloud/main/shared/pipes/state-cutting-sms.pipe';
import { SmsMessageLogPropertyViewerDialogComponent } from './sms-message-log-property-viewer-dialog/sms-message-log-property-viewer-dialog.component';

@NgModule({
  imports: [
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    CdkTableModule,
    MatRippleModule,
    MatDialogModule,
    MatToolbarModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    SmsMessageLogRoutingModule,
  ],
  declarations: [
    SmsMessageLogComponent,
    StateCuttingSmsPipe,
    SmsMessageLogPropertyViewerDialogComponent,
  ],
  providers: [
    SmsMessageLogService,
  ],
  entryComponents: [
    SmsMessageLogPropertyViewerDialogComponent,
  ]
})
export class SmsMessageLogModule { }
