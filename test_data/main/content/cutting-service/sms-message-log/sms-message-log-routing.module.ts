import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SmsMessageLogComponent } from './sms-message-log.component';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { SmsMessageLogService } from './sms-message-log.service';

const routes: Routes = [
  {
    path: 'cutting/sms-message/log',
    component: SmsMessageLogComponent,
    resolve: {
      profile: SmsMessageLogService,
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class SmsMessageLogRoutingModule { }
