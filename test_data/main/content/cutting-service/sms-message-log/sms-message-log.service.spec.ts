import { TestBed, inject } from '@angular/core/testing';

import { SmsMessageLogService } from './sms-message-log.service';

describe('SmsMessageLogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SmsMessageLogService]
    });
  });

  it('should be created', inject([SmsMessageLogService], (service: SmsMessageLogService) => {
    expect(service).toBeTruthy();
  }));
});
