import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import { CuttingOrderQuestion } from 'cloud-shared-lib';
import { BehaviorSubject, Observable } from 'rxjs';

export enum CuttingOrderQuestionStatusFilter {
  All,
  Opened,
  Closed
}

export interface CuttingOrderQuestionListResponse {
  questionsDTO: CuttingOrderQuestion[];
  totalCount: number;
}

@Injectable()
export class CuttingOrderQuestionListService {

  onQuestionsChanged = new BehaviorSubject<CuttingOrderQuestionListResponse>(null);

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    return new Promise((resolve, reject) => {
      Promise.all([
        this.getQuestions(0, 10, QuestionStatusFilter.Opened, ''),
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getQuestions(pageIndex: number, pageSize: number, statusFilter: QuestionStatusFilter, filterText: string) {

    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filterText', filterText)
      .set('statusFilter', statusFilter.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingOrderQuestion, { params: paramsHttp })
        .subscribe((response: CuttingOrderQuestionListResponse) => {

          this.onQuestionsChanged.next(response);

          resolve(response);
        }, reject);
    });
  }

  setReadedSelected(ids: number[]): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.setReadedSelected, ids)
        .subscribe(() => {

          const questionIndex = this.onQuestionsChanged.value.questionsDTO
            .map((q, i) => {
              if (ids.includes(q.id)) {
                return i;
              }
              return -1;
            })
            .filter(i => i >= 0);

          questionIndex.forEach(i => {
            this.onQuestionsChanged.value.questionsDTO[i].isAnswered = true;
          });

          this.onQuestionsChanged.next(this.onQuestionsChanged.value);

          resolve();
        }, reject);
    });
  }
}

export enum QuestionStatusFilter {
  All,
  Opened,
  Closed
}
