export const locale = {
  lang: 'en',
  data: {
    CUTTINGORDERQUESTION: {
      SEARCH: 'Поиск',
      STATUSFILTER: {
        ALL: 'Все',
        CLOSED: 'Закрытые',
        OPENED: 'Открытые'
      },
      TABLE: {
        CLIENTNAME: 'Клиент',
        CREATIONDATE: 'Дата',
        DATELASTANSWER: 'Дата изменения',
        ID: 'Номер',
        ORDERNAME: 'Заказ',
        SUBJECT: 'Тема',
        SETREADEDSELECTED: 'Отметить выбранные как отвеченое'
      },
      TITLE: 'Обращения'
    }
  }
};
