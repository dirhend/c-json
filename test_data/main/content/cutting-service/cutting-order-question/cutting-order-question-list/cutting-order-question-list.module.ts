import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatRippleModule,
  MatSelectModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@cloud/main/shared/shared.module';

import { CuttingOrderQuestionListRoutingModule } from './cutting-order-question-list-routing.module';
import { CuttingOrderQuestionListComponent } from './cutting-order-question-list.component';
import { CuttingOrderQuestionListService } from './cutting-order-question-list.service';

@NgModule({
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatRippleModule,
    MatSelectModule,
    MatCheckboxModule,
    MatMenuModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    SharedModule,
    CuttingOrderQuestionListRoutingModule,
  ],
  declarations: [
    CuttingOrderQuestionListComponent,
  ],
  providers: [
    CuttingOrderQuestionListService,
  ]
})
export class CuttingOrderQuestionListModule { }
