import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { CuttingOrderQuestionListService } from './cutting-order-question-list.service';
import { CuttingOrderQuestionListComponent } from './cutting-order-question-list.component';

const routes: Routes = [
  {
    path: 'cutting/question/list',
    component: CuttingOrderQuestionListComponent,
    resolve: {
      profile: CuttingOrderQuestionListService,
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class CuttingOrderQuestionListRoutingModule { }
