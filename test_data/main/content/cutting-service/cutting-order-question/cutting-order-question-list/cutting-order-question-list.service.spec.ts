import { TestBed, inject } from '@angular/core/testing';

import { CuttingOrderQuestionListService } from './cutting-order-question-list.service';

describe('CuttingOrderQuestionListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuttingOrderQuestionListService]
    });
  });

  it('should be created', inject([CuttingOrderQuestionListService], (service: CuttingOrderQuestionListService) => {
    expect(service).toBeTruthy();
  }));
});
