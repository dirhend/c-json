import { CuttingOrderQuestionListModule } from './cutting-order-question-list.module';

describe('CuttingOrderQuestionListModule', () => {
  let cuttingOrderQuestionListModule: CuttingOrderQuestionListModule;

  beforeEach(() => {
    cuttingOrderQuestionListModule = new CuttingOrderQuestionListModule();
  });

  it('should create an instance', () => {
    expect(cuttingOrderQuestionListModule).toBeTruthy();
  });
});
