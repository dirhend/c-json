import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { CuttingOrderQuestionChatService } from './cutting-order-question-chat.service';
import { CuttingOrderQuestionChatComponent } from './cutting-order-question-chat.component';

const routes: Routes = [
  {
    path: 'cutting/question/:id/detail',
    component: CuttingOrderQuestionChatComponent,
    resolve: {
      profile: CuttingOrderQuestionChatService,
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class CuttingOrderQuestionChatRoutingModule { }
