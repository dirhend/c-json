import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CuttingOrderQuestionChatRoutingModule } from './cutting-order-question-chat-routing.module';
import { CuttingOrderQuestionChatComponent } from './cutting-order-question-chat.component';
import { CuttingOrderQuestionChatService } from './cutting-order-question-chat.service';
import { MatIconModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatDividerModule, MatChipsModule } from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { CKEditorModule } from 'ngx-ckeditor';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { CuttingOrderQuestionMessageComponent } from './cutting-order-question-message/cutting-order-question-message.component';

@NgModule({
  imports: [
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    MatChipsModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    CKEditorModule,
    SharedModule,
    CuttingOrderQuestionChatRoutingModule
  ],
  declarations: [
    CuttingOrderQuestionChatComponent,
    CuttingOrderQuestionMessageComponent,
  ],
  providers: [
    CuttingOrderQuestionChatService,
  ]
})
export class CuttingOrderQuestionChatModule { }
