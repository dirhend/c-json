import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiLinks } from '@cloud/app.api-links';
import { CuttingOrderQuestion } from 'cloud-shared-lib';
import { Message } from 'cloud-shared-lib';
import { Attachment } from 'cloud-shared-lib';

export interface CuttingOrderMessageResponse {
  messagesDTO: Message[];
  totalCount: number;
}

@Injectable()
export class CuttingOrderQuestionChatService {

  onQuestionChanged = new BehaviorSubject<CuttingOrderQuestion>(null);
  onMessagesChanged = new BehaviorSubject<CuttingOrderMessageResponse>(null);

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    return new Promise((resolve, reject) => {
      Promise.all([
        this.getQuestion(route.params.id),
        this.getMessages(route.params.id, false),
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getQuestion(questionId: number): Promise<CuttingOrderQuestion> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingOrderQuestion + '/' + questionId)
        .subscribe((response: CuttingOrderQuestion) => {
          this.onQuestionChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

  getMessages(questionId: number, all: boolean = false) {
    return new Promise((resolve, reject) => {
      let httpParams = new HttpParams();
      httpParams = httpParams.set('all', all ? 'true' : 'false');

      this.http.get(ApiLinks.cuttingOrderQuestionMessage(questionId), { params: httpParams })
        .subscribe((response: CuttingOrderMessageResponse) => {

          this.onMessagesChanged.next(response);

          resolve(response);
        }, reject);
    });
  }

  saveMessage(questionId: number, message: Message) {
    return new Promise((resolve, reject) => {

      this.http.post(ApiLinks.cuttingOrderQuestionMessage(questionId), message)
        .subscribe((response: Message) => {
          resolve(response);
        }, reject);
    });
  }

  uploadFiles(files: File[], questionId: number, messageId: number) {
    const formData = new FormData();
    files.forEach((f: File) => {
      formData.append('attachmentFiles', f, f.name);
    });

    return new Promise((resolve, reject) => {

      this.http.post(ApiLinks.cuttingOrderQuestionMessageFiles(questionId, messageId), formData)
        .subscribe((response: Attachment[]) => {
          resolve(response);
        }, reject);
    });
  }

  getAttachmentFile(messageId: number, attachmentId: number): Promise<Blob> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingOrderQuestionMessageFiles(this.onQuestionChanged.value.id, messageId) + '/' + attachmentId, { responseType: 'blob' })
        .subscribe((response) => {
          resolve(response);
        }, reject);
    });
  }
}
