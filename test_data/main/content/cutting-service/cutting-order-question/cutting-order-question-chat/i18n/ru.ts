export const locale = {
    lang: 'ru',
    data: {
        'CUTTINGORDERCHAT': {
            'ALL': 'Все',
            'AUTHOR': 'Автор',
            'DATE': 'Дата',
            'LOADALLMESS': 'Показать предыдущие {{ value }}',
            'ORDERNAME': '№ Заказа',
            'QUESTION': 'Обращение',
            'SEND': 'Отправить',
            'SNACKBAR': {
                'FILESIZEWARN': 'Максимальный объем файлов - 5MB',
                'ERRORPAID': 'Для загрузки файлов необходимо иметь активный тариф сервиса синхронизация заказов салона',
                'ERRORUPLOAD': 'Ошибка загрузки файлов',
                'ERRORSENDMESSAGE': 'Ошибка отправки сообщения'
            }
        }
    }
};
