import { TestBed, inject } from '@angular/core/testing';

import { CuttingOrderQuestionChatService } from './cutting-order-question-chat.service';

describe('CuttingOrderQuestionChatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuttingOrderQuestionChatService]
    });
  });

  it('should be created', inject([CuttingOrderQuestionChatService], (service: CuttingOrderQuestionChatService) => {
    expect(service).toBeTruthy();
  }));
});
