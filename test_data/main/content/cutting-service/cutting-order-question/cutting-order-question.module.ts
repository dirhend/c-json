import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CuttingOrderQuestionChatModule } from './cutting-order-question-chat/cutting-order-question-chat.module';
import { CuttingOrderQuestionListModule } from './cutting-order-question-list/cutting-order-question-list.module';

@NgModule({
  imports: [
    CuttingOrderQuestionChatModule,
    CuttingOrderQuestionListModule,
  ],
  declarations: []
})
export class CuttingOrderQuestionModule { }
