import { CuttingOrderQuestionModule } from './cutting-order-question.module';

describe('CuttingOrderQuestionModule', () => {
  let cuttingOrderQuestionModule: CuttingOrderQuestionModule;

  beforeEach(() => {
    cuttingOrderQuestionModule = new CuttingOrderQuestionModule();
  });

  it('should create an instance', () => {
    expect(cuttingOrderQuestionModule).toBeTruthy();
  });
});
