import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import { FuseConfirmDialogModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { NgxLocalizedNumbers } from 'ngx-localized-numbers';

import { AppointManagerDialogComponent } from './appoint-manager-dialog/appoint-manager-dialog.component';
import { OrderListRoutingModule } from './order-list-routing.module';
import { OrderListTableComponent } from './order-list-table/order-list-table.component';
import {
  OrderTableFieldsDialogComponent,
} from './order-list-table/order-table-fields-dialog/order-table-fields-dialog.component';
import { OrderListComponent } from './order-list.component';
import { OrderListService } from './order-list.service';

@NgModule({
  imports: [
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatToolbarModule,
    MatRippleModule,
    MatDialogModule,
    MatMenuModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatTabsModule,
    MatDatepickerModule,

    CommonModule,
    NgxDnDModule,
    FuseSharedModule,
    TranslateModule,
    OrderListRoutingModule,
    NgxLocalizedNumbers,
    FuseConfirmDialogModule,
  ],
  declarations: [
    OrderListComponent,
    OrderListTableComponent,
    AppointManagerDialogComponent,
    OrderTableFieldsDialogComponent
  ],
  providers: [
    OrderListService
  ],
  entryComponents: [
    AppointManagerDialogComponent,
    OrderTableFieldsDialogComponent,
  ]
})
export class OrderListModule { }
