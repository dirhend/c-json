import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrderListComponent } from './order-list.component';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { OrderListService } from './order-list.service';

const routes: Routes = [
  {
    path: 'cutting/order/list',
    component: OrderListComponent,
    resolve: {
      profile: OrderListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class OrderListRoutingModule { }
