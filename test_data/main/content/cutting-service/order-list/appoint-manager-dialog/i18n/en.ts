export const locale = {
    lang: 'en',
    data: {
        'APPOINTMANAGERDIALOG': {
            'TITLE': 'Менеджеры',
            'SAVE': 'ОК',
            'CANCEL': 'ОТМЕНА',
            'TABLE': {
                'SNP': 'ФИО'
            }
        }
    }
};
