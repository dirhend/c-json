export const locale = {
    lang: 'ru',
    data: {
        'APPOINTMANAGERDIALOG': {
            'TITLE': 'Менеджеры',
            'SAVE': 'ОК',
            'CANCEL': 'ОТМЕНА',
            'TABLE': {
                'SNP': 'ФИО'
            }
        }
    }
};
