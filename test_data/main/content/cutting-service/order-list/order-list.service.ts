import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Params, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import { LocalStorageManager } from '@cloud/main/shared/helpers/local-storage-manager.helper';
import { CuttingFactory, CuttingOrder, CuttingOrderStatus, OrderStatusAction, User, UserOptions } from 'cloud-shared-lib';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

export interface CuttingOrderResponse {
  ordersDTO: CuttingOrder[];
  totalCount: number;
}

export interface CuttingFactoryResponse {
  factoriesDto: CuttingFactory[];
  defaultId: number;
}

@Injectable()
export class OrderListService {
  onCuttingOrdersChanged = new BehaviorSubject<CuttingOrderResponse>(null);
  onCuttingOrderStatusListChanged = new BehaviorSubject<CuttingOrderStatus[]>([]);
  onFactoriesChanged: BehaviorSubject<CuttingFactory[]> = new BehaviorSubject([]);
  onUserOptionsChanged: BehaviorSubject<UserOptions> = new BehaviorSubject(new UserOptions());
  statusFilterIds: number[] = [];
  paginatorParams: { pageIndex: number; pageSize: number } = {
    pageIndex: 0,
    pageSize: 10
  };
  routeParams: Params;

  constructor(private http: HttpClient) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.routeParams = route.params;

    return new Promise((resolve, reject) => {
      Promise.all([
        this.getCuttingOrderStatusList(), //
        this.getFactoryList(),
        this.getUserOptions()
      ]).then(() => {
        let statuses: number[] = [];
        const lsFilter = LocalStorageManager.orderListFilter;
        if (lsFilter && lsFilter.length > 0) {
          statuses = lsFilter;
        } else {
          statuses =
            this.statusFilterIds.length === 0
              ? this.onCuttingOrderStatusListChanged.value
                  .filter(s => (s.action === OrderStatusAction.Ordering && s.isDefault) || s.action === OrderStatusAction.Confirm)
                  .map(s => s.id)
              : this.statusFilterIds;
        }

        this.getOrders(this.paginatorParams.pageIndex, this.paginatorParams.pageSize, '', statuses, false)
          .then(resolve)
          .catch(reject);
      }, reject);
    });
  }

  getOrders(pageIndex: number, pageSize: number, filter: string, statusId: number[], archive: boolean): Promise<CuttingOrderResponse> {
    let searchParams = new HttpParams();
    if (archive) {
      searchParams = searchParams.set('archived', 'true');
    }

    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filter', filter)
      .set('statusId', statusId.join(','))
      .set('archived', archive ? 'true' : 'false');

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingOrder, { params: paramsHttp }).subscribe((response: CuttingOrderResponse) => {
        this.onCuttingOrdersChanged.next(response);

        resolve(response);
      }, reject);
    });
  }

  setOrderStatus(orderId: number, statusId: number) {
    const paramsHttp: HttpParams = new HttpParams().set('orderId', orderId.toString()).set('statusId', statusId.toString());

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.setCuttingOrderStatus, undefined, { params: paramsHttp }).subscribe((response: CuttingOrder) => {
        const orderIndex = this.onCuttingOrdersChanged.value.ordersDTO.findIndex((o: CuttingOrder) => o.id === orderId);

        if (orderIndex !== -1) {
          this.onCuttingOrdersChanged.value.ordersDTO[orderIndex] = response;
          this.onCuttingOrdersChanged.next(this.onCuttingOrdersChanged.value);
        }

        resolve(response);
      }, reject);
    });
  }

  setOrderFactory(orderId: number, factoryId?: number) {
    const paramsHttp: HttpParams = new HttpParams().set('orderId', orderId.toString()).set('factoryId', factoryId ? factoryId.toString() : undefined);

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.setCuttingOrderFactory, undefined, { params: paramsHttp }).subscribe((response: CuttingOrder) => {
        const orderIndex = this.onCuttingOrdersChanged.value.ordersDTO.findIndex((o: CuttingOrder) => o.id === orderId);

        if (orderIndex !== -1) {
          this.onCuttingOrdersChanged.value.ordersDTO[orderIndex] = response;
          this.onCuttingOrdersChanged.next(this.onCuttingOrdersChanged.value);
        }

        resolve(response);
      }, reject);
    });
  }

  setFactoryOrderId(orderId: number, factoryNumber: string) {
    const paramsHttp: HttpParams = new HttpParams().set('orderId', orderId.toString()).set('factoryNumber', factoryNumber);

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.setCuttinOrderFactoryId, undefined, { params: paramsHttp }).subscribe((response: CuttingOrder) => {
        const orderIndex = this.onCuttingOrdersChanged.value.ordersDTO.findIndex((o: CuttingOrder) => o.id === orderId);

        if (orderIndex !== -1) {
          this.onCuttingOrdersChanged.value.ordersDTO[orderIndex] = response;
          this.onCuttingOrdersChanged.next(this.onCuttingOrdersChanged.value);
        }

        resolve(response);
      }, reject);
    });
  }

  downloadOrder(orderId: number): Promise<{ body: Blob; filename: string }> {
    const httpParams: HttpParams = new HttpParams().set('orderId', orderId.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.downloadCuttingOrder, { params: httpParams, responseType: 'blob', observe: 'response' }).subscribe(response => {
        const filename = this.getFileNameByContentDisposition(response.headers.get('Content-Disposition'));

        resolve({ body: response.body, filename: filename });
      }, reject);
    });
  }

  downloadOrderPanelDrawings(orderId: number): Promise<{ body: Blob; filename: string }> {
    const httpParams: HttpParams = new HttpParams().set('orderId', orderId.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.downloadCuttingOrderPanelDrawings, { params: httpParams, responseType: 'blob', observe: 'response' }).subscribe(response => {
        const filename = this.getFileNameByContentDisposition(response.headers.get('Content-Disposition'));

        resolve({ body: response.body, filename: filename });
      }, reject);
    });
  }

  getCuttingOrderStatusList(): Promise<CuttingOrderStatus[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingOrderStatus).subscribe((response: CuttingOrderStatus[]) => {
        this.onCuttingOrderStatusListChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  getManagers(pageIndex: number, pageSize: number) {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('withMainUser', 'true');

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.manager, { params: paramsHttp }).subscribe((response: User[]) => {
        resolve(response);
      }, reject);
    });
  }

  appointManager(manager: User, orderId: number) {
    const paramsHttp: HttpParams = new HttpParams().set('orderId', orderId.toString());

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.appiontManagerToCuttingOrder(manager.id), undefined, { params: paramsHttp }).subscribe(() => {
        const orderIndex: number = this.onCuttingOrdersChanged.value.ordersDTO.findIndex(o => +o.id === +orderId);

        if (orderIndex !== -1) {
          this.onCuttingOrdersChanged.value.ordersDTO[orderIndex].managerId = manager.id;
          this.onCuttingOrdersChanged.value.ordersDTO[orderIndex].managerName = `${manager.personSurname} ${manager.personName} ${manager.personPatronymic}`;
          this.onCuttingOrdersChanged.next(this.onCuttingOrdersChanged.value);
        }

        resolve();
      }, reject);
    });
  }

  getFactoryList(): Promise<CuttingFactory[]> {
    const paramsHttp: HttpParams = new HttpParams();

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.factories, { params: paramsHttp }).subscribe((response: CuttingFactoryResponse) => {
        this.onFactoriesChanged.next(response.factoriesDto);
        resolve(response.factoriesDto);
      }, reject);
    });
  }

  private getFileNameByContentDisposition(contentDisposition: string) {
    const regex = /.*''/gi;
    const matches = contentDisposition.replace(regex, '');

    return decodeURI(matches) || 'order';
  }

  toggleArchived(orderId: number, value: boolean): any {
    const searchParams = new HttpParams().set('value', value ? 'true' : 'false');

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingOrderToggleArchived(orderId), undefined, { params: searchParams }).subscribe(() => {
        const orderIndex = this.onCuttingOrdersChanged.value.ordersDTO.findIndex(o => o.id === orderId);
        this.onCuttingOrdersChanged.value.ordersDTO.splice(orderIndex, 1);
        this.onCuttingOrdersChanged.value.totalCount--;
        this.onCuttingOrdersChanged.next(this.onCuttingOrdersChanged.value);

        resolve();
      }, reject);
    });
  }

  getUserOptions(): Promise<UserOptions> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.userOptions).subscribe((response: UserOptions) => {
        this.onUserOptionsChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  saveUserOptions(opt: UserOptions): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.userOptions, opt).subscribe(() => {
        this.onUserOptionsChanged.next(opt);
        resolve();
      }, reject);
    });
  }

  saveOrder(order: CuttingOrder): Promise<CuttingOrder> {
    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.cuttingOrder + '/' + order.id, order).subscribe((response: CuttingOrder) => {
        const orderIndex = this.onCuttingOrdersChanged.value.ordersDTO.findIndex((o: CuttingOrder) => o.id === order.id);

        if (orderIndex !== -1) {
          this.onCuttingOrdersChanged.value.ordersDTO[orderIndex] = response;
          this.onCuttingOrdersChanged.next(this.onCuttingOrdersChanged.value);
        }

        resolve(response);
      }, reject);
    });
  }
}
