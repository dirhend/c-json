export const locale = {
  lang: 'en',
  data: {
    CUTTINGORDERLIST: {
      TITLE: 'Заказы',
      PROJECTBUILDING: 'Выполняется формирование проекта',
      PANELDRAWINGSBUILDING: 'Выполняется формирование чертежей',
      DOWNLOADPROJECT: 'Скачать файлы для производства',
      DOWNLOADDPANELRAWINGS: 'Скачать чертежи деталей',
      STATUSFILTER: {
        TITLE: 'Статус',
        ALL: 'Все'
      },
      SEARCH: 'Поиск',
      TABLE: {
        dateCreate: 'Дата создания',
        name: 'Название',
        statusName: 'Статус',
        amountTempEstimate: 'Сумма',
        dateEdit: 'Дата изменения',
        dateOfIssue: 'Дата выдачи',
        DATEORDERING: 'Дата оформления',
        dateConfirm: 'Дата принятия',
        note: 'Описаниe',
        COMPLETED: 'Выполнен',
        clientName: 'Клиент',
        download_project: 'Файлы для производства',
        download_panel_drawings: 'Чертежи деталей',
        factoryOrderId: 'Производственный номер',
        managerName: 'Менеджер',
        factory: 'Производство',
        factory_empty: 'По умолчанию',
        menu: 'Меню',
        DATE_OF_ISSUE_PLACEHOLDER: 'Выберите дату',
        MENU_ITEMS: {
          REMOVEFROMARCHIVE: 'Убрать из архива',
          ADDTOARCHIVE: 'Переместить в архив',
          APPOINTMANAGER: 'Назначить ответственного'
        },
        HEADER_MENU: {
          FIELD_SETTINGS: 'Настройка столбцов'
        }
      },
      SNACKBAR: {
        STATUSCHANGED: 'Статус изменен',
        STATUSCHANGEFAIL: 'Не удалось изменить статус',
        ORDERDOWNLOADFAIL: 'Не удалось скачать файл',
        DONTSETTEDFACTORYID: 'Не задан производственный номер',
        ORDERSETFACTORYIDFAIL: 'Не удалось изменить производственный номер',
        NOACTIVETARIFFS: 'Нет активных тарифов',
        MANAGERAPPOINTED: 'Ответственный назначен',
        MANAGERAPPOINDFAIL: 'Не удалось назначить ответственного',
        ORDERSETFACTORYFAIL: 'Не удалось установить производство',
        ORDERREMOVEDFROMARCHIVE: 'Заказ перемещен в основной список',
        ORDERADDEDTOARCHIVE: 'Заказ перемещен в архив',
        ORDERREMOVEFROMARCHIVEFAIL: 'Не удалось переместить заказ в основной список',
        ORDERADDTOARCHIVEFAIL: 'Не удалось переместить заказ в архив',
        FIELDS_SETTINGS_SAVED: 'Изменения сохранены',
        FIELDS_SETTINGS_SAVE_FALIL: 'Не удалось сохранить изменения'
      },
      CONFIRM: {
        FROMARCHIVECONFIRM: 'Вернуть заказ {{ orderName }} в основоной список заказов?',
        TOARCHIVECONFIRM: 'Переместить заказ {{ orderName }} в архив?',
        CHANGEMANAGERANTERDOWNLOAD: 'Заказ закреплен за менеджером "{{ value }}".<br>Если вы его скачаете то станете ответственным по заказу.<br>Продолжить скачивание ?'
      }
    }
  }
};
