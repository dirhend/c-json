export const locale = {
  lang: 'en',
  data: {
    ORDER_TABLE_FIELDS_DIALOG: {
      TITLE: 'Настройка столбцов',
      SAVE: 'ОК',
      CANCEL: 'ОТМЕНА',
      TABLE: {
        NAME: 'Название',
        SHOWING: 'Отображение'
      }
    }
  }
};
