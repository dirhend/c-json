import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ClientsTurnoverModule } from './clients-turnover/clients-turnover.module';
import { CuttingPaymentListModule } from './payment-list/payment-list.module';
import { CuttingInfoModule } from './cutting-info/cutting-info.module';

@NgModule({
  imports: [
    CommonModule,
    ClientsTurnoverModule,
    CuttingPaymentListModule,
    CuttingInfoModule
  ]
})
export class CuttingReportsModule { }

