import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTooltipModule,
} from '@angular/material';
import { StateCuttingTaskPipe } from '@cloud/main/shared/pipes/state-cutting-task.pipe';
import { TimePipe } from '@cloud/main/shared/pipes/time.pipe';
import { FuseHighlightModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { CuttingInfoLogDialogComponent } from './cuting-info-log-dialog/cuting-info-log-dialog.component';
import { CuttingInfoRoutingModule } from './cutting-info-routing.module';
import { CuttingInfoComponent } from './cutting-info.component';
import { CuttingInfoService } from './cutting-info.service';

@NgModule({
  imports: [
    MatIconModule,
    MatSelectModule,
    MatButtonModule,
    MatFormFieldModule,
    MatTableModule,
    CdkTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSortModule,
    FuseHighlightModule,
    MatDialogModule,
    MatInputModule,
    MatTooltipModule,

    CommonModule,
    TranslateModule,
    FuseSharedModule,
    CuttingInfoRoutingModule
  ],
  declarations: [
    CuttingInfoComponent,
    StateCuttingTaskPipe,
    TimePipe,
    CuttingInfoLogDialogComponent
  ],
  providers: [CuttingInfoService],
  entryComponents: [CuttingInfoLogDialogComponent]
})
export class CuttingInfoModule {}
