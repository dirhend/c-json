import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CuttingInfo } from 'cloud-shared-lib';
import { ApiLinks } from '@cloud/app.api-links';

@Injectable()
export class CuttingInfoService {

  cuttingsInfo: CuttingInfo[];
  onCuttingsInfoChanged: BehaviorSubject<any> = new BehaviorSubject({});
  cuttingsInfoTotalCount = 0;

  constructor(
    private http: HttpClient
  ) { }

  /**
  * Resolve
  * @param {ActivatedRouteSnapshot} route
  * @param {RouterStateSnapshot} state
  * @returns {Observable<any> | Promise<any> | any}
  */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    return new Promise((resolve, reject) => {

      Promise.all([
        this.getCuttingsInfo(0, 10, 0, '', '')
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getCuttingsInfo(pageIndex: number, pageSize: number, filterCutting: number, searchText: string, sort: string): Promise<any> {

    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filter', filterCutting.toString())
      .set('searchText', searchText)
      .set('sort', sort)
      .set('isMy', 'true');

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingInfo, { params: paramsHttp })
        .map((res: {
          totalCount: number,
          tasks: CuttingInfo[]
        }) => {
          this.cuttingsInfoTotalCount = res.totalCount;
          return res.tasks;
        })
        .subscribe((response: any) => {
          this.cuttingsInfo = response;
          this.onCuttingsInfoChanged.next(this.cuttingsInfo);
          resolve(response);
        }, reject);
    });
  }

}
