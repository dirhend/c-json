export const locale = {
    lang: 'en',
    data: {
        'CUTTINGINFO': {
            'TITLE': 'Информация по раскрою',
            'SEARCH': 'Поиск',
            'FILTER': {
                'TITLE': 'Фильтр',
                'ALL': 'Все',
                'DONE': 'Выполненные',
                'UNDONE': 'Невыполненные',
                'INPROCESS': 'В процессе'
            },
            'TABLE': {
                'CUTTING_TIME': 'Время выполнения',
                'STATE': 'Состояние',
                'DATE_CREATED_ORDER': 'Дата создания заказа',
                'CLIENT_NAME': 'Имя клиента',
                'ORDER_NAME': 'Наименование заказа',
                'SHOW_LOG': 'Просмотр лога',
                'DATE_START_CUTTING': 'Дата начала выполнения',
            }
        }
    }
};
