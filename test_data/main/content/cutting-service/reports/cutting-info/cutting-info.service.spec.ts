import { TestBed, inject } from '@angular/core/testing';
import { CuttingInfoService } from './cutting-info.service';

describe('CuttingInfoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuttingInfoService]
    });
  });

  it('should be created', inject([CuttingInfoService], (service: CuttingInfoService) => {
    expect(service).toBeTruthy();
  }));
});
