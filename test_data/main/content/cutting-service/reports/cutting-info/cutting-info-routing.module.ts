import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CuttingInfoService } from './cutting-info.service';
import { CuttingInfoComponent } from './cutting-info.component';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'cutting/info',
    component: CuttingInfoComponent,
    resolve: {
      profile: CuttingInfoService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class CuttingInfoRoutingModule { }
