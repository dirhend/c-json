export const locale = {
  lang: 'ru',
  data: {
    CLIENTS_TURNOVER: {
      TITLE: 'Оборот по клиентам',
      SEARCH: 'Поиск',
      TABLE: {
        NAME: 'Наименование',
        DATE: 'Дата изменения статуса',
        ORDER_NAME: 'Заказ',
        ORDER_DATE: 'Дата принятия заказа',
        SALON_NAME: 'Салон',
        CHECK: 'Обработан',
        ORDERS_COUNT: 'Кол-во заказов',
        TOTAL_ORDERS_AMOUNT: 'Сумма заказов'
      },
      DATE: 'Дата'
    }
  }
};
