export const locale = {
  lang: 'en',
  data: {
    CLIENTS_TURNOVER: {
        TITLE: 'Оборот по клиентам',
      SEARCH: 'Поиск',
      TABLE: {
        DATECREATION: 'Дата регистрации',
        FULLNAME: 'ФИО',
        COMPANY: 'Фирма',
        EMAIL: 'Email',
        PHONE: 'Телефон',
        ORDERS_COUNT: 'Кол-во заказов',
        TOTAL_ORDERS_AMOUNT: 'Сумма заказов'
      },
      DATE: 'Дата'
    }
  }
};
