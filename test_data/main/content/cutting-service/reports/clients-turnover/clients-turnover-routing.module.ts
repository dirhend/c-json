import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';

import { ClientsTurnoverComponent } from './clients-turnover.component';

const routes: Routes = [
  {
    path: 'cutting/clients-turnover',
    component: ClientsTurnoverComponent,
    children: [],
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsTurnoverRoutingModule { }
