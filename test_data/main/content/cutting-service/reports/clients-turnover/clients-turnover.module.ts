import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTooltipModule,
} from '@angular/material';
import { CuttingReportsService } from '@cloud/main/shared/services/cutting-reports/cutting-reports.service';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxLocalizedNumbers } from 'ngx-localized-numbers';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';

import { ClientsTurnoverRoutingModule } from './clients-turnover-routing.module';
import { ClientsTurnoverComponent } from './clients-turnover.component';

@NgModule({
  imports: [
    CommonModule,

    MatIconModule,
    MatButtonModule,
    MatTableModule,
    CdkTableModule,
    MatSortModule,
    MatSelectModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatDatepickerModule,
    SatDatepickerModule,
    SatNativeDateModule,

    FuseSharedModule,
    TranslateModule,
    SharedModule,
    NgxLocalizedNumbers,

    ClientsTurnoverRoutingModule
  ],
  declarations: [ClientsTurnoverComponent],
  providers: [
    CuttingReportsService
  ]
})
export class ClientsTurnoverModule { }
