import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import { CuttingOrder, CuttingPaymentHistory } from 'cloud-shared-lib';
import { BehaviorSubject, Observable } from 'rxjs';

export interface CuttingOrderResponse {
  ordersDTO: CuttingOrder[];
  totalCount: number;
}

@Injectable()
export class CuttingPaymentListService {
  payments: CuttingPaymentHistory[];
  onPaymentsChanged: BehaviorSubject<any> = new BehaviorSubject({});
  paymentsTotalCount = 0;

  constructor(private http: HttpClient) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([this.getPayments(0, 10, 1)]).then(() => {
        resolve();
      }, reject);
    });
  }

  getPayments(pageIndex: number, pageSize: number, filterPayment: number): Promise<any> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filter', filterPayment.toString());

    return new Promise((resolve, reject) => {
      this.http
        .get(ApiLinks.cuttingPaymentsAll, { params: paramsHttp })
        .map((res: { totalCount: number; data: CuttingPaymentHistory[] }) => {
          this.paymentsTotalCount = res.totalCount;
          return res.data;
        })
        .subscribe((response: any) => {
          this.payments = response;
          this.onPaymentsChanged.next(this.payments);
          resolve(response);
        }, reject);
    });
  }

  getOrders(pageIndex: number, pageSize: number, filter: string): Promise<CuttingOrder[]> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filter', filter)
      // .set('statusId', statusId.join(','))
      .set('archived', 'false');

    return new Promise((resolve, reject) => {
      this.http
        .get(ApiLinks.cuttingOrder, { params: paramsHttp })
        .map((r: CuttingOrderResponse) => r.ordersDTO)
        .subscribe((response: CuttingOrder[]) => {
          resolve(response);
        }, reject);
    });
  }

  createPayment(item: CuttingPaymentHistory) {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingPayments, item).subscribe((response: CuttingPaymentHistory) => {
        // const paymentIndex = this.payments.findIndex(p => p.id === response.id);

        // if (paymentIndex !== -1) {
        //   this.payments[paymentIndex] = response;
        // }

        this.payments.unshift(response);
        this.onPaymentsChanged.next(this.payments);
        resolve(response);
      }, reject);
    });
  }

  deletePayment(item: CuttingPaymentHistory) {
    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.cuttingPayments + '/' + item.id).subscribe(() => {
        const paymentIndex = this.payments.findIndex(p => p.id === item.id);

        if (paymentIndex !== -1) {
          this.payments.splice(paymentIndex, 1);
          this.onPaymentsChanged.next(this.payments);
        }

        resolve();
      }, reject);
    });
  }
}
