import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuttingPaymentListComponent } from './payment-list.component';
import { CuttingPaymentListService } from './payment-list.service';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'cutting/payment-list',
    component: CuttingPaymentListComponent,
    resolve: {
      data: CuttingPaymentListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class CuttingPaymentListRoutingModule { }
