import { TestBed, inject } from '@angular/core/testing';
import { CuttingPaymentListService } from './payment-list.service';

describe('PaymentListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuttingPaymentListService]
    });
  });

  it('should be created', inject([CuttingPaymentListService], (service: CuttingPaymentListService) => {
    expect(service).toBeTruthy();
  }));
});
