export const locale = {
    lang: 'en',
    data: {
        'PAYMENTLIST': {
            'TITLE': 'История платежей',
            'CREATE_NEW': 'Добавить',
            'FILTER': {
                'TITLE': 'Фильтр',
                'ALL': 'Все',
                'PAID': 'Выполненные',
                'UNPAID': 'Невыполненные'
            },
            'TABLE': {
                'DATE': 'Дата',
                'MONEY': 'Сумма',
                'POINTS': 'Начислено баллов',
                'PAYMENTSYSTYPE': 'Платежная система',
                'PAYMENTSYSSTATUS': 'Статус',
                'ORDER': 'Заказ',
                'CLIENT': 'Клиент',
                'COMMENT': 'Комментарий',
            },
            'PAY_STATE': {
                'SUCCESS': 'Выполнен',
                'IN_PROGRESS': 'Не подтвержден'
            },
            'PAY_SYSTEMS': {
                '2': 'Счет на оплату',
                '0': 'Банковской картой',
                '1': 'Через PayPal',
                '3': 'Через Wooppay'
            },
            'SNACKBAR': {
                'PAYMENT_CREATED': 'Платеж создан',
                'PAYMENT_CREATE_FAIL': 'Не удалось создать платеж',
                'PAYMENT_DELETED': 'Платеж удален',
                'PAYMENT_DELETE_FAIL': 'Не удалось удалить платеж',
            }
        }
    }
};
