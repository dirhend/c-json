import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material';
import { FuseHighlightModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { NgxLocalizedNumbers } from 'ngx-localized-numbers';

import {
  CuttingPaymentDetailDialogComponent,
} from './cutting-payment-detail-dialog/cutting-payment-detail-dialog.component';
import { CuttingPaymentListRoutingModule } from './payment-list-routing.module';
import { CuttingPaymentListComponent } from './payment-list.component';
import { CuttingPaymentListService } from './payment-list.service';

@NgModule({
  imports: [
    MatIconModule,
    MatSelectModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    CdkTableModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatSortModule,
    FuseHighlightModule,
    MatDialogModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatAutocompleteModule,

    CommonModule,
    TranslateModule,
    FuseSharedModule,
    SharedModule,
    CuttingPaymentListRoutingModule,
    NgxLocalizedNumbers,
  ],
  declarations: [
    CuttingPaymentListComponent,
    CuttingPaymentDetailDialogComponent,
  ],
  providers: [CuttingPaymentListService],
  entryComponents: [CuttingPaymentDetailDialogComponent],
})
export class CuttingPaymentListModule { }
