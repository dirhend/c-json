export const locale = {
    lang: 'ru',
    data: {
        'CUTTING_PAYMENT_DETAIL_DIALOG': {
            'TITLE': 'Платеж',
            'OK': 'ОК',
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'DATE': 'Дата',
                'AMOUNT': 'Сумма',
                'ORDER': 'Заказ',
                'COMMENT': 'Комментарий',
                'DEFAULT_COMMENT': 'Наличные в кассу'
            }
        }
    }
};
