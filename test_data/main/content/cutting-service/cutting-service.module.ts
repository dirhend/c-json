import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ClientDetailModule } from './client-detail/client-detail.module';
import { ClientListModule } from './client-list/client-list.module';
import { CuttingOrderDetailModule } from './cutting-order-detail/cutting-order-detail.module';
import { CuttingOrderQuestionModule } from './cutting-order-question/cutting-order-question.module';
import { CuttingSettingsModule } from './cutting-settings/cutting-settings.module';
import { ManagerListModule } from './manager-list/manager-list.module';
import { OrderListModule } from './order-list/order-list.module';
import { CuttingReportsModule } from './reports/cutting-reports.module';
import { SmsMessageLogModule } from './sms-message-log/sms-message-log.module';

@NgModule({
  imports: [
    CommonModule,
    OrderListModule,
    ClientListModule,
    ClientDetailModule,
    CuttingSettingsModule,
    CuttingOrderDetailModule,
    ManagerListModule,
    SmsMessageLogModule,
    CuttingOrderQuestionModule,
    CuttingReportsModule
  ],
  declarations: []
})
export class CuttingServiceModule { }
