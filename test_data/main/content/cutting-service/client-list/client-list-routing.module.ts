import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';

import { ClientListComponent } from './client-list.component';
import { ClientListService } from './client-list.service';

const routes: Routes = [
  {
    path: 'cutting/client/list',
    component: ClientListComponent,
    resolve: {
      profile: ClientListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class ClientListRoutingModule { }
