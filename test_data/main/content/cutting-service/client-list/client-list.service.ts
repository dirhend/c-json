import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Headers, Http, Response, URLSearchParams } from '@angular/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import { LocalStorageManager } from '@cloud/main/shared/helpers/local-storage-manager.helper';
import { CuttingClient, CuttingClientIdentification } from 'cloud-shared-lib';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ClientListService {

  onCuttingClientsChanged = new BehaviorSubject<CuttingClient[]>(null);
  clientsTotalCount = 0;

  constructor(
    private httpClient: HttpClient,
    private http: Http,
  ) { }

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    return new Promise((resolve, reject) => {
      Promise.all([
        this.getClients(0, 10, '')
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getClients(pageIndex: number, pageSize: number, filter: string): Promise<CuttingClient[]> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filter', filter);

    return new Promise((resolve, reject) => {

      this.httpClient.get(ApiLinks.cuttingClient, { params: paramsHttp })
        .map((response: { clientsDTO: CuttingClient[], totalCount: number }) => {
          this.clientsTotalCount = response.totalCount;
          return response.clientsDTO;
        })
        .subscribe((response: CuttingClient[]) => {
          this.onCuttingClientsChanged.next(response);

          resolve(response);
        }, reject);
    });
  }

  goToClient(clientId: number) {

    return new Promise((resolve, reject) => {

      this.httpClient.post(ApiLinks.goToClient(clientId), undefined)
        .subscribe((response: CuttingClientIdentification) => {

          LocalStorageManager.cuttingClient = response;
          LocalStorageManager.cuttingClientRegData = LocalStorageManager.client.id;

          resolve(response);
        }, reject);
    });
  }

  goToTestClient() {
    const userId: number = LocalStorageManager.client.id;

    const paramsHttp: URLSearchParams = new URLSearchParams();
    paramsHttp.append('userId', userId.toString());

    const contentHeaders: Headers = new Headers();
    contentHeaders.append('Accept', 'application/json');
    contentHeaders.append('Content-Type', 'application/json');

    return new Promise((resolve, reject) => {

      this.http.post(ApiLinks.cuttingClientTest, undefined, { search: paramsHttp, headers: contentHeaders })
        .map((response: Response) => response.json())
        .subscribe((response: CuttingClientIdentification) => {

          LocalStorageManager.cuttingClient = response;
          LocalStorageManager.cuttingClientRegData = userId;

          resolve(response);
        }, reject);
    });
  }

  sendActivationMessageToEmail(clientId: number): Promise<any> {
    const url = ApiLinks.cuttingClient + '/' + clientId.toString() + '/send-activation-message';

    return new Promise((resolve, reject) => {
      this.httpClient.post(url, undefined)
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }

  deleteClient(clientId: number): any {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(`${ApiLinks.cuttingClient}/${clientId}`)
        .subscribe(() => {

          const clientIndex = this.onCuttingClientsChanged.value.findIndex(c => c.id === clientId);
          if (clientIndex !== -1) {
            this.onCuttingClientsChanged.value.splice(clientIndex, 1);
            this.onCuttingClientsChanged.next(this.onCuttingClientsChanged.value);
            this.clientsTotalCount--;
          }

          resolve();
        }, reject);
    });
  }

  manualActivateClient(clientId: number): any {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${ApiLinks.cuttingClient}/${clientId}/activate`, {})
        .subscribe(() => {
          const clientIndex = this.onCuttingClientsChanged.value.findIndex(c => c.id === clientId);
          if (clientIndex !== -1) {
            this.onCuttingClientsChanged.value[clientIndex].activated = true;
            this.onCuttingClientsChanged.next(this.onCuttingClientsChanged.value);
          }

          resolve();
        }, reject);
    });    
  }
}
