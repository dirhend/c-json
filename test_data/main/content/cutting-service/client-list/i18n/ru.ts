export const locale = {
  lang: 'ru',
  data: {
    CUTTINGCLIENTLIST: {
      TITLE: 'Клиенты',
      SEARCH: 'Поиск',
      GOTOTESTCLIENT: 'ПЕРЕЙТИ К ТЕСТОВОМУ КЛИЕНТУ',
      DELETECLIENTCONFIRM: 'Удалить клиента "{{ value }}"?',
      CREATE_CLIENT: 'СОЗДАТЬ',
      TABLE: {
        DATECREATION: 'Дата регистрации',
        FULLNAME: 'ФИО',
        COMPANY: 'Фирма',
        EMAIL: 'Email',
        PHONE: 'Телефон',
        TYPE: 'Тип',
        ACTIVATED: 'Активирован',
        REMARK: 'Примечание',
        MENU: {
          SEND_EMAIL_CONFIRM_MESSAGE: 'Повторить отправку письма для активации',
          CHECK_EMAIL: 'Проверить E-mail',
          DELETECLIENT: 'Удалить клиента',
          GO_TO_CLIENT_ACCOUNT: 'Перейти в аккаунт клиента',
          MANUAL_ACTIVATE: 'Активировать принудительно'
        }
      },
      SNACKBAR: {
        COPIEDTOCLIPBOARD: 'E-mail скопирован в буфер обмена',
        SENDACTIVATECOMPLETE: 'Сообщение отправлено',
        SENDACTIVATEERROR: 'Сообщение не отправлено',
        CLIENTDELETED: 'Клиент удален',
        CLIENTDELETEFAIL: 'Не удалось удалить клиента',
        CLIENTACTIVATED: 'Клиент активирован',
        CLIENTACTIVATEDFAIL: 'Не удалось активировать клиента'
      }
    }
  }
};
