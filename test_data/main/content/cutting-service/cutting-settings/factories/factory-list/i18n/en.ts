export const locale = {
    lang: 'en',
    data: {
        'FACTORY_LIST': {
            'TITLE': 'Список производств',
            'NEW': 'Добавить производство',
            'TABLE': {
                'IS_CURRENT': 'Осн.',
                'NAME': 'Название'
            },
            'SNACKBAR': {
                'FACTORY_ADDED': 'Производство успешно добавлено',     
                'FACTORY_EXIST': 'Производство с таким названием уже существует',
                'FACTORY_ADDFAIL': 'Не удалось добавить производство',
                'FACTORYEDITED': 'Информация по производству обновлена',
                'FACTORYDELETE': 'Удалить производство {{value}}?',
                'FACTORYDELETED': 'Производство удалено',
                'FACTORY_IS_CURRENT': 'Нельзя удалить основное производство'
                }
        }
    }
};
