export const locale = {
    lang: 'ru',
    data: {
        'FACTORY_CARD': {
            'TITLE': 'Информация о производстве',
            'OK': 'ОК',
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'NAME': 'Название',
                'ADDRESS': 'Адрес',
                'ERROR': {
                    'NAMEEMPTY': 'Название не задано',
                    'ADDRESSEMPTY': 'Адрес не задан',
                }
            }
        }
    }
};
