export const locale = {
  lang: 'en',
  data: {
    COMMONCUTTINGSETTINGS: {
      TITLE: 'Конфигурация раскроя',
      OVERHANG: 'Свес',
      BUTTTHICKNESS: 'Толщина облицовки',
      ALLOWANCE: {
        TITLE: 'Припуск',
        NONE: 'Без припуска',
        BYBUTTTHICKNESS: 'По толщине облицовки',
        MANUALVALUE: 'Значение',
        MANUALVALUEIF: 'если толщина облицовки больше или равна'
      },
      ORDER: {
        TITLE: 'Заказ',
        PREFIX: 'Префикс',
        SHOWCUTTINGMAPS: 'Отображать карты раскроя',
        DEFAULT_MANAGER: 'Менеджер по умолчанию',
        DEFAULT_MANAGER_DONT_SET: 'Не задан',
        DEFAULT_MANAGER_CHANGE: 'Изменить',
        DEFAULT_MANAGER_REMOVE: 'Удалить'
      },
      CLIENTREGISTER: {
        TITLE: 'Регистрация клиента',
        USEPHONENUMBERWHENREGISTERING: 'Вводить номер телефона при регистрации',
        CONFIRM: {
          TITLE: 'Подтверждение',
          USENONE: 'Нет',
          USEEMAIL: 'По E-mail',
          USEPHONE: 'По номеру телефона'
        }
      },
      QUESTIONS: {
        TITLE: 'Обращения',
        NOTIFICATION: 'Отправлять уведомление о новом обращении на email каждые',
        NOTIFICATIONNONE: 'Нет',
        IMMEDIATELY: 'Сразу',
        NOTIFICATIONMINUTE: 'минут',
        NOTIFICATIONHOUR: 'час',
        NOTIFICATIONHOUR2: 'часа'
      },
      PANEL_SIZE: {
        TITLE: 'Размер панелей',
        MIN_HEIGHT: 'Минимальная длина',
        MIN_WIDTH: 'Минимальная ширина'
      }
    }
  }
};
