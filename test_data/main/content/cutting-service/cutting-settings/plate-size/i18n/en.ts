export const locale = {
    lang: 'en',
    data: {
        'PLATESIZE': {
            'ADDPLATE': 'Добавить формат',
            'USEMATERIALBASESIZES': {
                'TITLE': 'Использовать размеры плиты из базы материалов',
                'WARNING': `При выборе этой опции, у пользователя не будет возможности выбрать формат плиты. При отсутствии формата плиты в базе материалов, 
                будет использоваться размер по умолчанию из справочника`
            },
            'PLATETYPE': {
                'TITLE': 'Тип материала',
                'PLATE': 'Плитный',
                'LINEAR': 'Погонный',
            },
            'TABLE': {
                'HEIGHT': 'Длина',
                'WIDTH': 'Ширина',
                'DEFAULT': 'По умолчанию'
            },
            'SNACKBAR': {
                'PLATESIZEDELETED': 'Формат плиты удален',
                'PLATESIZEDELETEFAIL': 'Не удалось удалить формат плиты',
                'PLATESIZEADDED': 'Формат плиты добавлен',
                'PLATESIZEADDFAIL': 'Не удалось добавить формат плиты',
                'PLATESIZEEDITED': '',
                'PLATESIZEEDITFAIL': 'Не удалось применить изменения',
                'DONTDELETEBYDEFAULTSIZE': 'Невозможно удалить формат По умолчанию',
                'DONTDELETELASTSIZE': 'Должен остаться хотя бы один формат'
            }
        }
    }
};
