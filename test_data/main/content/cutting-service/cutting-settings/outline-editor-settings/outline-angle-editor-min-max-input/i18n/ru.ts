export const locale = {
    lang: 'ru',
    data: {
        'MINMAXFORM': {
            'MAX': 'Макс.',
            'MIN': 'Мин.',
            'ERROR' : {
                'EMPTY': 'Поле не должно быть пустым',
                'MINGREATERMAX': 'Минимум больше максимума',
                'OUTOFRANGE': 'Значение должно находиться в диапазоне от 0 до 10000'
            }
        }
    }
};
