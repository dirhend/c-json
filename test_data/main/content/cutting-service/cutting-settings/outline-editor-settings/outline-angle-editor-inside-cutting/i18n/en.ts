export const locale = {
    lang: 'en',
    data: {
        'INSIDECUTFORM': {
            'MAX': 'Макс. для XY',
            'MIN': 'Мин. для XY',
            'RADIUSMIN': 'Мин. радиус',
            'RADIUSMAX': 'Макс. радиус',
            'ERROR' : {
                'EMPTY': 'Поле не должно быть пустым',
                'MINGREATERMAX': 'Минимум больше максимума',
                'OUTOFRANGE': '≥{{ min }}, ≤{{ max }}'
            }
        }
    }
};
