export const locale = {
    lang: 'ru',
    data: {
        'OUTLINEEDITORSETTINGS': {
            'ENABLED': 'Отображать настройку у клиента',
            'ROUNDING': 'Наружный радиус',
            'INSIDEROUNDING': 'Внутренний радиус',
            'CUT': 'Спил',
            'INSIDECUT': 'Г-образный вырез',
            'DIAGONALCUT': 'Диагональ',
            'CHAMFER': 'Фаска'
        }
    }
};
