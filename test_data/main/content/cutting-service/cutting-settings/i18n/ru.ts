export const locale = {
  lang: 'ru',
  data: {
    CUTTINGSETTINGS: {
      TITLE: 'Настройки',
      IFRAMECODE: {
        TITLE: 'Код для вставки на страницу'
      },
      TABS: {
        COMMON: 'Общие',
        PLATESIZES: 'Форматы плит',
        CUTTINGORDERSTATUSSETTINGS: 'Статусы заказа',
        CUTTINGORDERAMOUNTSETTINGS: 'Оплата заказа',
        GROOVESETTINGS: 'Пазы',
        SERVICESINTEDTATION: 'Сторонние сервисы',
        OUTLINEEDITORSETTINGS: 'Обработка углов',
        PREPAREFORFACTORY: 'Подготовка к производству',
        FASTENERSSETTINGS: 'Присадка',
        CLIENT_GROUPS: 'Категории скидок'
      },
      SNACKBAR: {
        CUTSETTINGSLOADED: 'Конфигурация раскроя загружена',
        CUTSETTINGSLOADFAIL: 'Не удалось загрузить конфигурацию раскроя'
      }
    }
  }
};
