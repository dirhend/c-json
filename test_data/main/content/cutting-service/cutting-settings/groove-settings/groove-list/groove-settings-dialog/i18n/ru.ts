export const locale = {
    lang: 'ru',
    data: {
        'GROOVESETTINGSDIALOG': {
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'ANGLE': 'Угол (Fi)',
                'DEPTH': 'Глубина (G)',
                'DESIGNATION': 'Обозначение',
                'ERROR': {
                    'DEPTHEMPTY': 'Поле Глубина не задано',
                    'DEPTHMAX': 'Максимальное значение: {{ max }}',
                    'DEPTHMIN': 'Минимальное значение: {{ min }}',
                    'DESIGNATIONEMPTY': 'Поле Обозначение не задано',
                    'DESIGNATIONMAX': 'Максимальная длина обозначения - {{ max }}',
                    'NAMEEMPTY': 'Поле Название не задано',
                    'NAMEMAX': 'Максимальная длина названия - {{ max }}',
                    'SHIFTEMPTY': 'Поле Смещение не задано',
                    'SHIFTMAX': 'Максимальное значение: {{ max }}',
                    'SHIFTMIN': 'Минимальное значение: {{ min }}',
                    'WIDTHEMPTY': 'Поле Ширина не задано',
                    'WIDTHMAX': 'Максимальное значение: {{ max }}',
                    'WIDTHMIN': 'Минимальное значение: {{ min }}',
                    'ANGLEEMPTY': 'Поле Угол не задано',
                    'ANGLEMAX': 'Максимальное значение: {{ max }}',
                    'ANGLEMIN': 'Минимальное значение: {{ min }}'
                },
                'NAME': 'Название',
                'SHIFT': 'Смещение (dx)',
                'WIDTH': 'Ширина (H)'
            },
            'OK': 'ОК',
            'TITLE': 'Новый паз'
        }
    }
};
