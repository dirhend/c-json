export const locale = {
    lang: 'en',
    data: {
        'GROOVELIST': {
            'ADDGROOVE': 'Добаить паз',
            'TABLE': {
                'NAME': 'Название',
                'SIGN': 'Обозначение',
                'WIDTH': 'Ширина (H)',
                'DEPTH': 'Глубина (G)',
                'SHIFT': 'Смещение (dx)',
                'ANGLE': 'Угол (Fi)'
            },
            'SNACKBAR': {
                'GROOVEDELETED': 'Паз удален',
                'GROOVEDELETEFAIL': 'Не удалось удалить паз',
                'GROOVEADDED': 'Паз добавлен',
                'GROOVEADDFAIL': 'Не удалось добавить паз'
            }
        }
    }
};
