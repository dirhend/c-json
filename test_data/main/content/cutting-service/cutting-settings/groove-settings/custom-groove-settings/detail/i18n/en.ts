export const locale = {
    lang: 'en',
    data: {
        'CUSTOMGROOVEDETAIL': {
            'DESIGNATION': 'Обозначение',
            'NAME': 'Название',
            'DESIGNATIONINFO': '"%h%", "%g%", "%dx%", "%fi%" будут заменены значениями соответствующих параметров',
            'DELETESCHEMA': 'Удалить шаблон',
            'TABLE': {
                'ANGLE': 'Угол (Fi)',
                'DEPTH': 'Глубина (G)',
                'MAXPARAMS': 'Макс.',
                'MINPARAMS': 'Мин.',
                'SHIFT': 'Смещение (dx)',
                'WIDTH': 'Ширина (H)'
            },
            'SNACKBAR': {
                'SCHEMADEELTED': 'Шаблон удален',
                'SCHEMADEELTEFAIL': 'Не удалось удалить шаблон'
            }
        }
    }
};
