export const locale = {
  lang: 'en',
  data: {
    CUTTING_CLIENT_GROUP_DETAIL_DIALOG: {
      TITLE: 'Настройки категории',
      EDIT_CLIENT_LIST: 'Добавить клиентов',
      FORM: {
        NAME: 'Название',
        MATERIAL_DISCOUNT_PERCENTAGE: 'Скидка на материалы',
        OPERATION_DISCOUNT_PERCENTAGE: 'Скидка на операции',
        ERROR: {
          MAX_LENGTH: 'Максимальная длинна - {{ maxlength }}',
          EMPTY: 'Поле не должно быть пустым',
          RANGE: 'Число должно находиться в диапазоне от {{ min }} до {{ max }}'
        }
      },
      OK: 'ОК',
      CANCEL: 'ОТМЕНА'
    }
  }
};
