export const locale = {
  lang: 'ru',
  data: {
    CUTTING_CLIENT_GROUPS: {
      ADD_GROUP: 'Добавить категорию',
      TABLE: {
        NAME: 'Название',
        MATERIAL_DISCOUNT: 'Скидка на материалы',
        OPERATION_DISCOUNT: 'Скидка на операции'
      },
      DELETE_CONFIRM: 'Удалить категорию "{{ name }}"?',
      SNACKBAR: {
        GROUP_ADDED: 'Категория добавлена',
        GROUP_ADD_FAIL: 'Не удалось добавить категорию',
        GROUP_CHANGED: 'Изменения сохранены',
        GROUP_CHANGE_FAIL: 'Не удалось сохранить изменения',
        DELETED: 'Категория удалена',
        DELETE_FAIL: 'Не удалось удалить категорию',
        GROUP_HAS_CLIENTS: 'Невозможно удалить категорию т. к. она привязана к клиентам'
      }
    }
  }
};
