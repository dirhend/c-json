export const locale = {
  lang: 'ru',
  data: {
    CUTTING_CLIENT_GROUP_LIST: {
      TITLE: 'Клиенты',
      SEARCH: 'Поиск',
      TABLE: {
        SURNAME: 'Фамилия',
        NAME: 'Имя',
        PATRONYMIC: 'Отчество'
      },
      OK: 'ОК',
      CANCEL: 'ОТМЕНА'
    }
  }
};
