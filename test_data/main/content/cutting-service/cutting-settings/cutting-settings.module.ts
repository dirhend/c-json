import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatPaginatorModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSlideToggleModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import {
  AddCuttingOrderStatusDialogComponent,
} from '@cloud/main/content/cutting-service/cutting-settings/cutting-order-status/add-cutting-order-status-dialog/add-cutting-order-status-dialog.component';
import { CuttingOrderStatusActionPipe } from '@cloud/main/shared/pipes/cutting-order-status-action.pipe';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { FuseHighlightModule, FuseMaterialColorPickerModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { TreeModule } from 'angular-tree-component';
import { CloudSharedLibModule } from 'cloud-shared-lib';
import { CKEditorModule } from 'ngx-ckeditor';
import { CustomFormsModule } from 'ngx-custom-validators';
import { NgxLocalizedNumbers } from 'ngx-localized-numbers';

import { environment } from '../../../../../environments/environment';
import { CommonSettingsComponent } from './common-settings/common-settings.component';
import {
  CCGroupDetailDialogComponent,
} from './cutting-client-groups-settings/cc-group-detail-dialog/cc-group-detail-dialog.component';
import {
  CcGroupEditClientListDialogComponent,
} from './cutting-client-groups-settings/cc-group-edit-client-list-dialog/cc-group-edit-client-list-dialog.component';
import {
  CuttingClientGroupsSettingsComponent,
} from './cutting-client-groups-settings/cutting-client-groups-settings.component';
import { CuttingOrderAmountComponent } from './cutting-order-amount/cutting-order-amount.component';
import { CuttingOrderStatusComponent } from './cutting-order-status/cutting-order-status.component';
import {
  StatusEmailEditDialogComponent,
} from './cutting-order-status/status-email-edit-dialog/status-email-edit-dialog.component';
import {
  StatusSmsEditDialogComponent,
} from './cutting-order-status/status-sms-edit-dialog/status-sms-edit-dialog.component';
import { CuttingSettingsRoutingModule } from './cutting-settings-routing.module';
import { CuttingSettingsComponent } from './cutting-settings.component';
import { CuttingSettingsService } from './cutting-settings.service';
import { CuttingFactoryDetailDialogComponent } from './factories/factory-detail-dialog/factory-detail-dialog.component';
import { CuttingFactoryListComponent } from './factories/factory-list/factory-list.component';
import {
  CustomFastenersSettingsComponent,
} from './fasteners-settings/custom-fasteners-settings/custom-fasteners-settings.component';
import {
  CustomHoleSchemaDetailDialogComponent,
} from './fasteners-settings/custom-fasteners-settings/custom-hole-schema-detail-dialog/custom-hole-schema-detail-dialog.component';
import {
  FastenersSetDetailDialogComponent,
} from './fasteners-settings/fasteners-sets-settings/detail-dialog/fasteners-set-detail-dialog.component';
import {
  FileUploadComponent,
} from './fasteners-settings/fasteners-sets-settings/detail-dialog/file-upload/file-upload.component';
import {
  CustomHoleDetailDialogComponent,
} from './fasteners-settings/fasteners-sets-settings/detail-dialog/holes/detail-dialog/custom-hole-detail-dialog.component';
import {
  FastenersSetHolesComponent,
} from './fasteners-settings/fasteners-sets-settings/detail-dialog/holes/fasteners-set-holes.component';
import {
  FastenersSetsSettingsComponent,
} from './fasteners-settings/fasteners-sets-settings/fasteners-sets-settings.component';
import {
  StandardFastenersSetsDialogComponent,
} from './fasteners-settings/fasteners-sets-settings/standard-fasteners-sets-dialog/standard-fasteners-sets-dialog.component';
import { FastenersSettingsComponent } from './fasteners-settings/fasteners-settings.component';
import { CustomGrooveSettingsComponent } from './groove-settings/custom-groove-settings/custom-groove-settings.component';
import { CustomGrooveDetailComponent } from './groove-settings/custom-groove-settings/detail/custom-groove-detail.component';
import {
  CustomGrooveSchemaInputValueRangeComponent,
} from './groove-settings/custom-groove-settings/detail/schema-input-value-range/custom-groove-schema-input-value-range.component';
import { GrooveListComponent } from './groove-settings/groove-list/groove-list.component';
import {
  GrooveSettingsDialogComponent,
} from './groove-settings/groove-list/groove-settings-dialog/groove-settings-dialog.component';
import { GrooveSettingsComponent } from './groove-settings/groove-settings.component';
import {
  OutlineAngleEditorInsideCuttingComponent,
} from './outline-editor-settings/outline-angle-editor-inside-cutting/outline-angle-editor-inside-cutting.component';
import {
  OutlineAngleEditorMinMaxInputComponent,
} from './outline-editor-settings/outline-angle-editor-min-max-input/outline-angle-editor-min-max-input.component';
import { OutlineEditorSettingsComponent } from './outline-editor-settings/outline-editor-settings.component';
import { PlateSizeComponent } from './plate-size/plate-size.component';
import { PrepareForFactoryComponent } from './prepare-for-factory/prepare-for-factory.component';
import { ServicesIntegrationComponent } from './services-integration/services-integration.component';
import { SmsAgentComponent } from './services-integration/sms-agent/sms-agent.component';
import { CuttingButtListComponent } from './sheet-and-butt-material-dialog/cutting-butt-list/cutting-butt-list.component';
import {
  SelectButtDialogComponent,
} from './sheet-and-butt-material-dialog/cutting-butt-list/select-butt-dialog/select-butt-dialog.component';
import {
  CuttingMaterialListComponent,
} from './sheet-and-butt-material-dialog/cutting-material-list/cutting-material-list.component';
import {
  SheetAndButtMaterialDialogComponent,
} from './sheet-and-butt-material-dialog/sheet-and-butt-material-dialog.component';
import { UploadedFileSettingsComponent } from './uploaded-file-settings/uploaded-file-settings.component';

@NgModule({
  imports: [
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatListModule,
    MatDialogModule,
    MatToolbarModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    CdkTableModule,
    MatRippleModule,
    MatTabsModule,
    MatSelectModule,
    MatCheckboxModule,
    MatCardModule,
    MatBadgeModule,
    MatAutocompleteModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatExpansionModule,
    MatSidenavModule,

    TreeModule,
    NgxDnDModule,
    CloudSharedLibModule.forRoot(environment),
    FormsModule,
    CustomFormsModule,

    CommonModule,
    SharedModule,
    FuseSharedModule,
    TranslateModule,
    CuttingSettingsRoutingModule,
    FuseHighlightModule,
    CKEditorModule,
    NgxLocalizedNumbers,
    FuseMaterialColorPickerModule
  ],
  declarations: [
    CuttingSettingsComponent,
    CuttingMaterialListComponent,
    CuttingButtListComponent,
    SelectButtDialogComponent,
    PlateSizeComponent,
    CuttingOrderStatusComponent,
    CuttingOrderStatusActionPipe,
    CommonSettingsComponent,
    AddCuttingOrderStatusDialogComponent,
    CuttingOrderAmountComponent,
    GrooveSettingsComponent,
    GrooveSettingsDialogComponent,
    CustomGrooveSettingsComponent,
    CustomGrooveDetailComponent,
    CustomGrooveSchemaInputValueRangeComponent,
    GrooveListComponent,
    StatusEmailEditDialogComponent,
    StatusSmsEditDialogComponent,
    ServicesIntegrationComponent,
    SmsAgentComponent,
    OutlineEditorSettingsComponent,
    OutlineAngleEditorMinMaxInputComponent,
    OutlineAngleEditorInsideCuttingComponent,
    PrepareForFactoryComponent,
    UploadedFileSettingsComponent,
    CuttingFactoryListComponent,
    CuttingFactoryDetailDialogComponent,
    SheetAndButtMaterialDialogComponent,
    FastenersSettingsComponent,
    CustomFastenersSettingsComponent,
    CustomHoleSchemaDetailDialogComponent,
    FastenersSetsSettingsComponent,
    FastenersSetDetailDialogComponent,
    FileUploadComponent,
    FastenersSetHolesComponent,
    CustomHoleDetailDialogComponent,
    StandardFastenersSetsDialogComponent,
    CuttingClientGroupsSettingsComponent,
    CCGroupDetailDialogComponent,
    CcGroupEditClientListDialogComponent
  ],
  providers: [CuttingSettingsService],
  entryComponents: [
    SelectButtDialogComponent,
    AddCuttingOrderStatusDialogComponent,
    GrooveSettingsDialogComponent,
    StatusEmailEditDialogComponent,
    StatusSmsEditDialogComponent,
    CuttingFactoryDetailDialogComponent,
    SheetAndButtMaterialDialogComponent,
    CustomHoleSchemaDetailDialogComponent,
    FastenersSetDetailDialogComponent,
    CustomHoleDetailDialogComponent,
    StandardFastenersSetsDialogComponent,
    CCGroupDetailDialogComponent,
    CcGroupEditClientListDialogComponent
  ]
})
export class CuttingSettingsModule {}
