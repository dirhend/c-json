export const locale = {
    lang: 'en',
    data: {
        'SELECTMATCHEDBUTTDIALOG': {
            'TITLE': 'Облицовка',
            'OK': 'ОК',
            'SEARCH': 'Поиск',
            'CANCEL': 'ОТМЕНА',
            'TABLE': {
                'ARTICLE': 'Артикул',
                'NAME': 'Название',
                'THICKNESS': 'Толщина'
            }
        }
    }
};
