export const locale = {
    lang: 'ru',
    data: {
        'CUTTINGBUTTLIST': {
            'TABLE': {
                'ARTICLE': 'Артикул',
                'NAME': 'Название',
                'THICKNESS': 'Толщина'
            },
            'SNACKBAR': {
                'BUTTADDED': 'Облицовка добавлена',
                'BUTTTHICKNESSDUPLICATE': 'Облицовка с такой толщиной уже добавлена',
                'BUTTADDFAIL': 'Не удалось добавить облицовку',
                'BUTTDELETED': 'Облицовка удалена',
                'BUTTDELETEFAIL': 'Не удалось удалить облицовку'
            }
        }
    }
};
