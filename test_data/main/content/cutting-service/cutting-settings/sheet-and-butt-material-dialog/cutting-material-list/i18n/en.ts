export const locale = {
    lang: 'en',
    data: {
        'CUTTINGMATERIALLIST': {
            'ADDMATCHEDBUTTBTN': 'Добавить облицовку',
            'TABLE': {
                'ARTICLE': 'Артикул',
                'NAME': 'Название'
            }
        }
    }
};
