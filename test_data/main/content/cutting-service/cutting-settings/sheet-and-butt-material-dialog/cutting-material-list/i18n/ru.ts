export const locale = {
    lang: 'ru',
    data: {
        'CUTTINGMATERIALLIST': {
            'ADDMATCHEDBUTTBTN': 'Добавить облицовку',
            'TABLE': {
                'ARTICLE': 'Артикул',
                'NAME': 'Название'
            }
        }
    }
};
