import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { CuttingSettingsComponent } from './cutting-settings.component';
import { CuttingSettingsService } from './cutting-settings.service';

const routes: Routes = [
  {
    path: 'cutting/settings',
    component: CuttingSettingsComponent,
    resolve: {
      profile: CuttingSettingsService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class CuttingSettingsRoutingModule { }
