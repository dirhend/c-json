export const locale = {
    lang: 'ru',
    data: {
        'FASTENERSSETTINGS': {
            'FASTENERS_SETS': {
                'TITLE': 'Группы отверстий',
                'ADD': 'Добавить',
                'ADD_FROM_STANDARD': 'Добавить из стандартных',
            },
            'CUSTOM': {
                'TITLE': 'Шаблоны отверстий для ручного добавления',
            }
        }
    }
};
