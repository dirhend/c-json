export const locale = {
    lang: 'en',
    data: {
        'CUSTOM_HOLE_DETAIL_DIALOG': {
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'MAX_DEPTH_WARN': `Убедитесь, что минимальная глубина не может быть больше толщины материала.`,
                'DIAMETER': 'Диаметр',
                'ERROR': {
                    'EMPTY': 'Поле не должно быть пустым',
                    'MAX': 'Максимальное значение - {{ max }}',
                    'MAX_LENGTH': 'Максимальная длина {{ max }}',
                    'MIN': 'Минимальное значение - {{ min }}',
                    'MIN_GREATER_MAX': 'Минимальное значение больше максимального',
                },
                'MAX_DEPTH': 'Максимальная глубина',
                'MIN_DEPTH': 'Минимальная глубина',
                'NAME': 'Название',
                'THROUGH': 'Сквозное',
                'EDGE_HOLE_BY_CENTER': 'По центру',
                'EDGE_HOLE_OFFSET': 'Отступ от лицевой стороны',
                'TYPE': {
                    'PLATE': 'Пласть',
                    'EDGE': 'Торец',
                    'TITLE': 'Расположение'
                }
            },
            'OK': 'ОК',
            'TITLE': 'Отверстие',
            'SNACKBAR': {
                'CHANGES_SAVED': 'Изменения сохранены',
                'CHANGES_SAVE_FAIL': 'Не удалось сохранить изменения'
            }
        }
    }
};
