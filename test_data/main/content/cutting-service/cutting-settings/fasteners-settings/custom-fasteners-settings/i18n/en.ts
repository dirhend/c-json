export const locale = {
    lang: 'en',
    data: {
        'CUSTOM_FASTENERS_SETTINGS': {
            'TABLE': {
                'NAME': 'Название',
                'DIAMETER': 'Диаметр',
                'MINDEPTH': 'Минимальная глубина',
                'MAXDEPTH': 'Максимальная глубина',
                'TYPE': {
                    '1': 'Торец',
                    '0': 'Пласть',
                    'TITLE': 'Расположение'
                }
            }
        }
    }
};
