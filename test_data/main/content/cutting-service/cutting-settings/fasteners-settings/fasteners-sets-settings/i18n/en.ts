export const locale = {
  lang: 'en',
  data: {
    FASTENERS_SET: {
      TABLE: {
        NAME: 'Название',
        TYPE: {
          1: 'Относительно торца',
          0: 'По пласти',
          2: 'По пласти относительно торца',
          TITLE: 'Позиционирование'
        }
      },
      SET_DELETE: 'Удалить {{value}}?'
    }
  }
};
