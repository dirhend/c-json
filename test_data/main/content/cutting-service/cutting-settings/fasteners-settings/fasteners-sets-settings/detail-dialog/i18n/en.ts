export const locale = {
  lang: 'en',
  data: {
    FASTENERS_SET_DETAIL: {
      TITLE: 'Группа отверстий',
      OK: 'ОК',
      CANCEL: 'ОТМЕНА',
      HOLES: 'Отверстия',
      FORM: {
        NAME: 'Наименование',
        ICON: 'Изображение группы',
        TYPE: {
          TITLE: 'Позиционирование',
          0: 'По пласти',
          1: 'По торцу',
          2: 'По пласти относительно торца',
          DELETE_EDGE_HOLES: 'Удалите торцовые отверстия'
        },
        FILE_WITHOUT_CHANGES: 'Без изменений',
        ERROR: {
          INVALID_EXT: 'Можно загрузить только',
          MAX_LENGTH: 'Максимальная длина - {{ value }}',
          EMPTY: 'Заполните поле'
        }
      },
      SNACKBAR: {
        SET_CREATED: 'Группа отверстий добавлена',
        SET_CREATE_FAIL: 'Не удалось добавить группу отверстий',
        SET_SAVED: 'Изменения сохранены',
        SET_SAVE_FAIL: 'Не удалось сохранить изменения'
      }
    }
  }
};
