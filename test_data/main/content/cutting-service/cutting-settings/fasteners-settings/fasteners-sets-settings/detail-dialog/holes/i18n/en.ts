export const locale = {
  lang: 'en',
  data: {
    FASTENERS_SET_HOLES: {
      ADD_HOLE: 'Добавить отверстие',
      TABLE: {
        X: 'X',
        Y: 'Y',
        Z: 'Z',
        DIAMETER: 'Диаметр',
        DEPTH: 'Глубина',
        TYPE: {
          2: 'Торец',
          1: 'Обратная сторона',
          0: 'Лицевая сторона',
          TITLE: 'Расположение'
        }
      }
    }
  }
};
