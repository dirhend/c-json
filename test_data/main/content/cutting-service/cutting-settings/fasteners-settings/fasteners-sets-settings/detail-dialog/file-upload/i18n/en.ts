export const locale = {
    lang: 'en',
    data: {
        'FILE_UPLOAD': {
            'CHOOSE_FILE': 'Выберите файл',
            'OR_DARAG_AND_DROP': ' или перетащите файл сюда',
        }
    }
};
