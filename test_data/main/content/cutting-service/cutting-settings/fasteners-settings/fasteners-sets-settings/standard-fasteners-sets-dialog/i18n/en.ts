export const locale = {
    lang: 'en',
    data: {
        'STANDARD_FASTENERS_SET_SCHEMAS': {
            'TITLE': 'Стандартные группы отверстий',
            'OK': 'ДОБАВИТЬ',
            'CANCEL': 'ОТМЕНА',
            'SNACKBAR': {
                'FASTENERS_SET_ADDED': 'Группа отверстий доббавлена',
                'FASTENERS_SET_ADDE_FAIL': 'Не удалось добавить группу отверстий',
            }
        }
    }
};
