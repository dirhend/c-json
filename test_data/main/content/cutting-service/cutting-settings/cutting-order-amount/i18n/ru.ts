export const locale = {
  lang: 'ru',
  data: {
    CUTTINGORDERAMOUNT: {
      ROUNDINGCONSIDER: {
        TITLE: 'При расчете стоимости заказа учитывать',
        MATERIALCOUNT: 'Способ округления "Количество материала"',
        SCOPEOFWORK: 'Способ округления "Объем работ"',
        ADD_FASTENERS_TO_ESTIMATE: 'Наименование присадки в таблице материалов'
      },
      INVIOCENUMBER: {
        TITLE: 'Номер счета',
        NEXTNUMBER: 'Следующий номер',
        PREFIX: 'Префикс',
        POSTFIX: 'Постфикс'
      },
      INVIOCEINFO: {
        ONLYMAINWORK: 'Отображать только услугу раскроя',
        ALLWORKS: 'Отображать полную спецфикацию',
        TITLE: 'Настройки счета',
        NAMEMAINWORK: 'Название услуги раскроя в спецификации',
        PAYREQUISITTITLE: 'Заголовок платежных реквизитов'
      },
      PAYSYSTEM: {
        TITLE: 'Платежные системы',
        INFO: 'Для подключения создайте обращение'
      },
      SPECIFICATIONINFO: {
        TITLE: 'Настройки спецификации',
        HIDENULLCOST: 'Скрывать позиции с 0 стоимостью',
        SINGLE_ESTIMATE_LIST: 'Отображать материалы и работы одним списком'
      }
    }
  }
};
