export const locale = {
    lang: 'ru',
    data: {
        'UPLOADEDFILES': {
            'HISTORY': 'История',
            'MATERIALBASE': {
                'TITLE': 'База материалов',
                'NOLOADED': 'Не загружена',
                'HOWLOAD': 'Как загрузить?',
                'SHEETANDBUTTMATERAIL': 'Таблица СЛКМ'
            },
            'TEXTURES': {
                'TITLE': 'Текстуры для базы материалов',
                'NOLOADED': 'Не загружены'
            },
            'CUTSETTINGS': {
                'TITLE': 'Конфигурация раскроя',
                'LOADSETTINGS': 'Загрузить',
                'NOLOADED': 'Не загружена'
            },
            'DATESYNC': 'Дата загрузки'
        }
    }
};
