import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import {
  CuttingButt,
  CuttingClient,
  CuttingClientGroup,
  CuttingCustomGrooveSchema,
  CuttingCustomHoleSchema,
  CuttingFactory,
  CuttingFastenersSet,
  CuttingFileSettingsSyncDate,
  CuttingManualSettings,
  CuttingMatBaseGroup,
  CuttingMaterial,
  CuttingMessageSchema,
  CuttingOrderInvoiceNumber,
  CuttingOrderStatus,
  CuttingPanelGroove,
  CuttingPlateSize,
  MaterialBaseSyncHistory,
  MessageTypeEnum,
  TypeGroupMaterial,
  UserIdentification,
  UserOptions
} from 'cloud-shared-lib';
import * as _ from 'lodash';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

export interface MatBaseSyncHistResponse {
  totalCount: number;
  historyDTO: MaterialBaseSyncHistory[];
}

export interface CuttingMaterialResponse {
  materialsDTO: CuttingMaterial[];
  materialsCount: number;
  buttCountList: number[];
}

export interface CuttingButtResponse {
  totalCount: number;
  buttsDTO: CuttingButt[];
}

export interface CuttingFactoryResponse {
  factoriesDto: CuttingFactory[];
  defaultId: number;
}

export enum PlateSizeDeleteError {
  deleteLastSize,
  deleteDefaultSize,
  invalidId
}

@Injectable()
export class CuttingSettingsService {
  matchingButtSelectedMatGroupNodeId = 0;
  matchingButtSelectedMaterialId = 0;

  onManualSettingsChanged: BehaviorSubject<any> = new BehaviorSubject({});
  onPlateSizeListChanged: BehaviorSubject<any[]> = new BehaviorSubject([]);
  onFilesSyncDateChanged: BehaviorSubject<any> = new BehaviorSubject({});
  onCuttingOrderStatusListChanged: BehaviorSubject<CuttingOrderStatus[]> = new BehaviorSubject([]);
  onUserOptionsChanged: BehaviorSubject<UserOptions> = new BehaviorSubject(new UserOptions());
  onGroovesChanged: BehaviorSubject<CuttingPanelGroove[]> = new BehaviorSubject([]);
  onCustomGroovesSchemasChanged: BehaviorSubject<CuttingCustomGrooveSchema[]> = new BehaviorSubject([]);
  onInvoiceNumberSettingsChanged: BehaviorSubject<CuttingOrderInvoiceNumber> = new BehaviorSubject(new CuttingOrderInvoiceNumber());
  onFactoriesChanged: BehaviorSubject<CuttingFactoryResponse> = new BehaviorSubject(null);
  onCustomHoleChanged: BehaviorSubject<CuttingCustomHoleSchema[]> = new BehaviorSubject([]);
  onFastenersSetChanged: BehaviorSubject<CuttingFastenersSet[]> = new BehaviorSubject([]);

  constructor(private http: HttpClient) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getManualSettings(),
        this.getFilesSyncDate(),
        this.getPlateSizes(),
        this.getCuttingOrderStatusList(),
        this.getUserOptions(),
        this.getInvoiceNumberSettings(),
        this.getGrooves(),
        this.getCustomGroovesSchemas(),
        this.getFactoryList(),
        Promise.all([this.getCustomHoleScemas(), this.getFastenersSet()])
      ]).then(() => {
        resolve();
      }, reject);
    });
  }

  uploadCuttingSettings(file: File) {
    const input = new FormData();
    input.append('settingsFile', file);

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingSettingsFile, input).subscribe((response: Date) => {
        this.onFilesSyncDateChanged.value.cuttingSettings = response;

        this.onFilesSyncDateChanged.next(this.onFilesSyncDateChanged.value);

        resolve();
      }, reject);
    });
  }

  saveManualSettings(settings: CuttingManualSettings): Promise<any> {
    const overhung = {};
    if (settings.overhang.length > 0) {
      settings.overhang.forEach(o => {
        const key = o[0];
        overhung[o[0].toString()] = o[1];
      });
    }

    const settingsCopy: any = { ...settings };
    settingsCopy.overhang = overhung;

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingManualSettings, settingsCopy).subscribe(() => {
        resolve();
      }, reject);
    });
  }

  getManualSettings(): Promise<CuttingManualSettings> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingManualSettings).subscribe((settings: CuttingManualSettings) => {
        // crutch for dinamic eterable (Dictionary)
        const overhangs: any[] = [];
        Object.keys(settings.overhang).forEach(k => overhangs.push([k, settings.overhang[k]]));
        settings.overhang = overhangs;

        this.onManualSettingsChanged.next(settings);

        resolve(settings);
      }, reject);
    });
  }

  getFilesSyncDate(): Promise<CuttingFileSettingsSyncDate> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingSettingFilesSyncDate).subscribe((response: CuttingFileSettingsSyncDate) => {
        this.onFilesSyncDateChanged.next(response);

        resolve(response);
      }, reject);
    });
  }

  getMaterialGroups(): Promise<CuttingMatBaseGroup[]> {
    const paramsHttp: HttpParams = new HttpParams().append('groupType', TypeGroupMaterial.Plate.toString()).append('groupType', TypeGroupMaterial.Linear.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingMaterialGroup, { params: paramsHttp }).subscribe((response: CuttingMatBaseGroup[]) => {
        resolve(response);
      }, reject);
    });
  }

  getButtGroups(): Promise<CuttingMatBaseGroup[]> {
    const paramsHttp: HttpParams = new HttpParams().set('groupType', TypeGroupMaterial.Butt.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingMaterialGroup, { params: paramsHttp }).subscribe((response: CuttingMatBaseGroup[]) => {
        resolve(response);
      }, reject);
    });
  }

  getMaterials(groupId: number, pageIndex: number, pageSize: number): Promise<CuttingMaterialResponse> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('groupId', groupId.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingMaterial, { params: paramsHttp }).subscribe((response: CuttingMaterialResponse) => {
        resolve(response);
      }, reject);
    });
  }

  getButts(groupId: number, pageIndex: number, pageSize: number, thicknessAntiFilter: number[], nameFilter: string): Promise<CuttingButtResponse> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('groupInMatBaseId', groupId.toString())
      .set('thicknessAntiFilter', thicknessAntiFilter.join(','))
      .set('nameFilter', nameFilter);

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingButt, { params: paramsHttp }).subscribe((response: CuttingButtResponse) => {
        resolve(response);
      }, reject);
    });
  }

  getMatchedButts(materialId: number, pageIndex: number, pageSize: number): Promise<CuttingButtResponse> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('materialInMatBaseId', materialId.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingMatchedButt, { params: paramsHttp }).subscribe((response: CuttingButtResponse) => {
        resolve(response);
      }, reject);
    });
  }

  addMatchedButts(materialId: number, buttId: number[], pageSize: number): Promise<CuttingButt[]> {
    const paramsHttp: HttpParams = new HttpParams().set('materialInMatBaseId', materialId.toString()).set('pageSize', pageSize.toString());

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingMatchedButt, buttId, { params: paramsHttp }).subscribe((response: CuttingButt[]) => {
        resolve(response);
      }, reject);
    });
  }

  deleteMatchedButts(materialId: number, buttId: number): Promise<number> {
    const paramsHttp: HttpParams = new HttpParams().set('materialInMatBaseId', materialId.toString());

    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.cuttingMatchedButt + '/' + buttId, { params: paramsHttp }).subscribe(() => {
        resolve(buttId);
      }, reject);
    });
  }

  getPlateSizes(): Promise<CuttingPlateSize[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingPlateSizes).subscribe((response: CuttingPlateSize[]) => {
        this.onPlateSizeListChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  addPlateSizes(plateSize: CuttingPlateSize): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingPlateSizes, plateSize).subscribe((response: number) => {
        plateSize.id = response;
        this.onPlateSizeListChanged.value.push(plateSize);
        this.onPlateSizeListChanged.next(this.onPlateSizeListChanged.value);
        resolve(response);
      }, reject);
    });
  }

  deletePlateSize(plateSizeId: number): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.cuttingPlateSizes + '/' + plateSizeId).subscribe(() => {
        const index = this.onPlateSizeListChanged.value.findIndex((s: CuttingPlateSize) => +s.id === +plateSizeId);
        this.onPlateSizeListChanged.value.splice(index, 1);
        this.onPlateSizeListChanged.next(this.onPlateSizeListChanged.value);
        resolve();
      }, reject);
    });
  }

  savePlateSize(plateSize: CuttingPlateSize): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.cuttingPlateSizes, plateSize).subscribe(() => {
        resolve();
      }, reject);
    });
  }

  getCuttingOrderStatusList(): Promise<CuttingOrderStatus[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingOrderStatus).subscribe((response: CuttingOrderStatus[]) => {
        this.onCuttingOrderStatusListChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  addCuttingOrderStatus(item: CuttingOrderStatus): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingOrderStatus, item).subscribe((response: number) => {
        item.id = response;
        this.onCuttingOrderStatusListChanged.value.push(item);

        this.onCuttingOrderStatusListChanged.next(this.onCuttingOrderStatusListChanged.value);
        resolve(response);
      }, reject);
    });
  }

  saveCuttingOrderStatus(item: CuttingOrderStatus): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.cuttingOrderStatus + '/' + item.id, item).subscribe((response: number) => {
        resolve();
      }, reject);
    });
  }

  deleteCuttingOrderStatus(statusId: number): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.cuttingOrderStatus + '/' + statusId).subscribe(() => {
        const statusIndex = this.onCuttingOrderStatusListChanged.value.findIndex(s => +s.id === +statusId);
        if (statusIndex !== -1) {
          this.onCuttingOrderStatusListChanged.value.splice(statusIndex, 1);
          this.onCuttingOrderStatusListChanged.next(this.onCuttingOrderStatusListChanged.value);
        }
        resolve();
      }, reject);
    });
  }

  setUseMatSizeFromMatBase(val: boolean): Promise<void> {
    const opt: UserOptions = { ...this.onUserOptionsChanged.value };
    opt.cuttingOptions.useMatSizesFromMatBase = val;

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.userOptions, opt).subscribe(() => {
        this.onUserOptionsChanged.value.cuttingOptions.useMatSizesFromMatBase = val;
        this.onUserOptionsChanged.next(this.onUserOptionsChanged.value);
        resolve();
      }, reject);
    });
  }

  getUserOptions(): Promise<UserOptions> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.userOptions).subscribe((response: UserOptions) => {
        this.onUserOptionsChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  saveUserOptions(opt: UserOptions): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.userOptions, opt).subscribe(() => {
        this.onUserOptionsChanged.next(opt);
        resolve();
      }, reject);
    });
  }

  getInvoiceNumberSettings(): Promise<CuttingOrderInvoiceNumber> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.invoiceNumberSettings).subscribe((response: CuttingOrderInvoiceNumber) => {
        this.onInvoiceNumberSettingsChanged.next(response);
        resolve();
      }, reject);
    });
  }

  saveInvoiceNUmberSettings(item: CuttingOrderInvoiceNumber): Promise<CuttingOrderInvoiceNumber> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.invoiceNumberSettings, item).subscribe((response: CuttingOrderInvoiceNumber) => {
        this.onInvoiceNumberSettingsChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  getGrooves(): Promise<CuttingPanelGroove[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.grooves).subscribe((response: CuttingPanelGroove[]) => {
        this.onGroovesChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  addGroove(groove: CuttingPanelGroove): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.grooves, groove).subscribe((response: number) => {
        groove.id = response;

        this.onGroovesChanged.value.push(groove);
        this.onGroovesChanged.next(this.onGroovesChanged.value);

        resolve(response);
      }, reject);
    });
  }

  deleteGroove(grooveId: number) {
    return new Promise((resolve, reject) => {
      this.http.delete(`${ApiLinks.grooves}/${grooveId}`).subscribe(() => {
        const index = this.onGroovesChanged.value.findIndex((g: CuttingPanelGroove) => +g.id === +grooveId);
        if (index !== -1) {
          this.onGroovesChanged.value.splice(index, 1);
          this.onGroovesChanged.next(this.onGroovesChanged.value);
        }

        resolve();
      }, reject);
    });
  }

  getGrooveNames(): Promise<string[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.grooveNames).subscribe((response: string[]) => {
        resolve(response);
      }, reject);
    });
  }

  getCustomGroovesSchemas(): Promise<CuttingCustomGrooveSchema[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.customGrooveSchema).subscribe((response: CuttingCustomGrooveSchema[]) => {
        this.onCustomGroovesSchemasChanged.next(response);

        resolve(response);
      }, reject);
    });
  }

  addCustomGrooveSchema(schema: CuttingCustomGrooveSchema): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.customGrooveSchema, schema).subscribe((response: number) => {
        schema.id = response;
        this.onCustomGroovesSchemasChanged.value.push(schema);
        this.onCustomGroovesSchemasChanged.next(this.onCustomGroovesSchemasChanged.value);

        resolve(response);
      }, reject);
    });
  }

  deleteCustomGrooveScheme(schemaId: number): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.customGrooveSchema + '/' + schemaId).subscribe(() => {
        const index = this.onCustomGroovesSchemasChanged.value.findIndex(s => +s.id === +schemaId);
        if (index !== -1) {
          this.onCustomGroovesSchemasChanged.value.splice(index, 1);
          this.onCustomGroovesSchemasChanged.next(this.onCustomGroovesSchemasChanged.value);
        }

        resolve();
      }, reject);
    });
  }

  saveCustomGrooveSchema(schema: CuttingCustomGrooveSchema): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.customGrooveSchema + '/' + schema.id, schema).subscribe(() => {
        const index = this.onCustomGroovesSchemasChanged.value.findIndex(s => +s.id === +schema.id);
        if (index !== -1) {
          this.onCustomGroovesSchemasChanged.value[index] = schema;
          this.onCustomGroovesSchemasChanged.next(this.onCustomGroovesSchemasChanged.value);
        }

        resolve();
      }, reject);
    });
  }

  getMessageSchema(statusId: number, messageType: MessageTypeEnum): Promise<CuttingMessageSchema> {
    const paramsHttp: HttpParams = new HttpParams().set('statusId', statusId.toString()).set('messageType', messageType.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.cuttingEmailSchema, { params: paramsHttp }).subscribe((response: CuttingMessageSchema) => {
        resolve(response);
      }, reject);
    });
  }

  saveMessageSchema(schema: CuttingMessageSchema): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.cuttingEmailSchema, schema).subscribe((response: number) => {
        resolve(response);
      }, reject);
    });
  }

  setStatusMessageNotificationActive(statusId: number, messageType: MessageTypeEnum, value: boolean) {
    const paramsHttp: HttpParams = new HttpParams()
      .set('statusId', statusId.toString())
      .set('messageType', messageType.toString())
      .set('value', value.toString());

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingEmailSchemaNotifi, undefined, { params: paramsHttp }).subscribe((response: number) => {
        switch (messageType) {
          case MessageTypeEnum.Email:
            this.onCuttingOrderStatusListChanged.value.find(s => s.id === statusId).enableEmailNotification = value;
            break;

          case MessageTypeEnum.Sms:
            this.onCuttingOrderStatusListChanged.value.find(s => s.id === statusId).enableSMSNotification = value;
            break;
        }

        this.onCuttingOrderStatusListChanged.next(this.onCuttingOrderStatusListChanged.value);

        resolve(response);
      }, reject);
    });
  }

  getSmsAgentBalnce(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.smsAgentBalance).subscribe((response: number) => {
        resolve(response);
      }, reject);
    });
  }

  // factories
  getFactoryList(): Promise<CuttingFactoryResponse> {
    const paramsHttp: HttpParams = new HttpParams();

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.factories, { params: paramsHttp }).subscribe((response: CuttingFactoryResponse) => {
        this.onFactoriesChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  addFactory(item: CuttingFactory): Promise<CuttingFactory> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.factories, item).subscribe((response: CuttingFactory) => {
        this.onFactoriesChanged.value.factoriesDto.push(response);
        this.onFactoriesChanged.next(this.onFactoriesChanged.value);

        resolve(response);
      }, reject);
    });
  }

  editFactory(item: CuttingFactory): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(`${ApiLinks.factories}/${item.id}`, item).subscribe(() => {
        const factoryIndex = this.onFactoriesChanged.value.factoriesDto.findIndex(s => s.id === item.id);
        if (factoryIndex !== -1) {
          this.onFactoriesChanged.value.factoriesDto[factoryIndex].name = item.name;
          this.onFactoriesChanged.value.factoriesDto[factoryIndex].address = item.address;
        }

        this.onFactoriesChanged.next(this.onFactoriesChanged.value);

        resolve();
      }, reject);
    });
  }

  deleteFactory(item: CuttingFactory): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(`${ApiLinks.factories}/${item.id}`).subscribe(() => {
        const factoryIndex = this.onFactoriesChanged.value.factoriesDto.findIndex(s => s.id === item.id);
        this.onFactoriesChanged.value.factoriesDto.splice(factoryIndex, 1);

        this.onFactoriesChanged.next(this.onFactoriesChanged.value);

        resolve();
      }, reject);
    });
  }

  setDefaultFactory(factoryId: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${ApiLinks.factories}/${factoryId}/SetDefault`, undefined).subscribe(() => {
        this.onFactoriesChanged.value.defaultId = factoryId;
        this.onFactoriesChanged.next(this.onFactoriesChanged.value);

        resolve();
      }, reject);
    });
  }

  getCustomHoleScemas(): Promise<CuttingCustomHoleSchema[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.fastenersHoleSchema).subscribe((response: CuttingCustomHoleSchema[]) => {
        this.onCustomHoleChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  deleteCustomHoleScemas(schemaId: number): Promise<void> {
    return new Promise((resolve, reject) => {
      this.http.delete(ApiLinks.fastenersHoleSchema + '/' + schemaId).subscribe(() => {
        const schemaIndex = this.onCustomHoleChanged.value.findIndex(h => h.id === schemaId);

        if (schemaIndex !== -1) {
          this.onCustomHoleChanged.value.splice(schemaIndex, 1);
          this.onCustomHoleChanged.next(this.onCustomHoleChanged.value);
        }

        resolve();
      }, reject);
    });
  }

  addCustomHoleSchema(holeSchema: CuttingCustomHoleSchema): Promise<CuttingCustomHoleSchema> {
    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.fastenersHoleSchema, holeSchema).subscribe((response: CuttingCustomHoleSchema) => {
        this.onCustomHoleChanged.value.push(response);
        this.onCustomHoleChanged.next(this.onCustomHoleChanged.value);
        resolve(response);
      }, reject);
    });
  }

  saveCustomHoleSchema(holeSchema: CuttingCustomHoleSchema): Promise<CuttingCustomHoleSchema> {
    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.fastenersHoleSchema, holeSchema).subscribe((response: CuttingCustomHoleSchema) => {
        const schemaIndex = this.onCustomHoleChanged.value.findIndex(h => h.id === holeSchema.id);

        if (schemaIndex !== -1) {
          this.onCustomHoleChanged.value[schemaIndex] = response;
          this.onCustomHoleChanged.next(this.onCustomHoleChanged.value);
        }

        resolve(response);
      }, reject);
    });
  }

  getFastenersSet(): Promise<CuttingFastenersSet[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.fastenersSet).subscribe((response: CuttingFastenersSet[]) => {
        this.onFastenersSetChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  deleteFastenersSet(fastenersSetId: number) {
    const url = ApiLinks.fastenersSet + '/' + fastenersSetId.toString();
    return new Promise((resolve, reject) => {
      this.http.delete(url).subscribe((response: number) => {
        const setIndex = this.onFastenersSetChanged.value.findIndex(s => s.id === response);
        if (setIndex !== -1) {
          this.onFastenersSetChanged.value.splice(setIndex, 1);
          this.onFastenersSetChanged.next(this.onFastenersSetChanged.value);
        }

        resolve(response);
      }, reject);
    });
  }

  createFastenersSet(item: CuttingFastenersSet, image: File): Promise<CuttingFastenersSet> {
    const formData = new FormData();
    formData.append('image', image);
    for (const key in item) {
      if (item.hasOwnProperty(key)) {
        const element = item[key];
        formData.append(key, _.isObject(item[key]) || _.isArray(item[key]) ? JSON.stringify(element) : element);
      }
    }

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.fastenersSet, formData).subscribe((response: CuttingFastenersSet) => {
        this.onFastenersSetChanged.value.push(response);
        this.onFastenersSetChanged.next(this.onFastenersSetChanged.value);
        resolve(response);
      }, reject);
    });
  }

  saveFastenersSet(item: CuttingFastenersSet, image: File) {
    const formData = new FormData();
    formData.append('image', image);
    for (const key in item) {
      if (item.hasOwnProperty(key)) {
        const element = item[key];
        formData.append(key, _.isObject(item[key]) || _.isArray(item[key]) ? JSON.stringify(element) : element);
      }
    }

    return new Promise((resolve, reject) => {
      this.http.put(ApiLinks.fastenersSet, formData).subscribe((response: number) => {
        const setIndex = this.onFastenersSetChanged.value.findIndex(s => s.id === item.id);
        if (setIndex !== -1) {
          item.id = response;
          this.onFastenersSetChanged.value[setIndex] = item;
          this.onFastenersSetChanged.next(this.onFastenersSetChanged.value);
        }

        resolve(response);
      }, reject);
    });
  }

  getStandardFastenersSet(): Promise<CuttingFastenersSet[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.standardFastenersSet).subscribe((response: CuttingFastenersSet[]) => {
        resolve(response);
      }, reject);
    });
  }

  getManager(id: number): Promise<UserIdentification> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.manager + '/' + id).subscribe((response: UserIdentification) => {
        resolve(response);
      }, reject);
    });
  }

  getCuttingClientGroups(): Observable<CuttingClientGroup[]> {
    return this.http.get<CuttingClientGroup[]>(ApiLinks.cuttingClientGroup);
  }

  addCuttingClientGroup(item: CuttingClientGroup): Observable<CuttingClientGroup> {
    return this.http.post<CuttingClientGroup>(ApiLinks.cuttingClientGroup, item);
  }

  saveCuttingClientGroup(item: CuttingClientGroup): Observable<CuttingClientGroup> {
    return this.http.put<CuttingClientGroup>(ApiLinks.cuttingClientGroup, item);
  }

  deleteCuttingClientGroup(item: CuttingClientGroup): Observable<CuttingClientGroup> {
    return this.http.delete<CuttingClientGroup>(`${ApiLinks.cuttingClientGroup}/${item.id}`);
  }

  getClients(pageIndex: number, pageSize: number, filter: string): Observable<{ clientsDTO: CuttingClient[]; totalCount: number }> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('filter', filter);

    return this.http.get<{ clientsDTO: CuttingClient[]; totalCount: number }>(ApiLinks.cuttingClient, { params: paramsHttp });
  }
}
