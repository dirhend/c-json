export const locale = {
    lang: 'ru',
    data: {
        'SMSSERVICE': {
            'TITLE': 'СМС-Агент',
            'USECYRILLIC': 'Использовать кириллицу',
            'MAXSMSLENGTH': 'Максимальная длина сообщения {{ maxLength }} символов',
            'ENABLE': 'Включено',
            'SENDERNAME': 'Имя отправителя',
            'LOGIN': 'Логин',
            'PASSWORD': 'Пароль',
            'SERVER': 'Сервер',
            'MESSAGEHISTORY': 'История отправки SMS',
            'BALANCE': 'Баланс',
            'REFRESHBALANCE': 'Обновить баланс',
            'ERROR': {
                'REQUIRED': 'Поле не должно быть пустым',
                'MAXLENGTH': 'Максимальная длина - {{ value }}',
                'DONTUSEDSYM': 'Используйте {{ validSym }}',
                'INVALIDURL': 'Некорректный url-адрес'
            },
            'REGINSTRINGINAGENT': {
                'FOR': 'Для регистрации в сервисе отправки SMS-уведомлений перейдите по',
                'LINK': 'ссылке',
            },
            'SNACKBAR': {
                'GETBALANCEFAIL': 'Не удалось получить баланс'
            }
        }
    }
};
