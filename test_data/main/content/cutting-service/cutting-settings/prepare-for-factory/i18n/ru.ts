export const locale = {
    lang: 'ru',
    data: {
        'PREPAREFORFACTORY': {
            'FILEFORFACTORY': {
                'TITLE': 'Файл для производства',
                'USEFACTORYNUM': 'Использовать производственный номер заказа',
                'TYPE': 'Формат файла',
                'FILENAME': 'Имя файла',
                'USECLOUDNUM': 'Использовать внутренний номер заказа'
            },
            'MAKEDRAWINGS': {
                'TITLE': 'Выпуск чертежей',
                'MAKEDRAWINGSFORPANEL': 'Формировать чертежи',
                'FORALLPANELS': 'На все панели',
                'FORPANELSWITHPARAMS': 'На панели с дополнительной обработкой'
            }
        }
    }
};
