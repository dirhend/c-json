export const locale = {
    lang: 'en',
    data: {
        'STATUSEMAILEDITDIALOG': {
            'CANCEL': 'ОТМЕНА',
            'SAVE': 'ОК',
            'TITLE': 'Редактор шаблона письма',
            'SNACKBAR': {
                'SCHEMASAVEFAIL': 'Не удалось сохранить шаблон',
                'SCHEMASAVED': 'Шаблон сохранен',
                'SCHEMADELETED' : 'Шаблон удален',
                'SCHEMADELETEFAIL': 'Не удалось удалить шаблон',
            },
            'VARIABLESINFO': '%username% - ФИО клиента\n%order_number% - Номер заказа\n%new_order_status% - Новый статус заказа'
        }
    }
};
