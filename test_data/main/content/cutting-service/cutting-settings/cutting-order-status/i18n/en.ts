export const locale = {
    lang: 'en',
    data: {
        'CUTTINGORDERSTATUSLIST': {
            'EDITMESSAGE': 'Редактировать сообщение об измеении статуса',
            'SAVESTATUS': 'SAVE',
            'DELETESTATUSCONFIRM': 'Удалить статус "{{ value }}"?',
            'ADDSTATUS': 'Добавить статус',
            'COLOR': 'Цвет',
            'TABLE': {
                'NAME': 'Название',
                'ACTION': 'Тип',
                'COLOR': 'Цвет'
            },
            'SYSTEMSTATUSES': {
                'TITLE': 'Системные статусы'
            },
            'CUSTOMSTATUSES': {
                'TITLE': 'Пользовательские статусы'
            },
            'SNACKBAR': {
                'STATUSALREDYADDED': 'Статус с таким именем уже существует',
                'STATUSADDED': 'Статус добавлен',
                'STATUSADDFAIL': 'Не удалось добавить статус',
                'STATUSDELETED': 'Статус удален',
                'STATUSDELETEFAIL': 'Не удалось удалить статус'
            }
        }
    }
};
