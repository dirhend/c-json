export const locale = {
    lang: 'ru',
    data: {
        'CUTTINGORDERSTATUSDIALOG': {
            'TITLE': 'Карточка статуса',
            'SAVE': 'ДОБАВИТЬ',
            'CANCEL': 'ОТМЕНА',
            'AFTERACTION': 'Назначать после статуса',
            'NAME': 'Название'
        }
    }
};
