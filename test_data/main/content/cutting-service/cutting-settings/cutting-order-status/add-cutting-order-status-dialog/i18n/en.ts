export const locale = {
    lang: 'en',
    data: {
        'CUTTINGORDERSTATUSDIALOG': {
            'TITLE': 'Карточка статуса',
            'SAVE': 'ДОБАВИТЬ',
            'CANCEL': 'ОТМЕНА',
            'AFTERACTION': 'Назначать после статуса',
            'NAME': 'Название'

        }
    }
};
