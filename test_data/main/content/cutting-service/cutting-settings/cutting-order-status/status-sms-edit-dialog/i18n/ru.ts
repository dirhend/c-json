export const locale = {
    lang: 'ru',
    data: {
        'STATUSSMSEDITDIALOG': {
            'CANCEL': 'ОТМЕНА',
            'SAVE': 'ОК',
            'TITLE': 'Редактор шаблона SMS',
            'SNACKBAR': {
                'SCHEMASAVEFAIL': 'Не удалось сохранить шаблон',
                'SCHEMASAVED': 'Шаблон сохранен',
            },
            'ERROR': {
                'REQUIRED': 'Поле не должно быть пустым',
                'MAXLENGTH': 'Максимальная длина - {{ maxlength }}',
                'PATTERN': 'Используйте латинские символы'
            },
            'VARIABLESINFO': '%username% - ФИО клиента\n%order_number% - Номер заказа\n%new_order_status% - Новый статус заказа'
        }
    }
};
