import { TestBed, inject } from '@angular/core/testing';

import { CuttingSettingsService } from './cutting-settings.service';

describe('CuttingSettingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuttingSettingsService]
    });
  });

  it('should be created', inject([CuttingSettingsService], (service: CuttingSettingsService) => {
    expect(service).toBeTruthy();
  }));
});
