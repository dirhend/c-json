export const locale = {
  lang: 'en',
  data: {
    CUTTINGORDERDETAIL: {
      TITLE: 'Карточка заказа',
      DOWNLOAD_PROJECT: 'Файлы для производства',
      DOWNLOADDPANELRAWINGS: 'Чертежи деталей',
      PROJECTBUILDING: 'Выполняется формирование проекта',
      PANELDRAWINGSBUILDING: 'Выполняется формирование чертежей',
      TABS: {
        COMMON: 'Общее',
        CUTTINGSPEC: 'Спецификация заказа',
        PANELLIST: 'Список панелей',
        DOCUMENTS: 'Документы',
        DOWNLOAD_HISTORY: 'История скачиваний',
        FILES: 'Модели',
      },
      SNACKBAR: {
        ORDERDOWNLOADFAIL: 'Не удалось скачать файл',
        DONTSETTEDFACTORYID: 'Не задан производственный номер',
        NOACTIVETARIFFS: 'Нет активных тарифов'
      }
    }
  }
};
