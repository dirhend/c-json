import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
  MAT_DATE_LOCALE,
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import {
  CuttingOrderDetailService,
} from '@cloud/main/content/cutting-service/cutting-order-detail/cutting-order-detail.service';
import { SafeHtmlPipe } from '@cloud/main/shared/pipes/safe-html.pipe';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { TreeModule } from 'angular-tree-component';
import { CloudSharedLibModule, CuttingPanelListModule } from 'cloud-shared-lib';
import { CustomFormsModule } from 'ngx-custom-validators';
import { NgxLocalizedNumbers } from 'ngx-localized-numbers';

import { environment } from '../../../../../environments/environment';
import { CuttingOrderCommonComponent } from './cutting-order-common/cutting-order-common.component';
import { CuttingOrderDetailRoutingModule } from './cutting-order-detail-routing.module';
import { CuttingOrderDetailComponent } from './cutting-order-detail.component';
import { CuttingOrderDocumentsComponent } from './cutting-order-documents/cutting-order-documents.component';
import {
  CuttingOrderDownloadHistoryComponent,
} from './cutting-order-download-history/cutting-order-download-history.component';
import {
  CadmodelMatLinkGlanceDialogComponent,
} from './cutting-order-files/cadmodel-mat-link-glance-dialog/cadmodel-mat-link-glance-dialog.component';
import { CuttingOrderFilesComponent } from './cutting-order-files/cutting-order-files.component';
import { CuttingSpecificationComponent } from './cutting-specification/cutting-specification.component';
import {
  EstimateMaterialDetailDialogComponent,
} from './cutting-specification/estimate-material-detail-dialog/estimate-material-detail-dialog.component';
import {
  MaterialListComponent,
} from './cutting-specification/estimate-material-detail-dialog/material-list/material-list.component';
import {
  EstimateOperationDetailDialogComponent,
} from './cutting-specification/estimate-operation-detail-dialog/estimate-operation-detail-dialog.component';
import {
  OperationListComponent,
} from './cutting-specification/estimate-operation-detail-dialog/operation-list/operation-list.component';
import { CuttingMapsComponent } from './order-specification/cutting-maps/cutting-maps.component';
import { MapViewerDialogComponent } from './order-specification/cutting-maps/map-viewer-dialog/map-viewer-dialog.component';
import {
  CuttingUsedClientOffcutsComponent,
} from './order-specification/cutting-used-client-offcuts/cutting-used-client-offcuts.component';
import { OffcutListComponent } from './order-specification/offcut-list/offcut-list.component';
import { OrderSpecificationComponent } from './order-specification/order-specification.component';

@NgModule({
  imports: [
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSelectModule,
    MatDividerModule,
    MatToolbarModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatProgressBarModule,
    MatCardModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,

    FormsModule,
    CustomFormsModule,

    TreeModule,
    CuttingPanelListModule.forRoot(environment),
    CloudSharedLibModule.forRoot(environment),

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    CuttingOrderDetailRoutingModule,
    SharedModule,
    NgxLocalizedNumbers,
  ],
  declarations: [
    CuttingOrderDetailComponent,
    CuttingSpecificationComponent,
    EstimateOperationDetailDialogComponent,
    EstimateMaterialDetailDialogComponent,
    OrderSpecificationComponent,
    MaterialListComponent,
    OffcutListComponent,
    CuttingOrderDocumentsComponent,
    CuttingMapsComponent,
    SafeHtmlPipe,
    MapViewerDialogComponent,
    OperationListComponent,
    CuttingOrderCommonComponent,
    CuttingOrderDownloadHistoryComponent,
    CuttingUsedClientOffcutsComponent,
    CuttingOrderFilesComponent,
    CadmodelMatLinkGlanceDialogComponent
  ],
  providers: [
    CuttingOrderDetailService,
    FuseNavigationService,
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
  ],
  entryComponents: [
    EstimateOperationDetailDialogComponent,
    EstimateMaterialDetailDialogComponent,
    MaterialListComponent,
    MapViewerDialogComponent,
    OperationListComponent,
    CadmodelMatLinkGlanceDialogComponent
  ]
})
export class CuttingOrderDetailModule { }
