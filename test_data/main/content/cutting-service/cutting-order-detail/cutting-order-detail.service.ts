import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ApiLinks } from '@cloud/app.api-links';
import {
  Attachment,
  AutoGenFileEnum,
  customResponse,
  CuttingCadModel,
  CuttingCuttedMaterial,
  CuttingMatBaseGroup,
  CuttingMaterial,
  CuttingMeasure,
  CuttingOperation,
  CuttingOrder,
  CuttingOrderDownloadProjectHistoryDto,
  CuttingOrderStatus,
  Estimate,
  ICustomResponce,
  TypeGroupMaterial,
  UserOptions,
} from 'cloud-shared-lib';
import { BehaviorSubject, Observable } from 'rxjs';

export interface CuttingMaterialResponse {
  materialsDTO: CuttingMaterial[];
  materialsCount: number;
}

export interface CuttingOperationResponse {
  operationsDTO: CuttingOperation[];
  operationsCount: number;
}

export interface CuttingOrderDownloadHistoryResponse {
  downloadHistoryListDTO: CuttingOrderDownloadProjectHistoryDto[];
  totalCount: number;
}

@Injectable()
export class CuttingOrderDetailService {
  onEstimateChanged: BehaviorSubject<Estimate> = new BehaviorSubject(undefined);
  onMaterialsChanged: BehaviorSubject<CuttingMaterial[]> = new BehaviorSubject([]);
  onOrderChanged: BehaviorSubject<CuttingOrder> = new BehaviorSubject(undefined);
  onUserOptionsChanged: BehaviorSubject<UserOptions> = new BehaviorSubject(new UserOptions());

  orderId = 0;

  orderAttachments: Attachment[] = [];

  constructor(private http: HttpClient) {}

  /**
   * Resolve
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.orderId = route.params.id;

    return new Promise((resolveData, rejectData) => {
      Promise.all([this.getOrder(), this.getMaterials(), this.getUserOptions()]).then(() => {
        resolveData();
      }, rejectData);
    });
  }

  getEstimate(): Promise<Estimate> {
    return new Promise((resolveData, rejectData) => {
      this.http.get(ApiLinks.cuttingOrderEstimate(this.orderId)).subscribe((response: Estimate) => {
        this.onEstimateChanged.next(response);
        resolveData(response);
      }, rejectData);
    });
  }

  getUserOptions(): Promise<UserOptions> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.userOptions).subscribe((response: UserOptions) => {
        this.onUserOptionsChanged.next(response);
        resolve(response);
      }, reject);
    });
  }

  saveEstimate(estimate: Estimate): Promise<Estimate> {

    return new Promise((resolveData, rejectData) => {
      this.http.post(ApiLinks.cuttingOrderEstimate(this.orderId), estimate).subscribe(() => {
        this.onEstimateChanged.next(estimate);

        resolveData(estimate);
      }, rejectData);
    });
  }

  getMaterials(): Promise<CuttingMaterial[]> {
    return new Promise((resolveData, rejectData) => {
      this.http.get(ApiLinks.cuttingOrderMaterial(this.orderId)).subscribe((response: CuttingMaterial[]) => {
        this.onMaterialsChanged.next(response);
        resolveData(response);
      }, rejectData);
    });
  }

  getOrder(): Promise<CuttingOrder> {
    return new Promise((resolveData, rejectData) => {
      this.http.get(ApiLinks.cuttingOrder + '/' + this.orderId).subscribe((response: CuttingOrder) => {
        this.onOrderChanged.next(response);
        resolveData(response);
      }, rejectData);
    });
  }

  saveOrder(order: CuttingOrder): Promise<CuttingOrder> {
    return new Promise((resolveData, rejectData) => {
      this.http.put(ApiLinks.cuttingOrder + '/' + this.orderId, order).subscribe((response: CuttingOrder) => {
        this.onOrderChanged.next(response);
        resolveData(response);
      }, rejectData);
    });
  }

  getOrderAttachments(): Promise<Attachment[]> {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.orderAttachments(this.orderId), {}).subscribe((response: Attachment[]) => {
        this.orderAttachments = response;
        resolve(response);
      }, reject);
    });
  }

  getMeasures(): Promise<CuttingMeasure[]> {
    return new Promise((resolveData, rejectData) => {
      this.http.get(ApiLinks.measures).subscribe((response: CuttingMeasure[]) => {
        resolveData(response);
      }, rejectData);
    });
  }

  getOrderDownloadHistory(orderId: number, pageIndex: number, pageSize: number): Promise<CuttingOrderDownloadHistoryResponse> {
    const httpParams = new HttpParams().set('pageIndex', pageIndex.toString()).set('pageSize', pageSize.toString());

    const url = ApiLinks.cuttingOrderDownloadHistory(orderId);

    return new Promise((resolveData, rejectData) => {
      this.http.get(url, { params: httpParams }).subscribe((response: CuttingOrderDownloadHistoryResponse) => {
        resolveData(response);
      }, rejectData);
    });
  }

  getMatBaseMaterialGroups(typeGroups: TypeGroupMaterial[]): Promise<CuttingMatBaseGroup[]> {
    let paramsHttp: HttpParams = new HttpParams();

    typeGroups.forEach(t => {
      paramsHttp = paramsHttp.append('groupType', t.toString());
    });

    return new Promise((resolveData, rejectData) => {
      this.http.get(ApiLinks.cuttingMaterialGroup, { params: paramsHttp }).subscribe((response: CuttingMatBaseGroup[]) => {
        resolveData(response);
      }, rejectData);
    });
  }

  getMatBaseMaterials(groupId: number, pageIndex: number, pageSize: number): Promise<CuttingMaterialResponse> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('groupId', groupId.toString());

    return new Promise((resolveData, rejectData) => {
      this.http.get(ApiLinks.cuttingMaterial, { params: paramsHttp }).subscribe((response: CuttingMaterialResponse) => {
        resolveData(response);
      }, rejectData);
    });
  }

  downloadAutoGenOrderDocument(type: AutoGenFileEnum): Promise<Blob> {
    let link = '';
    switch (type) {
      case AutoGenFileEnum.Invoice:
        link = ApiLinks.generateCuttingInvoice(this.orderId);
        break;

      case AutoGenFileEnum.OrderSpecif:
        link = ApiLinks.generateCuttingSpecification(this.orderId);
        break;

      case AutoGenFileEnum.WorkSpecif:
        link = ApiLinks.generateCuttingWorkSpecif(this.orderId);
        break;

      case AutoGenFileEnum.CuttingCard:
        link = ApiLinks.generateCuttingCards(this.orderId);
        break;
    }

    return new Promise((resolveData, rejectData) => {
      this.http.get(link, { responseType: 'blob' }).subscribe((response: Blob) => {
        resolveData(response);
      }, rejectData);
    });
  }

  getHtmlAutoGenOrderDocument(type: AutoGenFileEnum): Promise<string> {
    let link = '';
    switch (type) {
      case AutoGenFileEnum.Invoice:
        link = ApiLinks.generateCuttingInvoiceHtml(this.orderId);
        break;

      case AutoGenFileEnum.OrderSpecif:
        link = ApiLinks.generateCuttingSpecificationHtml(this.orderId);
        break;

      case AutoGenFileEnum.WorkSpecif:
        link = ApiLinks.generateCuttingWorkSpecifHtml(this.orderId);
        break;

      case AutoGenFileEnum.CuttingCard:
        link = ApiLinks.generateCuttingCardsHtml(this.orderId);
        break;
    }

    return new Promise((resolve, reject) => {
      this.http.get(link, { responseType: 'text' }).subscribe(response => {
        resolve(response);
      }, reject);
    });
  }

  getCuttingMaps(materialId: number): Observable<string[]> {
    const httpParams = new HttpParams().set('materialId', materialId.toString());

    return this.http.get<string[]>(ApiLinks.cuttingMaps(this.orderId), { params: httpParams });
  }

  downloadOrder(orderId: number): Promise<{ body: Blob; filename: string }> {
    const httpParams: HttpParams = new HttpParams().set('orderId', orderId.toString());

    return new Promise((resolveData, rejectData) => {
      this.http.get(ApiLinks.downloadCuttingOrder, { params: httpParams, responseType: 'blob', observe: 'response' }).subscribe(response => {
        const filename = this.getFileNameByContentDisposition(response.headers.get('Content-Disposition'));

        resolveData({ body: response.body, filename: filename });
      }, rejectData);
    });
  }

  getMatBaseOperations(groupId: number, pageIndex: number, pageSize: number): Promise<CuttingOperationResponse> {
    const paramsHttp: HttpParams = new HttpParams()
      .set('pageIndex', pageIndex.toString())
      .set('pageSize', pageSize.toString())
      .set('groupId', groupId.toString());

    return new Promise((resolveData, rejectData) => {
      this.http.get(ApiLinks.operations, { params: paramsHttp }).subscribe((response: CuttingOperationResponse) => {
        resolveData(response);
      }, rejectData);
    });
  }

  getMatBaseOperationGroups(): Promise<CuttingMatBaseGroup[]> {
    return new Promise((resolveData, rejectData) => {
      this.http.get(ApiLinks.operationGroups).subscribe((response: CuttingMatBaseGroup[]) => {
        resolveData(response);
      }, rejectData);
    });
  }

  getPanelAttachmentFile(orderId: number, fileId: number): Promise<Blob> {
    return new Promise((resolve, reject) => {
      this.http.get(`${ApiLinks.orderAttachments(orderId)}/file/${fileId}`, { responseType: 'blob', observe: 'response' }).subscribe(response => {
        resolve(response.body);
      }, reject);
    });
  }

  downloadOrderPanelDrawings(orderId: number): Promise<{ body: Blob; filename: string }> {
    const httpParams: HttpParams = new HttpParams().set('orderId', orderId.toString());

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.downloadCuttingOrderPanelDrawings, { params: httpParams, responseType: 'blob', observe: 'response' }).subscribe(response => {
        const filename = this.getFileNameByContentDisposition(response.headers.get('Content-Disposition'));

        resolve({ body: response.body, filename: filename });
      }, reject);
    });
  }

  setPaymingDate(date: Date) {
    // add timezone. post will remove the time zone
    date.setMinutes(date.getMinutes() - date.getTimezoneOffset());

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.cuttingOrderSetPaymingDate(this.onOrderChanged.value.id), date).subscribe((response: CuttingOrderStatus) => {
        this.onOrderChanged.value.datePayming = date;
        this.onOrderChanged.value.dateConfirm = new Date();
        this.onOrderChanged.value.statusId = response.id;
        this.onOrderChanged.value.statusName = response.name;
        this.onOrderChanged.value.statusAction = response.action;
        this.onOrderChanged.next(this.onOrderChanged.value);

        resolve();
      }, reject);
    });
  }

  updateOrderDiscount(order: CuttingOrder): Observable<{ operations: number; materials: number }> {
    return this.http.post<{ operations: number; materials: number }>(ApiLinks.updateOrderDiscount(order.id), undefined);
  }

  getCuttedMaterials(orderId: number): Observable<CuttingCuttedMaterial[]> {
    return this.http.get<CuttingCuttedMaterial[]>(ApiLinks.cuttedMaterial(orderId));
  }

  private getFileNameByContentDisposition(contentDisposition: string) {
    const regex = /.*''/gi;
    const matches = contentDisposition.replace(regex, '');

    return decodeURI(matches) || 'order';
  }

  getOrderFiles(orderId: number): Observable<ICustomResponce<CuttingCadModel[]>> {
    return this.http.get(ApiLinks.cuttingOrderFiles(orderId)).pipe(customResponse<CuttingCadModel[]>());
  }
}
