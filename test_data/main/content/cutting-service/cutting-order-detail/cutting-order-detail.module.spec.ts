import { CuttingOrderDetailModule } from './cutting-order-detail.module';

describe('CuttingOrderDetailModule', () => {
  let cuttingOrderDetailModule: CuttingOrderDetailModule;

  beforeEach(() => {
    cuttingOrderDetailModule = new CuttingOrderDetailModule();
  });

  it('should create an instance', () => {
    expect(cuttingOrderDetailModule).toBeTruthy();
  });
});
