export const locale = {
  lang: 'ru',
  data: {
    CUTTINGORDERCOMMON: {
      FORM: {
        ORDER_NUMBER: '№ Заказа',
        PRODUCTION_NUMBER: 'Производственный №',
        DATE_CREATE: 'Дата создания',
        DATE_ORDERING: 'Дата оформления',
        DATE_CONFIRM: 'Дата принятия',
        DATE_PAYMING: 'Дата оплаты',
        DATE_OF_ISSUE: 'Дата выдачи',
        SUM_ORDER_WITHOUT_DISCOUNT: 'Сумма заказа без скидки',
        DISSCOUNT_PERCENTAGE: 'Скидка (материалы/операции)',
        DISCOUNT: 'Итого скидка',
        DISSCOUNT_CURRENCY: 'Скидка на заказ',
        TOTAL: 'Итого',
        REFRESH_DISCOUNT: 'Обновить скидку',
        REFRESH_DISCOUNT_CONFIRM: 'После обновления итоговая стоимость заказа будет пересчитана. Обновить скидку для заказа?',
        SUM_PAYMENTS: 'Сумма платежей',
        STATE: 'Текущий статус',
        REMARK: 'Примечание',
        CLIENT: 'Клиент',
        MANAGER: 'Менеджер',
        FACTORY: {
          TITLE: 'Производство',
          EMPTY: 'По умолчанию'
        },
        DATEPAYMINGPLACEHOLDER: 'Выберите дату',
        DATEPAYMINGWARNING: 'При установке даты оплаты, статус заказа изменится на Оплачено',
        DATE_OF_ISSUE_INFO: 'При установке даты, клиенту будет отправлено уведомление об изменении даты выдачи',
        DATE_OF_ISSUE_PLACEHOLDER: 'Выберите дату',
        ERROR: {
          REQUIRED: 'Заполните поле',
          NOT_NUMBER: 'Введите число'
        }
      },
      SNACKBAR: {
        STATUSCHANGED: 'Статус изменен',
        STATUSCHANGEFAIL: 'Не удалось изменить статус',
        ORDERDOWNLOADFAIL: 'Не удалось скачать файл',
        DONTSETTEDFACTORYID: 'Не задан производственный номер',
        ORDERSETFACTORYIDFAIL: 'Не удалось изменить производственный номер',
        NOACTIVETARIFFS: 'Нет активных тарифов',
        MANAGERAPPOINTED: 'Ответственный назначен',
        MANAGERAPPOINDFAIL: 'Не удалось назначить ответственного',
        ORDERSETFACTORYFAIL: 'Не удалось установить производство',
        ORDERREMOVEDFROMARCHIVE: 'Заказ перемещен в основной список',
        ORDERADDEDTOARCHIVE: 'Заказ перемещен в архив',
        ORDERREMOVEFROMARCHIVEFAIL: 'Не удалось переместить заказ в основной список',
        ORDERADDTOARCHIVEFAIL: 'Не удалось переместить заказ в архив'
      }
    }
  }
};
