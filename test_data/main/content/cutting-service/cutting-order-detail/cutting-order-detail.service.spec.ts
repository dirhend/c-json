import { TestBed, inject } from '@angular/core/testing';

import { CuttingOrderDetailService } from './cutting-order-detail.service';

describe('CuttingOrderDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CuttingOrderDetailService]
    });
  });

  it('should be created', inject([CuttingOrderDetailService], (service: CuttingOrderDetailService) => {
    expect(service).toBeTruthy();
  }));
});
