export const locale = {
    lang: 'ru',
    data: {
        'CUTTINGOFFCTLIST': {
            'TABLE': {
                'TAKEAWAY': 'Включить в заказ',
                'HEIGHT': 'Длина',
                'WIDTH': 'Ширина',
                'COUNT': 'Кол-во',
                'SQUARE': 'Площадь'
            }
        }
    }
};
