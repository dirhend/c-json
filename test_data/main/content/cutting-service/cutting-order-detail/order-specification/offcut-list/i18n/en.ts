export const locale = {
    lang: 'en',
    data: {
        'CUTTINGOFFCTLIST': {
            'TABLE': {
                'TAKEAWAY': 'Включить в заказ',
                'HEIGHT': 'Длина',
                'WIDTH': 'Ширина',
                'COUNT': 'Кол-во',
                'SQUARE': 'Площадь'
            }
        }
    }
};
