export const locale = {
  lang: 'en',
  data: {
    CUTTINGORDERSPEC: {
      MATERIAL: 'Материал',
      PLATESIZE: 'Размер плиты',
      VIEWLIST: {
        PANELS: 'Панели',
        OFFCUTS: 'Обрезки',
        CUTTINGMAPS: 'Карты раскроя',
        PANELS_SCHEMA: 'Чертежи панелей'
      }
    }
  }
};
