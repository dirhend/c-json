export const locale = {
    lang: 'ru',
    data: {
        'OTHERMATERIALLIST': {
            'TITLE': 'Материалы',
            'OK': 'ОК',
            'CANCEL': 'ОТМЕНА',
            'TABLE': {
                'TEXTURE': 'Текстура',
                'NAME': 'Наименование',
                'ARTICLE': 'Артикул'
            }
        }
    }
};
