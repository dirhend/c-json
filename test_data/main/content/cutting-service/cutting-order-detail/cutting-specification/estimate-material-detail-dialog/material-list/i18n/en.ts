export const locale = {
    lang: 'en',
    data: {
        'OTHERMATERIALLIST': {
            'TITLE': 'Материалы',
            'OK': 'ОК',
            'CANCEL': 'ОТМЕНА',
            'TABLE': {
                'TEXTURE': 'Текстура',
                'NAME': 'Наименование',
                'ARTICLE': 'Артикул'
            }            
        }
    }
};