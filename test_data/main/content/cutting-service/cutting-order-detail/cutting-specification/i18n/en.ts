export const locale = {
    lang: 'en',
    data: {
        'CUTTINGSPECIFICATION': {
            'MATERIAL': 'Материал',
            'DELETEPOS': 'Удалить позицию?',
            'ADD': 'Добавить',
            WITH_DISCOUNT: 'со скидкой',
            'MATERIALTABLE': {
                'TITLE': 'Материалы',
                'NAME': 'Название',
                'COUNT': 'Количество',
                'MEASURE': 'Ед. изм.',
                'PRICE': 'Стоимость'
            },
            'OPERATIONTABLE': {
                'TITLE': 'Работы',
                'NAME': 'Название',
                'COUNT': 'Количество',
                'MEASURE': 'Ед. изм.',
                'PRICE': 'Стоимость'
            }
        }
    }
};
