export const locale = {
    lang: 'ru',
    data: {
        'OTHEROPERATIONLIST': {
            'CANCEL': 'ОТМЕНА',
            'OK': 'ОК',
            'TABLE': {
                'ARTICLE': 'Артикул',
                'MEASURE': 'Ед. изм.',
                'NAME': 'Название'
            },
            'TITLE': 'Работы'
        }
    }
};
