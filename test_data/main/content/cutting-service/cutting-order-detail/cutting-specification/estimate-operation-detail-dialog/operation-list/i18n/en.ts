export const locale = {
    lang: 'en',
    data: {
        'OTHEROPERATIONLIST': {
            'CANCEL': 'ОТМЕНА',
            'OK': 'ОК',
            'TABLE': {
                'ARTICLE': 'Артикул',
                'MEASURE': 'Ед. изм.',
                'NAME': 'Название'
            },
            'TITLE': 'Работы'
        }
    }
};
