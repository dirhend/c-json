export const locale = {
    lang: 'ru',
    data: {
        'ESTIMATEOPERATIONDETAILDIALOG': {
            'TITLE': 'Работа',
            'SAVE': 'СОХРАНИТЬ',
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'NAME': 'Название',
                'PRICE': 'Стоимость',
                'MEASURE': 'Ед. измерения',
                'COUNT': 'Количество',
                'ERROR': {
                    'NAMEEMPTY': 'Название не задано',
                    'PRICEEMPTY': 'Стоимость не задана',
                    'COUNTEMPTY': 'Количество не задана',
                    'MEASUREEMPTY': 'Ед. измерения не задана'
                }
            }
        }
    }
};
