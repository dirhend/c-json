export const locale = {
  lang: 'en',
  data: {
    CADMODEL_MAT_LINK_GLANCE: {
      TITLE: 'Cоответствие материалам из модели',
      MATERIALS_TITLE: 'Материалы',
      SLOTS_TITLE: 'Пазы',
      BUTTS_TITLE: 'Облицовка',
      OK: 'ОК',
      CANCEL: 'ОТМЕНА',
      TABLE: {
        ORIGINAL_MATERIAL: 'Материал из модели',
        ORIGINAL_SLOT: 'Паз из модели',
        LINKED_SLOT: 'Паз для производства',
        THICKNESS: 'Толщина',
        LINKED_MATERIAL: 'Материал для производства',
        ORIGINAL_BUTT: 'Облицовка из модели',
        LINKED_BUTT: 'Облицовка для производства',
        SELECT_MATERIAL_FROM_BASE: 'Выбрать материал',
        SIZE: 'Формат'
      }
    }
  }
};
