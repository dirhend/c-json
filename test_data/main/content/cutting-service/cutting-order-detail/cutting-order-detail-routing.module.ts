import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { UserPanelListService } from 'cloud-shared-lib';

import { CuttingOrderDetailComponent } from './cutting-order-detail.component';
import { CuttingOrderDetailService } from './cutting-order-detail.service';

const routes: Routes = [
  {
    path: 'cutting/order/detail/:id',
    component: CuttingOrderDetailComponent,
    resolve: {
      profile: CuttingOrderDetailService,
      panelList: UserPanelListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class CuttingOrderDetailRoutingModule { }
