export const locale = {
  lang: 'ru',
  data: {
    CUTTINGORDERDOCS: {
      TABLE: {
        NAME: 'Название',
        TYPE: 'Тип',
        SIZE: 'Размер',
        DATE: 'Дата'
      },
      SNACKBAR: {
        FILE_DOWNLOAD_FAIL: 'Не удалось загрузить документ'
      }
    }
  }
};
