import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule, MatIconModule, MatListModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { ChangeLogRoutingModule } from './changelog-routing.module';
import { ChangeLogComponent } from './changelog.component';

@NgModule({
  imports: [
    MatIconModule,
    MatListModule,
    MatCardModule,
    BrowserAnimationsModule,

    ChangeLogRoutingModule,
    CommonModule,
    FuseSharedModule,
    TranslateModule,
  ],
  declarations: [
    ChangeLogComponent,
  ]
})
export class ChangeLogModule { }
