import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../../shared/guards/auth.guard';
import { ChangeLogComponent } from './changelog.component';

const routes: Routes = [
  {
    path: 'changelog',
    component: ChangeLogComponent,
    // canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class ChangeLogRoutingModule { }
