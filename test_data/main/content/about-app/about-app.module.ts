import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutAppRoutingModule } from './about-app-routing.module';
import { AboutAppComponent } from './about-app.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule, MatListModule } from '@angular/material';

@NgModule({
  imports: [
    MatIconModule,
    MatListModule,

    CommonModule,
    FuseSharedModule,
    TranslateModule,
    AboutAppRoutingModule
  ],
  declarations: [AboutAppComponent]
})
export class AboutAppModule { }
