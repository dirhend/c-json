import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { SupportListComponent } from './support-list.component';
import { SupportListService } from './support-list.service';

const routes: Routes = [
  {
    path: 'support',
    component: SupportListComponent,
    children: [],
    resolve: {
      data: SupportListService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class SupportListRoutingModule { }
