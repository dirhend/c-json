import { TestBed, inject } from '@angular/core/testing';

import { SupportListService } from './support-list.service';

describe('SupportListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SupportListService]
    });
  });

  it('should be created', inject([SupportListService], (service: SupportListService) => {
    expect(service).toBeTruthy();
  }));
});
