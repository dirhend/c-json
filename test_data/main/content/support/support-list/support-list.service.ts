import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpBackend, HttpHandler, HttpHeaders, HttpEventType, HttpRequest } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ApiLinks } from '@cloud/app.api-links';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { SupportInfo } from 'cloud-shared-lib';
import { Support, SupportResponse } from 'cloud-shared-lib';
import { SupportClose } from 'cloud-shared-lib';

@Injectable()
export class SupportListService {

  onSupportListChanged = new BehaviorSubject<any>(null);
  onUserGuidChanged = new BehaviorSubject<any>(null);

  supportListTotalCount: number;
  supportList: SupportInfo[] = [];
  statusFilter = 1;



  private _userGuid = '';
  get userGuid(): string {
    this._userGuid = localStorage.getItem('supportUserGuid');
    return this._userGuid;
  }
  set userGuid(userGuid: string) {
    localStorage.setItem('supportUserGuid', userGuid);
    this._userGuid = userGuid;
  }

  constructor(private http: HttpClient) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([
        this.getUserGuid().then(() => {
          this.getSupportList(0, 10);
        })
      ]).then(() => {
        resolve();
      },
        reject
      );
    });
  }

  getUserGuid() {
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.supportUser)
        .subscribe((response: any) => {

          this.userGuid = response;
          this.onUserGuidChanged.next(this.userGuid);
          resolve(response);
        }, reject);
    });
  }

  postCreateSupport(support: Support) {
    let headersHttp: HttpHeaders = new HttpHeaders();
    headersHttp = headersHttp.append('x-access-cloud', this.userGuid);

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.supportList, support, { headers: headersHttp })
        .subscribe((response: SupportResponse) => {
          resolve(response);
        }, reject);
    });
  }



  uploadFiles(files: File[], questionId: number, answerId: number) {
    const paramsHttp: HttpParams = new HttpParams()
      .set('question_id', questionId.toString())
      .set('answer_id', answerId.toString());

    let headersHttp: HttpHeaders = new HttpHeaders();
    headersHttp = headersHttp.append('x-access-cloud', this.userGuid);

    const input = new FormData();
    files.forEach((f: File) => {
      input.append(f.name, f);
    });

    return this.http.request(new HttpRequest('POST', ApiLinks.supportFiles, input,
      { params: paramsHttp, headers: headersHttp, reportProgress: true }));
  }

  getSupportList(pageIndex: number, pageSize: number, text: string = '') {
    pageIndex++;
    let paramsHttp: HttpParams = new HttpParams()
      .set('page', pageIndex.toString())
      .set('item_per_page', pageSize.toString())
      .set('state', this.statusFilter.toString())
      .set('first_no_answered', 'false');

    if (text) {
      paramsHttp = paramsHttp.set('id', text);
    }

    let headersHttp: HttpHeaders = new HttpHeaders();
    headersHttp = headersHttp.append('x-access-cloud', this.userGuid);
    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.supportList, { headers: headersHttp, params: paramsHttp })
        .map((res: {
          totalItems: number,
          data: SupportInfo[]
        }) => {
          this.supportListTotalCount = res.totalItems;
          return res.data;
        })
        .subscribe((response: SupportInfo[]) => {
          this.supportList = response;
          this.onSupportListChanged.next(this.supportList);
          resolve(response);
        }, reject);
    });
  }

}

