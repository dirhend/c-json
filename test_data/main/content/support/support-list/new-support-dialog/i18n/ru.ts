export const locale = {
    lang: 'ru',
    data: {
        'NEWSUPPORT': {
            'TITLE': 'Новое обращение',
            'OK': 'ОК',
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'TEXT': 'Описание',
                'SUBJECT': 'Тема',
                'CATEGORY': 'Категория',
                'CATEGORYLIST':
                {
                    'SYNCORDER': 'Синхронизация заказов салона',
                    'CUTTING': 'Обработка заказов раскроя',
                    'UPDATEPRICELIST': 'Актуализация данных в салоне'
                },
                
            },
            'SNACKBAR':
            {
                'SUPPORTCREATED': 'Обращение создано',
                'SUPPORTERROR': 'Ошибка создания обращения',
                'SUBJECTEMPTY': 'Заголовок не должен быть пустым',
                'TEXTEMPTY': 'Описание не может быть пустым',
                'SUPPORTERRORFILE': 'Ошибка загрузки файлов',
                'MAXSIZEFILE': 'Максимальный объем файлов - 60MB'
            }
        }
    }
};
