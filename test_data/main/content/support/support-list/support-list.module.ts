import { CdkTableModule } from '@angular/cdk/table';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatChipsModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRippleModule,
  MatSelectModule,
  MatTableModule,
  MatToolbarModule,
} from '@angular/material';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { CKEditorModule } from 'ngx-ckeditor';

import { SharedModule } from '../../../shared/shared.module';
import { NewSupportDialogComponent } from './new-support-dialog/new-support-dialog.component';
import { SupportListRoutingModule } from './support-list-routing.module';
import { SupportListComponent } from './support-list.component';
import { SupportListService } from './support-list.service';

@NgModule({
  imports: [
    CommonModule,
    SupportListRoutingModule,

    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    CdkTableModule,
    MatPaginatorModule,
    MatToolbarModule,
    MatRippleModule,
    MatListModule,
    MatSelectModule,
    MatChipsModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    CKEditorModule,
    MatProgressBarModule,

    FuseSharedModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [
    NewSupportDialogComponent, //
    SupportListComponent
  ],
  providers: [SupportListService],
  entryComponents: [NewSupportDialogComponent]
})
export class SupportListModule {}
