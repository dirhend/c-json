export const locale = {
    lang: 'ru',
    data: {
        'SUPPORTLIST': {
            'TITLE': 'Поддержка',
            'SEARCH': 'Поиск',
            'CREATE': 'Задать вопрос',
            'STATUSFILTER': {
                'ALL': 'Все',
                'OPENED': 'Открытые',
                'CLOSED': 'Закрытые'
            },
            'TABLE': {
                'ID': '#',
                'CREATIONDATE': 'Дата создания',
                'SUBJECT': 'Тема сообщения',
                'DATELASTANSWER': 'Обновлен',
                'CATEGORYNAME': 'Категория',
                'CATEGORY':
                {
                    9: 'Синхронизация заказов салона',
                    10: 'Актуализация данных в салоне',
                    11: 'Обработка заказов раскроя'
                }
            }
        }
    }
};
