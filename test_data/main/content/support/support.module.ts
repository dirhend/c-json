import { NgModule } from '@angular/core';
import { SupportListModule } from './support-list/support-list.module';
import { SupportDetailModule } from './support-detail/support-detail.module';

@NgModule({
  imports: [
    SupportDetailModule,
    SupportListModule,
  ],
  declarations: [
  ],
  entryComponents: [
  ]
})
export class SupportModule { }
