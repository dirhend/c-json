export const locale = {
    lang: 'ru',
    data: {
        'SUPPORTCLOSE': {
            'TITLE': 'Закрытие обращения',
            'CLOSE': 'ЗАКРЫТЬ ОБРАЩЕНИЕ',
            'CANCEL': 'ОТМЕНА',
            'TEXT': 'Комментарий',
            'RATE': 'Оцените качество работы технической поддержки: ',
            'SNACKBAR':
            {
                'SUPPORTCLOSE': 'Обращение закрыто',
                'SUPPORTERROR': 'Ошибка закрытия обращения'
            }
        }
    }
};
