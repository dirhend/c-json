export const locale = {
    lang: 'ru',
    data: {
        'SUPPORTMESSAGE': {
            'QUESTION': 'Обращение',
            'SALONNAME': 'Салон',
            'DATE': 'Дата',
            'AUTHOR': 'Автор',
            'LOADALLMESS': 'Показать предыдущие {{ value }}',
            'SEND': 'Отправить',
            'CLOSEQUESTION': 'Закрыть обращение',
            'RESTOREQUESTION': 'Возобновить обращение',
            'ORDERNAME': '№ Заказа',
            'YOU': 'ВЫ',
            'SERVICE': {
                'TITLE': 'Услуга',
                1: 'Синхронизация заказов салона',
                2: 'Обновление цен прайс-листа',
                3: 'Актуализация данных в салоне'
            },
            'SNACKBAR': {
                'FILESIZEWARN': 'Максимальный объем файлов - 60MB',
                'TEXTEMPTY': 'Описание не может быть пустым',
                'SENDMESSAGE': 'Сообщение отправлено',
                'ERRORSENDMESSAGE': 'Ошибка отправки сообщения'
            }
        }
    }
};
