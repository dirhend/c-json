import { TestBed, inject } from '@angular/core/testing';
import { SupportDetailService } from './support-detail.service';

describe('SupportDetailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SupportDetailService]
    });
  });

  it('should be created', inject([SupportDetailService], (service: SupportDetailService) => {
    expect(service).toBeTruthy();
  }));
});
