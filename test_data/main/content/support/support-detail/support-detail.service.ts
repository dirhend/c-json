import { Injectable } from '@angular/core';
import { ApiLinks } from '@cloud/app.api-links';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams, HttpHeaders, HttpRequest } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Message } from 'cloud-shared-lib';
import { Attachment } from 'cloud-shared-lib';
import { SupportAnswer } from 'cloud-shared-lib';
import { Support } from 'cloud-shared-lib';
import { SupportListService } from '../support-list/support-list.service';
import { SupportFile } from 'cloud-shared-lib';
import { SupportClose } from 'cloud-shared-lib';

@Injectable()
export class SupportDetailService {

  onSupportChanged = new BehaviorSubject<Support>(null);
  onSupportAnswersChanged = new BehaviorSubject<SupportAnswer[]>(null);
  answersTotalCount = 0;
  supportId: number = 0;

  private _userGuid: string = '';
  get userGuid(): string {
    this._userGuid = localStorage.getItem('supportUserGuid');
    return this._userGuid;
  }
  
  constructor(private http: HttpClient) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.supportId = route.params.id;

    return new Promise((resolve, reject) => {
      Promise.all([
        this.getSupport(this.supportId),
        this.getLastAnswers(this.supportId)
        // this.get(route.params.id, false)
      ]).then(() => {
        resolve();
      },
        reject
      );
    });
  }

  getSupport(supportId: number): Promise<Support>
  {
    let headersHttp: HttpHeaders = new HttpHeaders();
    headersHttp = headersHttp.append('x-access-cloud', this.userGuid);

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.getSupport(supportId), { headers: headersHttp })
        .subscribe((response: Support) => {
          this.onSupportChanged.next(response);
          resolve(response);
        }, reject);
    });
  }

  getLastAnswers(supportId: number) {
    let headersHttp: HttpHeaders = new HttpHeaders();
    headersHttp = headersHttp.append('x-access-cloud', this.userGuid);

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.getLastAnswersSupport(supportId), { headers: headersHttp })
        .map((response: {
          answers: SupportAnswer[],
          total_answer_count: number
        }) => {
          this.answersTotalCount = response.total_answer_count;
          return response.answers;
        })
        .subscribe((response: SupportAnswer[]) => {

          this.onSupportAnswersChanged.next(response);

          resolve(response);
        }, reject);
    });
  }

  getAnswers(supportId: number) {
    let headersHttp: HttpHeaders = new HttpHeaders();
    headersHttp = headersHttp.append('x-access-cloud', this.userGuid);

    return new Promise((resolve, reject) => {
      this.http.get(ApiLinks.getAnswersSupport(supportId), { headers: headersHttp })
        .map((response: {
          answers: SupportAnswer[],
          total_answer_count: number
        }) => {
          this.answersTotalCount = response.total_answer_count;
          return response.answers;
        })
        .subscribe((response: SupportAnswer[]) => {

          this.onSupportAnswersChanged.next(response);

          resolve(response);
        }, reject);
    });
  }

  saveMessage(support: Support) {
    return new Promise((resolve, reject) => {
      let headersHttp: HttpHeaders = new HttpHeaders();
      headersHttp = headersHttp.append('x-access-cloud', this.userGuid);

      this.http.put(ApiLinks.getSupport(support.id), support, { headers: headersHttp })
        .subscribe((response: number) => {
          resolve(response);
        }, reject);
    });
  }

  uploadFiles(files: File[], questionId: number, answerId: number) {
    let paramsHttp: HttpParams = new HttpParams()
      .set('question_id', questionId.toString())
      .set('answer_id', answerId.toString());
    
    let headersHttp: HttpHeaders = new HttpHeaders();
    headersHttp = headersHttp.append('x-access-cloud', this.userGuid);

    const input = new FormData();
    files.forEach((f: File) => {
      input.append(f.name, f);
    });

    return this.http.request(new HttpRequest('POST', ApiLinks.supportFiles, input, 
      { params: paramsHttp, headers: headersHttp, reportProgress: true }));
  }

  postCloseSupport(supportClose: SupportClose)
  {
    let headersHttp: HttpHeaders = new HttpHeaders();
    headersHttp = headersHttp.append('x-access-cloud', this.userGuid);

    return new Promise((resolve, reject) => {
      this.http.post(ApiLinks.getCloseSupport(supportClose.id), supportClose, { headers: headersHttp })
        .subscribe((response: any) => {
          resolve(response);
        }, reject);
    });
  }
}
