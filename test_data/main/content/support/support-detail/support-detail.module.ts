import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatChipsModule,
  MatDividerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatProgressBarModule,
  MatToolbarModule,
  MatTooltipModule,
} from '@angular/material';
import { StarRatingComponent } from '@cloud/main/shared/components/star-rating/star-rating.component';
import { SharedModule } from '@cloud/main/shared/shared.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { CKEditorModule } from 'ngx-ckeditor';

import { SupportCloseDialogComponent } from './support-close-dialog/support-close-dialog.component';
import { SupportDetailRoutingModule } from './support-detail-routing.module';
import { SupportDetailComponent } from './support-detail.component';
import { SupportDetailService } from './support-detail.service';
import { SupportMessageComponent } from './support-message/support-message.component';

@NgModule({
  imports: [
    CommonModule,
    SupportDetailRoutingModule,

    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDividerModule,
    MatChipsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatProgressBarModule,

    FuseSharedModule,
    TranslateModule,
    CKEditorModule,
    SharedModule
  ],
  declarations: [
    SupportDetailComponent, //
    SupportMessageComponent,
    SupportCloseDialogComponent,
    StarRatingComponent
  ],
  providers: [SupportDetailService],
  entryComponents: [
    SupportCloseDialogComponent, //
    StarRatingComponent
  ]
})
export class SupportDetailModule {}
