import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@cloud/main/shared/guards/auth.guard';
import { SupportDetailComponent } from './support-detail.component';
import { SupportDetailService } from './support-detail.service';

const routes: Routes = [
  {
    path: 'support/:id/detail',
    component: SupportDetailComponent,
    resolve: {
      data: SupportDetailService
    },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class SupportDetailRoutingModule { }
