    function textarea_Umbruch(id)
    {
      var o=get_object(id);
      var str=document.selection.createRange().text;

      str=myReplaceAll(str,String.fromCharCode(10),"");
      str=myReplaceAll(str,String.fromCharCode(13),"");

      o.focus();
      document.selection.createRange().text=str;
    }

    function textarea_fett(id)
    {
      html_tag(id,'b');
    }

    function textarea_kursiv(id)
    {
      html_tag(id,'i');
    }

    function textarea_unterstrichen(id)
    {
      html_tag(id,'u');
    }

    function textarea_Listeneintrag(id)
    {
      html_tag(id,'li');
    }

    function textarea_Liste(id)
    {
      html_tag(id,'ul');
    }

    function textarea_Absatz(id)
    {
      html_tag(id,'p');
    }

    function xtextarea_QLink(id,tree_id)
    {
      id=id.substr(3,id.length-3);

      window.frames[id].focus();
      this[id].document.execCommand('createlink',true,'xyz.html');

    }

    function textarea_QLink(id,tree_id)
    {
      var ok=true;

      if(tree_id=="")
      {
        alert("Diese Funktion bitte nur in einer Session aufrufen!");
        tree_id=-1;
        ok=false;
      }

      if(ok)
      {
        var wert=showModalDialog('include/qlink/index.php?tree_id='+tree_id, id, 'dialogHeight:320px; dialogWidth:600px; resizable:no; status:no; scroll:no; help:no');

        if(wert)
        {
          //id=id.substr(3,id.length-3);

          if(window.frames[id])
          {
            window.frames[id].focus();

            var o=get_object(id);
            var str=document.selection.createRange().text;
            o.focus();

            document.selection.createRange().pasteHTML(wert);
          }
          else
          {
            html_insert(id,wert);
          }
        }
      }
    }

    function myReplaceAll(sText,sFind,sNew)
    {
      while(my_strpos(sText,sFind,0))
        sText=sText.replace(sFind,sNew);

      return sText;
    }

    function my_strpos(sString,sFind,iPos)
    {
      var ret="";

      for(var i=iPos;i<sString.length;i++)
      {
        var ok=true;

        for(var j=0;j<sFind.length;j++)
        {
          if(sString.charAt(i+j)!=sFind.charAt(j))
            ok=false;
        }

        if(ok)
        {
          ret=i;
          break;
        }
      }

      return ret;
    }

    function html_insert(id,htmltext)
    {
      pos1=my_strpos(htmltext,"--",0);
      while(pos1)
      {
        pos2=my_strpos(htmltext,"--",pos1+1);

        if(pos2)
        {
          pstr=htmltext.substring(pos1+2,pos2);
          value=prompt(pstr,"");
          htmltext=htmltext.substring(0,pos1)+value+htmltext.substring(pos2+2,htmltext.length);
          pos1=my_strpos(htmltext,"--",pos1+1);
        }
        else
          pos1="";
      }

      var o=get_object(id);
      var str=document.selection.createRange().text;
      o.focus();
      document.selection.createRange().text=htmltext;
    }

    function html_tag(id,tag)
    {
      var o=get_object(id);
      var str=document.selection.createRange().text;
      o.focus();
      document.selection.createRange().text='<'+tag+'>'+str+'</'+tag+'>';
    }