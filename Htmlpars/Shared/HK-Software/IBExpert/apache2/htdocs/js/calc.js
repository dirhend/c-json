  function showCalc(element)
  {
    var wert = showModalDialog('include/calc/calc.php', element.value, 'dialogHeight:280px; dialogWidth:230px; resizable:no; status:no; scroll:no; help:no');

    try
    {
      var old_value=element.value;

      element.focus();
      element.value=wert;

      if(wert!=old_value)
      {
        var eval_str="anonymous();"+element.onchange;
        eval(eval_str);
      }
    }
    catch (excp) {}
  }