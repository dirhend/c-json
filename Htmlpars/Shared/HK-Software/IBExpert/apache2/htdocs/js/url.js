function urlencode(str) {
	var result = "";

	for (i = 0; i < str.length; i++) {
		if (str.charAt(i) == " ") result += "+";
		else result += str.charAt(i);
	}

	return escape(result);
}

function urldecode(str)
{
 	var result = str.replace(/\+/g, " ");


	for (i = 0; i< str.length; i++) {
		if (str.charAt(i) == "+") result += " ";
		else result += str.charAt(i);
	}

	return unescape(result);

}