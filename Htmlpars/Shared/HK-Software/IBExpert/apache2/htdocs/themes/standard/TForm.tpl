
<div id="{id}" style="position:absolute; left:{Left}px; top:{Top}px; width:{Width}px; height:{Height}px;">

  <div id="{id}_title"
       style="display:none;
              background-color:#0099CC;
              right:0px;
              color:white;
              font-weight:bold;
              border:1px solid black;
              position:absolute;
              top:0px;
              left:0px;
              height:{TitleHeight}px;
              z-index:{tz};
              font-family:Arial;
              padding:7px;
              cursor:move;">
    {caption} {style}<a style="display:none;" href="javascript:{name}Server.Request('action=init');">init</a>
    <span id="status"></span>
    <div style="height:0px;"></div>
  </div>

  <div id="lock{id}" style="vertical-align:bottom; text-align:center; z-index:10; background-image:url(images/lock.gif); position:absolute; left:1px; top:<tpl:php({Top}+1)/>px; width:<tpl:php({Width}-2)/>px; height:<tpl:php({ClientHeight}-2)/>px;">
    <center>
      <div style="margin-top:20px; width:160px; border:2px solid black; background-color:white; padding:10px;">Working ...<br><img src="images/wait.gif"></div>
    </center>
  </div>

  <div id="mainform{name}" style="overflow:auto; border:1px solid black; position:absolute; right:0px; top:<tpl:php({Top})/>px; left:0px; height:<tpl:php({ClientHeight})/>px; background-color:{FormColor}; z-index:{z};">
    <div style="height:0px;"></div>
    {content}
  </div>

</div>

<script language="javascript">

  document.title="{caption}";
  //Drag.init(document.getElementById("{id}"));

</script>