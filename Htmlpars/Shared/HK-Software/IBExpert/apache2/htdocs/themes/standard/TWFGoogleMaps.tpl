<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key={key}"
        type="text/javascript"></script>

<script language="javascript">

  var map{id};

</script>

<div id="{id}"
     style="border:inset 2px;">
  <div id="map_{id}" style="width:{Width}px; height:{Height}px; text-align:left;">
  </div>
</div>

<script language="javascript">

  if({load})
  {
    if (GBrowserIsCompatible())
    {
      map{NAME} = new GMap2(document.getElementById("map_{NAME}"));
      map{NAME}.addControl(new GSmallMapControl());
      map{NAME}.addControl(new GMapTypeControl());

      setTimeout("gload{id}()",1);
      //alert({Lat});
    }
  }

  function gload{id}()
  {
      map{NAME}.setCenter(new GLatLng({Lat}, {Lng}), {Zoom});
  }

</script>