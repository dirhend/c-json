<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
  <head>
    <meta http-equiv="content-type" xcontent="text/html; charset=UTF-8"/>
    <link rel="stylesheet" type="text/css" href="css/kalender.css">
    <style type="text/css">
      .ColumnsRow
      {
        background-color:#e0e0d0;
        color:black;
      }

      .ColumnsRowCell
      {
        cursor:default;
        border:2px outset;
        padding-left:2px;
        padding-right:5px;
        overflow:hidden;
      }

      .Row
      {
        background-color:white;
        color:black;
      }

      .RowCell
      {
        border-bottom:1px solid #b0b0b0;
        border-right:1px solid #b0b0b0;
        cursor:default;
        white-space:nowrap;
        border-bottom:1px solid #b0b0b0;
        border-right:1px solid #b0b0b0;
        overflow:hidden;
      }

      .RowSelected
      {
        background-color:navy;
        color:white;
      }
    </style>
  </head>

  <script type="text/javascript" src="js/dom-drag.js"></script>
  <script language="javascript" src="js/kalender.js" type="text/javascript"></script>
  <script language="javascript" src="js/timeedit.js" type="text/javascript"></script>

  <script language="javascript">
    var browserIE=false;
    var browserFF=false;

    if(document.all)
      browserIE=true;
    else
      browserFF=true;
  </script>

  <body onLoad="init();" style="margin:0px; padding:0px; background-color:#808080;">

    <div id="debug_button" style="display:none; border:1px solid red; z-index:1000; position:absolute; right:0px;">
      Debug <a href="javascript:debug_aufzu();">auf/zu</a>
      <a href="javascript:debug_del();">l�schen</a>
    </div>
    <div id="debug" style="display:none; background-color:red; color:white; border:1px solid red; z-index:1000; width:100px; position:absolute; top:50px;right:0px;">
      <textarea id="debug_text"></textarea>
    </div>

    <div style="display:none; z-Index:1000; position:absolute; right:0px; top:50px;">
      <textarea cols="100" rows="50" id="output" wrap="off"></textarea>
    </div>

    {content}

  </body>
</html>