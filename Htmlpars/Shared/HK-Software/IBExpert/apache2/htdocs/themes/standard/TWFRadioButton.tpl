<input id="{id}"
       name="{parent_name}"
       tabindex="{TabOrder}"
       style="{style(-width-borderstyle)}"
       type="radio" {checked}>
  <span id="C{id}"
        style="{style(-width-left-borderstyle)} left:20px; cursor:pointer;"
        onClick="document.getElementById('{id}').checked=!document.getElementById('{id}').checked;">{Caption}</span>