<div id="o{id}"
     <tpl:if({isEventOnMouseMove})/>onMouseMove="{name}OnMouseMove(this);"<tpl:endif/>
     <tpl:if({isEventOnClick})/>onClick="{name}OnClick(this);"<tpl:endif/>
     <tpl:if({isEventOnDblClick})/>onDblClick="{name}OnDblClick(this);"<tpl:endif/>
     <tpl:if({isEventOnEnter})/>onMouseOver="{name}OnEnter(this);"<tpl:endif/>
     <tpl:if({isEventOnExit})/>onMouseOut="{name}OnExit(this);"<tpl:endif/>
     <tpl:if({isEventOnKeyDown})/>onKeyDown="{name}OnKeyDown(this);"<tpl:endif/>
     <tpl:if({isEventOnKeyPress})/>onKeyPress="{name}OnKeyPress(this);"<tpl:endif/>
     <tpl:if({isEventOnKeyUp})/>onKeyUp="{name}OnKeyUp(this);"<tpl:endif/>
     <tpl:if({akLeft})/>left="{Left}"<tpl:endif/>
     <tpl:if({akTop})/>top="{Top}"<tpl:endif/>
     <tpl:if({akRight})/>right="{Right}"<tpl:endif/>
     <tpl:if({akBottom})/>bottom="{Bottom}"<tpl:endif/>
     <tpl:if({akWidth})/>width="{Width}"<tpl:endif/>
     <tpl:if({akHeight})/>height="{Height}"<tpl:endif/>
     style="border:0px solid black;
            overflow:{overflow};
            position:absolute;
            <tpl:if({akLeft})/>left:{Left}px;<tpl:endif/>
            <tpl:if({akTop})/>top:{Top}px;<tpl:endif/>
            <tpl:if({akRight})/>right:{Right}px;<tpl:endif/>
            <tpl:if({akBottom})/>bottom:{Bottom}px;<tpl:endif/>
            <tpl:if({akWidth})/>width:{Width}px;<tpl:endif/>
            <tpl:if({akHeight})/>height:{Height}px;<tpl:endif/>
            display:{Display};">{content}<div style="height:0px;"></div></div>

{control_events}