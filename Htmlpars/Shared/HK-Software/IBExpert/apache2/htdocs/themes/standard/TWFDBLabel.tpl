<div id="div{id}"
     style="white-space:{wrap}; {style}" {disabled}>
  <a href="javascript:"
     id="{id}"
     style="{style}"
     target="_blank"
     href="{href}">{Caption}</a>
</div>