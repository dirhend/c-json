<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com

  if(!isset($_SERVER['QUERY_STRING']))
    header( 'Location: http://www.ibexpert.com' ) ;

  $root_dir=$_SERVER['SCRIPT_FILENAME'];
  $root_dir=str_replace("index.php5","",$root_dir);

  //ini_set("error_reporting","E_ALL & ~E_NOTICE");

  //phpinfo(); die();

  /*
  include("include/xmlparser.php");

  $pxml=new XMLToArray();

  $xml="<root><a n=\"1\"/></root>";
  $nodes=$pxml->parse($xml);
  $nodes=$nodes["_ELEMENTS"][0];

  die("Ok".count($nodes));
  */

  //phpinfo(); die();
  session_start();
  session_register("sDemoTime");
  session_register("sDFM");

  //die("ok");
  define("CRLF",chr(13));

  $id=$_REQUEST['id'];
  $license=$_REQUEST['license'];
  $xml=$_REQUEST['xml'];
  $object=$_REQUEST['object'];
  $action=$_REQUEST['action'];
  $action_value=$_REQUEST['action_value'];
  $ajax=$_REQUEST['ajax'];
  $no_responde=$_REQUEST['no_responde'];


  if($action=="formdata")
    die();

  include("include/TFormBuilder.php");
  include("include/Controls.php");

  include("include/PDB.php");
  include("include/config.php");
  include("include/modules.php");
  include("include/xmlparser.php");

  //if($license)
  //  $s_db_reg_hash=$license;

  $main_db=new PDB();

  $ca=new TClientAction();

  if($xml)
  {
    $fh=fopen("log.xml","wb+");
    fwrite($fh,$xml);
    fclose($fh);

    $fh=fopen("log.txt","wb+");
    fwrite($fh,$xml);
    fclose($fh);

    //$xml=urldecode($xml);

    //echo $xml."<br>";
    //$xml='<service name="calc"><methode name="a" params="4" /><methode name="b" params="5" /><methode name="Mul" params="" /></service>';
    //$xml='<request><action s="object--WFEdit1_action--OnKeyUp_WFEDIT1_TEXT--aaa&WFEDIT2_TEXT--"></action></request>';
    $xml=str_replace("--eq--","=",$xml);
    $xml=str_replace("--and--","&",$xml);
    //$xml=str_replace("--add--","+",$xml);
    $xml=str_replace("&","--||--",$xml);

    /*
    $fh=fopen("log.xml","wb+");
    fwrite($fh,$xml);
    fclose($fh);
    */

    //$ca->alert($xml);
    //$xml="<root><a n=\"1\"/></root>";
    //$xml="<r><a s=\"object=WFButton1--||--action=OnClick--||--WFEDIT1_TEXT=moinmoin\"></a></r>";
    //echo $xml."<br>"; die();
    $pxml=new XMLToArray();
    $nodes=$pxml->parse($xml);
    $nodes=$nodes["_ELEMENTS"][0];

    //$keys=array_keys($nodes);

    //$ca->alert("nodes:".count($nodes));

    for($i=0;$i<count($nodes);$i++)
    {
      if(key($nodes)=="_ELEMENTS")
      {
        $cnodes=current($nodes);
        for($j=0;$j<count($cnodes);$j++)
        {
          $is_action=true;
          $a=$cnodes[$j]["s"];
          //$ca->alert($a);
          //$a=str_replace("--add--","+",$a);
          $action_stack[count($action_stack)]=$a;

          //echo $a.".";
        }
      }

      next($nodes);
    }
  }

  if(!$is_action && !$object && !$ajax)
    $refresh=true;

  //echo $object."->".$action." (".$id.")";

  //if($xml)
  //  die("xml:".$xml);

  if(!$id && !$license)
    $id="Register";

  $sql="select count(*) anz from rdb\$relations where rdb\$relation_name='IBE\$SCRIPTS'";
  $main_db->queryandfetch($sql);

  if($main_db->Row['ANZ']==1)
  {
    $sql="select ibe\$script_form form from ibe\$scripts where ibe\$script_name='".$id."'";
    $main_db->query($sql);
    $row=$main_db->fetch();

    $dfm=$row['FORM'];
  }

  //unset($main_db);
  //$main_db=null;
  if($dfm)
    $form=new TFormBuilder($dfm,$action_stack,$theme);
  else
    die("kein Formular gefunden");

  if(!$ajax)
  {
    $form->Show();

    if($dh=@opendir("config"))
    {
      ?>

        <div style="position:absolute; bottom:0px; padding:2px; background-color:#ff8080; color:black; font-size:12px; font-family:courier new;"><b>Warning:</b> Please rename the "config" directory!</div>

      <?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com

    }
  }
  //if($ajax && $xml)


  if($ajax)
  {
    $ca->Responde();
  }

  //echo "test";

  function _echo($s)
  {
    global $action;

    if(!$_REQUEST['action'])
      echo $s;
  }

?>