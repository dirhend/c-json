<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFMemo extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->BorderStyle="bsSingle";
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="LINES_TEXT")
      {
        $value=str_replace("\\n",CRLF,$value);
        $this->ca->ModifyAttribute($this,"value",$value);
      }
    }

    function Get()
    {
      parent::Get();

      $params="";
      if($this->ScrollBars=="ssBoth" ||
         $this->ScrollBars=="ssHorizontal")
        $params.="wrap=\"off\"";

      //$control="<textarea id=\"".strtoupper($this->Name)."\" ".$params." style=\"".$this->GetStyle()." height:".($this->Height-1)."px;\">".$this->Lines->HTMLText."</textarea>";

      $this->ThemeTemplate->Set("params",$params);
      $this->ThemeTemplate->Set("Text",$this->Lines->HTMLText);

      return str_replace("{content}",$this->ThemeTemplate->Get(),$this->Template);
    }

  }

?>