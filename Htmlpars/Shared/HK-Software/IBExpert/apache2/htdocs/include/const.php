<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  $colors['clBlack']="#000000";
  $colors['clMaroon']="#800000";
  $colors['clGreen']="#008000";
  $colors['clOlive']="#808000";
  $colors['clNavy']="#000080";
  $colors['clPurple']="#800080";
  $colors['clTeal']="#008080";
  $colors['clGray']="#808080";
  $colors['clSilver']="#C0C0C0";
  $colors['clRed']="#FF0000";
  $colors['clLime']="#00FF00";
  $colors['clYellow']="#FFFF00";
  $colors['clBlue']="#0000FF";
  $colors['clFuchsia']="#FF00FF";
  $colors['clAqua']="#00FFFF";
  $colors['clLtGray']="#C0C0C0";
  $colors['clDkGray']="#808080";
  $colors['clWhite']="#FFFFFF";

  $colors['clMoneyGreen']="#C0DCC0";
  $colors['clSkyBlue']="#A6CAF0";
  $colors['clCream']="#FFFBF0";
  $colors['clMedGray']="#A0A0A4";

  $colors['clBtnFace']="RGB(236,233,216)";
  $colors['clWindow']="RGB(255,255,255)";
  $cursor['crDefault']="";
  $cursor['crAppStart']="wait";
  $cursor['crArrow']="default";
  $cursor['crCross']="crosshair";
  $cursor['crDrag']="move";
  $cursor['crHandPoint']="pointer";
  $cursor['crHelp']="help";
  $cursor['crHourGlass']="wait";
  $cursor['crHSplit']="e-resize";
  $cursor['crIBeam']="text";
  $cursor['crMultiDrag']="move";
  $cursor['crNo']=""; // not Supported
  $cursor['crNoDrop']=""; // not Supported
  $cursor['crSizeAll']="move";
  $cursor['crSizeNESW']="ne-resize";
  $cursor['crSizeNS']="n-resize";
  $cursor['crSizeNWSE']="nw-resize";
  $cursor['crSizeWE']="e-resize";
  $cursor['crSQLWait']="wait";
  $cursor['crUpArrow']=""; // not Supported
  $cursor['crVSplit']="n-resize";
  $borderstyle['bsNone']="0px solid white";
  $borderstyle['bsSingle']="2px inset";

  function mapcolor($value)
  {
    global $colors;

    $arr=$colors;

    $ret=$value;

    for($i=0;$i<count($arr);$i++)
    {
      if(strtoupper(key($arr))==strtoupper($value))
        $ret=current($arr);

      next($arr);
    }

    //echo $value." (".$ret.")<br>";

    return $ret;

  }

  function mapcursor($value)
  {
    global $cursor;

    $arr=$cursor;

    for($i=0;$i<count($arr);$i++)
    {
      if(strtoupper(key($arr))==strtoupper($value))
        return current($arr);

      next($arr);
    }
  }

  function mapborderstyle($value)
  {
    global $borderstyle;

    $arr=$borderstyle;

    for($i=0;$i<count($arr);$i++)
    {
      if(strtoupper(key($arr))==strtoupper($value))
        return current($arr);

      next($arr);
    }
  }

  function mapfontsize($value)
  {
    return $value*1.375;
  }

  function mapfontstyle($value)
  {
    $value=strtoupper($value);
    $style="";

    if(strpos($value,"FSBOLD"))
    {
      $static_style.="font-weight:bold; ";
      $dynamic_style[count($dynamic_style)]="fontWeight=\"bold\"";
    }
    if(strpos($value,"FSITALIC"))
    {
      $static_style.="font-style:italic; ";
      $dynamic_style[count($dynamic_style)]="fontStyle=\"italic\"";
    }
    if(strpos($value,"FSUNDERLINE"))
    {
      $static_style.="text-decoration:underline; ";
      $dynamic_style[count($dynamic_style)]="textDecoration=\"underline\"";
    }
    if(strpos($value,"FSSTRIKEOUT"))
    {
      $static_style.="text-decoration:line-through; ";
      $dynamic_style[count($dynamic_style)]="textDecoration=\"line-through\"";
    }

    $style['static']=$static_style;
    $style['dynamic']=$dynamic_style;

    return $style;
  }

?>