<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TDatabase extends TComponent
  {
    var $db;

    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->db=new PDB();
    }

    function Get()
    {
      parent::Get();

      $sql=$this->SQL->Text;

      $sql=str_replace("\"","\\\"",$sql);
      $sql=str_replace(CRLF," ",$sql);

      //$control="<span style=\"border:1px solid red; ".$this->GetStyle()."\">".$this->Name."</span>";
      $control.="<div id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\">";

      $ret=$control;

      return $ret;
    }

    function Show()
    {
      echo $this->Get();
    }
  }

?>