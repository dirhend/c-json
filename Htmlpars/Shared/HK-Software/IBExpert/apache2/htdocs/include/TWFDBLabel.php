<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBLabel extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->AutoSize="True";
      $this->WordWrap="False";

      $this->DataSource="WFDataSource1";
      $this->FilenameField="FILENAME";
      $this->TextField="DESCRIPTION";
      $this->DataField="DOWNLOAD_DATA";
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
    }

    function Set($attribute,$value)
    {
      global $id,$license;

      parent::Set($attribute,$value);

      if($attribute=="CAPTION")
        $this->ca->ModifyAttribute($this,"caption",$value);
      else if($attribute=="update")
      {
        //$this->ca->alert(strlen());

        $file="download/".uniqid()."_".$this->dataset->query->Row[$this->FilenameField];
        $fh=fopen($file,"wb+");
        fwrite($fh,$this->dataset->query->Row[$this->DataField]);
        fclose($fh);

        $this->ca->ModifyAttribute($this,"caption",$this->dataset->query->Row[$this->TextField]);

        $js.=<<<END
          var o=document.getElementById("{NAME}");
          if(o)
            o.href="{file}";
END;
        $js=str_replace("{file}",$file,$js);

        $this->ca->exJS($js,$this,$value);
      }
      else if($attribute=="COLOR")
      {
        $value=mapcolor($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");
          if(o)
            o.style.backgroundColor="{value}";
END;
        //$js=str_replace("{value}",$value,$js);

        $this->ca->exJS($js,$this,$value);
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      $component=$this->ThemeTemplate->Get();
      $wrap="nowrap";
      if($this->WordWrap=="True")
        $wrap="wrap";

      $component=str_replace("{wrap}",$wrap,$component);

      return str_replace("{content}",$component,$this->Template);
    }

    function Show()
    {
      echo $this->Get();
    }
  }

?>