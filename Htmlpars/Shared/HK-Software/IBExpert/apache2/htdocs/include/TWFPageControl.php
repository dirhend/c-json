<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFPageControl extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Init()
    {
      parent::Init();

      $this->ClientHeight=$this->Height-24;
      $this->ClientWidth=$this->Width-4;
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="TABINDEX")
      {
        $js=<<<END

        {NAME}Click({value})

END;
        $this->ca->exJS($js,$this,$value);
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Color)
        $this->Color="clBtnFace";
      if(!$this->Font)
        $this->Font=new TFont();

      $ret=$this->Template;

      /*
      $style="border:2px outset;";

      $control="<div id=\"".strtoupper($this->Name)."Sheets\" ".
               "     style=\"".$this->GetStyle()." z-index:10; position:absolute; top:0px; left:0px; height:24px; width:".$this->ClientWidth."px;\"></div>".
               "<div id=\"".strtoupper($this->Name)."\" ".
               "     style=\"".$style."; z-index:0; position:absolute; top:25px; left:0px; height:".($this->ClientHeight)."px; width:".$this->ClientWidth."px;\">{content}</div>";
      */

      $control=$this->ThemeTemplate->Get();

      $js=<<<END

        <script language="javascript">
          {name}Sheets=new Array();
        </script>

END;

      if(file_exists("js/".$this->Theme."/TWFPageControl.js"))
      {
        $js.='<script language="javascript" src="js/'.$this->Theme.'/TWFPageControl.js"></script>';
      }

      $js2=<<<END

        <script language="javascript">
          var o=document.getElementById('{name}Sheets');
          var sheets="";

          for(i=0;i<{name}Sheets.length;i++)
          {
            if({name}Sheets[i]['tabvisible']=="True")
              sheets+="<div id=\""+{name}Sheets[i]['name']+"Sheet\" style=\"xoverflow:hidden; background-color:{backgroundcolor}; padding:2px; padding-left:5px; padding-right:5px; position:relative; float:left;\"><a id=\""+{name}Sheets[i]['name']+"Link\" style=\"color:black; text-decoration:none;\" href=\"javascript:{name}Click("+i+");\">"+{name}Sheets[i]['caption']+"</a></div>";
            else
              sheets+="<div id=\""+{name}Sheets[i]['name']+"Sheet\" style=\"display:none; xoverflow:hidden; background-color:{backgroundcolor}; padding:2px; padding-left:5px; padding-right:5px; position:relative; float:left;\"><a id=\""+{name}Sheets[i]['name']+"Link\" style=\"color:black; text-decoration:none;\" href=\"javascript:{name}Click("+i+");\">"+{name}Sheets[i]['caption']+"</a></div>";

            //alert({name}Sheets[i]['name']+"Sheet");
          }

          o.innerHTML="<div style=\" position:absolute; left:0px;\">"+sheets+"</div>";

          function {name}Click(no)
          {
            for(var i=0;i<{name}Sheets.length;i++)
            {
              var po=document.getElementById("o"+{name}Sheets[i]['name']);
              var o=document.getElementById({name}Sheets[i]['name']);
              var osheetbutton=document.getElementById({name}Sheets[i]['name']+'Sheet');
              var osheetbuttonlink=document.getElementById({name}Sheets[i]['name']+'Link');

              if(i==no)
              {
                po.style.zIndex="100";
                o.style.display="";

                if(window.TabSheetShow)
                  TabSheetShow(i,no,po,o,osheetbuttonlink,osheetbutton,browserIE);
                else
                {
                  //o.style.zIndex="1";
                  ////o.style.width="100px";
                  ////o.style.height="100px";
                  osheetbuttonlink.style.fontWeight="bold";
                  osheetbutton.style.borderTop="2px outset";
                  osheetbutton.style.borderLeft="2px outset";
                  osheetbutton.style.borderRight="2px outset";
                  osheetbutton.style.borderBottom="2px solid {backgroundcolor}";
                  osheetbutton.style.top="0px";
                  if(browserIE)
                    osheetbutton.style.height="27px";
                  else
                    osheetbutton.style.height="19px";
                }
              }
              else
              {
                po.style.zIndex="0";
                o.style.display="none";

                if(window.TabSheetHide)
                  TabSheetHide(i,no,po,o,osheetbuttonlink,osheetbutton,browserIE);
                else
                {
                  //o.style.zIndex="1";
                  osheetbuttonlink.style.fontWeight="";
                  osheetbutton.style.borderTop="2px outset";
                  osheetbutton.style.borderLeft="2px outset";
                  if(i==no-1 && browserFF)
                    osheetbutton.style.borderRight="2px solid {backgroundcolor}";
                  else
                    osheetbutton.style.borderRight="2px outset";

                  osheetbutton.style.borderBottom="0px";
                  osheetbutton.style.top="2px";
                  if(browserIE)
                    osheetbutton.style.height="23px";
                  else
                    osheetbutton.style.height="17px";
                }
              }

              sheets+="<a href=\"javascript:{name}Click("+i+");\">"+{name}Sheets[i]['caption']+"</a>";
            }
          }

          {name}Click({tabindex});

        </script>

END;

      $tabindex=$this->TabIndex;

      $ii=0;
      if(!$tabindex)
        $tabsheets=$this->GetChilds("TWFTabSheet");
        for($i=0;$i<count($tabsheets);$i++)
        {
          if($tabsheets[$i]->Name==$this->ActivePage)
            $tabindex=$ii;

          if($tabsheets[$i]->Parent->Name==$this->Name)
            $ii++;
        }

      if(!$tabindex)
        $tabindex=0;

      global $license;

      $file="themes/".$this->Theme."/TWFTabSheetTabVisible.tpl";
      $fh=fopen($file,"r");
      $tab_visible_tpl=fread($fh,filesize($file));
      fclose($fh);

      $file="themes/".$this->Theme."/TWFTabSheetTabHidden.tpl";
      $fh=fopen($file,"r");
      $tab_hidden_tpl=fread($fh,filesize($file));
      fclose($fh);

      $js2=str_replace("{tab_visible_tpl}",$tab_visible_tpl,$js2);
      $js2=str_replace("{tab_hidden_tpl}",$tab_hidden_tpl,$js2);

      $js=str_replace("{name}",strtoupper($this->Name),$js);
      $js2=str_replace("{name}",strtoupper($this->Name),$js2);
      $js2=str_replace("{tabindex}",$tabindex,$js2);
      $js2=str_replace("{backgroundcolor}",mapcolor($this->Color),$js2);

      $ret=str_replace("{content}",$js.$control,$ret);

      $ret=str_replace("{content}",$this->ShowComponents().$js2,$ret);

      $tmp_childs=$this->GetChilds("TWFTabSheet");
      $childs=array();
      $child_anz=0;
      for($i=0;$i<count($tmp_childs);$i++)
      {
        if($tmp_childs[$i]->Parent->Name==$this->Name)
          $childs[count($childs)]=$tmp_childs[$i];
      }

      $tabsheet_buttons="";
      for($i=0;$i<count($childs);$i++)
      {
        if($childs[$i]->TabVisible=="True")
        {
          $temp_tab=$tab_visible_tpl;

          $temp_tab=str_replace("{pcname}",strtoupper($this->Name),$temp_tab);
          $temp_tab=str_replace("{tabname}",$childs[$i]->Name,$temp_tab);
          $temp_tab=str_replace("{tabcaption}",$childs[$i]->Caption,$temp_tab);
          $temp_tab=str_replace("{tabid}",strtoupper($childs[$i]->Name),$temp_tab);
          $temp_tab=str_replace("{i}",$i,$temp_tab);

          $tabsheet_buttons.=$temp_tab;
        }
        else
        {
          $temp_tab=$tab_hidden_tpl;

          $temp_tab=str_replace("{pcname}",strtoupper($this->Name),$temp_tab);
          $temp_tab=str_replace("{tabname}",$childs[$i]->Name,$temp_tab);
          $temp_tab=str_replace("{tabcaption}",$childs[$i]->Caption,$temp_tab);
          $temp_tab=str_replace("{tabid}",strtoupper($childs[$i]->Name),$temp_tab);
          $temp_tab=str_replace("{i}",$i,$temp_tab);

          $tabsheet_buttons.=$temp_tab;
        }
      }

      $ret=str_replace("{tabsheet_buttons}",$tabsheet_buttons,$ret);

      return $ret;
    }
  }

?>