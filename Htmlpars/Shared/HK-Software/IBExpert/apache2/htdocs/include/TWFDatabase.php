<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDatabase extends TComponent
  {
    var $db;

    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->db=null;
      $this->db=new PDB(-1);
    }

    function Init()
    {
      $arr=explode(CRLF,$this->DBParams->Text);

      for($i=0;$i<count($arr);$i++)
      {
        $kv=explode("=",$arr[$i]);

        if(strtoupper($kv[0])=="USER_NAME")
          $user_name=$kv[1];
        else if(strtoupper($kv[0])=="PASSWORD")
          $password=$kv[1];
      }

      $this->db->Connect($this->DatabaseName,$user_name,$password);
      /*
      $this->db->queryandfetch("select * from country");
      echo $this->db->Row['COUNTRY_NAME'];
      die($this->db);
      */
    }

    function Get()
    {
      parent::Get();

      $sql=$this->SQL->Text;

      $sql=str_replace("\"","\\\"",$sql);
      $sql=str_replace(CRLF," ",$sql);

      //$control="<span style=\"border:1px solid red; ".$this->GetStyle()."\">".$this->Name."</span>";
      $control.="<div id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\">";

      $ret=$control;

      return $ret;
    }

    function Show()
    {
      echo $this->Get();
    }
  }

?>