<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBImage extends TControl
  {
    var $datasource;
    var $dataset;

    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->Setter=$this->NameSetter;
      $this->Color=mapcolor("clWhite");
      $this->BorderStyle="bsSingle";
      $this->Stretch="False";
      $this->Center="True";
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
    }

    function Set($property,$value)
    {
      parent::Set($property,$value);

      if($property=="IMAGE")
        $this->ca->ModifyAttribute($this,"setter",$value,$coding);
      else if($property=="RAWIMAGE")
      {
        $uid=uniqid("");

        $fh=fopen("blob/".$uid.".data","w+");
        fwrite($fh,$value);
        fclose($fh);

        $this->ca->ModifyAttribute($this,"setter","blob/".$uid.".data",$coding);
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      $type="text";
      if($this->PasswordChar)
        $type="password";

      $value=$this->dataset->row[$this->DataField];

      $control=$this->ThemeTemplate->Get();

      $js=<<<END

      <script language="javascript">

        var {name}Imgage=new Image();

        function {name}Setter(value)
        {
          var io=document.getElementById('{name}');

          {name}Image=new Image();

          {name}Image.src=value;

          setTimeout("{name}Loader()",10);
        }

        function {name}Loader()
        {
          if({name}Image.complete==true)
          {
            var oio=document.getElementById('o{name}');
            var io=document.getElementById('{name}');
            var k=1;

            var kx={name}Image.width/({width}-4);
            var ky={name}Image.height/({height}-4);

            io.width=0;
            io.height=0;
            io.src="images/space.gif";

            if("{stretch}"=="False")
              k=1;
            else
            {
              if(kx>ky)
                k=kx;
              else
                k=ky;
            }

            io.width={name}Image.width/k;
            io.height={name}Image.height/k;

            if("{center}"=="True")
            {
              var left=(oio.width-io.width)/2;
              if(left<0)
                left=0;

              var top=(oio.height-io.height)/2;
              if(top<0)
                top=0;

              io.style.left=left+"px";
              io.style.top=top+"px";
            }

            io.src={name}Image.src;
          }
          else
          {
            setTimeout("{name}Loader()",10);
          }
        }

      </script>

END;
      $js=str_replace("{width}",$this->Width,$js);
      $js=str_replace("{height}",$this->Height,$js);
      $js=str_replace("{name}",strtoupper($this->Name),$js);
      $js=str_replace("{stretch}",$this->Stretch,$js);
      $js=str_replace("{center}",$this->Center,$js);

      $ret=str_replace("{content}",$control.$js,$this->Template);

      return $ret;
    }

    function Show()
    {
      echo $this->Get();
    }

  }

?>