<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TIBTable extends TDataSet
  {
    var $query;
    var $row;
    var $odb;

    function __construct($owner=null)
    {
      global $action;

      $this->AddSaveProperty("Active");

      parent::__construct($owner);

      $this->odb=false;
    }

    function __destruct()
    {
    }

    function Init()
    {
      parent::Init();

      if($this->TableName)
        echo "ES GEHT!!!";

      /* falsch */
      $this->MasterSource=$this->FieldDefs->MasterSource;
      $this->TableName=$this->FieldDefs->TableName;
      $this->ReadOnly=$this->FieldDefs->ReadOnly;
      $this->IndexFieldNames=$this->FieldDefs->IndexFieldNames;
      $this->MasterFields=$this->FieldDefs->MasterFields;
      /* /falsch */


      if($this->MasterSource)
        $this->mastersource=$this->GetComponentByName($this->MasterSource);

      if($this->Database)
        $this->database=$this->GetComponentByName($this->Database);

      if($this->database)
      {
        $this->database->Init();

        $this->query=new PDBQuery($this->database->db,$this);
        $this->query->Name=$this->Name;

        $sql="select * from ".$this->TableName;

        $this->SQL=new TStrings();
        $this->SQL->Text=$sql;

        $this->ReadOnly=$this->ReadOnly;

        if($this->Active=="True" && !$this->datasource)
        {
          //$this->ca->Alert("init:".$this->Name." (".$this->GetSQL().")");
          $this->query->open($this->GetSQL());
        }
        $this->query->PrepareRemote();
        $this->row=$this->query->Row;
      }
    }

    function Set($property,$value="")
    {
      parent::Set($property,$value);

      if($property=="update")
      {
        if($this->mastersource)
        {
          //$this->ca->Alert($this->Name."Set(".$property.") (".$this->mastersource->dataset->row.")");

          //if($this->mastersource->dataset)
          //  $this->mastersource->dataset->Init();

          $this->mastersource->dataset->query->GetCurrentRow();
          if($this->Active=="True" && $this->mastersource->dataset->row[$this->MasterFields])
          {
            $sql=$this->SQL->Text." where ".$this->IndexFieldNames."=".$this->mastersource->dataset->row[$this->MasterFields];
            $this->Open($sql);
          }
          else
            $this->Close();
        }
      }
    }

    function GetSQL()
    {
      $sql=$this->SQL->Text;

      if($this->mastersource->dataset->row[$this->MasterFields])
        $sql.=" where ".$this->IndexFieldNames."=".$this->mastersource->dataset->row[$this->MasterFields];

      return $sql;
    }

    function Get()
    {
      parent::Get();

      if($this->mastersource)
      {
        //die();
        //die("q:".$this->mastersource->dataset->row);
        //$sql.=" where ".$this->FieldDefs->IndexFieldNames."=".$this->mastersource->query;
        //$this->ca->Alert($sql);
      }

      if(!$this->Font)
        $this->Font=new TFont();

      //$control="<span style=\"border:1px solid red; ".$this->GetStyle()."\">".$this->Name.": SQL->Text=".$sql."</span>";
      $control.="<div id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\">";

      $ret=$control;

      return $ret;
    }

    function Show()
    {
      $this->Get();
    }
  }

?>