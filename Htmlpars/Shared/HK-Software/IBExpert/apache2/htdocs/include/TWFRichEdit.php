<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFRichEdit extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->BorderStyle="bsSingle";
    }

    function Get()
    {
      parent::Get();

      $params="";
      if($this->ScrollBars=="ssBoth" ||
         $this->ScrollBars=="ssHorizontal")
        $params.="wrap=\"off\"";

      //$uret="<textarea id=\"".strtoupper($this->Name)."\" ".$params." style=\"".$this->GetStyle()."\">".$this->Lines->HTMLText."</textarea>";

      include_once("include/spaw/spaw_control.class.php");

      $value="";
      $value=str_replace("\\","\\\\\\\\",$value);

      $sw=new SPAW_Wysiwyg(strtoupper($this->Name),$value,'de','full','default',$this->Width,$this->Height);

      $control=$sw->GetHtml();
      $ret=$this->Template;
      $ret=str_replace("{content}",$this->ThemeTemplate->Get(),$ret);
      $ret=str_replace("{content}",$control,$ret);

      return $ret;
    }

  }

?>