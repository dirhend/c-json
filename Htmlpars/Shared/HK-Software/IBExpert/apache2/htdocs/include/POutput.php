<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com

  include_once("PTemplate.php");
  include_once("PDB.php");

class POutput
{
  var $ID;

  var $o_tpl;
  var $o_dbconnect;
  var $o_result;
  var $ret;
  var $tpl_name;
  var $getSQL;
  var $getQueryData;
  //var $getFieldProcess;
  var $sql;
  var $level;
  var $isData;

  var $lineStart;
  var $line;

  var $QueryData;
  var $Pageing;

  function setPageing($pageingname,$pageing)
  {
    $this->PageingName=$pageingname;
    $this->Pageing=$pageing;

    eval('global $'.$this->PageingName.'_back;');
    eval('global $'.$this->PageingName.'_next;');

    eval('$back=$'.$this->PageingName.'_back;');
    eval('$next=$'.$this->PageingName.'_next;');

    if($back!="")
      $this->PageingIndex=$back-$pageing;
    else if($next!="")
      $this->PageingIndex=$next+$pageing;

    $this->PageingIndexCounter=$this->PageingIndex;

    /*
    echo "back:".$back."<br>";
    echo "next:".$next."<br>";
    echo "index:".$this->PageingIndex."<br>";
    */
  }

  function setLineStart($no)
  {
    $this->$lineStart=$no;
  }

  function POutput($ID,$tpl="", $sql="", $id_tpl=0, $id_sql=0)
  {
    //$tpl_arr=explode("/",$tpl);
    $this->level=0;
    $this->ID=$ID;
    $this->isData=false;

    $this->tpl_name=$tpl;
    $this->sql=$sql;
    $this->o_tpl=new PTemplate($ID,$tpl,$id_tpl);

    //if ($sql)
    $this->o_dbconnect=new PDB($id_sql,$sql);

    $this->lineStart=0;
    $this->line=0;

    $this->PageingIndex=0;
  }

  function SetLevel($level)
  {
    $this->level=$level;
    $this->o_tpl->level=$level;
  }

  function SetQueryData($querydata)
  {
    $this->QueryData=$querydata;
    $this->QueryDataCursor=0;
    $this->isQueryData=true;
  }

  function _query()
  {
    if($this->Pageing)
    {
      if($this->line>=$this->Pageing+$this->PageingIndexCounterBack)
        return;
    }

    $this->line+=$this->PageingIndexCounter;

    for($i=0;$i<=$this->PageingIndexCounter;$i++)
    {
      if($this->o_dbconnect->queried)
        $ret=$this->o_dbconnect->fetch();
      else
      {
        $ret=@current($this->QueryData);
        @next($this->QueryData);
      }
    }
    if($this->PageingIndexCounter>0)
      $this->PageingIndexCounterBack=$this->PageingIndexCounter;
    $this->PageingIndexCounter=0;

    return $ret;
  }

  function Get()
  {
    global $GLOBAL_DATA;

    if(!$this->o_dbconnect->queried && !$this->isQueryData)
      return "sql f�r ".$this->tpl_name." fehlerhaft oder fehlt<br>SQL: ".$this->sql;

    $this->line=0;

    if($this->ret)
     return $this->ret;
    else
    {
      $this->ret="";
      $this->ret.=$this->o_tpl->Get("header");

      if($this->o_dbconnect->queried)
        $fields=$this->o_dbconnect->fields();

      while($row=$this->_query())
      {
        $this->isData=true;

        if($this->o_dbconnect->queried)
        {
          for($i=0;$i<count($fields);$i++)
            $this->o_tpl->SetField($fields[$i]['alias'],$row[$fields[$i]['alias']]);
        }
        else if($this->QueryData)
        {
          $qdfields=$row->Data;
          for($i=0;$i<count($qdfields);$i++)
          {
            $this->o_tpl->SetField(key($qdfields),current($qdfields));
            next($qdfields);
          }
        }

        $this->o_tpl->SetField("line0",$this->line+$this->$lineStart);
        $this->o_tpl->SetField("line",$this->line+$this->$lineStart+1);

        $body=$this->o_tpl->Get("body");

        // kflsd ({tpl:EMPLOYEE({COUNTRY})}) fdsakl

        $pos1=strpos($body,"<otpl:");
        while($pos1)
        {
          // befehl suchen
          $pos2=strpos($body,"(",$pos1);
          $tpos2=strpos($body,"/>",$pos1);

          $key="";
          $value="";

          if($pos2>$tpos2 || !$pos2)
          {
            $d=1;
            $pos2=strpos($body,":",$pos1);
            $command=substr($body,$pos2+1,$tpos2-$pos2-1);

            $pos5=strpos($body,"/>",$pos1+1);
          }
          else
          {
            $command=substr($body,$pos1+6,$pos2-$pos1-6);
            $pos3=strpos($body,"(",$pos2+1);
            $tpos3=strpos($body,"/>",$pos2+1);
            if($pos3 && $pos3<$tpos3)
            {
              $key=substr($body,$pos2+1,$pos3-$pos2-1);

              $pos4=strpos($body,")",$pos3+1);
              $value=substr($body,$pos3+1,$pos4-$pos3-1);
            }
            else
            {
              $pos3=strpos($body,")",$pos2+1);
              $value=substr($body,$pos2+1,$pos3-$pos2-1);
            }
            $pos5=strpos($body,"/>",$pos4+1);
          }

          //echo $command."($key,$value)<hr>";
          if(!$pos2)
          {
            echo "<div style=\"border:2px solid red; background-color:white; padding:10px;\">Fehler in Sub-Template Syntax.</div>"; "<br>Beispiel:<br>".
                  "{tpl:sub(Templatename/Variante({ID}))}";
            $pos1="";
            $body="";
          }
          else
          {
            if($command=="sub")
            {
              $getSQL=$this->getSQL;
              $getQueryData=$this->getQueryData;

              if(function_exists($getSQL))
                $sql=$getSQL($key,$value);

              if(function_exists($getQueryData))
                $querydata=$getQueryData($key,$row);

              if($key)
              {
                $sub_tpl=new POutput($this->ID,$key,$sql);
                $sub_tpl->SetLevel($this->level+1);
                $sub_tpl->setSQLFunc($getSQL);
                $sub_tpl->setQueryDataFunc($getQueryData);
                $sub_tpl->setFieldProcess($this->o_tpl->getFieldProcess);
                $sub_tpl->setQueryData($querydata);
                $new_body=$sub_tpl->Get();
              }

            }
          }

          $body=substr($body,0,$pos1).$new_body.substr($body,$pos5+2);
          $pos1=strpos($body,"<otpl:");
        }


        $this->ret.=$body;

        $this->line++;
      }

      $this->ret.=$this->o_tpl->Get("footer");

      $this->ret=str_replace("{pageing_back}","?".$this->PageingName."_back=".$this->PageingIndex,$this->ret);
      $this->ret=str_replace("{pageing_next}","?".$this->PageingName."_next=".$this->PageingIndex,$this->ret);

      return $this->ret;
    }
  }

  function setSQLFunc($getSQL)
  {
    $this->getSQL=$getSQL;
  }

  function setQueryDataFunc($getQueryData)
  {
    $this->getQueryData=$getQueryData;
  }

  function setFieldProcess($getFieldProcess)
  {
    $this->o_tpl->getFieldProcess=$getFieldProcess;
  }

  function Show()
  {
   if (!$this->sql)
   {
    $this->o_tpl->Show("header");
    $this->o_tpl->Show("body");
    $this->o_tpl->Show("footer");

   }
   else
     echo $this->Get();
  }

  function template($tpl)
  {
    $this->ret="";

    $this->o_tpl->SetTemplate($tpl);
    $this->o_dbconnect->query();
  }

  function query($sql)
  {
    $this->ret="";

    $this->o_dbconnect->query($sql);
  }

  function SaveToFile($datei)
  {
    file_put_contents($datei, $this->Get());
    if (file_exists($datei))
    {
     echo "Datei erfolgreich erstellt!";
    }
    else
    {
     echo "Datei wurde nicht erstellt!";
    }
  }

}
?>