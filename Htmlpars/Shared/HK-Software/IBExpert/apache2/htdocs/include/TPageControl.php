<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TPageControl extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Init()
    {
      parent::Init();

      $this->ClientHeight=$this->Height-24;
      $this->ClientWidth=$this->Width-4;
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="TABINDEX")
      {
        $js=<<<END

        {NAME}Click({value})

END;
        $this->ca->exJS($js,$this,$value);
      }

    }

    function Get()
    {
      parent::Get();

      if(!$this->Color)
        $this->Color="clBtnFace";
      if(!$this->Font)
        $this->Font=new TFont();

      $ret=$this->Template;

      /*
      $style="border:2px outset;";

      $control="<div id=\"".strtoupper($this->Name)."Sheets\" ".
               "     style=\"".$this->GetStyle()." z-index:10; position:absolute; top:0px; left:0px; height:24px; width:".$this->ClientWidth."px;\"></div>".
               "<div id=\"".strtoupper($this->Name)."\" ".
               "     style=\"".$style."; z-index:0; position:absolute; top:25px; left:0px; height:".($this->ClientHeight)."px; width:".$this->ClientWidth."px;\">{content}</div>";
      */

      $control=$this->ThemeTemplate->Get();

      $js=<<<END

        <script language="javascript">
          {name}Sheets=new Array();
        </script>

END;

      $js2=<<<END

        <script language="javascript">
          var o=document.getElementById('{name}Sheets');
          var sheets="";

          for(i=0;i<{name}Sheets.length;i++)
          {
            sheets+="<div id=\""+{name}Sheets[i]['name']+"Sheet\" style=\"xoverflow:hidden; background-color:{backgroundcolor}; padding:2px; padding-left:5px; padding-right:5px; position:relative; float:left;\"><a id=\""+{name}Sheets[i]['name']+"Link\" style=\"color:black; text-decoration:none;\" href=\"javascript:{name}Click("+i+");\">"+{name}Sheets[i]['caption']+"</a></div>";
          }

          o.innerHTML="<div style=\" position:absolute; left:0px;\">"+sheets+"</div>";

          function {name}Click(no)
          {
            for(i=0;i<{name}Sheets.length;i++)
            {
              var po=document.getElementById("o"+{name}Sheets[i]['name']);
              var o=document.getElementById({name}Sheets[i]['name']);
              var osheetbutton=document.getElementById({name}Sheets[i]['name']+'Sheet');
              var osheetbuttonlink=document.getElementById({name}Sheets[i]['name']+'Link');

              if(i==no)
              {
                po.style.zIndex="1";

                o.style.display="";
                //o.style.zIndex="1";
                o.style.width="100px";
                o.style.height="100px";
                osheetbuttonlink.style.fontWeight="bold";
                osheetbutton.style.borderTop="2px outset";
                osheetbutton.style.borderLeft="2px outset";
                osheetbutton.style.borderRight="2px outset";
                osheetbutton.style.borderBottom="0px";
                osheetbutton.style.top="0px";
                osheetbutton.style.height="27px";
              }
              else
              {
                po.style.zIndex="0";

                o.style.display="none";
                //o.style.zIndex="1";
                osheetbuttonlink.style.fontWeight="";
                osheetbutton.style.borderTop="2px outset";
                osheetbutton.style.borderLeft="2px outset";
                osheetbutton.style.borderRight="2px outset";
                osheetbutton.style.borderBottom="0px";
                osheetbutton.style.top="2px";
                osheetbutton.style.height="23px";
              }

              sheets+="<a href=\"javascript:{name}Click("+i+");\">"+{name}Sheets[i]['caption']+"</a>";
            }
          }

          {name}Click({tabindex});

        </script>

END;

      $tabindex=$this->TabIndex;

      if(!$tabindex)
        $tabsheets=$this->GetChilds("TTabSheet");
        for($i=0;$i<count($tabsheets);$i++)
          if($tabsheets[$i]->Name==$this->ActivePage)
            $tabindex=$i;

      if(!$tabindex)
        $tabindex=0;


      $js=str_replace("{name}",strtoupper($this->Name),$js);
      $js2=str_replace("{name}",strtoupper($this->Name),$js2);
      $js2=str_replace("{tabindex}",$tabindex,$js2);
      $js2=str_replace("{backgroundcolor}",$this->Color,$js2);

      $ret=str_replace("{content}",$js.$control,$ret);

      $ret=str_replace("{content}",$this->ShowComponents().$js2,$ret);

      return $ret;
    }
  }

?>