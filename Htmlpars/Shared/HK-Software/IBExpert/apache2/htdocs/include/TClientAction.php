<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TClientAction
  {
     var $action;

     function __construct()
     {
       $this->action="";
     }

     function f($s)
     {
       return $s;
     }

     function PostFormData($form)
     {
       //i{name}ServerFormData

       $js=<<<END

         var o=document.getElementById("{form}Form");
         var a=o.action.value;
         var sb=document.getElementById("{form}FormDataSubmit");

         o.action.value="formdata";

         sb.click();

         o.action.value=a;
END;

       $js=str_replace("{form}",$form,$js);
       $this->JS($js);

       //$this->ca->
     }

     function Add($a)
     {
       if($a['js'])
       {
         $this->action.='<js><![CDATA['.CRLF.$a['js'].CRLF.']]></js>'.CRLF;
       }
     }

     function exJS($js,$o,$value="")
     {
       $js=str_replace("{NAME}",strtoupper($o->Name),$js);
       $js=str_replace("{name}",$o->Name,$js);
       $js=str_replace("{value}",$value,$js);

       $this->JS($js);
     }

     function JS($js)
     {
       $tst_js=trim(str_replace(CRLF,"",$js));
       if($tst_js)
         $this->action.='<js><![CDATA['.CRLF.$tst_js.CRLF.']]></js>'.CRLF;
     }

     function ModifyAttribute($o,$attribute,$value,$coding="")
     {
       $value=str_replace(chr(0),"",$value);

       if($attribute=="lookup")
       {
         //echo $o->listdatasource." (".$value.")";
         $attribute="setter";
       }

       $this->action.='<ma id="'.strtoupper($o->Name).'" a="'.$attribute.'" c="'.$coding.'"><![CDATA['.utf8_encode($value).']]></ma>'.CRLF;
     }

     function Alert($a)
     {
       $a=str_replace(CRLF,"\\n",$a);
       $a=str_replace("'","\\'",$a);
       $a=str_replace(chr(10),"",$a);
       $a=str_replace(chr(13),"",$a);
       //$a=str_replace("\n"," ",$a);
       //$a=str_replace(CRLF," ",$a);
       //$a=str_replace("'"," ",$a);
       //die($a);

       $a=utf8_encode($a);
       $js="alert('".$a."');";

       //die('<js><![CDATA['.CRLF.$js.CRLF.']]></js>');

       $this->action.='<js><![CDATA['.CRLF.$js.CRLF.']]></js>'.CRLF;
     }

     function Responde()
     {
       global $no_responde;

       $r=$this->GetResponde();
       //if($this->RespondeContent)
       if(!$no_responde)
       {
         header('Content-Type: text/xml');
         echo $r;

         $fh=fopen("log/responde.xml","wb+");
         fwrite($fh,$this->GetResponde());
         fclose($fh);
       }
     }

     function GetResponde()
     {
       $this->RespondeContent=false;
       if($this->action)
         $this->RespondeContent=true;

       $ret='<?xml version="1.0" encoding="utf-8" standalone="yes"?>'.
            '<response>'.
            $this->action.CRLF.
            '</response>';

       return $ret;
     }
  }

?>