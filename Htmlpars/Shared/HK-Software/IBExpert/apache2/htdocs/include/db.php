<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com



function db_get_id($db_in="",$bez)
{
  if($db_in)
    $db=$db_in;
  else
    global $db;

  $sql="select id_struktur from stat_id where bez='".$bez."'";
  $result=db_query($db,$sql);
  $row=db_fetch($result);

  if($row['ID_STRUKTUR']=="")
    echo "<b>PHPtree-Warnung:</b> db_get_id(bez) <i>".$bez."</i> ist nicht in Tabelle STAT_ID vorhanden!<br>";

  return $row['ID_STRUKTUR'];
}

function db_get_new_id($db_in="",$gen_name="GLOB_GEN_ID")
{
  if($db_in)
    $db=$db_in;
  else
    global $db;

  if(!$gen_name)
    $gen_name="GLOB_GEN_ID";

  $sql="select gen_id(".$gen_name.",1) new_id from rdb\$database";
  $result=db_query($db,$sql);
  $row=db_fetch($result);

  return $row['NEW_ID'];
}

function db_direct_connect($id_database,$host,$user,$pass,$file,$params)
{
  $param['id_database']=$id_database;
  $param['host']=$host;
  $param['user']=$user;
  $param['pass']=$pass;
  $param['file']=$file;
  $param['params']=$params;

  /*
  echo $host.":".$file."<br>";
  $db['db']=ibase_connect($host.":".$file,$user,$pass,"win1252","100","3");
  $db['id_database']="firebird";

  $sql="select * from benutzer";
  $result=db_query($db,$sql);
  while($row=db_fetch($result))
    echo $row['BEZ']."<br>";

  die("p:".$test);
  */

  return ex_db_connect($id_database,$param);
}

function db_connect($id_database)
{
  return ex_db_connect($id_database,"");
}

function ex_db_connect($id_database,$param)
{
  global $c,$s_wdb,$s_id_user,$s_detail_level;

  if($param['params'])
    if($param['params']['charset'])
      $param['charset']=$param['params']['charset'];

  //echo $param['db_charset']."-".$param['params']['charset']."<br>";
  $db="";

  $c['std_database']="firebird";

  if($param)
    $id_database=$param['id_database'];

  if($id_database=="1")
    $id_database=$c['std_database'];

  if($id_database=="firebird")
  {
    if($param=="")
    {
      //echo "s_db:".$s_wdb."<br>";
      $db_file=$c['db'][$s_wdb]['db_file'];
      $db_user=$c['db'][$s_wdb]['db_user'];
      $db_pass=$c['db'][$s_wdb]['db_pass'];
      $db_charset=$c['db'][$s_wdb]['db_charset'];
    }
    else
    {
      $db_file=$param['file'];
      $db_user=$param['user'];
      $db_pass=$param['pass'];
      $db_host=$param['host'];
      $db_charset=$param['charset'];
      if($db_host)
        $db_file=$db_host.":".$db_file;
    }

    if($db_charset=="")
      $db_charset="win1252";

    $db_cache="100";
    $db_dialec="3";

    /*
    echo "db_file:".$db_file."<br>".
         "db_user:".$db_user."<br>".
         "db_pass:".$db_pass."<br>";
    */

    if(!$db['db']=ibase_connect($db_file,$db_user,$db_pass,$db_charset,"100","3"))
    //if(!$db['db']=@ibase_connect("/home/www/web4/db/phptree.gdb","SYSDBA","masterkey","win1252","100","3"))
    {
      $db="";
      _error("Datenbankverbindung konnte nicht Erstellt werden. <br>".$db_file."<br>Treiber:".$id_database);
      //die();
    }

    //else
    //  echo "db_open:".$db['db']."<br>";
  }
  else if($id_database=="odbc")
  {
    $db_file=$param['file'];
    $db_user=$param['user'];
    $db_pass=$param['pass'];
    $db_host=$param['host'];

    if(!$db['db']=odbc_connect($db_file,$db_user,$db_pass))
    {
      _error("Datenbankverbindung konnte nicht Erstellt werden. <br>".$db_file."<br>Treiber:".$id_database);
      //die();
    }
  }
  else if($id_database=="mysql")
  {
    $db_file=$param['file'];
    $db_user=$param['user'];
    $db_pass=$param['pass'];
    $db_host=$param['host'];

    $db['db_name']=$db_file;

    if(!$db['db']=mysql_connect($db_host,$db_user,$db_pass))
    {
      _error("Datenbankverbindung zum Server konnte nicht Erstellt werden. <br>".$db_file."<br>Treiber:".$id_database);
      //die();
    }
    else if(!mysql_select_db($db_file,$db['db']))
    {
      _error("Datenbankverbindung zur Datenbank konnte nicht Erstellt werden. <br>".$db_file."<br>Treiber:".$id_database);
    }
  }
  else
  {
    _error("Datenbank-Treiber f�r \"".$id_database."\" nicht implementiert!");
  }

  if($db)
    $db['id_database']=$id_database;


  if($id_database==$c['std_database'] && !$param && $db)   // && $param weil cms-system bei einer db_query abst�rzt (es sieht so aus, als ob es probleme mit current_connetion gibt)
  {
      $_id_user=$s_id_user;

      if($_id_user)
      {
        $sql="execute procedure phptree\$set_db_connections(".$_id_user.",".$s_detail_level.")";
        db_tquery($db,$sql);
        //echo $sql."<br>";
      }
  }

  return $db;
}

function db_disconnect($db)
{
  if($db['id_database']=="firebird")
  {
    ibase_close($db['db']);
    //echo "db_close:".$db['db']."<br>";
  }
  else if($db['id_database']=="mysql")
  {
    mysql_close($db['db']);
  }
  else if($db['id_database']=="odbc")
  {
    odbc_close($db['db']);
  }
}

function db_free_result($result)
{
  if($result['id_database']=="firebird")
    ibase_free_result($result['result']);
}

function db_tabledesc($db,$tabelle)
{
  if($db['id_database']=="firebird")
  {
    $sql="select rdb\$description bez from rdb\$relations where rdb\$relation_name='".$tabelle."'";
    $result=db_query($db,$sql);
    $row=db_fetch($result);

    return $row['RDB$DESCRIPTION'];
  }
}

function db_extabledesc($db,$tabelle)
{
  unset($desc);

  $desc_str=db_tabledesc($db,$tabelle);

  $desc_arr=explode(";",$desc_str);

  for($i=0;$i<count($desc_arr);$i++)
  {
    $_desc=explode("=",$desc_arr[$i]);

    $desc[$_desc[0]]=$_desc[1];
  }

  return $desc;
}

function count_char($str,$ch)
{
  $ret=0;

  $_ch++;

  $pos=strpos($_ch.$str,$ch);
  while($pos)
  {
    $pos=strpos($_ch.$str,$ch,$pos+strlen($ch));
    $ret++;
  }

  return $ret;
}

function db_prepare_blob($data)
{
  $data=str_replace("%%BLOB_START%%","%%BLOB_START%%",$data);
  $data=str_replace("%%BLOB_ENDE%%","%%BLOB_ENDE%%",$data);
  //$data=str_replace("<textarea","<textarea",$data);
  //$data=str_replace("</textarea>","</textarea>",$data);

  $data="%%BLOB_START%%".$data."%%BLOB_ENDE%%";

  return $data;
}

function get_blob_subtype($table,$field)
{
    $relation=$table;;
    $name=$field;

    $sql2="SELECT ".
          "  f.RDB\$FIELD_SUB_TYPE ".
          "FROM ".
          "  RDB\$RELATION_FIELDS rf, ".
          "  RDB\$FIELDS f ".
          "WHERE ".
          "  rf.RDB\$RELATION_NAME='".$relation."' AND ".
          "  rf.RDB\$FIELD_NAME='".$name."' AND ".
          "  f.RDB\$FIELD_NAME=rf.RDB\$FIELD_SOURCE";
    $result2=ibase_query($sql2);
    $row2=ibase_fetch_row($result2);

    if(!trim($row2[0]))
    {
      $sql2="SELECT ".
            "  f.RDB\$FIELD_SUB_TYPE ".
            "FROM ".
            "  RDB\$procedure_parameters pp, ".
            "  RDB\$FIELDS f ".
            "WHERE ".
            "  pp.RDB\$procedure_NAME='".$relation."' AND ".
            "  pp.RDB\$parameter_NAME='".$name."' AND ".
            "  pp.RDB\$parameter_type=1 AND ".
            "  f.RDB\$FIELD_NAME=pp.RDB\$FIELD_SOURCE";

      $result2=ibase_query($sql2);
      $row2=ibase_fetch_row($result2);
    }

    return $row2[0];

}

function get_default_value($table,$field)
{
    global $db;

    $relation=$table;;
    $name=$field;

    $sql2="SELECT RDB\$FIELD_SOURCE FROM RDB\$RELATION_FIELDS WHERE RDB\$RELATION_NAME='".$relation."' AND RDB\$FIELD_NAME='".$name."'";
    $result2=ibase_query($sql2);
    $row2=ibase_fetch_row($result2);

    $sql="SELECT RDB\$DEFAULT_SOURCE DS FROM RDB\$FIELDS WHERE RDB\$FIELD_NAME='".trim($row2[0])."'";
    $result=db_query($db,$sql);
    $row=db_fetch($result);

    $default_source=substr($row['RDB$DEFAULT_SOURCE'],8);

    return $default_source;

}

function db_get_tables($db)
{
  $ret="";
  if($db['id_database']=="firebird")
  {
    $tables=db_fetch_allrows($db,"RDB\$RELATION_NAME","RDB\$RELATIONS","RDB\$FLAGS=1","");
    for($i=0;$i<$tables['row_anz'];$i++)
    {
      $ret[$i]=$tables[$i]['RDB$RELATION_NAME'];
    }
  }
  else if($db['id_database']=="mysql")
  {
    $result=mysql_list_tables($db['db_name'],$db['db']);
    $menge=mysql_num_rows($result);
    for($i=0;$i<$menge;$i++)
      $ret[$i]=mysql_tablename($result,$i);
  }
  else
    echo "Fehler db ist ohne Inhalt db_get_tables ".$db['id_database'];

  return $ret;
}

function db_blob_query($db,$sql,$table)
{
  $sql_insert=false;
  $sql_update=false;

  if($db['id_database']=="firebird")
  {
    $_sql="SELECT * FROM ".$table;
    $result=db_query($db,$_sql);
    $fields=db_fields($result);

    $bfields="";
    $bfield_anz=0;

    if(strpos(" ".strtoupper($sql),"UPDATE")==1)
      $sql_update=true;

    if(strpos(" ".strtoupper($sql),"INSERT")==1)
      $sql_insert=true;

    for($i=0;$i<count($fields);$i++)
    {
      if($fields[$i]['type']=="BLOB")
      {
        $bfields[$bfield_anz++]['name']=$fields[$i]['alias'];
      }
    }

    if($sql_insert)
    {
      // 1. teil der inser-anweisung (bis values) in gro�buchstaben umwandeln
      $pos1=strpos($sql,")");
      $sql=strtoupper(substr($sql,0,$pos1)).substr($sql,$pos1);
    }

    for($i=0;$i<$bfield_anz;$i++)
    {

      $name=$bfields[$i]['name'];

      if($sql_insert)
      {
        $pos1=strpos($sql,"(");
        $pos2=strpos($sql,$name,$pos1);
        $pos3=strpos($sql,")",$pos1);

        $komma_str=substr($sql,$pos1,$pos2-$pos1);

        $anz=count_char($komma_str,",");

        $pos4=$pos3;
        for($j=0;$j<$anz;$j++)
          $pos4=strpos($sql,"%%BLOB_START%%",$pos4);

        $pos5=strpos($sql,"%%BLOB_ENDE%%",$pos4);

        if($pos4 && $pos5)
        {
          $bfields[$i]['ok']=1;
          $bfields[$i]['data']=substr($sql,$pos4+14,$pos5-$pos4-14);

          $sql=substr($sql,0,$pos4-1)."?".substr($sql,$pos5+14);
        }
      }
      else if($sql_update)
      {
        $pos1=strpos($sql,$name."='%%BLOB_START%%");

        if($pos1)
        {
          $pos2=strpos($sql,"%%BLOB_ENDE%%",$pos1);

          if($pos1<$pos2)
          {
            $bfields[$i]['ok']=1;
            $data=substr($sql,$pos1+strlen($name),$pos2-$pos1-strlen($name));

            $data=substr($sql,$pos1+strlen($name)+16,$pos2-$pos1-strlen($name)-16);
            $bfields[$i]['data']=$data;

            $sql=substr($sql,0,$pos1+strlen($name)+1)."?".substr($sql,$pos2+14); //."?".substr($sql,$pos2-strlen($name)+19);
          }
        }
      }
    }

    $sth=ibase_prepare($sql);
    $trans=ibase_trans();

    $eval_str='ibase_execute($sth';

    for($i=0;$i<$bfield_anz;$i++)
    {
      if($bfields[$i]['ok'])
      {
        $blob_id=ibase_blob_create();

        $p=65535;

        $len=strlen($bfields[$i]['data']);
        $anz=$len/$p;

        for($j=0;$j<$anz;$j++)
        {
          if($j*$p+$p>$len)
            $_p=$len-($j*$p);
          else
            $_p=$p;

          $tmp=substr($bfields[$i]['data'],$j*$p,$_p);
          ibase_blob_add($blob_id,$tmp);
        }

        $bfields[$i]['blob_id_str']=ibase_blob_close($blob_id);

        $eval_str.=',$bfields['.$i.'][\'blob_id_str\']';
      }

    }

    $eval_str.=');';

    eval($eval_str);

    ibase_commit($trans);
  }
}

function db_tquery($db,$sql)
{
  return _db_query($db,$sql,true);
}

function db_query($db,$sql)
{
  return _db_query($db,$sql,false);
}

function _db_query($db,$sql,$trans)
{
  global $c,$s_wdb;

  $sql_logfile=$c['db'][$s_wdb]['sql_logfile'];

  if($sql_logfile)
  {
    $fh=fopen($sql_logfile,"a+");
    fwrite($fh,$sql.chr(13).chr(10));
    fclose($fh);
  }

 if($db['db'])
 {
  if($db['id_database']=="firebird")
  {
    if($trans)
    {
      //$th=ibase_trans(IBASE_READ | IBASE_COMMITTED | IBASE_REC_VERSION | IBASE_NOWAIT,$db['db']);
      $th=ibase_trans(IBASE_NOWAIT,$db['db']);

      if(!$result['result']=ibase_query($th,$sql))
      {
        ibase_rollback($th);

        _error("Fehler: ".ibase_errmsg()."<br>SQL-Fehler:".$sql."<hr>Datenbank:".$db['id_database']);
      }
      else
      {
        $result['db']=$db['db'];
        $result['id_database']=$db['id_database'];

        ibase_commit($th);

        return $result;
      }
    }
    else
    {
      if(!$result['result']=ibase_query($db['db'],$sql))
      {
        _error("SQL-Fehler:".$sql."<hr>Datenbank:".$db['id_database']);
      }
      else
      {
        $result['db']=$db['db'];
        $result['id_database']=$db['id_database'];

        return $result;
      }
      die("ok3");
    }
  }
  else if($db['id_database']=="odbc")
  {
    if(!$result['result']=odbc_exec($db['db'],$sql))
    {
      _error("SQL-Fehler:".$sql."<hr>Datenbank:".$db['id_database']);
    }
    else
    {
      $result['db']=$db['db'];
      $result['id_database']=$db['id_database'];

      return $result;
    }
  }
  else if($db['id_database']=="mysql")
  {
    if(!$result['result']=mysql_query($sql,$db['db']))
    {
      _error("SQL-Fehler:".$sql."<hr>Datenbank:".$db['id_database']);
    }
    else
    {
      $result['db']=$db['db'];
      $result['id_database']=$db['id_database'];

      return $result;
    }

  }
  else
    echo "Fehler: \$db ist ohne Inhalt";
 }
 else
   return "";
  //return $result; // ge�ndert
}

function db_fetch_1row($db,$fields,$table,$where)
{
  $row="";

  $sql="SELECT ".$fields." FROM ".$table;

  if($where<>"")
    $sql.=" WHERE ".$where;

  $result=db_query($db,$sql);

  $row=db_fetch($result);

  db_free_result($result);

  return $row;
}

function db_fetch_allrows($db,$fields,$table,$where,$order)
{
  $sql="SELECT ".$fields." FROM ".$table;

  if($where<>"")
    $sql.=" WHERE ".$where;

  if($order<>"")
    $sql.=" ORDER BY ".$order;

  //echo $sql."<br>";
  $result=db_query($db,$sql);

  $i=0;
  while($row=db_fetch($result))
    $rows[$i++]=$row;

  db_free_result($result);

  $rows['row_anz']=$i;

  //die($sql);
  return $rows;
}

function xdb_fetch_allrows($db,$fields,$table,$where,$order)
{
  $sql="SELECT ".$fields." FROM ".$table;

  if($where<>"")
    $sql.=" WHERE ".$where;

  if($order<>"")
    $sql.=" ORDER BY ".$order;

  $result=db_query($db,$sql);

  $i=0;
  while($row=db_fetch($result))
    $rows[$i++]=$row;

  db_free_result($result);

  $rows['row_anz']=$i;

  //die($sql);
  die($sql);
  return $rows;
}

function db_get_fields($db,$table)
{
  $ret="";

  $sql="SELECT * FROM ".$table;
  $result=db_query($db,$sql);

  $ret=db_fields($result);

  return $ret;
}

function db_fieldsandblobtype($result)
{
  $fields=_db_fields($result);

  if($result['id_database']=="firebird")
  {
    for($i=0;$i<count($fields);$i++)
    {
      if($fields[$i]['type']=="BLOB")
      {
        $fields[$i]['subtype']=get_blob_subtype($fields[$i]['relation'],$fields[$i]['name']);
      }
    }
  }

  return $fields;
}

function db_fields($result)
{
  return _db_fields($result);
}

function _db_fields($result)
{
  if($result['id_database']=="firebird")
  {
      for($i=0;$i<ibase_num_fields($result['result']);$i++)
        $fields[$i]=ibase_field_info($result['result'],$i);
  }
  else if($result['id_database']=="mysql")
  {
    for($i=0;$i<mysql_num_fields($result['result']);$i++)
    {
      $fields[$i]['name']=mysql_field_name($result['result'],$i);
      $fields[$i]['alias']=mysql_field_name($result['result'],$i);
    }
  }
  else
  {
    $msg="<b>Warnung:</b> Datenbanktreiber unterst�tzt db_fields nicht!!! ".$result['id_database']."<br>";
    echo $msg;
  }

  return $fields;
}

function db_fields_and_desc($result)
{
  global $db;

  if($result['id_database']=="firebird")
  {
    $obj=ibase_fetch_object($result['result']);
    $row=get_object_vars($obj);

    // wenn man fields_and_desc bei einer leeren tabelle aufruft, dann sollte die if-abfrage auf $row nicht vorhanden sein,
    // wozu also die if-abfrage??

    //if(!$row)
    //  echo "<div>warnung bei db_fields_and_desc()</div>";
    //if($row)

    for($i=0;$i<ibase_num_fields($result['result']);$i++)
    {
      $fields[$i]=ibase_field_info($result['result'],$i);

      $sql2="SELECT RDB\$DESCRIPTION DESCRIPTION FROM RDB\$RELATION_FIELDS WHERE RDB\$RELATION_NAME='".$fields[$i]['relation']."' AND RDB\$FIELD_NAME='".$fields[$i]['name']."'";
      //echo $sql2;
      //$sql2="SELECT BEZ FROM STRUKTUR";
      $result2=db_query($db,$sql2,"RDB\$RELATION_FIELDS");
      $row2=db_fetch($result2);
      $fields[$i]['desc']['string']=$row2['RDB$DESCRIPTION'];

      $fd=explode(";",$fields[$i]['desc']['string']);

      for($j=0;$j<count($fd);$j++)
      {
        $fd_kv=explode("=",$fd[$j]);

        if($fd_kv[0])
        {
          $fd_kv[1]=str_replace("%%eq%%","=",$fd_kv[1]);

          $fields[$i]['desc'][$fd_kv[0]]=$fd_kv[1];
        }
      }

      //echo "desc:".$row2['RDB$DESCRIPTION'];
      //echo $sql;
    }
  }
  else if($result['id_database']=="odbc")
  {
    if(odbc_fetch_row($result['result']))
    {
      $line=array("odbc_affected_rows"=>odbc_num_rows($result['result']));
      for($f=1;$f<=odbc_num_fields($result['result']);$f++)
      {
        $fn=odbc_field_name($result['result'],$f);
        $fct=odbc_result($result['result'],$fn);
        $newline=array($fn => $fct);

        $fields[$f-1]['name']=$fn;
        $fields[$f-1]['alias']=$fn;
        //$fields[$f-1]['type']="dummy";

        //$line=array_merge($line,$newline);
        //echo $f.": ".$fn."=".$fct."<br>";
      }
      //$row=$line;
    }
    else
    {
      //$row="";
    }

    //$fields[0]['name']="Firma";
  }
  else if($result['id_database']=="mysql")
  {
    for($i=0;$i<mysql_num_fields($result['result']);$i++)
    {
      $fields[$i]['name']=mysql_field_name($result['result'],$i);
      $fields[$i]['alias']=mysql_field_name($result['result'],$i);
    }
  }
  else
  {
    if($result['id_database']=="")
      echo "<b>Fehler:</b> die Funktion <u>db_fields_and_desc</u> wurde mit einem \$result['id_database']=\"\" aufgerufen!<br>";
    else
      echo "<b>Warnung:</b> Datenbanktreiber unterst�tzt db_fields_and_desc nicht!!! (".$result['id_database'].")<br>";
  }

  return $fields;
}

function db_fetch($result)
{
  global $debug;

  if($result['id_database']=="firebird")
  {

    if($debug==2)
    {
      //die(".".$result['result']);

      $sql="SELECT * FROM GET_STRUKTUR(-138503,299)";
      $result=ibase_query($sql);

      $row=ibase_fetch_row($result);
      //$row=ibase_fetch_row($result['result']);

      die("..".$row[0]);
    }

    $obj=ibase_fetch_object($result['result']);
    $row=get_object_vars($obj);

    /*
    if($obj)
    foreach ($obj as $col=>$val)
      echo "ja:".$col."=".$val."<br>";
    */
    //echo ".".$obj->DATUMx.".";

    if($row)
    {
      for($i=0;$i<ibase_num_fields($result['result']);$i++)
      {
        $field=ibase_field_info($result['result'],$i);

        if($field['type']=="BLOB")
        {
          //echo $field['alias']."<br>";
          //$field['alias']=$field['name'];
          $blob_data="";
          //$alias=str_replace('$','\$',$field['alias']);

          if(!strpos(" ".$field['alias'],"$"))
          {
            $eval_str='$blob_info=@ibase_blob_info($obj->'.$field['alias'].');';
            //echo $eval_str."<br>";
            eval($eval_str);
          }
          else
            unset($blob_info);
          //if(!@eval($eval_str))
          //  _error("Fehler in db_fetch() eval_str:\"".$eval_str."\"");

          if($blob_info)
          {
            eval('$blob_id=@ibase_blob_open($obj->'.$field['alias'].');');

            /* ge�ndert am 4.4.2004 */

            if($blob_id)
            {
              $blob_data=ibase_blob_get($blob_id,$blob_info[0]); // Wenn eine "0" im Blobfeld gespeichert ist, dann wird diese hier in $blob_data gespeichert.
                                                               // Die while-schleife alleine "�bersieht" die 0.
              while($blob_tmp_data=ibase_blob_get($blob_id,$blob_info[0]))
                $blob_data.=$blob_tmp_data;

              ibase_blob_close($blob_id);
            }
          }

          $blob_data=str_replace("%%BLOB_START%%","%%BLOB_START%%",$blob_data);
          $blob_data=str_replace("%%BLOB_ENDE%%","%%BLOB_ENDE%%",$blob_data);

          //$blob_data=str_replace("<textarea","<textarea",$blob_data);
          //$blob_data=str_replace("</textarea>","</textarea>",$blob_data);

          $blob_data=str_replace("<textarea","<textarea",$blob_data);
          $blob_data=str_replace("</textarea>","</textarea>",$blob_data);

          //$row[$field['name']]=$blob_data;
          //$row[$field['name'].'__LEN__']=$blob_info[0];

          $row[$field['alias']]=$blob_data;
          $row[$field['alias'].'__LEN__']=$blob_info[0];

        }
        else
        {
          $row[$field['name']]=rtrim($row[$field['name']]);
        }
        $row['FIELD_INFO'][$field['alias']]=$field;
      }
    }
  }
  else if($result['id_database']=="odbc")
  {
    if(odbc_fetch_row($result['result']))
    {
      $line=array("odbc_affected_rows"=>odbc_num_rows($result['result']));
      for($f=1;$f<=odbc_num_fields($result['result']);$f++)
      {
        $fn=odbc_field_name($result['result'],$f);
        $fct=odbc_result($result['result'],$fn);
        $newline=array($fn => $fct);
        $line=array_merge($line,$newline);
        //echo $f.": ".$fn."=".$fct."<br>";
      }
      $row=$line;
    }
    else
    {
      $row="";
    }
  }
  else if($result['id_database']=="mysql")
  {
    $row=mysql_fetch_array($result['result']);
  }

  if($row)
  {
    return $row;
  }
}

function _error($text)
{
  ?>

  <p style="border:2px solid black; background-color:#d0d0d0"><?php echo $text;?></p>

  <?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com

}
?>