<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TQuery extends TDataSet
  {
    var $query;
    var $row;
    var $odb;

    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function __destruct()
    {
    }

    function Init()
    {
      global $main_db;

      parent::Init();

      $this->query=new PDBQuery($this->db);
      $this->query->Name=$this->Name;

      //echo $this->SQL->Text."<br>";

      $this->query->open($this->SQL->Text);
      $this->query->PrepareRemote();

      $this->row=$this->query->Row;

      //echo $this->query->Row;
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      $sql=$this->SQL->Text;

      $sql=str_replace("\"","\\\"",$sql);
      $sql=str_replace(CRLF," ",$sql);

      //$control="<span style=\"border:1px solid red; ".$this->GetStyle()."\">".$this->Name.": SQL->Text=".$sql."</span>";
      $control.="<div id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\">";

      $ret=$control;

      return $ret;
    }

    function Show()
    {
      $this->Get();
    }
  }

?>