<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TLabel extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->AutoSize="True";
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="CAPTION")
        $this->ca->ModifyAttribute($this,"caption",$value);
      else if($attribute=="COLOR")
      {
        $value=mapcolor($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");
          if(o)
            o.style.backgroundColor="{value}";
END;
        //$js=str_replace("{value}",$value,$js);

        $this->ca->exJS($js,$this,$value);
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      return str_replace("{content}",$this->ThemeTemplate->Get(),$this->Template);
    }

    function Show()
    {
      echo $this->Get();
    }
  }

?>