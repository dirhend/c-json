<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TTreeView extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->Color="clWindow";

      $this->Table="STRUKTUR";
      $this->Key="ID";
      $this->StructureKey="ID_STRUKTUR";
      $this->OrderKey="R";
    }

    function Init()
    {
      global $main_db;

      parent::Init();

      $this->db=$main_db;
    }

    function Action($object,$action,$action_value)
    {
      if($this->Name==$object)
      {
        $subtree=$this->GetTree($action_value);
        if($subtree)
        {
          $subtree=str_replace("'","\\'",$subtree);

          $js=<<<END

            var o=document.getElementById('{name}Childs{action_value}');
            var o_ioc_img=document.getElementById('{name}ItemOpenCloseImage{action_value}');

            if(o.style.display=='none')
            {
              o.innerHTML='{subtree}';
              o.style.display='';
              o_ioc_img.innerHTML="&nbsp;-&nbsp;";
            }
            else
            {
              o.style.display='none';
              o_ioc_img.innerHTML="&nbsp;+&nbsp;";
            }

END;

          $js=str_replace("{name}",$this->Name,$js);
          $js=str_replace("{action_value}",$action_value,$js);
          $js=str_replace("{subtree}",$subtree,$js);

          $this->ca->js(utf8_encode($js));
        }
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      $type="text";
      if($this->PasswordChar)
        $type="password";

      $value=""; //$this->dataset->row[$this->DataField];

      $control="<div id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\" style=\"overflow:scroll; border:2px inset; ".$this->GetStyle()."\">{content}</div>";

      $js=<<<END

      <script language="javascript">

        function {name}OpenClose(id)
        {
          {server}.Request("object={name}&action=oc&action_value="+id);
        }

      </script>

END;
      $tree=$this->GetTree(-1);
      $js=str_replace("{name}",$this->Name,$js);
      $ret=str_replace("{content}",$control,$this->Template);
      $ret=str_replace("{content}",$tree.$js,$ret);

      return $ret;
    }

    function GetTree($id)
    {
      $sql="select * from ".$this->Table." where ".$this->StructureKey."=".$id." order by ".$this->OrderKey;
      $this->db->query($sql);

      $tree="";
      while($this->db->fetch())
      {
        $db=new PDB();
        $sql="select count(*) anz from ".$this->Table." where ".$this->StructureKey."=".$this->db->Row[$this->Key];
        $db->queryandfetch($sql);
        $anz=$db->Row['ANZ'];

        $tree.="<div id=\"{name}Item".$this->db->Row[$this->Key]."\">";

        if($anz)
          $tree.="  <a style=\"width:15px;\" id=\"{name}ItemOpenCloseImage".$this->db->Row[$this->Key]."\" href=\"javascript:{name}OpenClose(".$this->db->Row[$this->Key].")\">&nbsp;+&nbsp;</a>";
        else
          $tree.="  <span style=\"width:15px;\">&nbsp;&nbsp;&nbsp;</span>";
        $tree.="".
               "  ".$this->db->Row['BEZ'].
               "  <div id=\"{name}Childs".$this->db->Row[$this->Key]."\" style=\"display:none; padding-left:16px;\"></div>".
               "</div>";
      }

      $tree=str_replace("{name}",$this->Name,$tree);

      return $tree;
    }

    function Show()
    {
      echo $this->Get();
    }

  }

?>