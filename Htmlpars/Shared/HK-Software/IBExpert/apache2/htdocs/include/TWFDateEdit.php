<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDateEdit extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->BorderStyle="bsSingle";
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="TEXT" || $attribute=="DATE")
      {
        $value=substr($value,8,2).".".substr($value,5,2).".".substr($value,0,4);
        $this->ca->ModifyAttribute($this,"value",$value);
      }
    }

    function Get()
    {
      parent::Get();

      if($this->Date)
        $this->Text=$this->Date;
      else
        $this->Text=substr($this->Text,8,2).".".substr($this->Text,5,2).".".substr($this->Text,0,4);

      if(!$this->Font)
        $this->Font=new TFont();

      $this->onEvents['onChange'].=$this->Name."OnChange(this);";

      if($this->CharCase=="ecUpperCase")
        $this->onEvents['onKeyUp'].="this.value=this.value.toUpperCase();";
      else if($this->CharCase=="ecLowerCase")
        $this->onEvents['onKeyUp'].="this.value=this.value.toLowerCase();";

      $js=<<<END


END;
      $control=$this->ThemeTemplate->Get();

      if($this->Password!="True")
        $control=str_replace("{type}","text",$control);
      else
        $control=str_replace("{type}","password",$control);

      $ret=str_replace("{content}",$control.$js,$this->Template);

      return $ret;
    }

    function Show()
    {
      echo $this->Get();
    }

  }

?>