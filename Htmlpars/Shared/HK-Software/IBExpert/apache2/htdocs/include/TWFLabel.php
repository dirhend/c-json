<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFLabel extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->AutoSize="True";
      $this->WordWrap="False";
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="CAPTION")
        $this->ca->ModifyAttribute($this,"caption",$value);
      else if($attribute=="COLOR")
      {
        $value=mapcolor($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");
          if(o)
            o.style.backgroundColor="{value}";
END;
        //$js=str_replace("{value}",$value,$js);

        $this->ca->exJS($js,$this,$value);
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      $component=$this->ThemeTemplate->Get();
      $wrap="nowrap";
      if($this->WordWrap=="True")
        $wrap="wrap";

      $component=str_replace("{wrap}",$wrap,$component);
      
      return str_replace("{content}",$component,$this->Template);
    }

    function Show()
    {
      echo $this->Get();
    }
  }

?>