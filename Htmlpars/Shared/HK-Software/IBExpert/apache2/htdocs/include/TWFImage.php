<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFImage extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Action($object,$action)
    {
      parent::Action($object,$action);
    }

    function Set($property,$value)
    {
      parent::Set($property,$value);

      //if($property=="PICTURE")
      //  $this->ca->ModifyAttribute($this,"value",$value,$coding);
    }

    function Get()
    {
      parent::Get();

      $ret=$this->Template;

      if(!$this->Font)
        $this->Font=new TFont();

      $control=$this->ThemeTemplate->Get();

      $hex=$this->Picture;

      $pos1=strpos($hex,"00");
      while(substr($hex,$pos1,2)=="00")
        $pos1+=2;
      $hex=substr($hex,$pos1);

      //gif
      //$hex=substr($this->Picture,28);
      //jpeg
      //$hex=substr($this->Picture,30);
      //bmp
      //$hex=substr($this->Picture,24);

      $len=strlen($hex);

      $src="blob/".$this->Name."_".time().".picture";
      $fh=fopen($src,"wb+");
      fwrite($fh,pack("H".$len, $hex));
      fclose($fh);

      $image=str_replace("{content}",$control,$ret);
      $ret=str_replace("{src}",$src,$image);
      //echo "picture:".$this->Picture."<br>";

      return $ret;
    }



  }

?>