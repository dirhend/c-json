<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBGridColumns extends TObject
  {
    function __construct($owner)
    {
      parent::__construct($owner);
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      $arr=explode("_",$attribute);

      if($arr[0] && $arr[1])
      {
        if(substr($arr[0],0,5)=="ITEMS")
        {
          $items=$arr[0];
          $pos1=strpos($items,"[");
          $pos2=strpos($items,"]",$pos1);

          $index=substr($items,$pos1+1,$pos2-$pos1-1);

          if($arr[1]=="WIDTH")
          {
            $js=<<<END

            WFDBGrid{parent_name}.SetColWidth({index},{width});
END;
           $js=str_replace("{parent_name}",$this->Parent->Name,$js);
           $js=str_replace("{index}",$index,$js);
           $js=str_replace("{width}",$value,$js);

           $this->ca->js($js);

          }
          else if($arr[1]=="VISIBLE")
          {
            $js=<<<END

            WFDBGrid{parent_name}.SetColVisible({index},"{visible}");
END;
           $js=str_replace("{parent_name}",$this->Parent->Name,$js);
           $js=str_replace("{index}",$index,$js);
           $js=str_replace("{visible}",$value,$js);

           $this->ca->js($js);

          }
        }
      }
    }
  }

?>