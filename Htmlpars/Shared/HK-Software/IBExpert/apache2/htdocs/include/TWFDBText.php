<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBText extends TControl
  {
    var $datasource;
    var $dataset;

    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->HTML="False";
      $this->ScrollBars="False";
    }

    function Init()
    {
      parent::Init();

      if($this->DataSource)
        $this->datasource=$this->GetComponentByName($this->DataSource);
      if($this->datasource->DataSet)
        $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="update")
      {
        $this->Set("LINES_TEXT",$value);
      }
      else if($attribute=="LINES_TEXT")
      {
        if($this->HTML=="False")
        {
          $value=str_replace("<","&lt;",$value);
          $value=str_replace(">","&gt;",$value);
          $value=str_replace(CRLF,"<br>",$value);
          $value=str_replace(" ","&nbsp;",$value);
        }

        $this->ca->ModifyAttribute($this,"caption",$value);
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      return str_replace("{content}",$this->ThemeTemplate->Get(),$this->Template);
    }

  }

?>