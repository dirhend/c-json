<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFFrame extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->Color=mapcolor("clBtnFace");
    }

    function Init()
    {
      parent::Init();
    }

    function Set($property,$value)
    {
      parent::Set($property,$value);

      if($property=="COLOR")
      {
        $js=<<<END

          var o=document.getElementById("{NAME}");
          o.style.backgroundColor="{value}";
END;
        $js=str_replace("{NAME}",strtoupper($this->Name),$js);
        $js=str_replace("{value}",strtoupper($value),$js);

        $this->ca->JS($js);
      }
      else if($property=="SRC")
      {
        $js=<<<END
          var o=document.getElementById("{name}");
          o.src="{value}";

END;
        $js=str_replace("{name}",$this->Name,$js);
        $js=str_replace("{value}",strtoupper($value),$js);

        $this->ca->JS($js);
      }
    }

    function Get()
    {
      parent::Get();

      $ret=$this->Template;

      //die($this->ThemeTemplate->Get());
      if(!$this->Font)
        $this->Font=new TFont();

      $control=str_replace("{content}",$this->ThemeTemplate->Get(),$ret);

      return str_replace("{content}",$this->ShowComponents(),$control);
    }
  }

?>