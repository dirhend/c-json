<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  include_once("db.php");

  class PDB
  {
     var $db;
     var $sql;
     var $result;
     var $result_backup;
     var $fields;
     var $is_trans;
     var $trans;
     var $s_sdb;
     var $Row;
     var $queried;

     var $remotedata=array();
     var $remotecursor;
     var $max_remotecursor;

     var $databasename;
     var $username;
     var $password;

     function PDB($id="0",$sql="")
     {
       global $s_wdb,$s_db_reg_hash;

       if($id=="0" && $s_db_reg_hash)
         $id=$s_db_reg_hash;

       $this->s_wdb=$id;
       $s_wdb=$this->s_wdb;

       if($id>-1)
       {
         $this->db=db_connect(1);

         if($sql)
           $this->query($sql);
       }

       $this->is_trans=false;
     }

     function Connect($DatabaseName,$username,$password)
     {
       $param['id_database']="firebird";
       $param['file']=$DatabaseName;
       $param['user']=$username;
       $param['pass']=$password;

       $this->username=$username;
       $this->password=$password;
       $this->databasename=$DatabaseName;

       $this->db=ex_db_connect("firebird",$param);

       if($sql)
         $this->query($sql);

       $this->is_trans=false;
     }

     function PrepareRemoteNav()
     {
       $this->remotecursor=0;
       $this->max_remotecursor=0;
       while($this->fetch())
         $this->remotedata[$this->max_remotecursor++]=$this->Row;
     }

     function DBNavFirst()
     {
       $this->remotecursor=0;

       $this->Row=$this->remotedata[$this->remotecursor];
     }

     function DBNavPrev()
     {
       if($this->remotecursor>0)
         $this->remotecursor--;

       $this->Row=$this->remotedata[$this->remotecursor];
     }

     function DBNavNext()
     {
       if($this->remotecursor<$this->max_remotecursor-1)
         $this->remotecursor++;

       $this->Row=$this->remotedata[$this->remotecursor];
     }

     function DBNavLast()
     {
       $this->remotecursor=$this->max_remotecursor-1;

       $this->Row=$this->remotedata[$this->remotecursor];
     }

     function GetNewID($gen_name="")
     {
       return db_get_new_id($this->db,$gen_name);
     }

     function PrepareBlob($value)
     {
       return db_prepare_blob($value);
     }

     function GetID($id_name)
     {
       return $this->GetStatID($id_name);
     }

     function GetStatID($id_name)
     {
       return db_get_id($this->db,$id_name);
     }

     /*
     function Clone()
     {
       return new PDB($this->s_wdb);
     }
     */

     function query($param="")
     {
       if($param)
         $this->sql=$param;

       $l=$this->db;

       if ($this->trans)
         $l['db']=$this->trans;

       if($this->result=db_query($l, $this->sql))
         $this->queried=true;

       $ret=$this->result;

       return $ret;
       //$this->result_backup=db_query($this->db, $this->sql);
     }

     function BlobQuery($param,$tabelle)
     {
       if($param)
         $this->sql=$param;

       $l=$this->db;

       if ($this->trans)
         $l['db']=$this->trans;

       if($ret=$this->result=db_blob_query($l, $this->sql, $tabelle))
         $this->queried=true;

       return $ret;
     }

     function QueryAndFetch($param="")
     {
       $this->query($param);
       return $this->fetch();
     }

     function fields()
     {
       return db_fields($this->result);
     }

     function fetch()
     {
       $ret=db_fetch($this->result);
       $this->Row=$ret;

       return $ret;
     }

     function StartTransaction($trans_arg=IBASE_DEFAULT)
     {
       $trans=ibase_trans($trans_arg, $this->db['db']);

       $this->trans=$trans;
       $this->is_trans=true;
     }

     function Commit()
     {
       if($this->trans)
         $l=$this->trans;
       else
         $l=$this->db['db'];

       $commit=ibase_commit($l);
       $this->is_trans=1;
     }

     function Rollback()
     {
       if($this->trans)
         $l=$this->trans;
       else
         $l=$this->db['db'];

       $rollback=ibase_rollback($l);
       $this->is_trans=2;
     }



  }
?>