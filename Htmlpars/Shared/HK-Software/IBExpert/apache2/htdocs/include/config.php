<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  $script=$_SERVER['SCRIPT_FILENAME'];
  $script_name=$_SERVER['SCRIPT_NAME'];
  $script_name=$_SERVER['REQUEST_URI'];

  //$script="/var/www/test/index.php";
  //$script_name="/test/index.php?id=test01";

  if(strpos($script,"/"))
    $ch="/";
  else
    $ch="\\";

  $pos1=strpos($script,$ch);
  $pos2=$pos1;
  while($pos1)
  {
    $pos2=$pos1;
    $pos1=strpos($script,$ch,$pos1+1);
  }

  $script_dir=substr($script,0,$pos2);

  if($license)
  {
    $file="db_reg/db_reg_".$license.".php";

    if(file_exists($file))
      include($file);
    else
      die("unknown license");
  }

  if(file_exists("db_reg/db_main_reg.php"))
  {
    include("db_reg/db_main_reg.php");

    $db_index=0;
    $c['db'][$db_index]['db_user']=$db_main_username[$license];
    $c['db'][$db_index]['db_pass']=$db_main_password[$license];
  }

  $pos1=strpos(" ".$script,"/");
  if(!$pos1)
    $pos1=strpos($script,"\\");
  $pos2=$pos1;
  while($pos1)
  {
    $pos2=$pos1;
    $pos1=strpos($script,"/",$pos2+1);
    if(!$pos1)
      $pos1=strpos($script,"\\",$pos2+1);
  }
  $dir=substr($script,0,$pos2);

  //echo $dir." ($script)"; die();

  $pos1=strpos(" ".$script_name,"/");
  $pos2=$pos1-1;
  while($pos1)
  {
    $pos2=$pos1;
    $pos1=strpos($script_name,"/",$pos1+1);
  }

  $url_dir=substr($script_name,0,$pos2);

  if($url_dir=="/" || $url_dir=="\\")
    $url_dir="";

  $spaw_dir=$url_dir."/include/spaw/";
  $spaw_root=$dir."/include/spaw/";

?>