<?php

  class TWFDBButton extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->Direction="bdDownload";
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
    }

    function Action($object,$action)
    {
      parent::Action($object,$action);

      if($object==$this->Name)
      {

        if($action=="OnClick")
        {
          $this->ca->PostFormData($this->FormName);  // um automatische Verfollständigung von Formularfeldern im Browser mit Daten zu füttern

          if($this->OnClickScript)
          {
            $script="scripts/".$this->OnClickScript;
            if(file_exists("scripts/".$this->OnClickScript))
            {
              require("include/scripts_header.php");
              require($script);
            }
            else
              $this->ca->alert($script." existiert nicht!");
          }
        }


        if($action=="OnClick" && $this->Direction=="bdDownload")
        {
          $filename="default.bin";

          $data=$this->dataset->query->Row[$this->DataField];
          if($this->FilenameField)
            if($this->dataset->query->Row[$this->FilenameField])
              $filename=$this->dataset->query->Row[$this->FilenameField];

          /*
          $now=gmdate("D, d M Y H:i:s")." GMT";
          header("Content-Type: application/octet-stream");
          header("Content-length: ".strlen($data));
          header("Expires: ".$now);
          header("Content-Disposition: attachment; filename=\"".$filename."\"");
          header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
          header("Pragma: public");

          echo $data;
          */

          if(strlen($data))
          {
            $now=gmdate("D, d M Y H:i:s")." GMT";

            header("Content-Type: application/octet-stream");
            header("Content-Length: ".strlen($data));
            header("Expires: ".$now);
            header("Content-Disposition: attachment; filename=\"".$filename."\"");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Pragma: public");

            echo $data;
            die();
          }
        }
      }
    }

    function Click()
    {
      $js=<<<END

        var o=document.getElementById('{NAME}');
        o.click();

END;

      $this->ca->exJS($js,$this);
    }

    function Set($property,$value)
    {
      parent::Set($property,$value);

      if($property=="CAPTION")
        $this->ca->ModifyAttribute($this,"value",$value,$coding);
    }

    function Get()
    {
      parent::Get();

      $ret=$this->Template;

      if(!$this->Font)
        $this->Font=new TFont();

      $control=$this->ThemeTemplate->Get();

      /*
      $control=<<<END

      <input id="{name}" type="button" style="border-style:outset; border-width:2px; {style}" value="{caption}">

END;

      $control=str_replace("{name}",strtoupper($this->Name),$control);
      $control=str_replace("{style}",$this->GetStyle(),$control);
      $control=str_replace("{caption}",$this->Caption,$control);
      */

      $ret=str_replace("{content}",$control,$ret);
      if($this->Direction=="bdDownload")
      {
        $ret=str_replace("{type}","button",$ret);
      }
      else
      {
        $ret=str_replace("{type}","file",$ret);
      }

      return $ret;
    }



  }

?>