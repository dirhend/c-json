<?php

// IBExpertWebForms
// copyright  ©  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFButton extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->Color=mapcolor("clBtnFace");
    }

    function Action($object,$action)
    {
      parent::Action($object,$action);

      if($object==$this->Name)
      {
        if($action=="OnClick")
        {
          $this->ca->PostFormData($this->FormName);  // um automatische Verfollständigung von Formularfeldern im Browser mit Daten zu füttern

          if($this->OnClickScript)
          {
            $script="scripts/".$this->OnClickScript;
            if(file_exists("scripts/".$this->OnClickScript))
            {
              require("include/scripts_header.php");
              require($script);
            }
            else
              $this->ca->alert($script." existiert nicht!");
          }
        }
      }
    }

    function Click()
    {
      $js=<<<END

        var o=document.getElementById('{NAME}');
        o.click();

END;

      $this->ca->exJS($js,$this);
    }

    function Set($property,$value)
    {
      parent::Set($property,$value);

      if($property=="CAPTION")
        $this->ca->ModifyAttribute($this,"value",$value,$coding);
    }

    function Get()
    {
      parent::Get();

      $ret=$this->Template;

      if(!$this->Font)
        $this->Font=new TFont();

      $control=$this->ThemeTemplate->Get();

      /*
      $control=<<<END

      <input id="{name}" type="button" style="border-style:outset; border-width:2px; {style}" value="{caption}">

END;

      $control=str_replace("{name}",strtoupper($this->Name),$control);
      $control=str_replace("{style}",$this->GetStyle(),$control);
      $control=str_replace("{caption}",$this->Caption,$control);
      */

      $ret=str_replace("{content}",$control,$ret);

      return $ret;
    }



  }

?>