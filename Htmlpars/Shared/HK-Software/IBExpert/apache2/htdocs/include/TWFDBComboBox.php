<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBComboBox extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->BorderStyle="bsSingle";
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
    }

    function Set($property,$value)
    {
      if($property=="TEXT")
        $this->ca->ModifyAttribute($this,"setter",$value,$coding);
      else if($property=="update")
      {
        //$this->ca->Alert($this->dataset);
        $this->Set("TEXT",$this->dataset->row[$this->DataField]);
      }
    }

    function Get()
    {
      parent::Get();

      $control=$this->ThemeTemplate->Get();

      $js=<<<END
        <script language="javascript">

          var {name}Options=new Array();

END;

      for($i=0;$i<count($this->Items->Strings);$i++)
      {
        $js.=<<<END

        {name}Options[{i}]="{value}";

END;

        $js=str_replace("{i}",$i,$js);
        $js=str_replace("{value}",$this->Items->Strings[$i],$js);
      }

      $js.=<<<END

        function {name}Setter(value)
        {
          var o=document.getElementById("{name}");
          var sel_index=0;

          while(o.length)
            o.options[o.length-1]=null;

          for(ii=0;ii<{name}Options.length;ii++)
          {
            if({name}Options[ii]==value)
              sel_index=ii;
          }

          if(value && sel_index==0)
          {
            oe=new Option(value,value,false,false);
            o.options[o.length]=oe;
          }

          for(ii=0;ii<{name}Options.length;ii++)
          {
            oe=new Option({name}Options[ii],{name}Options[ii],false,false);
            o.options[o.length]=oe;
          }

          if(sel_index>0)
            o.selectedIndex=sel_index;
        }

      </script>

END;

      $js=str_replace("{name}",strtoupper($this->Name),$js);

      $control=str_replace("{content}",$control.$js,$this->Template);
      $ret=str_replace("{options}",$options,$control);

      return $ret;
    }

  }

?>