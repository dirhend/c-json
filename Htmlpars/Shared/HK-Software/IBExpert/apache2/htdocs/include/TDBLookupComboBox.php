<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TDBLookupComboBox extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);

      $this->listdatasource=$this->GetComponentByName($this->ListSource);
      $this->listdataset=$this->GetComponentByName($this->listdatasource->DataSet);

      //echo $this->listdataset;
    }

    function Set($property,$value)
    {
      if($property=="LOOKUP")
        $this->ca->ModifyAttribute($this,"lookup",$value,$coding);
    }

    function Get()
    {
      parent::Get();

      $control=$this->ThemeTemplate->Get();

      $datafield_lookup=$this->dataset->row[$this->DataField];

      for($i=0;$i<count($this->listdataset->query->remotedata['rows']);$i++)
        if($datafield_lookup==$this->listdataset->query->remotedata['rows'][$i][$this->KeyField])
          $datafield_ListField=$this->listdataset->query->remotedata['rows'][$i][$this->ListField];

      //echo $datafield_ListField;

      $keyfield_lookup=$this->dataset->row[$this->DataField];
      //echo $this->listdataset->query->remotedata['cursor'];

      //echo $this->DataField;
      //echo $this->KeyField;
      //echo $this->ListField;


      $js=<<<END
        <script language="javascript">

          var {name}Options=new Array();
          var datafield_lookup="{datafield_lookup}";
END;

      for($i=0;$i<count($this->listdataset->query->remotedata['rows']);$i++)
      {
        $js.=<<<END

        {name}Options[{i}]=new Array();
        {name}Options[{i}]['key']="{key}";
        {name}Options[{i}]['value']="{value}";

END;

        $js=str_replace("{i}",$i,$js);
        $js=str_replace("{key}",$this->listdataset->query->remotedata['rows'][$i][$this->KeyField],$js);
        $js=str_replace("{value}",$this->listdataset->query->remotedata['rows'][$i][$this->ListField],$js);
      }

      $js.=<<<END

        function {name}Setter(value)
        {
          var o=document.getElementById("{name}");

          var sel_index=0;
          for(ii=0;ii<{name}Options.length;ii++)
           if(value=={name}Options[ii]['key'])
              sel_index=ii+1;

          o.selectedIndex=sel_index;
        }

        var o=document.getElementById("{name}");

        oe=new Option("","",false,false);
        o.options[o.length]=oe;

        for(ii=0;ii<{name}Options.length;ii++)
        {
          oe=new Option({name}Options[ii]['value'],{name}Options[ii]['key'],false,false);
          o.options[o.length]=oe;
        }


      </script>

END;

      $js=str_replace("{datafield_lookup}",$datafield_lookup,$js);

      $js=str_replace("{name}",strtoupper($this->Name),$js);

      $control=str_replace("{content}",$control.$js,$this->Template);
      $ret=str_replace("{options}",$options,$control);

      return $ret;
    }

  }

?>