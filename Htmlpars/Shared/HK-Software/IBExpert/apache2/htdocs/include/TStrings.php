<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TStrings
  {
    var $Text;
    var $HTMLText;
    var $Strings=Array();
    var $length;

    function TStrings()
    {
    }

    function SetStrings($arr)
    {
      $this->Strings=$arr;

      $this->Text="";
      $this->HTMLText="";

      for($i=0;$i<count($arr);$i++)
      {
        $this->Text.=$arr[$i].CRLF;
        $this->HTMLText.=$arr[$i].CRLF;
      }

      $this->length=count($arr);
    }

  }

?>