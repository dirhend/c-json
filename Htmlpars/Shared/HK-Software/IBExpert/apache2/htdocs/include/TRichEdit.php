<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TRichEdit extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Get()
    {
      parent::Get();

      $params="";
      if($this->ScrollBars=="ssBoth" ||
         $this->ScrollBars=="ssHorizontal")
        $params.="wrap=\"off\"";

      //$uret="<textarea id=\"".strtoupper($this->Name)."\" ".$params." style=\"".$this->GetStyle()."\">".$this->Lines->HTMLText."</textarea>";

      include_once("include/spaw/spaw_control.class.php");

      $value="";
      $value=str_replace("\\","\\\\\\\\",$value);
      $sw=new SPAW_Wysiwyg($this->Name,$value,'de','full','default',$this->Width,$this->Height);

      $control=$sw->getHtml();

      $ret=str_replace("{content}",$control,$this->Template);

      return $ret;
    }

  }

?>