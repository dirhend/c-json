<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TComponents
  {
     var $Length;
     var $Parent;
     var $Components=array();

     function __construct($parent)
     {
       $this->Length=0;
       $this->Parent=$parent;
     }

     function Add($component)
     {
       $this->Components[$this->Length++]=$component;
     }
  }

?>