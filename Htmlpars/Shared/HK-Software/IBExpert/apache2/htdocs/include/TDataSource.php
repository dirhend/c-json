<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TDataSource extends TComponent
  {
    var $dataset;

    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Init()
    {
      if($this->DataSet)
        $this->dataset=$this->GetComponentByName($this->DataSet);
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      //$control="<span style=\"border:1px solid red; ".$this->GetStyle()."\">".$this->Name.": DataSet=".$this->DataSet."</span>";
      $control.="<div id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\">";

      $ret=$control;

      return $ret;
    }

    function Show()
    {
      echo $this->Get();
    }
  }

?>