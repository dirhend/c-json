<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TIBQuery extends TDataSet
  {
    var $query;
    var $row;
    var $odb;

    function __construct($owner=null)
    {
      $this->AddSaveProperty("Active");

      parent::__construct($owner);

      $this->odb=false;

      //$this->ReadOnly=true;

      //echo "construct.query (".$this.")<br>";

      //if(!$this->odb)
      //  $this->Database="Database1";

    }

    function Init()
    {
      parent::Init();

      if($this->DataSource)
        $this->datasource=$this->GetComponentByName($this->DataSource);

      if($this->Database)
        $this->database=$this->GetComponentByName($this->Database);

      if($this->database)
      {
        $this->database->Init();

        $this->query=new PDBQuery($this->database->db,$this);
        $this->query->Name=$this->Name;

        if($this->Active=="True" && !$this->datasource)
          $this->query->open($this->SQL->Text);

        $this->query->PrepareRemote();
        $this->row=$this->query->Row;
      }
    }

    function Set($property,$value="")
    {
      parent::Set($property,$value);

      if($property=="SQL_TEXT")
      {
        $this->SQL->Text=$value;
        $this->Refresh=true;
        if($this->Active=="True")
          $this->query->open($this->SQL->Text);
        $this->query->PrepareRemote(true);
        $this->First();
      }
      else if($property=="ACTIVE")
      {
        /*
        $this->SQL->Text=$value;
        $this->query->open($this->SQL->Text);
        $this->query->PrepareRemote(true);
        $this->First();
        */
      }
      else if($property=="update")
      {
        if($this->datasource)
        {
          $sql=$this->GetSQL();

          if($sql && $this->Active=="True")
          {
            //$this->ca->Alert($this->Name."->Open(".$sql.")");
            $this->Open($sql);
          }
          else
            $this->Close();
        }
      }
    }

    function GetSQL()
    {
      if($this->datasource)
      {
        $ok=true;

        $sql=$this->SQL->Text;
        //$this->ca->Alert("init: ".$this->SQL->Text);
        for($i=0;$i<count($this->ParamData->Items);$i++)
        {
          $this->datasource->dataset->query->GetCurrentRow();
          if(!$this->datasource->dataset->row[$this->ParamData->Items[$i]->Name])
            $ok=false;

          $sql=str_replace(":".$this->ParamData->Items[$i]->Name,$this->datasource->dataset->row[$this->ParamData->Items[$i]->Name],$sql);

          /*
          if(!$this->datasource->dataset->query->Row[$this->ParamData->Items[$i]->Name])
            $ok=false;

          $sql=str_replace(":".$this->ParamData->Items[$i]->Name,$this->datasource->dataset->query->Row[$this->ParamData->Items[$i]->Name],$sql);
          */

          //$this->ca->Alert("work: :".$this->ParamData->Items[$i]->Name."=".$this->datasource->dataset->row[$this->ParamData->Items[$i]->Name]);

        }

        //$this->ca->Alert("fertig: ".$sql);
      }
      else
      {
        $ok=true;
        $sql=$this->SQL->Text;
      }

      //$this->ca->Alert($this->Name."GetSQL(".$sql.")");
      if($ok)
        return $sql;
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      $sql=$this->SQL->Text;

      $sql=str_replace("\"","\\\"",$sql);
      $sql=str_replace(CRLF," ",$sql);

      //$control="<span style=\"border:1px solid red; ".$this->GetStyle()."\">".$this->Name.": SQL->Text=".$sql."</span>";
      $control.="<div id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\">";

      $ret=$control;

      return $ret;
    }

    function Show()
    {
      $this->Get();
    }
  }

?>