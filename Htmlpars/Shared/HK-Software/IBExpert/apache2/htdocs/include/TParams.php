<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TParams extends TObject
  {
    var $length;

    function __construct()
    {
    }

    function SetStrings($arr)
    {
      $this->Strings=$arr;

      $this->Text="";
      $this->HTMLText="";

      for($i=0;$i<count($arr);$i++)
      {
        $this->Text.=$arr[$i].CRLF;
        $this->HTMLText.=$arr[$i].CRLF;
      }

      $this->length=count($arr);
    }

  }

?>