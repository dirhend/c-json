<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TRadioButton extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->BorderStyle="bsSingle";
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="COLOR")
      {
        $value=mapcolor($value);

        $js.=<<<END
          var o=document.getElementById("C{NAME}");
          if(o)
            o.style.backgroundColor="{value}";
END;
        $js=str_replace("{NAME}",strtoupper($this->Name),$js);
        $js=str_replace("{value}",$value,$js);

        $this->ca->JS($js);
      }
      else if($attribute=="CHECKED")
      {
        $js.=<<<END
          var o=document.getElementById("{NAME}");

          if(o)
            o.checked={value};
END;
        $js=str_replace("{NAME}",strtoupper($this->Name),$js);
        $js=str_replace("{value}",$value,$js);

        $this->ca->JS($js);
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      $ret=$this->Template;

      $control=$this->ThemeTemplate->Get();
      if($this->Checked)
        $checked="checked";

      $control=str_replace("{checked}",$checked,$control);

      $ret=str_replace("{content}",$control,$ret);

      return $ret;
    }

  }

?>