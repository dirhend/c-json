<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TComboBox extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Get()
    {
      parent::Get();

      for($i=0;$i<count($this->Items->Strings);$i++)
        $options.="<option>".$this->Items->Strings[$i]."</option>";

      $control=str_replace("{options}",$options,$this->ThemeTemplate->Get());

      return str_replace("{content}",$control,$this->Template);
    }

  }

?>