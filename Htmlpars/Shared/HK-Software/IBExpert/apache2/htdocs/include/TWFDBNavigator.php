<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBNavigator extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
    }

    function Set($property,$value)
    {
      parent::Set($property,$value);

      $js=<<<END

        var oFirst=document.getElementById("idDBNavigator{name}First");
        var oPrev=document.getElementById("idDBNavigator{name}Prev");
        var oNext=document.getElementById("idDBNavigator{name}Next");
        var oLast=document.getElementById("idDBNavigator{name}Last");
        var oCommit=document.getElementById("idDBNavigator{name}Commit");

        if(oCommit)
        {
          if({writeable})
            oCommit.disabled="";
          else
            oCommit.disabled="true";
        }

        if({cursor}>0)
        {
          if(oFirst)
            oFirst.disabled="";
          if(oPrev)
            oPrev.disabled="";
        }
        else
        {
          if(oFirst)
            oFirst.disabled="true";
          if(oPrev)
            oPrev.disabled="true";
        }

        if({cursor}<{max_cursor}-1)
        {
          if(oNext)
            oNext.disabled="";
          if(oLast)
            oLast.disabled="";
        }
        else
        {
          if(oNext)
            oNext.disabled="true";
          if(oLast)
            oLast.disabled="true";
        }

END;

      $cursor=$this->dataset->query->remotedata['cursor'];
      $max_cursor=$this->dataset->query->remotedata['max_cursor'];

      if(!$cursor)
        $cursor=0;
      if(!$max_cursor)
        $max_cursor=0;

      if($this->dataset->ReadOnly=="True" || !$this->dataset->query->remotedata)
        $js=str_replace("{writeable}","0",$js);
      else
        $js=str_replace("{writeable}","1",$js);

      $js=str_replace("{name}",$this->Name,$js);
      $js=str_replace("{cursor}",$cursor,$js);
      $js=str_replace("{max_cursor}",$max_cursor,$js);

      $this->ca->JS($js);
    }

    function Get()
    {
      parent::Get();

      if(!$this->VisibleButtons)
        $this->VisibleButtons="[nbFirst, nbPrior, nbNext, nbLast, nbInsert, nbDelete, nbEdit, nbPost, nbCancel, nbRefresh]";

      if(!$this->Font)
        $this->Font=new TFont();

      $button_width=$this->Width/9;

      $buttonstyle="text-decoration:none; font-weight:bold; border-width:2px; text-align:center; width:".$button_width."px; height:".$this->Height."px;";

      $script=<<<END

      <script language="javascript">

        function DBNavigator{name}First()
        {
          {server}.Request("object={name}&action=first");
        }

        function DBNavigator{name}Prev()
        {
          {server}.Request("object={name}&action=prev");
        }

        function DBNavigator{name}Next()
        {
          {server}.Request("object={name}&action=next");
        }

        function DBNavigator{name}Last()
        {
          {server}.Request("object={name}&action=last");
        }

        function DBNavigator{name}Commit()
        {
          //server.location.href="index.php?DBNavigator={name}&action=last";
          var o=document.getElementById('{form}Form');
          document.getElementById('object').value="{name}";
          document.getElementById('action').value="commit";

          o.submit();
        }

      </script>
END;

      $script=str_replace("{name}",$this->Name,$script);
      $script=str_replace("{form}",$this->FormName,$script);

      $vb=$this->VisibleButtons;
      $vb=str_replace("[","",$vb);
      $vb=str_replace("]","",$vb);
      $vb=str_replace(" ","",$vb);
      $vb_arr=explode(",",$vb);

      $count=0;
      for($i=0;$i<count($vb_arr);$i++)
      {
        $fields[$vb_arr[$i]]=1;
        $count++;
      }
      $fields['count']=$count;

      $this->ThemeTemplate->SetFields($fields);

      $buttons=$this->ThemeTemplate->Get();

      $ret=str_replace("{content}",$buttons.$script,$this->Template);

      return $ret;
    }

    function Action($object,$action)
    {
      if($object==$this->Name)
      {
        if($action=="next")
          $this->Next();
        else if($action=="prev")
          $this->Prev();
        else if($action=="first")
          $this->First();
        else if($action=="last")
          $this->Last();
        else if($action=="commit")
          $this->Commit();
      }
    }

    function First()
    {
      $this->dataset->First();
    }

    function Last()
    {
      $this->dataset->Last();
    }

    function Prev()
    {
      $this->dataset->Prev();
    }

    function Next()
    {
      $this->dataset->Next();
    }

    function Commit()
    {
      global $action;

      if($action=="commit")  // es wurde der commit-button von dbnavigator gedr�ckt
        $this->dataset->Commit();
      else
      {
        // der commit-befehl kommt aus der prozedure

        $js=<<<END

          DBNavigator{name}Commit();

END;
        $this->ca->exJS($js,$this);
      }
    }

    function Show()
    {
      echo $this->Get();
    }
  }

?>