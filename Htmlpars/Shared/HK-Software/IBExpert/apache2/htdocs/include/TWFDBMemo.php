<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBMemo extends TControl
  {
    var $datasource;
    var $dataset;

    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->BorderStyle="bsSingle";
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="LINES_TEXT")
        $this->ca->ModifyAttribute($this,"value",$value);

      if($attribute=="update")
      {
        $this->Set("LINES_TEXT",$value);

        $js=<<<END

          var o=document.getElementById("{NAME}");
          if("{readonly}"=="True")
            o.disabled="true";
          else
            o.disabled="";
END;

        $js=str_replace("{NAME}",strtoupper($this->Name),$js);
        $js=str_replace("{readonly}",$this->dataset->ReadOnly,$js);
        $this->ca->JS($js);
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      $params="";
      if($this->ScrollBars=="ssBoth" ||
         $this->ScrollBars=="ssHorizontal")
        $params.="wrap=\"off\"";

      $this->ThemeTemplate->Set("params",$params);
      $control=$this->ThemeTemplate->Get();

      $ret=str_replace("{content}",$control,$this->Template);

      return $ret;
    }

  }

?>