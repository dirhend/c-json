<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TControl extends TComponent
  {
    var $Template;
    var $Events;
    var $db;

    function __construct($owner=null)
    {
      //global $main_db;

      parent::__construct($owner);

      if(!$this->AutoSize)
        $this->AutoSize="False";

      if(!$this->Width)
        $this->Width=28;
      if(!$this->Height)
        $this->Height=28;
      if(!$this->Cursor)
        $this->Cursor="crDefault";

      //$this->Visible="True";
      //$this->db=$main_db;
    }

    function Init()
    {
      global $main_db;

      parent::Init();

      $this->Cursor=mapcursor($this->Cursor);

      if($this->Align=="alTop")
        $this->Anchors="[akLeft, akRight, akTop]";
      if($this->Align=="alBottom")
        $this->Anchors="[akLeft, akRight, akBottom]";
      else if($this->Align=="alLeft")
        $this->Anchors="[akLeft, akTop, akBottom]";
      else if($this->Align=="alRight")
        $this->Anchors="[akRight, akTop, akBottom]";

      if(!$this->Anchors)
        $this->Anchors="[akTop, akLeft]";

      $this->out($this->Name."-init",$this->Name);

      $anchors=$this->Anchors;
      $anchors=str_replace("[","",$anchors);
      $anchors=str_replace("]","",$anchors);
      $anchors=str_replace(" ","",$anchors);
      $anchors_arr=explode(",",$anchors);

      $fields['akLeft']="0";
      $fields['akRight']="0";
      $fields['akTop']="0";
      $fields['akBottom']="0";
      $fields['akWidth']="1";
      $fields['akHeight']="1";

      for($i=0;$i<count($anchors_arr);$i++)
        $fields[$anchors_arr[$i]]="1";

      if($fields['akLeft']=="1" &&
         $fields['akRight']=="1")
        $fields['akWidth']="0";

      if($fields['akTop']=="1" &&
         $fields['akBottom']=="1")
        $fields['akHeight']="0";

      $this->Anchors=$fields;
    }

    function GetFontStyle($font)
    {
      if(!$font)
        $font=$this->Font;

      return "font-size:30px;";
    }

    function GetStyle($styles="",$tcontrol=0)
    {
      $style="";
      if($this->Font)
      {
        $font_style=mapfontstyle($this->Font->Style);

        $style.=$font_style['static'];

        $fontsize=-($this->Font->Height);

        $style.="font-size:".$fontsize."px; ";

        $style.="font-family:".$this->Font->Name."; ";
      }

      $color=$this->Color;
      if($color)
        $color="background-color: ".$color.";";

        $this->Bottom="0";
      $width="";

      if(!$tcontrol)
      {
        if(!strpos(" ".$styles,"-width"))
          $width="width:100%; ";
        if(!strpos(" ".$styles,"-height"))
          $height="height:100%; ";

        if($this->Ctl3D=="False")
          $border="border: 1px solid black;";

        if(!strpos(" ".$styles,"-borderstyle"))
          if($this->BorderStyle)
          {
            $border=mapborderstyle($this->BorderStyle);
            $border="border: ".$border."; ";
          }

        /*
        if(!strpos(" ".$styles,"-width"))
          if($this->Anchors['akWidth'])
            $width="width:".($this->Width)."px; ";
        if($this->Anchors['akHeight'])
          $height="height:".$this->Height."px; ";
        */

        if($this->Cursor)
          $cursor="cursor:".$this->Cursor."; ";

        if(!strpos(" ".$styles,"-left"))
          if($this->Anchors['akLeft'])
            $left="left:0px; ";
        if($this->Anchors['akRight'])
          $right="right:0px; ";
        if($this->Anchors['akTop'])
          $top="top:0px; ";
        if($this->Anchors['akBottom'])
          $bottom="bottom:0px; ";
      }
      else
      {
        if(!strpos(" ".$styles,"-width"))
          if($this->Anchors['akWidth'])
            $width="width:".($this->Width)."px; ";
        if($this->Anchors['akHeight'])
          $height="height:".$this->Height."px; ";
        if($this->Anchors['akLeft'])
          $left="left:".$this->Left."px; ";
        if($this->Anchors['akRight'])
          $right="right:".$this->Right."px; ";
        if($this->Anchors['akTop'])
          $top="top:".$this->Top."px; ";
        if($this->Anchors['akBottom'])
          $bottom="bottom:".$this->Bottom."px; ";

      }

      $this->out($this->Name."-anker",$this->Anchors);

      $style.="position:absolute; ".$cursor.$border.$width.$height.$left.$right.$top.$bottom.$color."; color:".$this->Font->Color.";";

      return $style;
    }

    function out($f,$s)
    {
      /*
      $fh=fopen("debug/".$f.".txt","w");
      fwrite($fh,$s);
      fclose($fh);
      */
    }

    /*
    function Action($object,$action)
    {
      parent::Action($object,$action);
	}
    */

    function Set($property,$value)
    {
      //$this->ca->Alert($property);

      if($property=="LEFT" ||
         $property=="TOP" ||
         $property=="WIDTH" ||
         $property=="HEIGHT")
      {
        //$this->ca->Alert($property);

        $js.=<<<END
          var o=document.getElementById("o{NAME}");
          if(o)
            o.style.{property}="{value}px";
END;
      }
      else if($property=="VISIBLE")
      {
        $value=strtoupper($value);
        if($value=="FALSE" || !$value)
          $value="none";
        else
          $value="";

        $js.=<<<END
          var o=document.getElementById("o{NAME}");
          if(o)
            o.style.display="{value}";

END;
      }
      else if($property=="ENABLED")
      {
        $value=strtoupper($value);
        if($value=="FALSE" || !$value)
          $value="true";
        else
          $value="false";

        $js.=<<<END
          var o=document.getElementById("{NAME}");
          if(o)
            o.disabled={value};
END;
      }
      else if($property=="COLOR")
      {
        $value=mapcolor($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");
          if(o)
            o.style.backgroundColor="{value}";
END;
      }
      else if($property=="CURSOR")
      {
        $value=mapcursor($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");
          if(o)
            o.style.cursor="{value}";
END;
      }
      else if($property=="BORDERSTYLE")
      {
        $value=mapborderstyle($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");

          if(o)
            o.style.border="{value}";
END;
      }
      else if($property=="FONT_COLOR")
      {
        $value=mapcolor($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");

          if(o)
            o.style.color="{value}";
END;
      }
      else if($property=="FONT_SIZE")
      {
        $value=mapfontsize($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");

          if(o)
            o.style.fontSize="{value}px";
END;
      }
      else if($property=="FONT_NAME")
      {
        $value=mapfontsize($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");

          if(o)
            o.style.fontFamily="{value}";
END;
      }
      else if($property=="FONT_STYLE")
      {
        $value=mapfontstyle($value);

        $js.=<<<END
          var o=document.getElementById("{NAME}");

          if(o)
          {
            {commands}
            //o.style.fontFamily="{value}";
          }
END;
        $commands="";
        for($i=0;$i<count($value['dynamic']);$i++)
        {
          $commands.="o.style.".$value['dynamic'][$i].";".CRLF;
        }
        $js=str_replace("{commands}",$commands,$js);

      }
      else if($property=="DFM")
      {
        $js.=<<<END

          var dfmo=document.getElementById("{NAME}");
          //dfmo.innerHTML="moin<hr>zwei daf djflkadsfjkladoes";
          dfmo.innerHTML="{dfmvalue}";
END;

        //include_once("TFormBuilder2.php");
        $dfmbuilder=new TFormBuilder($value,"","standard",$this);

        $dfmvalue=$dfmbuilder->Get(false);

        $dfmvalue=str_replace("\"","\\\"",$dfmvalue);
        $dfmvalue=str_replace(chr(10)," ",$dfmvalue);
        $dfmvalue=str_replace(chr(13)," ",$dfmvalue);

        //$dfmvalue.="<hr>".time();

        $js=str_replace("{dfmvalue}",$dfmvalue,$js);
      }

      $js=str_replace("{NAME}",strtoupper($this->Name),$js);
      $js=str_replace("{name}",$this->Name,$js);
      $js=str_replace("{property}",strtolower($property),$js);
      $js=str_replace("{value}",$value,$js);

      $this->ca->JS($js);
    }

    function Get($propertys="")
    {
      parent::Get($propertys);

      $ControlTemplate=new PTemplate($this,"TControl");
      //$ControlTemplate->debug=1;
      $control_events=$this->GetControlEvents();

      $ControlTemplate->SetFields($this->Anchors);
      $ControlTemplate->SetFields($this->isEvents);

      $overflow="hidden";
      if($this->Parent->AutoSize=="True")
        $overflow="";

      $this->NoComponent="<div style=\"border:1px solid black; ".CRLF.
                      "             overflow:".$overflow."; ".CRLF.
                      "             position:absolute; ".CRLF.
                      "             top:".$this->Top."px; ".CRLF.
                      "             left:".$this->Left."px; ".CRLF.
                      "             width:".$this->Width."px; ".CRLF.
                      "             height:".$this->Height."px; ".CRLF.
                      "             font-size:10px; ".CRLF.
                      "             font-family:Arial;".CRLF.
                      "\"><img width=\"".($this->Width-2)."\" height=\"".($this->Height-2)."\" alt=\"".$this->Name."\"/></div>".CRLF;

      if($this->Visible=="False")
        $this->Display="none";

      /*
      $control_events=str_replace("{name}",$this->Name,$control_events);
      $control_events=str_replace("{NAME}",strtoupper($this->Name),$control_events);
      //$this->Template=str_replace("{control_events}",$control_events,$ControlTemplate->Get("TControl"));
      */

      $this->Template=str_replace("{control_events}",$this->ControlEvents,$ControlTemplate->Get("TControl"));

      return $this->NoComponent;
    }

    function Show()
    {
      echo "ok2";
      echo $this->Get();
    }
  }

?>