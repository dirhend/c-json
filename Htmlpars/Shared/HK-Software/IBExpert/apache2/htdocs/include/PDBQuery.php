<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class PDBQuery
  {
    var $SQL;
    var $db;
    var $Row;

    var $result;
    var $trans;
    var $remotecursor;
    var $max_remotecursor;

    function __construct($db,$owner=null)
    {
      global $ca;

      $this->Parent=$owner;

      //$ca->alert("construct");

      $this->db=$db->db;
    }

    function get_dbimage($v)
    {
      global $ca,$main_db;

      $db=$main_db;

      $pos1=strpos(" ".$v,"{\\rtf");
      if($pos1)
      {
        $rtf2html=new PRTF2HTML();
        $rtf2html->RTF=$v;
        $rtf2html->Convert2HTML();
        $v=$rtf2html->HTML;
      }

      $pos1=strpos(" ".$v,"<dbimg");
      while($pos1)
      {
        $pos2=strpos($v,"/>");

        $is=substr($v,$pos1+6,$pos2-$pos1-6);
        $is_arr=explode(" ",$is);

        for($i=0;$i<count($is_arr);$i++)
        {
          $is_kv=explode("=",$is_arr[$i]);
          $img_data[strtoupper($is_kv[0])]=str_replace("\"","",$is_kv[1]);
        }

        $img=session_id().$img_data['TABLE'].$img_data['IMAGEFIELD'].$img_data['ID'];

        if(!file_exists("blob/".$img))
        {
          //$ca->alert($img);
          $sql="select ".$img_data['IMAGEFIELD']." from ".$img_data['TABLE']." where ".$img_data['IDFIELD']."=".$img_data['ID'];
          $db->query($sql);
          $db->fetch();

          $img_data['DATA']=$db->Row[$img_data['IMAGEFIELD']];

          $fh=fopen("blob/".$img,"wb+");
          fwrite($fh,$img_data['DATA']);
          fclose($fh);

          if($img_data['RENDER'])
          {
            $im=new PImageMagick();
            $im->Type=$img_data['RENDER'];
            $im->Size=$img_data['RENDERSIZE'];
            $im->Source="blob/".$img;
            $im->Dest="blob/".$img.".".$img_data['RENDER'];
            $im->Render();
          }

        }

        $pos1=strpos(" ".$v,"<dbimg",$pos1+2);
      }

      if($img_data['RENDER'])
        $img.=".".$img_data['RENDER'];

      $v=str_replace("<dbimg","<img src=\"blob/".$img."\"",$v);

      return $v;
    }

    function get_dbimage_from_row($row)
    {
      global $ca;

      $arr=$row;

      for($i=0;$i<count($arr);$i++)
      {
        //$ca->alert($ret[key($ret)]);
        $arr[key($arr)]=$this->get_dbimage($arr[key($arr)]);

        next($arr);
      }

      return $arr;
    }

    function file()
    {
      global $dir,$id,$root_dir;

      $file="c:/htdocs/components/session_data/".$this->Name.".data";
      $file=$dir."/session_data/".$this->Name."_".session_id().".data";
      $file=$root_dir."session_data/".$id."_".$this->Name."_".session_id().".data";
      //echo $file."<br>";

      return $file;
    }

    function __destruct()
    {
      global $dir;

      if($this->PreparedRemote)
      {
        $fh=fopen($this->file().'Cursor',"wb+");
        fwrite($fh,serialize($this->remotedata['cursor']));
        fclose($fh);

        $fh=fopen($this->file().'MaxCursor',"wb+");
        fwrite($fh,serialize($this->remotedata['max_cursor']));
        fclose($fh);

        $fh=fopen($this->file().'Fields',"wb+");
        fwrite($fh,serialize($this->remotedata['fields']));
        fclose($fh);

        $fh=fopen($this->file().'SQL',"wb+");
        fwrite($fh,serialize($this->remotedata['sql']));
        fclose($fh);

        //if(!fileexists($this->file().'Cursor'))
        for($i=0;$i<count($this->remotedata['rows']);$i++)
        {
          if($this->remotedata['rows'][$i])
          {
            $fh=fopen($this->file().$i,"wb+");
            if($fh)
            {
              fwrite($fh,serialize($this->remotedata['rows'][$i]));
              fclose($fh);
            }
          }
        }
      }
    }

    function Close()
    {
      global $ca;

      //die("x");
      //$ca->Alert($this->Name."Close()");
      //$this->remotedata['cursor']=10;
      unset($this->remotedata);

      unset($this->Row); //=$this->remotedata['rows'][$row];
      if($this->Parent)
        unset($this->Parent->row); //=$this->Row;
    }

    function fields()
    {
      if($this->PreparedRemote)
      {
        return $this->remotedata['fields'];
      }
      else
      {
        return db_fieldsandblobtype($this->result);
      }
    }

    function Open($sql="")
    {
      global $ca;

      //$ca->Alert("sql: ".$sql);

      $this->sql=$sql;

       $l=$this->db;

       if ($this->trans)
         $l['db']=$this->trans;

       if($this->sql)
         if($this->result=db_query($l, $this->sql))
           $this->queried=true;

       $ret=$this->result;

       return $ret;
       //$this->result_backup=db_query($this->db, $this->sql);
    }

    function GetCurrentRow()
    {
      return $this->GetRow($this->remotedata['cursor']);
    }

    function GetRow($row)
    {
      global $ca;

      if($this->remotedata['cursor']<$this->remotedata['max_cursor'])
      {
        if(!$this->remotedata['rows'][$row])
        {
          $file=$this->file().$row;

          if(file_exists($file))
          {
            $fh=fopen($file,"r");
            $data=fread($fh,filesize($file));
            $this->remotedata['rows'][$row]=unserialize($data);
            fclose($fh);
          }

          //die($this->remotedata['rows'][$row]['TEXT1']);
        }
      }
      else
        unset($this->remotedata['rows'][$row]);

      $this->Row=$this->remotedata['rows'][$row];
      if($this->Parent)
        $this->Parent->row=$this->Row;

      return $this->remotedata['rows'][$row];
    }

    function RefreshRemote($row)
    {
      for($i=0;$i<count($this->Fields);$i++)
        $this->remotedata['rows'][$this->remotedata['cursor']][$this->Fields[$i]['alias']]=$row[$this->Fields[$i]['alias']];

      $i=$this->remotedata['cursor'];
      $fh=fopen($this->file().$i,"wb+");
      fwrite($fh,serialize($this->remotedata['rows'][$i]));
      fclose($fh);
    }

    function PrepareRemote($newfetch=false)
    {
      global $refresh,$ca,$dir;

      $file=$this->file();

      if(file_exists($file.'Cursor') && !$refresh && !$newfetch)
      {
        if(filesize($file.'Cursor'))
        {
          $d=0;

          if($d)
          echo "c:".$this->remotedata;

          $fh=fopen($file.'Cursor',"r");
          $data=fread($fh,filesize($file.'Cursor'));
          $this->remotedata['cursor']=unserialize($data);
          fclose($fh);

          $fh=fopen($file.'MaxCursor',"r");
          $data=fread($fh,filesize($file.'MaxCursor'));
          $this->remotedata['max_cursor']=unserialize($data);
          fclose($fh);

          $fh=fopen($file.'Fields',"r");
          $data=fread($fh,filesize($file.'Fields'));
          $this->remotedata['fields']=unserialize($data);
          fclose($fh);

          $fh=fopen($file.'SQL',"r");
          $data=fread($fh,filesize($file.'SQL'));
          $this->remotedata['sql']=unserialize($data);
          fclose($fh);

          for($i=0;$i<$this->remotedata['max_cursor'];$i++)
          {
            //$this->GetRow($i);
            /*
            $fh=fopen($file.$i,"r");
            $data=fread($fh,filesize($file.$i));
            $this->remotedata['rows'][$i]=unserialize($data);
            fclose($fh);
            */
          }

          //die($file."-".$data." (".strlen($data).")");
          //die($file);

          $this->Row=$this->GetCurrentRow();
          $this->Fields=$this->remotedata['fields'];

          if($d)
          echo "c:".$this->remotedata['cursor'];
          if($d)
          echo "c:".$this->remotedata;

          //echo "read:(".$this->remotedata['cursor']." (".$this->Name."))";


        }
        //$ca->Alert("init:c:".$this->remotedata['cursor']);
      }
      else
      {
        if($this->sql)
        {
          $this->remotedata['sql']=$this->sql;
          $this->remotedata['cursor']=0;
          $this->remotedata['max_cursor']=0;

          $this->remotedata['fields']=db_fieldsandblobtype($this->result);

          while($this->fetch())
            $this->remotedata['rows'][$this->remotedata['max_cursor']++]=$this->Row;

          $this->First();
        }
      }

      $this->PreparedRemote=true;
    }

     function First()
     {
       $this->remotedata['cursor']=0;
       return $this->GetCurrentRow();
     }

     function Prev()
     {
       if($this->remotedata['cursor']>0)
         $this->remotedata['cursor']--;

       return $this->GetCurrentRow();
     }

     function Next()
     {
       if($this->remotedata['cursor']<$this->remotedata['max_cursor']-1)
         $this->remotedata['cursor']++;
       return $this->GetCurrentRow();
     }

     function Last()
     {
       $this->remotedata['cursor']=$this->remotedata['max_cursor']-1;

       return $this->GetCurrentRow();
     }

     function GoTo($pos)
     {
       global $ca;

       $this->remotedata['cursor']=$pos;
       return $this->GetCurrentRow();
     }

    function Fetch()
    {
      global $ca;

      $ret=db_fetch($this->result);

      $ret=$this->get_dbimage_from_row($ret);

      $this->Row=$ret;

      return $ret;
    }
  }

?>