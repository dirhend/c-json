<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TObject
  {
    var $_Saved;
    var $_Loaded;
    var $ca;
    var $Parent;
    var $Items;
    var $Theme;
    var $oLevel;

    function __construct($owner)
    {
      global $ca,$id;
      global $main_db;

      $this->db=$main_db;    // Wird ben�tigt, um die Events aus der IBEWebForms-Datenbank zu lesen

      $this->FormID=$id;
      $this->Parent=$owner;
      $this->oLevel=0;

      $this->ca=$ca;

      $this->Parent=$owner;
      //echo "construct (".$this.")<br>";
      if($owner)
      {
        $owner->uobject[count($owner->uobject)]=$this;
        if($owner->FormName)
          $this->FormName=$owner->FormName;
        else
          $this->FormName=$owner->Name;
      }

      $this->Class=get_class($this);

      $this->_Loaded=false;
      $this->_Saved=false;

      $this->Theme=$this->Parent->Theme;
      $this->ThemeTemplate=new PTemplate($this);
    }

    function __destruct()
    {
      global $dir;

      for($i=0;$i<count($this->SaveProperty);$i++)
      {
        $property=$this->SaveProperty[$i];
        $file=$dir."/session_data/".$this->Name.".".$property.".".session_id().".data";
        $fh=fopen($file,"wb+");
        fwrite($fh,serialize($this->$property));
        fclose($fh);
      }

      if($this->_Store && !$this->_Saved)
      {
        $this->Save();
      }
    }

    function Init()
    {
      //parent::Init();
    }

    function LoadProperties()
    {
      global $refresh,$dir;
      //if(!$refresh)
      for($i=0;$i<count($this->SaveProperty);$i++)
      {
        $property=$this->SaveProperty[$i];

        if($property)
        {
          $file=$dir."/session_data/".$this->Name.".".$property.".".session_id().".data";
                       die();
          $fh=fopen($file.".read","w");
          fwrite($fh,"read");
          fclose($fh);

          if(file_exists($file))
          if(filesize($file))
          {
            $fh=fopen($file,"r");
            $this->$property=unserialize(fread($fh,filesize($file)));
            fclose($fh);
          }
        }
      }
    }

    function AddSaveProperty($property)
    {
      $this->c[count($this->SaveProperty)]=$property;
    }

    function AddItem($item)
    {
      //echo "AddItem (".$this->Class."/".$item->Name.")<br>";

      if(!$this->Items)
        $this->Items=array();

      $this->Items[count($this->Items)]=$item;
    }

    function Set($property,$value)
    {
    }

    function ShowComponents()
    {
      $ret="";

      for($i=0;$i<count($this->uobject);$i++)
      {
        $component_ret=$this->uobject[$i]->Get();
        $ret.=$component_ret;
      }

      return $ret;
    }

    function load($o)
    {
      //eval('global $'.$o.';');

      $file="c:/htdocs/components/".$o.".o";

      if(file_exists($file))
      {
        echo "load (".$this.")<br>";;
        $fh=fopen($file,"rb");
        $load=fread($fh,filesize($file));
        fclose($fh);

        $ret=unserialize($load);
      }

      return $ret;
    }

    function Save()
    {
      $this->_Saved=true;

      $o=$this;
      $name=$this->Name;

      $save=serialize($this);

      //echo $o." (".$this->Name.",".strlen($save).")<br>";

      $file="c:/htdocs/components/".$name.".o";

      //$file="nix2.o";

      $fh=fopen($file,"wb+");
      fwrite($fh,$save);
      fclose($fh);
    }

    function process_value($value)
    {
      if(!is_object($value))
        if($value[0]=="'" && $value[strlen($value)-1]=="'")
          $value=substr($value,1,strlen($value)-2);

          return $value;
    }

    function parsedfm($dfm="")
    {
      if($dfm)
      {
        $dfm=explode(CRLF,$dfm);

        $no=0;
        for($i=0;$i<count($dfm);$i++)
        {
          if($this->Name=="xWFDBGrid1")
            echo $dfm[$i]." (".$i.")<br>";

          $pos1=strpos(" ".$dfm[$i],"object");
          //$pos2=strpos(" ".$dfm[$i],"end".CRLF);
          $pos2=strpos(" ".$dfm[$i]," end");
          $pos2b=strpos(" ".$dfm[$i],"end>");
          $pos3=strpos(" ".$dfm[$i]," = <");
          $pos4=(trim($dfm[$i])=="item");

          if($pos1 || $pos3 || $pos4)
          {
            if($pos3)
              $pos1=$pos3;

            if($pos4)
              $object="item";

            if(!$object && !$pos4)
            {
              $arr=explode(":",$dfm[$i]);
              $object=trim($arr[1]);
              $arr=explode(" ",trim($arr[0]));
              $this->Name=$arr[1];
              $this->LoadProperties();
              $this->Object=$object;

              //echo "object:".$dfm[$i]."<br>";
            }
            else
            {
              $i_offset=0;
              $uo_anz_offset=0;

              if(!$pos3 && !$pos4)
              {
                $arr=explode(":",$dfm[$i]);
                $uobject=trim($arr[1]);
              }
              else if($pos3)
              {
                $arr=explode("=",$dfm[$i]);

                $uobject=trim($arr[0]);
                if($this->Object=="TDBGrid" && $uobject=="Columns")
                {
                  $oproperty=$uobject;
                  $uobject="TDBGridColumns";
                  $i_offset=1;
                  $uo_anz_offset=1;
                }
                else if($this->Object=="TWFDBGrid" && $uobject=="Columns")
                {
                  $oproperty=$uobject;
                  $uobject="TWFDBGridColumns";
                  $i_offset=1;
                  $uo_anz_offset=1;
                }
                else if($this->Object=="TIBTable" && $uobject=="FieldDefs")
                {
                  $oproperty=$uobject;
                  $uobject="TFieldDefs";
                  $i_offset=1;
                  $uo_anz_offset=1;
                }
                else if($this->Object=="TIBTable" && $uobject=="IndexDefs")
                {
                  $oproperty=$uobject;
                  $uobject="TIndexDefs";
                  $i_offset=1;
                  $uo_anz_offset=1;
                }
                else if($this->Object=="TIBQuery" && $uobject=="ParamData")
                {
                  $oproperty=$uobject;
                  $uobject="TParams";
                  $i_offset=1;
                  $uo_anz_offset=1;
                }
                else if(1==0)
                  echo "--".$this->Object." (".$uobject.")--<br>";
              }
              else if($pos4)
              {
                $uobject="item";
                $i_offset=0;
                $uo_anz_offset=0;
              }

              //echo "Unterobject:".$uobject." (".$this->Object.")<br>";
              // Unterobject gefunden

              /* variante 1 delphi 6 */
              $uodfm="";
              $uo_anz=$uo_anz_offset;
              $uo_end_anz=0;
              for($j=$i+$i_offset;$j<count($dfm);$j++)
              {
                if(strpos(" ".$dfm[$j],"object") || trim($dfm[$j])=="item")
                  $uo_anz++;

                if(trim($dfm[$j])=="end" || trim($dfm[$j])=="end>")
                {
                  $uo_end_anz++;

                  if($uo_anz-$uo_end_anz==0)
                    break;
                }

                if(trim($dfm[$j])=="end>" && $uo_anz_offset)
                  break;

                $uodfm.=$dfm[$j].CRLF;
              }

              if($pos4)
              {
                if($uobject=="item")
                {
                  $uodfm=str_replace("item".CRLF,"",$uodfm);
                  //$uodfm=str_replace("end".CRLF,"",$uodfm);
                  //$uodfm=str_replace("end>".CRLF,"",$uodfm);
                  $uodfm=str_replace("end","",$uodfm);
                  $uodfm=str_replace("end>","",$uodfm);

                  if($this->Class=="TDBGridColumns")
                    $uobject="TColumn";
                  else if($this->Class=="TWFDBGridColumns")
                    $uobject="TColumn";
                  else if($this->Class=="TFieldDefs")
                    $uobject="TFieldDef";
                  else if($this->Class=="TIndexDefs")
                    $uobject="TIndexDef";
                  else if(1==0)
                    echo "--".$this->Class."--<br>";

                }

              }

              $file="include/".$uobject.".php";
              if(file_exists($file))
              {
                if($uobject=="TWFPanel" && strpos($uodfm,"Caption = 'google-maps"))
                  $uobject="TWFGoogleMaps";
                else if($uobject=="TWFEdit" && strpos(" ".$uodfm,"object dateedit"))
                  $uobject="TWFDateEdit";
                else if($uobject=="TWFEdit" && strpos(" ".$uodfm,"object timeedit"))
                  $uobject="TWFTimeEdit";
                else if($uobject=="TWFPanel" && strpos(" ".$uodfm,"object frame"))
                  $uobject="TWFFrame";

                $uo=new $uobject($this);
                $uo->No=$no++;
                $uo->oLevel=$this->oLevel+1;
                $uo->parsedfm($uodfm);

                if($pos4 && 1==0)
                  echo "(".$this->Parent->Class.") ".$uodfm."<hr>";

              }
              else
              {
                $uo=new TControl($this);
                $uo->oLevel=$this->oLevel+1;
                $uo->parsedfm($uodfm);

                //$uo=$this->uobject[count($this->uobject)-1];
              }

              //if($pos4 && $uobject=="item")
              //  return ;

              if($pos3 && $oproperty)
                $this->$oproperty=$uo;
              else if($pos4)
              {
                $this->AddItem($uo);
              }

              //eval('$this->'.$uo->Name.'=$this->uobject[count($this->uobject)-1];');

              $i=$j;
            }

            if($this->Object && $pos4)
            {
              //die("ok");
              return false;
            }
          }
          else if(!$pos2)
          {
            $property_exists=false;

            $property=trim($dfm[$i]);

            $arr=explode("=",$property);
            $property=trim($arr[0]);
            $value=trim($arr[1]);

            if($value=="" && $property)
            {
              $w=0;
              $vls="";
              for($j=$i+1;$w==0;$j++)
              {
                if(substr($dfm[$j],strlen($dfm[$j])-1,1)=="'")
                  $w=1;

                $vl=trim($dfm[$j]);
                //die(substr($dfm[$j],0,1));

                //echo "vl:".$vl."<br>";
                $vls=$vls.trim($dfm[$j]);
                if($j>$i+10000)
                  $w=1;
              }

              $vls=str_replace("' +'","",$vls);
              $vls=substr($vls,1,strlen($vls)-2);
              $value=$vls;
            }


            //if($property=="Title.Caption")
            //  echo($this->Class);

            /*
            if($value=="<")
            {
              $value=new TObject($this);
            }
            */

            $so_anz=0;
            $sub_objects[$so_anz]['property']="Font";
            $sub_objects[$so_anz++]['object']="TFont";
            $sub_objects[$so_anz]['property']="TitleFont";
            $sub_objects[$so_anz++]['object']="TFont";
            $sub_objects[$so_anz]['property']="Title";
            $sub_objects[$so_anz++]['object']="TColumnTitle";

            for($si=0;$si<count($sub_objects);$si++)
            {
              $so=$sub_objects[$si]['property'];
              $soo=$sub_objects[$si]['object'];

              if(substr($property,0,strlen($so)+1)==$so.".")
              {
                //echo $property."<br>";

                //include_once($soo.".php");

                $new_property=$so;
                $property=substr($property,strlen($so)+1);
                if(is_object($this->$new_property))
                {
                  $property_exists=true;
                  $obj=$this->$new_property;
                  if($property=="Color")
                    $value=mapcolor($value);

                  $obj->$property=$value;
                  $value=$obj;

                  $property=$new_property;
                }
                else
                {
                  //echo $property.":".$value."<br>";

                  $cn=$soo;
                  $nvalue=new $cn();
                  $nvalue->$property=$this->process_value($value);
                  $value=$nvalue;
                  $property=$new_property;
                }
              }
            }

            $osi=0;
            $o_hex[$osi]['object_name']="TImage";
            $o_hex[$osi++]['subobject_name']="Picture";

            $o_hex[$osi]['object_name']="TWFImage";
            $o_hex[$osi++]['subobject_name']="Picture";

            $osi=0;
            $o_strings[$osi]['object_name']="TMemo";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="Lines";

            $o_strings[$osi]['object_name']="TWFMemo";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="Lines";

            $o_strings[$osi]['object_name']="TComboBox";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="Items";

            $o_strings[$osi]['object_name']="TWFComboBox";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="Items";

            $o_strings[$osi]['object_name']="TDBComboBox";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="Items";

            $o_strings[$osi]['object_name']="TWFDBComboBox";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="Items";

            $o_strings[$osi]['object_name']="TQuery";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="SQL";

            $o_strings[$osi]['object_name']="TIBQuery";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="SQL";
            /*
            $o_strings[$osi]['object_name']="TIBQuery";
            $o_strings[$osi]['subobject_object']="TParams";
            $o_strings[$osi++]['subobject_name']="Params";
            */
            $o_strings[$osi]['object_name']="TIBDatabase";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="Params";

            $o_strings[$osi]['object_name']="TWFDatabase";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="DBParams";

            /* TWFDatabase ist zweimal definiert? */
            $o_strings[$osi]['object_name']="TWFDatabase";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="Params";

            $o_strings[$osi]['object_name']="TWFDataset";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="SelectSQL";

            $o_strings[$osi]['object_name']="TWFDataset";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="UpdateSQL";

            $o_strings[$osi]['object_name']="TWFDataset";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="DeleteSQL";

            $o_strings[$osi]['object_name']="TWFDataset";
            $o_strings[$osi]['subobject_object']="TStrings";
            $o_strings[$osi++]['subobject_name']="InsertSQL";

            for($s=0;$s<count($o_hex);$s++)
            {
              if($object==$o_hex[$s]['object_name'])
              {
                if($property==$o_hex[$s]['subobject_name'].".Data")
                {
                  $data=trim(str_replace("{","",$value));
                  for($j=$i+1;$j<count($dfm);$j++)
                  {
                    $v=trim($dfm[$j]);
                    if(strpos($v,"}"))
                    {
                      $v=str_replace("}","",$v);
                      $data.=$v;
                      break;
                    }
                    else
                      $data.=$v;
                  }

                  $this->Picture=$data;
                  $i=$j;
                }
              }
            }

            for($s=0;$s<count($o_strings);$s++)
            {
              if($object==$o_strings[$s]['object_name'])
              {
                if($property==$o_strings[$s]['subobject_name'].".".substr($o_strings[$s]['subobject_object'],1))
                {
                  $property=$o_strings[$s]['subobject_name'];

                  include_once($o_strings[$s]['subobject_object'].".php");

                  if(!$this->$property)
                    $lines=new $o_strings[$s]['subobject_object']();
                  else
                    $lines=$this->$property;

                  $value=substr($value,1);
                  $ai=0;
                  $arr=array();
                  $is_arr=true;
                  for($j=$i+1;$j<count($dfm);$j++)
                  {
                    $v=trim($dfm[$j]);

                    //'#39=''
                    //'''='

                    $v=str_replace("'#39","''",$v);
                    $v=str_replace("'''","'",$v);

                    if($v && substr($v,strlen($v)-2,2)=="')")
                    {
                      if($v[0]=="'" && $v[strlen($v)-2]=="'")
                      {
                        $v=substr($v,1,strlen($v)-3);
                      }
                      else if($v[0]=="'" && substr($v,strlen($v)-4)=="' +")
                      {
                        $is_arr=false;
                        $v=substr($v,1,strlen($v)-5);
                      }

                      if($is_arr)
                        $arr[$ai++]=$v;
                      else
                        $arr[0].=$v;

                      break;
                    }
                    else
                    {
                      $b_is_arr=$is_arr;

                      if($v[0]=="'" && $v[strlen($v)-1]=="'")
                      {
                        $is_arr=true;
                        $v=substr($v,1,strlen($v)-2);
                      }
                      else if($v[0]=="'" && substr($v,strlen($v)-3)=="' +")
                      {
                        $is_arr=false;
                        $v=substr($v,1,strlen($v)-4);
                      }

                      if($b_is_arr)
                        $arr[$ai++]=$v;
                      else
                      {
                        $aiindex=0;
                        if($ai>0)
                          $aiindex=$ai-1;
                        $arr[$aiindex].=$v;
                      }
                    }
                  }

                  $lines->SetStrings($arr);
                  $value=$lines;

                  $i=$j;
                }
              }
            }

            $value=$this->process_value($value);

            //echo $property."=".$value."<br>";

            if($property=="Color")
              $value=mapcolor($value);

            //echo "property:".$property."/value:".$value."<br>";

            if($property)
            {
              $this->$property=$value;

              //Setter-Funktion setzen

              /*
              $this->SetLeft=$this->Setter;
              echo "sl:".$this->Show;
              */

              /*
              $eval_str=<<<END

              function Set{property}()
              {
                //global {set}{property};
                echo "ok";
              }
END;

              //echo $this->Name."->".$property."<br>";

              $eval_str=str_replace("{property}",$property,$eval_str);
              $eval_str=str_replace("{this}",'$this',$eval_str);
              $eval_str=str_replace("{set}",'Set',$eval_str);

              if(!function_exists("Set".$property))
              {
                eval($eval_str);
                //echo $eval_str."<hr>";
              }
              */
            }
          }
          else if($pos2)
            $i_end=$i;
        }
      }
    }

    function Show()
    {
      $ret="<div style\"border:2px solid black;\">Show-Methode sollte �berladen werden!</div>";

      echo $ret;
    }

    function Get()
    {
      return "nix";
    }

  }

?>