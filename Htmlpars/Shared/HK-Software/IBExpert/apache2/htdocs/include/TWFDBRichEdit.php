<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBRichEdit extends TControl
  {
    var $datasource;
    var $dataset;

    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->BorderStyle="bsSingle";
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="LINES_TEXT")
      {
        /* Variante 1
        include_once("include/rtfclass.php");

		$r = new rtf( ( $value));
		$r->output( "html");
		$r->parse();

		if( count( $r->err) == 0) // no errors detected
          $value=$r->out;
        else
          $value="error";
        */
        // Variante 2

        include_once("include/PRTF2HTML.php");

        $rtf2html=new PRTF2HTML();

        $rtf2html->RTF=$value;
        $rtf2html->Convert2HTML();
        $value=$rtf2html->Get();
        $this->ca->ModifyAttribute($this,"richedit",$value);
      }
      else if($attribute=="update")
      {
        $this->Set("LINES_TEXT",$value);
      }
    }

    function Get()
    {
      parent::Get();

      $params="";
      if($this->ScrollBars=="ssBoth" ||
         $this->ScrollBars=="ssHorizontal")
        $params.="wrap=\"off\"";

      //$uret="<textarea id=\"".strtoupper($this->Name)."\" ".$params." style=\"".$this->GetStyle()."\">".$this->Lines->HTMLText."</textarea>";

      include_once("include/spaw/spaw_control.class.php");

      $sw=new SPAW_Wysiwyg(strtoupper($this->Name),"",'de','full','default',$this->Width-4,$this->Height-7);

      //$control="<div style=\"border:2px solid inset;\">".$sw->getHtml()."</div>";

      //$ret=str_replace("{content}",$control,$this->Template);

      $control=$sw->GetHtml();
      $ret=$this->Template;
      $ret=str_replace("{content}",$this->ThemeTemplate->Get(),$ret);
      $ret=str_replace("{content}",$control,$ret);

      return $ret;
    }

  }

?>