<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TTabSheet extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->Left=0;
      $this->Top=0;
    }

    function Init()
    {
      parent::Init();

      $this->Width=$this->Parent->Width-8;
      $this->Height=$this->Parent->Height-32;
    }

    function Get()
    {
      parent::Get();

      if(!$this->Color)
        $this->Color=mapcolor("clBtnFace");
      if(!$this->Font)
        $this->Font=new TFont();

      $ret=$this->Template;

      $style="border-style:outset; border-width:2px;";

      $sheets=""; //"<div id=\"".strtoupper($this->Name)."\" style=\"".$this->GetStyle()."\"><a href=\"\">".$this->Caption." (".$this->No.")</a></div>";
      $control="<div id=\"".strtoupper($this->Name)."\" style=\"display:; position:absolute; width:".($this->Parent->ClientWidth-4)."px; height:".($this->Parent->ClientHeight-4)."\">{content}</div>";

      $control=$this->ThemeTemplate->Get();

      $js=<<<END
        <script language="javascript">
          {parent_name}Sheets[{no}]=new Array();
          {parent_name}Sheets[{no}]['caption']="{caption}";
          {parent_name}Sheets[{no}]['name']="{name}";
        </script>
END;

      $js=str_replace("{parent_name}",strtoupper($this->Parent->Name),$js);
      $js=str_replace("{no}",$this->No,$js);
      $js=str_replace("{caption}",$this->Caption,$js);
      $js=str_replace("{pageindex}",$this->PageIndex,$js);
      $js=str_replace("{name}",strtoupper($this->Name),$js);
      $ret=str_replace("{content}",$js.$sheets.$control,$ret);

      $uuret=$this->ShowComponents();

      $ret=str_replace("{content}",$uuret,$ret);

      return $ret;
    }
  }

?>