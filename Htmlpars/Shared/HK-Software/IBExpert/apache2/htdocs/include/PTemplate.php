<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class PTemplate
  {
    function __construct($owner,$object="")
    {
      $this->Parent=$owner;
      $this->ObjectTemplate=$object;
    }

    function Get($object="")
    {
      //if($object)
      $this->ObjectTemplate=$object;

      $this->_get();
      $this->_parse();

      return $this->Template;
    }

    function Set($field, $value)
    {
      $this->_get();

      //$value="x";
      //echo $this->Name."->Set($field,$value)<br>";
      //if($data!="")
      {
        $getFieldProcess=$this->getFieldProcess;

        if($getFieldProcess=="")
          $this->Template=str_replace("{".$field."}", $value, $this->Template);
        else if(function_exists($getFieldProcess))
          $this->Template=str_replace("{".$field."}", $getFieldProcess($value), $this->Template);
      }
    }

    function SetFields($data)
    {
      $this->_get();

      if($this->debug)
        echo $this->Template."<hr>";

      $data_arr=$data;

      for($i=0;$i<count($data_arr);$i++)
      {
        //$this->tpl[$section]=str_replace("{".key($data_arr)."}", current($data_arr), $this->tpl[$section]);
          $this->Set(key($data_arr),current($data_arr));

        next($data_arr);
      }

      if($this->debug)
        echo $this->Template."<hr>";
    }

    function _get()
    {
      if($this->Parent->Theme)
        $this->Theme=$this->Parent->Theme;

      $this->oLevel=$this->Parent->oLevel;
      $this->TabOrder=$this->Parent->TabOrder;
      $this->TabStop=$this->Parent->TabStop;
      $this->Class=$this->Parent->Class;
      $this->Name=$this->Parent->Name;
      $this->ParentName=$this->Parent->Parent->Name;
      $this->Caption=$this->Parent->Caption;
      $this->Text=$this->Parent->Text;
      $this->Width=$this->Parent->Width;
      $this->Height=$this->Parent->Height;
      $this->Left=$this->Parent->Left;
      $this->Top=$this->Parent->Top;
      $this->ClientWidth=$this->Parent->ClientWidth;
      $this->ClientHeight=$this->Parent->ClientHeight;
      $this->ClientLeft=$this->Parent->ClientLeft;
      $this->ClientTop=$this->Parent->ClientTop;
      $this->Display=$this->Parent->Display;
      $this->Visible=$this->Parent->Visible;

      $this->onEvents=$this->Parent->onEvents;
      $this->Enabled=$this->Parent->Enabled;
      $this->ReadOnly=$this->Parent->ReadOnly;
      $this->ScrollBars=$this->Parent->ScrollBars;

      $this->FormName=$this->Parent->FormName;
      $this->FormID=$this->Parent->FormID;

      if(!$this->ObjectTemplate)
      {
        $this->ObjectTemplate=$this->Class;
      }

      if(!$this->TemplateLoaded[$this->ObjectTemplate])
      {
        //echo $this->Name." (".$this->Class."/".$this->ObjectTemplate.")<br>";

        // Variante vorhanden?
        $file="themes/".$this->Theme."/variants/".$this->ObjectTemplate."_".$this->Name.".tpl";

        if(!file_exists($file))
          $file="themes/".$this->Theme."/".$this->ObjectTemplate.".tpl";

        if(file_exists($file))
        {
          $fh=fopen($file,"r");
          $control=fread($fh,filesize($file));
          fclose($fh);

          $this->Template=$control;

          $this->TemplateLoaded[$this->ObjectTemplate]=true;
        }
      }
    }

    function _parse()
    {
      global $GLOBAL_DATA,$license,$id;

      if($GLOBAL_DATA)
        $this->SetFields($GLOBAL_DATA,$section);

      $body=$this->Template;

      $isTControl=0;
      if($this->ObjectTemplate=="TControl")
        $isTControl=1;

      if($this->Parent)
        $body=str_replace("{style}",$this->Parent->GetStyle("",$isTControl),$body);

      $overflow="hidden";
      if($this->Parent->AutoSize=="True")
        $overflow="";

      $body=str_replace("{overflow}",$overflow,$body);

      $width=$this->Parent->Parent->Width;
      if(!$width)
        $width=1000;
      $height=$this->Parent->Parent->Height;
      if(!$height)
        $height=600;

      //form
      //$this->Right=$width-$this->Left-$this->Width-10;
      $this->Right=$width-$this->Left-$this->Width;
      if($width=$this->Parent->Parent->Class=="TForm")
        $this->Right-=7;

      $this->Bottom=$height-$this->Top-$this->Height;
      if($this->Parent->Parent->Class=="TForm")
      {
        $this->Bottom-=($this->Parent->Parent->ClientTop-$this->Parent->Parent->Top); //$this->ClientTop-$this->Top;
      }

      $body=str_replace("{id}",strtoupper($this->Name),$body);

      if($this->TabStop!="False")
        $taborder=(($this->Parent->Parent->TabOrder+1)*1000+$this->TabOrder+1);
      else
        $taborder="";

      $body=str_replace("{id}",strtoupper($this->Name),$body);
      $body=str_replace("{TabOrder}",$taborder,$body);
      $body=str_replace("{NAME}",strtoupper($this->Name),$body);
      $body=str_replace("{Name}",$this->Name,$body);
      $body=str_replace("{name}",$this->Name,$body);
      $body=str_replace("{parent_name}",$this->ParentName,$body);
      $body=str_replace("{PARENT_NAME}",strtoupper($this->ParentName),$body);
      $body=str_replace("{Caption}",$this->Caption,$body);
      $body=str_replace("{Text}",$this->Text,$body);
      $body=str_replace("{Width}",$this->Width,$body);
      $body=str_replace("{Height}",$this->Height,$body);
      $body=str_replace("{Top}",$this->Top,$body);
      $body=str_replace("{Left}",$this->Left,$body);
      $body=str_replace("{ClientWidth}",$this->ClientWidth,$body);
      $body=str_replace("{ClientHeight}",$this->ClientHeight,$body);
      $body=str_replace("{ClientLeft}",$this->ClientLeft,$body);
      $body=str_replace("{ClientTop}",$this->ClientTop,$body);
      $body=str_replace("{Right}",$this->Right,$body);
      $body=str_replace("{Bottom}",$this->Bottom,$body);
      $body=str_replace("{Display}",$this->Display,$body);
      $body=str_replace("{ScrollBars}",$this->ScrollBars,$body);
      $body=str_replace("{FormName}",$this->FormName,$body);
      $body=str_replace("{FormID}",$this->FormID,$body);
      $body=str_replace("{license}",$license,$body);
      $body=str_replace("{formid}",$id,$body);

      if($this->Enabled=="False")
        $disabled="disabled";
      if($this->ReadOnly=="True")
        $disabled="disabled";

      $body=str_replace("{disabled}",$disabled,$body);

      $events="";
      for($i=0;$i<count($this->onEvents);$i++)
      {
        $events.=key($this->onEvents)."=\"".current($this->onEvents)."\" ";
      }

      $body=str_replace("{events}",$events,$body);

      $body=str_replace("","",$body);
      $body=str_replace("","",$body);
      $body=str_replace("","",$body);

      $pos1=strpos(" ".$body,"{style(");
      while($pos1)
      {
        $pos2=strpos($body,")}",$pos1);
        $s=substr($body,$pos1+6,$pos2-$pos1-6);

        $body=str_replace("{style(".$s.")}",$this->Parent->GetStyle($s),$body);

        $pos1=strpos(" ".$body,"{style(");
      }

      $body=str_replace("<tpl:level/>",$this->level,$body);

      /*
      global $f_tp_from;

      $body=str_replace("{\$f_tp_from}",$f_tp_from,$body);
      */

      $pos1=strpos(" ".$body,"{\$");
      while($pos1)
      {
        $pos1--;

        $pos2=strpos($body,"}",$pos1);

        $field=substr($body,$pos1+2,$pos2-$pos1-2);

        eval('global $'.$field.';');
        eval('$v=$'.$field.';');
        //$v=FormatField($v);

        $body=str_replace("{\$".$field."}",$v,$body);

        $pos1=strpos(" ".$body,"{\$",$pos1+2);
      }

        $pos1=strpos(" ".$body,"<tpl:");
        while($pos1)
        {
          $ii=0;
          $key="<tpl:";
          while(strpos(" ".$key,"<tpl:"))
          {
            $pos1-=1;

            $ii++;
            if($ii>10)
              die("e");

            // befehl suchen
            $pos2=strpos($body,"(",$pos1);
            $tpos2=strpos($body,"/>",$pos1);

            $key="";
            $value="";

            if($pos2>$tpos2 || !$pos2)
            {
              $pos2=strpos($body,":",$pos1);
              $command=substr($body,$pos2+1,$tpos2-$pos2-1);

              $pos5=strpos($body,"/>",$pos1+1);
            }
            else
            {
              $command=substr($body,$pos1+5,$pos2-$pos1-5);
              $pos3=strpos($body,"(",$pos2+1);
              $tpos3=strpos($body,"/>",$pos2+1);
              if($pos3 && $pos3<$tpos3)
              {
                $key=substr($body,$pos2+1,$pos3-$pos2-1);

                $pos4=strpos($body,")",$pos3+1);
                $value=substr($body,$pos3+1,$pos4-$pos3-1);
              }
              else
              {
                $pos3=strpos($body,")",$pos2+1);
                $value=substr($body,$pos2+1,$pos3-$pos2-1);
              }
              $pos5=strpos($body,"/>",$pos1+1);
            }

            if(strpos(" ".$key,"<tpl:"))
              $pos1=strpos(" ".$body,"<tpl:",$pos1+2);

          }

          $new_body="";

          //echo "command:".$command." ($key,$value)<hr>";

          if(!$pos2)
          {
            echo "<div style=\"border:2px solid red; background-color:white; padding:10px;\">Fehler in Sub-Template Syntax.</div>"; "<br>Beispiel:<br>".
                  "<tpl:sub(Templatename/Variante({ID}))/>";
            $pos1="";
            $body="";
          }
          else
          {
            if($command=="sub")
            {
              $new_body="<otpl:sub($key($value))/>";
            }
            else if($command=="include")
            {
              include_once($value.".php");

              //echo "key:".$key."-value:".$value."<br>";
            }
            else if($command=="php")
            {
              //echo $value."1<br>";

              $eval_str='return '.$value.';';

              $new_value=@eval($eval_str);
              if($new_value)
                $value=$new_value;

              if($key)
                $new_body=$key($value);
              else
                $new_body=$value;
            }
            else if($command=="if")
            {
              //echo $value."<br>";

              $_if=1;

              $ok=false;
              if(strpos(" ".$value,"=="))
              {

                $if_arr=explode("==",$value);
                $ok=false;
                if($if_arr[0]==$if_arr[1])
                  $ok=true;
              }
              else if(strpos(" ".$value,"!="))
              {
                $if_arr=explode("!=",$value);
                if($if_arr[0]!=$if_arr[1])
                  $ok=true;
              }
              else
              {
                if($key)
                {
                  $eval_str='return '.$value.';';
                  $value=@eval($eval_str);
                  $ok=$key($value);

                  //$eval_str=$key.'('.$value.');';
                }
                else
                {
                  $eval_str='return '.$value.';';

                  //echo $eval_str."<br>";
                  $ok=@eval($eval_str);
                }
                //$ok=$value;
              }

              if($ok)
              {
                //echo "value:".$value."<br>";
              }
              else if(!$ok)
              {
                //passenden endif finden

                $ei_pos_anz=0;
                $i_pos_anz=0;
                $ei_pos=$pos1;
                $i_pos=$pos1;

                $ii=0;

                $error=0;
                while($i_pos_anz>=$ei_pos_anz && !$error)
                {
                  $ei_pos=strpos($body,"<tpl:endif",$ei_pos+1);
                  $i_pos=strpos($body,"<tpl:if",$i_pos+1);

                  if(!$i_pos && !$ei_pos)
                  {
                    echo ("POutput: endif fehlt<br>");
                    $error=1;
                  }

                  if($ei_pos<$i_pos && $ei_pos)
                  {
                    $ei_pos_anz++;
                    // ok
                    //die("ok");
                  }
                  else if($i_pos<$ei_pos && $i_pos && $ei_pos)
                  {
                    $ipos_anz++;
                    //$ei_pos_anz++;
                  }
                  else if($i_pos && !$ei_pos)
                  {
                    $i_pos_anz++;
                  }
                  else if($ei_pos && !$i_pos)
                  {
                    $ei_pos_anz++;
                  }

                  /*
                  echo("($ii).<br>".
                      "i_pos_anz:".$i_pos_anz." ($i_pos)<br>".
                      "ei_pos_anz:".$ei_pos_anz." ($ei_pos)<br>".
                      "");
                  */

                  $ii++;

                  if($ii>10)
                    die("error");
                }

                //$pos5=$ei_pos;
                $pos5=strpos($body,"/>",$ei_pos);

                //$body=substr($body,0,$pos1).$new_body.substr($body,$pos5+2);
                //$body=substr($body,$pos5+2);

              }
            }

          }

          //if($_if)
          //  die("...".$body.".($pos1,$pos5,".strlen($body).")..");

          $body=substr($body,0,$pos1).$new_body.substr($body,$pos5+2);

          $pos1=strpos(" ".$body,"<tpl:");
        }


      $this->Template=$body;
    }

    function Show($section="body")
    {
      echo $this->Get($section);
    }
 }

?>