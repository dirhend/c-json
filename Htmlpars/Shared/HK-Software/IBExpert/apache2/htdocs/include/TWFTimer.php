<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFTimer extends TComponent
  {
    function __construct($owner)
    {
      $this->AddSaveProperty("Enabled");

      parent::__construct($owner);

      $this->Interval=1000;
      $this->Enabled="True";
    }

    function Init()
    {
      parent::Init();
    }

    /*
    function Action($property,$value)
    {
      parent::Action($property,$value);

      //$this->ca->alert("ok");
    }
    */

    function GetEventParams($property)
    {
      if($property=="ENABLED")
        $ret="Timer".$this->Name."Enabled;";

      return $ret;
    }

    function Set($property,$value)
    {
      if($property=="ENABLED")
      {
        if($value!="False")
          $value="True";
        else
          $value="False";

        $js=<<<END
          Timer{name}Enabled="{value}";
          if("{value}"=="True")
            setTimeout("Timer{name}Timer()",{interval})

END;
        $js=str_replace("{value}",$value,$js);
        $js=str_replace("{name}",$this->Name,$js);
        $js=str_replace("{interval}",$this->Interval,$js);

        $this->ca->JS($js);
      }
    }

    function Get()
    {
      parent::Get();

      $js=<<<END

      <script language="javascript">
        var Timer{name}Enabled="{enabled}";

        function Timer{name}Timer()
        {
          //alert("{enabled}");
          if(Timer{name}Enabled=="True")
          {
            //alert("ok");
            {name}OnTimer();
            setTimeout("Timer{name}Timer()",{interval});
          }
          //alert("ok");
        }

        if(Timer{name}Enabled=="True")
          setTimeout("Timer{name}Timer()",{interval});

          //setTimeout("{name}OnTimer(null)",{interval});
          //setTimeout("{name}OnTimer()",{interval});

      </script>

END;

      $js=str_replace("{interval}",$this->Interval,$js);
      $js=str_replace("{enabled}",$this->Enabled,$js);
      $js=str_replace("{name}",$this->Name,$js);
      $js=str_replace("{NAME}",strtoupper($this->Name),$js);


      return $this->Template.$js;
    }


  }

?>