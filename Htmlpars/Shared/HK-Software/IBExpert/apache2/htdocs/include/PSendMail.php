<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class PSendMail
  {
    function PSendMail()
    {
      global $ca;
    }

    function Start()
    {
      $db=new PDB();
      $db2=new PDB();

      $sql="select * from wf_sendmail where istransmitted='0'";
      $db->query($sql);
      while($db->fetch())
      {
        $mail=new PPHPMailer();
        $mail->do_debug=1000;
        $mail->Mailer="smtp";
        $mail->Port=25;
        $mail->SMTPAuth=true;
        $mail->Host=$db->Row['SMTP_HOST'];//$smtp_host;
        $mail->Username=$db->Row['SMTP_USERNAME'];//$smtp_username;
        $mail->Password=$db->Row['SMTP_PASSWORD'];//$smtp_password;

        if($db->Row['HTMLMAIL']=='1')
          $mail->IsHTML(true);

        $mail->From=$db->Row['FROM_ADDRESS'];
        $mail->FromName=$db->Row['FROM_NAME'];
        $mail->Subject=$db->Row['SUBJECT'];

        $body=$db->Row['BODY'];

        $mail->Body=$body;

        $to_address=explode(";",$db->Row['TO_ADDRESS']);
        for($i=0;$i<count($to_address);$i++)
          $mail->AddAddress($to_address[$i]);

        $db2->query("select * from wf_sendmail_attachment where id_wf_sendmail=".$db->Row['ID']);
        $afile=array();
        $attach=0;
        while($db2->fetch())
        {
          if(strlen($db2->Row['ATTACHMENT_DATA'])>0)
          {
            $attach=1;
            $file="d:/ftproot/htdocs/webforms/blob/".time()."_".$db2->Row['FILENAME'];
            $afile[count($afile)]=$file;
            $fh=fopen($file,"wb+");
            fwrite($fh,$db2->Row['ATTACHMENT_DATA']);
            fclose($fh);

            $mail->AddAttachment($file,$db2->Row['FILENAME']);
          }
        }

        if(!$attach)
        {
          $mail->AddBCC("info@m2-it.de");
        }

        $db2->query("update wf_sendmail set istransmitted='1' where id=".$db->Row['ID']);
        if(!$mail->Send())
        {
          $db2->query("select try from wf_sendmail where id=".$db->Row['ID']);
          $db2->fetch();
          $try=$db2->Row['TRY'];
          if(!$try)
            $try=1;
          else
            $try++;

          $db2->query("update wf_sendmail set istransmitted='0' where id=".$db->Row['ID']);

          $db2->query("update wf_sendmail set istransmitted='0',try=".$try." where id=".$db->Row['ID']);
          echo "error<br>";
        }

        for($i=0;$i<count($afile);$i++)
          unlink($afile[$i]);
      }

      $sql="delete from wf_sendmail where istransmitted='1'"; // or try>10";
      $db2->query($sql);
    }

  }

?>