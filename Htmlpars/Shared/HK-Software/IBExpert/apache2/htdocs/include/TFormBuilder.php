<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  include_once("include/const.php");

  include_once("include/TObject.php");
  include_once("include/TComponent.php");

  class TFormBuilder
  {
    var $dfm;
    var $object;
    var $test;

    function TFormBuilder($dfm,$action_stack,$theme="standard")
    {
      global $ca;

      $this->db=new PDB();

      if(!$theme)
        $theme="standard";

      $this->Theme=$theme;
      $this->ActionStack=$action_stack;

      /*
      $fh=fopen($dfm_file,"r");
      $this->dfm=fread($fh,filesize($dfm_file));
      fclose($fh);
      */

      $this->dfm=$dfm;
      $this->parse();
    }

    function parse()
    {
      if(!$this->dfm)
        die("kein Formular gefunden");

      $i_start="";
      $i_end="";

      $dfm=explode(CRLF,$this->dfm);
      $object="";
      for($i=0;$i<count($dfm);$i++)
      {
        $pos1=strpos(" ".$dfm[$i],"object");
        $pos2=strpos(" ".$dfm[$i],"end");
        if($pos1)
        {
          if(!$object)
          {
            $i_start=$i;
            // neues Object gefunden
            $arr=explode(":",$dfm[$i]);
            $object=$arr[1];
            //echo $object."<br>";
          }
          else
          {
            //echo "Unterobject:".$dfm[$i]."<br>";
            // Unterobject gefunden
            for($j=$i;$j<count($dfm);$j++)
            {
              if(trim($dfm[$j])=="end")
                break;
            }
            $i=$j;
          }
        }
        else if(!$pos2)
        {
          $property[count($property)]=trim($dfm[$i]);
        }
        else if($pos2)
          $i_end=$i;
      }

      $odfm="";
      for($i=$i_start;$i<$i_end-1;$i++)
      {
        $odfm.=$dfm[$i].CRLF;
      }

      if(strpos(" ".$object,"TForm"))
        $object="TForm";

      $this->object=new $object($this);
      $this->object->parsedfm($odfm);

      $eval_str='$this->'.$this->object->Name.'=$this->object;';

      eval($eval_str);
      $this->Form=$this->object;

      $this->Init();
      $this->Action();
    }

    function Get()
    {
      $IndexTemplate=new PTemplate(null);
      $IndexTemplate->Theme=$this->Theme;
      $index=$IndexTemplate->Get("index");

      $ret=str_replace("{content}",$this->object->Get(),$index);

      $translator=false;
      $sql="select count(*) anz ".
           "from rdb\$procedures ".
           "where rdb\$procedure_name='WF_TRANSLATE'";
      $this->db->queryandfetch($sql);
      if($this->db->Row['ANZ']==1)
        $translator=true;

      $pos1=strpos(" ".$ret,"<tc");
      while($pos1)
      {
        $pos2=strpos(" ".$ret,">",$pos1+1);
        $pos3=strpos(" ".$ret,"</tc>",$pos1+1);

        $content=substr($ret,$pos2,$pos3-$pos2-1);

        if($translator)
          $translated_content=$this->tc($content);
        else
          $translated_content=$content;

        //$translated_content="nix";

        $ret=substr($ret,0,$pos1-1).$translated_content.substr($ret,$pos3+4);

        $pos1=strpos(" ".$ret,"<tc");
      }

      return $ret;
    }

    function tc($content)
    {
      global $lng,$id;

      $content=str_replace("'","''",$content);

      $sql="select * from wf_translate('".$content."','".$lng."','".$id."')";
      $this->db->queryandfetch($sql);

      $content=$this->db->Row['TRANSLATED_CONTENT'];

      return $content;
    }

    function Init()
    {
      $this->Form->ParseComponents($this->Form,"Call","Init;");
    }

    function Action()
    {
      global $object,$action,$action_value,$ca;

      if(count($this->ActionStack))
      {
        for($i=0;$i<count($this->ActionStack);$i++)
        {
          $arr=explode("--||--",$this->ActionStack[$i]);
          for($j=0;$j<count($arr);$j++)
          {
            $kv_arr=explode("=",$arr[$j]);
            if($kv_arr[0]=="object")
              $object=$kv_arr[1];
            else if($kv_arr[0]=="action")
              $action=$kv_arr[1];
            else if($kv_arr[0]=="action_value")
              $action_value=$kv_arr[1];
            else
            {
              $_REQUEST[$kv_arr[0]]=$kv_arr[1];
            }
          }

          $this->Form->ParseComponents($this->Form,"Call","Action;'".$object."','".$action."','".$action_value."'");
          $object=""; $action=""; $action_value="";
        }
      }
      else if($action || $object || $action_value)
        $this->Form->ParseComponents($this->Form,"Call","Action;'".$object."','".$action."','".$action_value."'");
    }

    function Show()
    {
      echo $this->Get();
    }

  }

?>