<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFEdit extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->BorderStyle="bsSingle";
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="TEXT")
        $this->ca->ModifyAttribute($this,"value",$value);
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      if($this->OnChange)
        $this->onEvents['onChange'].=$this->Name."OnChange(this);";

      if($this->CharCase=="ecUpperCase")
        $this->onEvents['onKeyUp'].="this.value=this.value.toUpperCase();";
      else if($this->CharCase=="ecLowerCase")
        $this->onEvents['onKeyUp'].="this.value=this.value.toLowerCase();";

      $js=<<<END


END;
      $control=$this->ThemeTemplate->Get();

      if($this->Password!="True")
        $control=str_replace("{type}","text",$control);
      else
        $control=str_replace("{type}","password",$control);

      $ret=str_replace("{content}",$control.$js,$this->Template);

      return $ret;
    }

    function Show()
    {
      echo $this->Get();
    }

  }

?>