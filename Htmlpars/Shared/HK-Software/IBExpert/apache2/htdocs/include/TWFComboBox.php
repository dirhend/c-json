<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFComboBox extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);


      if($attribute=="ITEMS_TEXT")
      {
        $value=utf8_encode($value);
        $value=explode(CRLF,$value);

        $js=<<<END

          var {name}Options=new Array();
          var {name}OptionsValue=new Array();
          var {name}OptionsSelected=new Array();

END;


        for($i=0;$i<count($value);$i++)
        {
          $arr=explode("|",$value[$i]);

          $js.=<<<END

          //alert("{value}");
          {name}Options[{i}]="{value1}";
          {name}OptionsValue[{i}]="{value2}";
          {name}OptionsSelected[{i}]="{value3}";

END;

          $js=str_replace("{i}",$i,$js);
          $js=str_replace("{value1}",$arr[0],$js);
          $js=str_replace("{value2}",$arr[1],$js);
          $js=str_replace("{value3}",$arr[2],$js);
        }

        $js.=<<<END

          var o=document.getElementById("{NAME}");
          var sel_index=0;

          while(o.length)
            o.options[o.length-1]=null;

          /*
          for(var ii=0;ii<{name}Options.length;ii++)
          {
            if({name}Options[ii]==value)
              sel_index=ii;
          }
          */

          /*
          if(value && sel_index==0)
          {
            oe=new Option(value,value,false,false);
            o.options[o.length]=oe;
          }
          */

          for(ii=0;ii<{name}Options.length;ii++)
          {
            sel=false;
            if({name}OptionsSelected[ii])
              sel=true;
            oe=new Option({name}Options[ii],{name}OptionsValue[ii],false,sel);
            o.options[o.length]=oe;
          }

          /*

          if(sel_index>0)
            o.selectedIndex=sel_index;
          */
END;

        $this->ca->exJS($js,$this);
        //$this->ca->ModifyAttribute($this,"value",$value);
      }
    }

    function Get()
    {
      $this->Height+=1;
      parent::Get();

      $this->onEvents['onChange'].=$this->Name."OnChange(this);";

      for($i=0;$i<count($this->Items->Strings);$i++)
        $options.="<option>".$this->Items->Strings[$i]."</option>";

      $control=str_replace("{options}",$options,$this->ThemeTemplate->Get());

      return str_replace("{content}",$control,$this->Template);
    }

  }

?>