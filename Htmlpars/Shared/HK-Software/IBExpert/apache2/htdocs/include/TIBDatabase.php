<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TIBDatabase extends TComponent
  {
    var $db;
    var $DatabaseName;
    var $username;
    var $password;

    function __construct($owner=null)
    {
      parent::__construct($owner);

      //$this->db=new PDB();
    }

    function Init()
    {
      if(!$this->isInit)
      {
        for($i=0;$i<$this->Params->length;$i++)
        {
          $arr=explode("=",$this->Params->Strings[$i]);
          if(strtoupper(trim($arr[0]))=="USER_NAME")
            $this->username=trim($arr[1]);
          else if(strtoupper(trim($arr[0]))=="PASSWORD")
            $this->password=trim($arr[1]);
        }

        $this->db=new PDB(-1);
        if($this->Connected=="True")
        {
          //echo($this->DatabaseName);
          $this->db->Connect($this->DatabaseName,$this->username,$this->password);
        }
        /*
        echo $this->DatabaseName."<br>";
        echo $this->username."<br>";
        echo $this->password."<br>";
        */
      }

      $this->isInit=true;
    }

    function Get()
    {
      parent::Get();

      //$control="<span style=\"border:1px solid red; ".$this->GetStyle()."\">".$this->Name."</span>";
      $control.="<div id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\">";

      $ret=$control;

      return $ret;
    }

    function Show()
    {
      echo $this->Get();
    }
  }

?>