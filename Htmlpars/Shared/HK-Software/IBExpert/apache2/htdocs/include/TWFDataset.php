<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDataset extends TDataSet
  {
    var $query;
    var $row;
    var $odb;

    function __construct($owner=null)
    {
      $this->AddSaveProperty("EOF");

      parent::__construct($owner);
      $this->EOF="";
    }

    function __destruct()
    {
    }

    function Init()
    {
      global $main_db;

      parent::Init();

      if($this->DataSource)
        $this->datasource=$this->GetComponentByName($this->DataSource);

      if(strtoupper($this->Database)=="CURRENT_DATABASE")
      {
        $database=new TWFDatabase();
        $database->db=$main_db;
      }
      else
      {
        $database=$this->Database;

        if(!$database)
          $database=$this->DatabaseName;

        $database=$this->GetComponentByName($database);
      }

      //die($database);

      $this->query=new PDBQuery($database->db);
      $this->query->Name=$this->Name;

      if(!$this->datasource && $this->Active=="True")
      {
        $this->query->open($this->SelectSQL->Text);

        $this->row=$this->query->Row;
      }
      $this->query->PrepareRemote();
      //echo $this->query->Row;
    }

    function Set($property,$value="")
    {
      parent::Set($property,$value);

      if($property=="SELECTSQL_TEXT")
      {
        $this->SelectSQL->Text=$value;
        $this->Refresh=true;
        if($this->Active=="True")
        {
          $this->query->open($this->SelectSQL->Text);
          $this->query->PrepareRemote(true);
          $this->First();
        }
      }
      else if($property=="ACTIVE")
      {
        /*
        $this->SQL->Text=$value;
        $this->query->open($this->SQL->Text);
        $this->query->PrepareRemote(true);
        $this->First();
        */
      }
      else if($property=="update")
      {
        if($this->datasource)
        {
          $sql=$this->GetSQL();

          //$this->ca->alert($sql);

          //$this->ca->alert($this->datasource->dataset->Active);

          // wenn dataset von einem datasource abh�ngt, nachsehen ob das master-dataset offen ist
          if($sql && $this->datasource->dataset->Active=="True")
          {
            //$this->ca->Alert($this->Name."->Open(".$sql.")");
            $this->Open($sql);
          }
          else
            $this->Close();
        }
      }
    }

    function GetSQL()
    {
      if($this->datasource)
      {
        $ok=true;

        $sql=$this->SelectSQL->Text;

        $fields=$this->datasource->dataset->query->fields();

        for($i=0;$i<count($fields);$i++)
        {
          $sql=str_replace(":".$fields[$i]['alias'],$this->datasource->dataset->row[$fields[$i]['alias']],$sql);
        }

        for($i=0;$i<count($this->ParamData->Items);$i++)
        {
          $this->datasource->dataset->query->GetCurrentRow();
          if(!$this->datasource->dataset->row[$this->ParamData->Items[$i]->Name])
            $ok=false;

          $sql=str_replace(":".$this->ParamData->Items[$i]->Name,$this->datasource->dataset->row[$this->ParamData->Items[$i]->Name],$sql);

          /*
          if(!$this->datasource->dataset->query->Row[$this->ParamData->Items[$i]->Name])
            $ok=false;

          $sql=str_replace(":".$this->ParamData->Items[$i]->Name,$this->datasource->dataset->query->Row[$this->ParamData->Items[$i]->Name],$sql);
          */

          //$this->ca->Alert("work: :".$this->ParamData->Items[$i]->Name."=".$this->datasource->dataset->row[$this->ParamData->Items[$i]->Name]);

        }

        //$this->ca->Alert("fertig: ".$sql);
      }
      else
      {
        $ok=true;
        $sql=$this->SelectSQL->Text;
      }

      //$this->ca->Alert($this->Name."GetSQL(".$sql.")");
      if($ok)
        return $sql;
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      //$control="<span style=\"border:1px solid red; ".$this->GetStyle()."\">".$this->Name.": SQL->Text=".$sql."</span>";
      $control.="<div id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\">";

      $ret=$control;

      return $ret;
    }

    function Show()
    {
      $this->Get();
    }
  }

?>