<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFTabSheet extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->Left=0;
      $this->Top=0;

      $this->TabVisible="True";
    }

    function Init()
    {
      parent::Init();

      $this->Width=$this->Parent->Width-8;
      $this->Height=$this->Parent->Height-32;
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="TABVISIBLE")
      {
        $js=<<<END

        {parent_name}Sheets[{no}]['tabvisible']="{tabvisible}";
        var o=document.getElementById('{name}Sheet');
        o.style.display="{display}";

END;
        if(strtoupper($value)=="TRUE")
          $value="True";
        else
          $value="False";

        if($value=="True")
          $display="";
        else
          $display="none";

        $js=str_replace("{display}",$display,$js);
        $js=str_replace("{no}",$this->No,$js);
        $js=str_replace("{tabvisible}",$value,$js);
        $js=str_replace("{parent_name}",strtoupper($this->Parent->Name),$js);
        $js=str_replace("{name}",strtoupper($this->Name),$js);

        $this->ca->exJS($js,$this,$value);

      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Color)
        $this->Color=mapcolor("clBtnFace");
      if(!$this->Font)
        $this->Font=new TFont();

      $ret=$this->Template;

      $style="border-style:outset; border-width:2px;";

      $sheets=""; //"<div id=\"".strtoupper($this->Name)."\" style=\"".$this->GetStyle()."\"><a href=\"\">".$this->Caption." (".$this->No.")</a></div>";
      $control="x"; //<div id=\"".strtoupper($this->Name)."\" style=\"display:; position:absolute; width:".($this->Parent->ClientWidth-4)."px; height:".($this->Parent->ClientHeight-4)."\">{content}</div>";

      $control=$this->ThemeTemplate->Get();
      if($this->TabVisible!="False")
        $tabvisible="True";
      else
        $tabvisible="False";

      $js=<<<END
        <script language="javascript">
          {parent_name}Sheets[{no}]=new Array();
          {parent_name}Sheets[{no}]['caption']="{caption}";
          {parent_name}Sheets[{no}]['name']="{name}";
          {parent_name}Sheets[{no}]['tabvisible']="{tabvisible}";
        </script>
END;

      $js=str_replace("{tabvisible}",$tabvisible,$js);
      $js=str_replace("{parent_name}",strtoupper($this->Parent->Name),$js);
      $js=str_replace("{no}",$this->No,$js);
      $js=str_replace("{caption}",$this->Caption,$js);
      $js=str_replace("{pageindex}",$this->PageIndex,$js);
      $js=str_replace("{name}",strtoupper($this->Name),$js);
      $ret=str_replace("{content}",$js.$sheets.$control,$ret);

      $uuret=$this->ShowComponents();

      $ret=str_replace("{content}",$uuret,$ret);

      return $ret;
    }
  }

?>