<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFGoogleMaps extends TControl
  {
    function __construct($owner=null)
    {
      parent::__construct($owner);

      $this->Color=mapcolor("clBtnFace");
    }

    function Init()
    {
      parent::Init();

    }

    function nix()
    {
      $js=<<<END

            if (GBrowserIsCompatible())
            {
              map{NAME} = new GMap2(document.getElementById("map_{NAME}"));
              map{NAME}.addControl(new GSmallMapControl());
              map{NAME}.addControl(new GMapTypeControl());
            }
END;

      $js=str_replace("{NAME}",strtoupper($this->Name),$js);

      $this->ca->JS($js);

    }

    function setCenter($y,$x,$z)
    {
      /*
      $y="53.145345";
      $x="8.20207";
      $z="11";
      */

      $js=<<<END

            if (GBrowserIsCompatible())
            {
              map{NAME}.setCenter(new GLatLng({y}, {x}), {z});
            }
END;

      $js=str_replace("{NAME}",strtoupper($this->Name),$js);
      $js=str_replace("{x}",$x,$js);
      $js=str_replace("{y}",$y,$js);
      $js=str_replace("{z}",$z,$js);

      $this->ca->JS($js);

    }

    function Set($property,$value)
    {
      parent::Set($property,$value);

      if($property=="COLOR")
      {
        $js=<<<END

          var o=document.getElementById("{NAME}");
          o.style.backgroundColor="{value}";
END;
        $js=str_replace("{NAME}",strtoupper($this->Name),$js);
        $js=str_replace("{value}",strtoupper($value),$js);

        $this->ca->JS($js);
      }


    }

    function Get()
    {
      parent::Get();
      //die($this->Key);
      $ret=$this->Template;

      if(!$this->Font)
        $this->Font=new TFont();

      $control=str_replace("{content}",$this->ThemeTemplate->Get(),$ret);
      $control=str_replace("{key}",$this->Key,$control);
      $control=str_replace("{Lat}",$this->Lat,$control);
      $control=str_replace("{Lng}",$this->Lng,$control);
      $control=str_replace("{Zoom}",$this->Zoom,$control);

      $load=1;
      $control=str_replace("{load}",$load,$control);

      return str_replace("{content}",$this->ShowComponents(),$control);
    }
  }

?>