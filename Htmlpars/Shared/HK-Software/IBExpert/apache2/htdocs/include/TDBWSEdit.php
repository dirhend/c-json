<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBEdit extends TControl
  {
    var $datasource;
    var $dataset;

    function __construct($owner=null)
    {
      die();
      parent::__construct($owner);

      $this->BorderStyle="bsSingle";
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
    }

    function Set($attribute,$value)
    {
      parent::Set($attribute,$value);

      if($attribute=="TEXT")
        $this->ca->ModifyAttribute($this,"value",$value);

      if($attribute=="update")
      {
        $this->Set("TEXT",$value);

        $js=<<<END

          var o=document.getElementById("{NAME}");

          if("{readonly}"=="True")
            o.disabled="true";
          else
            o.disabled="";
END;

        $js=str_replace("{NAME}",strtoupper($this->Name),$js);
        $js=str_replace("{readonly}",$this->dataset->ReadOnly,$js);
        $this->ca->JS($js);
      }

    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();

      $type="text";
      if($this->PasswordChar)
        $type="password";

      //$control="<input id=\"".strtoupper($this->Name)."\" name=\"".$this->Name."\" style=\"".$this->GetStyle()." height:".($this->Height-1)."px;\" type=\"".$type."\" value=\"".$value."\">";
      $control=$this->ThemeTemplate->Get();

      $ret=str_replace("{content}",$control,$this->Template);

      return $ret;
    }

    function Show()
    {
      echo $this->Get();
    }

  }

?>