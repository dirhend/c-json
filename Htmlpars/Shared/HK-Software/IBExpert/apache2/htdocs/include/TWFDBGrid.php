<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TWFDBGrid extends TControl
  {
    var $datasource;
    var $dataset;

    function __construct($owner=null)
    {
      parent::__construct($owner);
    }

    function Init()
    {
      parent::Init();

      $this->datasource=$this->GetComponentByName($this->DataSource);
      $this->dataset=$this->GetComponentByName($this->datasource->DataSet);
      //echo "columns:"." (".$this->Columns->Items[0]->FieldName.")<br>";
    }

    function Action($object,$action,$action_value)
    {
      parent::Action($object,$action,$action_value);

      if($object==$this->Name && $action=="goto" && $this->dataset)
        $this->dataset->Goto($action_value);
    }

    function Set($property,$value)
    {
      parent::Set($property,$value);

              //$this->ca->alert($property);
      if($property=="ROWHEIGHT")
      {
        $js=<<<END
          WFDBGrid{name}.SetRowHeight({height});

END;
        if(!$value)
          $value=0;

        $js=str_replace("{name}",$this->Name,$js);
        $js=str_replace("{height}",$value,$js);

        $this->RowHeight=$value;

        $this->ca->JS($js);
      }
      else if($property=="ROW")
      {
        //if($this->isSetROW)
        //  return;
        $this->isSetROW=true;

        $js=<<<END
          WFDBGrid{name}.SetRowIndex({cursor});

          /*
          var sel_col=new Array();
          sel_col[0]={cursor};
          WFDBGrid{name}.setSelectedProperty("rows", sel_col);


          WFDBGrid{name}.setCurrentRow({cursor});
          */
END;
        if(!$value)
          $value=0;

        $js=str_replace("{name}",$this->Name,$js);
        $js=str_replace("{height}",$this->Height,$js);
        $js=str_replace("{cursor}",$value,$js);

        $this->ca->JS($js);
      }
      else if($property=="ROW")
      {
        //if($this->isSetROW)
        //  return;
        $this->isSetROW=true;

        $js=<<<END
          WFDBGrid{name}.SetRowIndex({cursor});

          /*
          var sel_col=new Array();
          sel_col[0]={cursor};
          WFDBGrid{name}.setSelectedProperty("rows", sel_col);


          WFDBGrid{name}.setCurrentRow({cursor});
          */
END;
        if(!$value)
          $value=0;

        $js=str_replace("{name}",$this->Name,$js);
        $js=str_replace("{height}",$this->Height,$js);
        $js=str_replace("{cursor}",$value,$js);

        $this->ca->JS($js);
      }
      else if($property=="ROWS")
      {
        //if($this->isSetROWS)
        //  return;

        //$this->ca->alert($value['rows'][0]['BEZ']);
        //$this->ca->Alert($this->Name.".".$property."=".$value." (".count($value['rows']).")");

        if($value['rows'])
          $this->isSetROWS=true;

        $js=<<<END
          //var newColumns=["a","b"];

          //WFDBGrid{name}Columns=["a","b"];
          //WFDBGrid{name}Data=[["a1","a2"],["b1","b2"]];

          //WFDBGrid{name}.setHeaderText(function(i){return WFDBGrid{name}Columns[i]});
          //WFDBGrid{name}.setCellText(function(i, j){return WFDBGrid{name}Data[i][j]});

          //WFDBGrid{name}.setRowCount(2);
          //WFDBGrid{name}.setColumnCount(2);

          //WFDBGrid{name}.refresh();


END;

        if($this->dataset->query)
          $tmp_fields=$this->dataset->query->remotedata['fields'];

        if(!$this->Columns->Items[0]->FieldName)
          for($i=0;$i<count($tmp_fields);$i++)
          {
            $fields[$i]=$tmp_fields[$i];
            $fields[$i]['caption']=$fields[$i]['alias'];
          }
        else
          for($i=0;$i<count($this->Columns->Items);$i++)
          {
            for($j=0;$j<count($tmp_fields);$j++)
            {
              if($tmp_fields[$j]['alias']==$this->Columns->Items[$i]->FieldName)
              {
                $index=count($fields);
                $fields[$index]=$tmp_fields[$j];
                if($this->Columns->Items[$i]->Title->Caption)
                  $fields[$index]['caption']=$this->Columns->Items[$i]->Title->Caption;
                else
                  $fields[$index]['caption']=$this->Columns->Items[$i]->FieldName;
              }
            }
          }

        $columns="";
        $visibles="";
        $colwidths="";
        $colvisibles="";
        if($this->Columns->Items[0]->FieldName)
        {
          for($i=0;$i<count($this->Columns->Items);$i++)
          {
            if($this->Columns->Items[$i]->Visible!="False")
            {
              if($columns!="")
                $columns.=",";
              if($colwidths!="")
                $colwidths.=",";

              if(!$this->Columns->Items[$i]->Title->Caption)
                $this->Columns->Items[$i]->Title->Caption=$this->Columns->Items[$i]->FieldName;

              $columns.="\"".$this->Columns->Items[$i]->Title->Caption."\"";

              if($this->Columns->Items[$i]->Width)
                $colwidth=$this->Columns->Items[$i]->Width;
              else
                $colwidth=$fields[$i]['length']*6;

              $this->Columns->Items[$i]->Width=$colwidth;
              $colwidths.=$colwidth;
            }
          }
        }
        else
        {
          for($i=0;$i<count($fields);$i++)
          {
            if($fields[$i]['alias']!="xID")
            {
              if($columns!="")
                $columns.=",";
              if($colwidths!="")
                $colwidths.=",";

              $columns.="\"".$fields[$i]['caption']."\"";

              $colwidth=$fields[$i]['length']*6;

              //$this->ca->alert("colwidth:".$colwidth);
              $this->Columns->Items[$i]->Width=$colwidth;
              $colwidths.=$colwidth;
            }
            //else
            //  $colwidths.=2;
          }
        }

        for($i=0;$i<$value['max_cursor'];$i++)
          $row=$this->dataset->query->GetRow($i);

        $isData=0;
        $data="";
        /*
        for($j=0;$j<count($fields);$j++)
        {
          $isData=1;

          if($j>0)
            $data.=",";

          $data.="[";
          for($i=0;$i<$value['max_cursor'];$i++)
          {
            if($i>0)
              $data.=",";

            if($fields[$j]['type']=="BLOB")
              //$data.="\"<span style='color:gray;'>(blob)</span>\"";
              $data.="\"-\"";
            else
              //$data.="\"".utf8_encode($this->dataset->query->remotedata['rows'][$i][$value['fields'][$j]['alias']])."\"";
              $data.="\"".utf8_encode($this->dataset->query->remotedata['rows'][$i][$fields[$j]['alias']])."\"";
          }
          $data.="]";
        }

        $data="[".$data."]";
        if($isData)
        {
          $fh=fopen("log.txt","wb+");
          fwrite($fh,$data);
          fclose($fh);
        }
        */

        for($i=0;$i<$value['max_cursor'];$i++)
        {
          if($i>0)
            $data.=",";

          $data.="[";
          $jj=0;
          for($j=0;$j<count($fields);$j++)
          {
            if($fields[$j]['alias']!="xID")
            {
              $isData=1;

              if($jj>0)
                $data.=",";

              if($fields[$j]['type']=="BLOB")
                //$data.="\"<span style='color:gray;'>(blob)</span>\"";
                $data.='"-"';
              else
              {
                //$data.="\"".utf8_encode($this->dataset->query->remotedata['rows'][$i][$value['fields'][$j]['alias']])."\"";
                $f=$this->dataset->query->remotedata['rows'][$i][$fields[$j]['alias']];
                $f=str_replace("\"","\\\"",$f);
                $data.="\"".utf8_encode($f)."\"";
              }

              $jj++;
            }
          }
          $data.="]";
        }

        $data="[".$data."]";

        /*
        for($i=0;$i<$value['max_cursor'];$i++)
        {
          $row=$this->dataset->query->GetRow($i);
          //echo $row['BEZ'].".";

          $js.=<<<END


END;
          $js=str_replace("{BEZ}",utf8_encode($row['BEZ']),$js);
        }
        */

        $js.=<<<END

          var WFDBGrid{name}Columns={columns};
          var WFDBGrid{name}Data={data};
          var WFDBGrid{name}ColWidths={colwidths};
          //var WFDBGrid{name}ColVisible={colvisibles};

          WFDBGrid{name}.ShowWait();
          WFDBGrid{name}.SetColWidths(WFDBGrid{name}ColWidths);
          WFDBGrid{name}.SetCols(WFDBGrid{name}Columns);
          WFDBGrid{name}.SetRows(WFDBGrid{name}Data);
          WFDBGrid{name}.SetRowIndex(0);

          //alert(WFDBGrid{name}ColWidths);

          //alert(WFDBGrid{name}Columns);
          /*
          //WFDBGrid{name}.setHeaderText(function(i){return WFDBGrid{name}Columns[i]});
          //WFDBGrid{name}.setCellText(function(i, j){return WFDBGrid{name}Data[i][j]});

          WFDBGrid{name}.setColumnCount(0);
          WFDBGrid{name}.setRowCount(0);
          WFDBGrid{name}.refresh();

          WFDBGrid{name}Columns={columns};
          WFDBGrid{name}Data={data};

          WFDBGrid{name}.setCellText(function(i, j){return WFDBGrid{name}Data[i][j]});
          WFDBGrid{name}.setHeaderText(function(i){return WFDBGrid{name}Columns[i]});
          //WFDBGrid{name}.setCellText(WFDBGrid{name}Data);
          //WFDBGrid{name}.setHeaderText(WFDBGrid{name}Columns);

          WFDBGrid{name}.setRowCount({row_count});
          WFDBGrid{name}.setColumnCount({col_count});

          setTimeout("WFDBGrid{name}.refresh();",1);
          */
          //WFDBGrid{name}.refresh();
END;

        $js=str_replace("{columns}","[".$columns."]",$js);
        $js=str_replace("{colwidths}","[".$colwidths."]",$js);
        $js=str_replace("{data}",$data,$js);

        $js=str_replace("{col_count}",count($fields),$js);
        $js=str_replace("{row_count}",$value['max_cursor'],$js);

        $js=str_replace("{name}",$this->Name,$js);
        $js=str_replace("{NAME}",strtoupper($this->Name),$js);

        $this->ca->JS($js);

        //$this->ca->Alert(count($value));
      }
    }

    function Get()
    {
      parent::Get();

      if(!$this->Font)
        $this->Font=new TFont();
      if(!$this->TitleFont)
        $this->TitleFont=new TFont();

      $control=<<<END
         <div id="{name}"></div>

	<script language="javascript">

      var WFDBGrid{name}Data={data};

      var WFDBGrid{name}Columns = [
          {columns}
      ];

      function wfdbgrid_{name}_goto(row)
      {
        alert("row:"+row);
        //{server}.Request("object={name}&action=goto");
      }

      var WFDBGrid{name}CurrentRow=-1;
      var WFDBGrid{name}SelectRowTimer=-1;

      var WFDBGrid{name}=new JSGrid();

      WFDBGrid{name}.ResizeGripImage="{resizegripimage}";
      WFDBGrid{name}.Left={left};
      WFDBGrid{name}.Top={top};
      WFDBGrid{name}.Width={width};
      WFDBGrid{name}.Height={height};
      WFDBGrid{name}.Name="WFDBGrid{name}";
      WFDBGrid{name}.ID="{name}";
      WFDBGrid{name}.Font_Name="{font_name}";
      WFDBGrid{name}.Font_Size="{font_size}";
      WFDBGrid{name}.Font_Color="{font_color}";
      WFDBGrid{name}.TitleFont_Name="{titlefont_name}";
      WFDBGrid{name}.TitleFont_Size="{titlefont_size}";
      WFDBGrid{name}.TitleFont_Color="{titlefont_color}";

      WFDBGrid{name}.OnRowSelect = function(rowIndex)
      {
        if(WFDBGrid{name}CurrentRow!=rowIndex)
        {
          WFDBGrid{name}CurrentRow=rowIndex;
          {server}.Request("object={name}&action=goto&action_value="+rowIndex,true);
        }
      }

      WFDBGrid{name}.Show();

      /*
      var DBGrid{name} = new AW.UI.Grid();

      //DBGrid{name}.setColumnResizable(true);
      DBGrid{name}.setId('{name}')
      DBGrid{name}.setSize({width}, {height});

      DBGrid{name}.setCellText(function(i, j){return DBGrid{name}Data[i][j]});
      DBGrid{name}.setHeaderText(function(i){return DBGrid{name}Columns[i]});
      //DBGrid{name}.setCellText(DBGrid{name}Data);
      //DBGrid{name}.setHeaderText(DBGrid{name}Columns);
      DBGrid{name}.setColumnCount({anz_cols});
      DBGrid{name}.setRowCount({anz_rows});

      //DBGrid{name}.setSelectorVisible(true);
      //DBGrid{name}.setSelectorText(function(i){return this.getRowPosition(i)+1});
      //DBGrid{name}.setSelectorWidth("30");
      //DBGrid{name}.setCellEditable(true);

      //DBGrid{name}.setVirtualMode(true);

      //DBGrid{name}.setSelectionMode("multi-row");
      //DBGrid{name}.setRowPosition(3);

      //DBGrid{name}.setSelectionMode("multi-row-marker");
      DBGrid{name}.setSelectionMode("multi-row");

      //DBGrid{name}.onHeaderClicked=function (event,col)
      //{
      //}


      //DBGrid{name}.onSelectedRowsChanged = function(rowIndex)
      //{
      //  //alert(this.getSelectedRow());
      //  //alert(rowIndex);
      //
      //  if(DBGrid{name}CurrentRow<rowIndex
      //     || DBGrid{name}CurrentRow>rowIndex)
      //  {
      //    DBGrid{name}CurrentRow=rowIndex;
      //    DBGrid{name}SelectRowTimer=10;
      //
      //    setTimeout("DBGrid{name}SelectRow("+rowIndex+");",100);
      //  }
      //}


      DBGrid{name}.onRowClicked = function(event,rowIndex)
      {
        if(DBGrid{name}CurrentRow!=rowIndex)
        {
          DBGrid{name}CurrentRow=rowIndex;
          {server}.Request("object={name}&action=goto&action_value="+rowIndex);
        }
      }

      //function DBGrid{name}SelectRow(rowIndex)
      //{
      //  //alert(DBGrid{name}SelectRowTimer);
      //  DBGrid{name}SelectRowTimer--;
      //
      //  if(DBGrid{name}SelectRowTimer==0)
      //    server.Request("object={name}&action=goto&action_value="+rowIndex);
      //  else
      //    setTimeout("DBGrid{name}SelectRow("+rowIndex+");",100);
      //}

      document.write(DBGrid{name});

      if({isData})
      {
        var sel_col=new Array();
        sel_col[0]=0;
        DBGrid{name}.setSelectedProperty("rows", sel_col);
      }
      */

  </script>

END;

      /*
      $fields=array();
      if(!$this->Columns->Items[0]->FieldName)
      {
        if($this->dataset->query)
          $fields=$this->dataset->query->Fields();

        for($i=0;$i<count($fields);$i++)
          $fields[$i]['caption']=$fields[$i]['alias'];
      }
      else
      {
        if($this->dataset->query)
          $tmp_fields=$this->dataset->query->Fields();

        for($i=0;$i<count($this->Columns->Items);$i++)
        {
          for($j=0;$j<count($tmp_fields);$j++)
          {
            if($tmp_fields[$j]['alias']==$this->Columns->Items[$i]->FieldName)
            {
              $index=count($fields);

              $fields[$index]=$tmp_fields[$j];
              $fields[$index]['caption']=$this->Columns->Items[$i]->Title->Caption;
            }
          }
        }
      }

      $columns="";
      for($i=0;$i<count($fields);$i++)
      {
        if($columns)
          $columns.=",";

        $columns.="\"".$fields[$i]['caption']."\"";
      }


      $data="";
      $isData=0;

      for($j=0;$j<count($fields);$j++)
      {
        $isData=1;

        if($j>0)
          $data.=",";

        $data.="[";
        for($i=0;$i<$this->dataset->query->remotedata['max_cursor'];$i++)
        {
          if($i>0)
            $data.=",";

          if($fields[$j]['type']=="BLOB")
            $data.="\"<span style='color:gray;'>(blob)</span>\"";
          else
            $data.="\"".$this->dataset->query->remotedata['rows'][$i][$fields[$j]['alias']]."\"";
        }
        $data.="]";
      }

      //$isData=0;
      */

      $isData=0;
      $data="";

      if($data)
      {
        $data="[".$data."]";
        $anz_rows=$this->dataset->query->remotedata['max_cursor'];
        $anz_cols=count($fields);
      }
      else
      {
        $data="[[\"\"]]";
        $columns="\"\"";
        $anz_rows=0;
        $anz_cols=0;
      }

      $resizegripimage="images/".$this->Theme."/resizegrip.jpg";

      if(!file_exists($resizegripimage))
        $resizegripimage="images/resizegrip.jpg";

      $control=str_replace("{resizegripimage}",$resizegripimage,$control);

      $control=str_replace("{name}",$this->Name,$control);
      $control=str_replace("{left}",$this->Left,$control);
      $control=str_replace("{top}",$this->Top,$control);
      $control=str_replace("{width}",$this->Width,$control);
      $control=str_replace("{height}",$this->Height,$control);
      $control=str_replace("{columns}",$columns,$control);
      $control=str_replace("{data}",$data,$control);
      $control=str_replace("{isData}",$isData,$control);
      $control=str_replace("{anz_rows}",$anz_rows,$control);
      $control=str_replace("{anz_cols}",$anz_cols,$control);

      $fontsize=-($this->Font->Height);
      $control=str_replace("{font_name}",$this->Font->Name,$control);
      $control=str_replace("{font_size}",$fontsize,$control);
      $control=str_replace("{font_color}",$this->Font->Color,$control);

      $titlefontsize=-($this->TitleFont->Height);
      $control=str_replace("{titlefont_name}",$this->TitleFont->Name,$control);
      $control=str_replace("{titlefont_size}",$titlefontsize,$control);
      $control=str_replace("{titlefont_color}",$this->TitleFont->Color,$control);

      //$control="<input id=\"".$this->Name."\" name=\"".$this->Name."\" style=\"".$this->GetStyle()."\" type=\"text\" value=\"".$this->dataset->row[$this->DataField]."\">";

      //$control="x";
      $ret=str_replace("{content}",$control,$this->Template);

      return $ret;
    }

    function Show()
    {
      echo $this->Get();
    }

  }

?>