<?php

// IBExpertWebForms
// copyright  �  2006-2008 Holger Klemt hklemt@ibexpert.biz
// www.ibexpert.com


  class TForm extends TObject
  {
    var $dfm;
    var $Components;

    function TForm($owner)
    {
      parent::__construct($owner);

      $this->Components=new TComponents($this);

      //$this->Width=100;
      //$this->Height=100;
      $this->Name="new Form";

      $this->AddTop=$GLOBALS['top'];
      $this->AddLeft=$GLOBALS['left'];
      $this->z=$GLOBALS['z'];
    }

    function GetStyle()
    {
    }

    function Get()
    {
      global $id,$license,$lng;

      $ret="";

      //$this->Left=0;
      //$this->Top=0;

      if($this->Class=="TForm" && !$this->ClientHeight)
        $this->ClientHeight=$this->Height-34;
      if($this->Class=="TForm" && !$this->ClientWidth)
        $this->ClientWidth=$this->Width;

      if($this->ClientHeight)
        $this->Height=$this->ClientHeight+34;
      if($this->ClientWidth)
        $this->Width=$this->ClientWidth;

      if($this->ClientHeight)
      {
        $this->TitleHeight=$this->Height-$this->ClientHeight;
        $this->TitleWidth=$this->Widht;

        $this->ClientTop=$this->Top+$this->Height-$this->ClientHeight;
        $this->ClientLeft=$this->Left;
      }

      /*
      echo $this->Left."<br>";
      echo $this->Top."<br>";
      echo $this->Width."<br>";
      echo $this->Height."<br>";
      echo $this->ClientTop."<br>";
      */

      /*
      if($this->ClientWidth)
        $this->Width=$this->ClientWidth;

      if($this->ClientHeight)
        $this->Height=$this->ClientHeight+36;
      */

      $ret=<<<END

        <link rel="stylesheet" type="text/css" href="include/spaw/lib/themes/default/css/toolbar.css"></link>
        <link rel="stylesheet" type="text/css" href="runtime/styles/xp/aw.css"></link>

        <script language="JavaScript" src="js/JSGrid.js"></script>
        <script language="JavaScript" src="js/XMLHttp.js"></script>
        <script language="JavaScript" src="include/spaw/spaw_script.js.php5"></script>
        <script language="JavaScript" src="include/spaw/lib/themes/default/js/toolbar.js.php5"></script>
        <script language="JavaScript" src="js/base64.js"></script>
        <script language="JavaScript" src="js/utf8.js"></script>
        <script language="JavaScript" src="js/url.js"></script>
        <script src="runtime/lib/aw.js"></script>


        <script language="javascript">
          var spaw_active_toolbar = true;
          var MouseX;
          var MouseY;

          function init()
          {
            calender=new Calendar();
            {name}Server.Request("action=init");
          }

          function debug_aufzu()
          {
            var o=document.getElementById("debug");
            if(o.style.display=='none')
              o.style.display='';
            else
              o.style.display='none';
          }

          function debug_del()
          {
            var debug=document.getElementById("debug");
            debug.innerHTML='';
          }

          function debug(s)
          {
            var o=document.getElementById("debug");
            if(o.style.display!='none')
            {
             var shtml=s;
             while(shtml.indexOf("<")>-1)
               shtml=shtml.replace(/</,"&lt;");
             while(shtml.indexOf(">")>-1)
               shtml=shtml.replace(/>/,"&gt;");

              var debug=document.getElementById("debug");
              debug.innerHTML=debug.innerHTML+shtml+"<br>";

              var debug=document.getElementById("debug_text");
              debug.value=debug.value+s;
            }
          }

          function dpl_mouse_pos(e)
          {
            MouseX    =  e? e.pageX : window.event.x;
            MouseY    =  e? e.pageY : window.event.y;
            if (document.layers) routeEvent(e);
          }

          function GetTop(e,v)
          {
            if(e.parentNode)
              v=v+GetTop(e.parentNode,v);

            if(e.style)
            if(e.style.top)
            {
              var top=e.style.top;

              top=top.replace(/px/,"");
              v+=Number(top);
            }

            return v;
          }

          function GetLeft(e,v)
          {
            if(e.parentNode)
              v=v+GetLeft(e.parentNode,v);

            if(e.style)
            if(e.style.left)
            {
              var left=e.style.left;

              left=left.replace(/px/,"");
              v+=Number(left);
            }

            return v;
          }

          if(document.layers)
            document.captureEvents(Event.MOUSEMOVE);

          document.onmousemove=dpl_mouse_pos;

          var {name}Server=new JXMLHttp();
          {name}Server.ID="{id}";
          {name}Server.Form="{name}";
          {name}Server.Script="{php_self}";
          {name}Server.License="{license}";
          {name}Server.Lng="{lng}";
        </script>

    <div style="display:none;">
      <iframe width="100%" height="25%" src="about:blank" name="i{name}Server" id="i{name}Server"></iframe>
    </div>

        <form id="{name}Form" xid="form" method="post" target="i{name}Server" enctype="multipart/form-data">
          <input type="hidden" name="lng" value="{lng}">
          <input type="hidden" name="no_responde" value="1">
          <input type="hidden" name="ajax" value="1">
          <input type="hidden" name="action" value="commit">
          <input type="hidden" name="object" id="object" value="">
          {form}
          <div style="display:none;">
            <input id="{name}FormDataSubmit" type="submit">
          </div>
        </form>

END;

      $this->ThemeTemplate->Set("TitleTop",$this->Top+$this->AddTop);
      $this->ThemeTemplate->Set("TitleLeft",$this->Left+$this->AddLeft);
      $this->ThemeTemplate->Set("TitleWidth",$this->Width-7);
      $this->ThemeTemplate->Set("TitleHeight",33);
      $this->ThemeTemplate->Set("tz",$this->z);

      $this->ThemeTemplate->Set("FormTop",$this->Top+$this->AddTop+33);
      $this->ThemeTemplate->Set("FormLeft",$this->Left+$this->AddLeft);
      $this->ThemeTemplate->Set("FormWidth",$this->Width-7);
      $this->ThemeTemplate->Set("FormHeight",$this->Height-33);
      $this->ThemeTemplate->Set("FormColor",$this->Color);
      $this->ThemeTemplate->Set("z}",$this->z);

      $ret=str_replace("{form}",$this->ThemeTemplate->Get(),$ret);

      $ret=str_replace("{caption}",$this->Caption,$ret);
      $ret=str_replace("{id}",$id,$ret);

      $ret=str_replace("{lng}",$lng,$ret);

      $ret=str_replace("{name}",$this->Name,$ret);
      $ret=str_replace("{php_self}",$_SERVER['PHP_SELF'],$ret);
      $ret=str_replace("{license}",$license,$ret);


      /*
      $ret=str_replace("{TitleTop}",$this->Top+$this->AddTop,$ret);
      $ret=str_replace("{TitleTop}",$this->Top+$this->AddTop,$ret);
      $ret=str_replace("{TitleLeft}",$this->Left+$this->AddLeft,$ret);
      $ret=str_replace("{TitleWidth}",$this->Width-7,$ret);
      $ret=str_replace("{TitleHeight}",33,$ret);
      $ret=str_replace("{tz}",$this->z,$ret);

      $ret=str_replace("{FormTop}",$this->Top+$this->AddTop+33,$ret);
      $ret=str_replace("{FormLeft}",$this->Left+$this->AddLeft,$ret);
      $ret=str_replace("{FormWidth}",$this->Width-7,$ret);
      $ret=str_replace("{FormHeight}",$this->Height-33,$ret);
      $ret=str_replace("{FormColor}",$this->Color,$ret);
      $ret=str_replace("{z}",$this->z,$ret);
      */
      $components=$this->ShowComponents();

      $components=str_replace("{server}",$this->Name."Server",$components);

      $ret=str_replace("{content}",$components,$ret);

      return $ret;
    }

    function GetRoot()
    {
      $owner=$this->Owner;

      if($owner)
      {
        while(!$owner)
          $owern=$owner->Owner;
      }
      else
        $owner=$this;

      return $owner;
    }

    function GetComponentByName($name)
    {
      $owner=$this->GetRoot();

      return $this->ParseComponents($owner,"search",$name);
    }

    function ParseComponents($root,$type,$value="",$ret=null)
    {
      if(!$root)
        $root=$this->GetRoot();

      for($i=0;$i<$root->Components->Length;$i++)
      {
        if($type=="search" && $root->Components->Components[$i]->Name==$value)
          $ret=$root->Components->Components[$i];
        else if($type=="Call")
        {
          $arr=explode(";",$value);

          if(method_exists($root->Components->Components[$i],$arr[0]))
          {
            $arr[1]=str_replace("{name}",$root->Components->Components[$i]->Name,$arr[1]);
            $eval_str='$ret=$root->Components->Components[$i]->'.$arr[0].'('.$arr[1].');';
            eval($eval_str);
          }
        }
        else if($type=="FindByProperty")
        {
            //echo $value;
          $arr=explode(";",$value);

          eval('$v=$root->Components->Components[$i]->'.$arr[0].';');
          if($v==$arr[1])
          {
            if(!$ret)
              $ret=array();

            $ret[count($ret)]=$root->Components->Components[$i];
          }
        }

        $ret=$this->ParseComponents($root->Components->Components[$i],$type,$value,$ret);
      }

      return $ret;
    }
  }

?>