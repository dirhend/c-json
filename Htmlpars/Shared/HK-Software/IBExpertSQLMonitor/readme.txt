Common informations about IBMonitor:

This is an Interbase/Firebird administrator/developper tool, 
combine SQL monitor functionality with server performance motitoring.
SQL monitor ability is access library independent, so you can log SQL 
traffic made by any components or tools, connecting to IB/FB by tcp/ip.

Main module (hkProxy service) is a NT service, working as proxy server 
between client and IB/FB server. It logs SQL traffic and calculate traffic 
statistics.

StatToHtml service transform logs and statistics into HTML form, also filter 
logs by execution time, enabling You to see only time consuming statements.

StatToDB service made to write logs to selected IB/FB database, enabling You 
made any further analisys of its contents.


Features list:
	- log all SQL command, with corresponding plans and execution time.

	- filter SQL commands by include/exclude template.
		for example, if you don't want to log system (containing "RDB$") traffic
		or want to log only SALES table related statements

	- calcule traffic statistics (i/o bytes, statemets count) by hostnames and sessions

	- transform all logs and stats into html form and upload it on selected ftp (*)

	- filter logged statements by execution time, 
		if You want to see only time consuming commands (*)

	- save all logs into selected database, if needed for further analisys (**)

	- show runtime SQL traffic and server performance info in tables and diagrams
		(common statistics, active connections, etc.)

	- separate services with flexible setup for specific functionalities

	- single Control Center for all modules. Also used for runtime info monitoring.
	

(*) 	using separate StatToHtml service
(**) 	using separate StatToDB service



2. Modules configuration

______________SQL Proxy (hkProxy)___________

	Bind_IP, Bind_Port	- IP address and port, to get packets from.
				You have add this IP/Port to database name, which traffic You want to log.
				Default is 127.0.0.1/3051

	Map_IP, Map_Port	- IP address and port, to redirect packets from Bind_IP/Port
				Default is 127.0.0.1/3050

	Log_Dir			- folder to save write logfiles (must have trailing slash)
				Default is C:\temp\

	StatsSaveInterval	- time interval (in seconds) of traffic statistics saving
				Default is 5 

	DateTimeFormat		- date and time format to be used in log files
				Default is YYYY/MM/DD HH:NN:SS

	NoPacketTimeout		- time interval (in seconds). 
				If no packets passed for during time - connection is market as "timed out".
				Default is 120

	FullSaveOnServiceStop	- if FALSE then only changed connections statistics will saved on service stop.
				Default is FALSE

	FILTER=			- set of rules to filter traffic by statement contents.
				Example: 
					"FILTER=-RDB$;-IBE$" - will cut all statements containing "RDB$" or "IBE$"
					"FILTER=+EMPL;+CUST" - will log only statements containing "EMPL" or "CUST"
				no default value

	StatusRefreshInterval	- time interval (in seconds) of runtime info refresh
				Default is 5

	LogLevels:
	_SELECT, _CREATE, 
	_ALTER, _DROP, 
	_INSERT, _UPDATE, 
	_DELETE, _EXECUTELEVEL  - log detailing level for different SQL statements.
				SQL 		- statement only 
				SQL_TIME	- statement and execution time
				SQL_TIME_PLAN	- statement, plan and execution time
				Default is 
					SQL_TIME_PLAN 	for SELECT and EXECUTE 
					SQL_TIME	for other
	
	ProcessPriority		- service's execution priority (Normal, Idle). 
				Default if Idle.
	

______________StatToHtml___________

	Log_Dir			- path to SQL proxy logfiles (must have trailing slash) 
				Default is C:\temp\

	StatsSaveInterval	- time interval (in seconds) of statistics and logfiles processing
				Default is 30

	TimeFilter		- value to filter statements in logfiles by its execution time
				Example: 			
					TimeFilter=30 msec - will put to html only statements, 
							which execution time>=30msec
				no default value

	WrapLineLength		- maximal line length of SQL statements, to write in html. 
				More long lines will be wrapped.
				Default is 0 (do not wrap)

	HighLightSQL		- if TRUE, then SQL statements in html will be highlighted for more readability.
				Default is TRUE

	DateTimeFormat		- date and time format to be used in statistics html files
				Default is YYYY/MM/DD HH:NN:SS

	HtmlRefreshInterval	- html pages autorefresh interval (in seconds)
				Default is 5

	StatusRefreshInterval	- time interval (in seconds) of runtime info refresh
				Default is 5

	Host, Port		- host and port, to upload html files to, by FTP protocol
				Default is localhost/21

	User, Password		- FTP server username and password
				no default value

	TmpDir			- path (with trailing slash) to service's temp folder, 
				to store html files, uploaded to FTP.
				Default is <application folder>+FtpTmp

	ProcessPriority		- service's execution priority (Normal, Idle). 
				Default and recommended value is Idle.

______________StatToDB___________

	DatabaseName		- database, to write logfiles into
	UserName, Password	- username and password for database (default SYSDBA/masterkey)
	SqlRole			- SQL role for database
				no default value	

	CharactersSet		- database's character set
				no default value

	StatsSaveInterval	- time interval (in seconds) of logfiles saving to database
				Default is 10

	Log_Dir			- path to SQL proxy logfiles (must have trailing slash) 
				Default is C:\temp\

	StatusRefreshInterval	- time interval (in seconds) of runtime info refresh
				Default is 5

	ProcessPriority		- service's execution priority (Normal, Idle). 
				Default and recommended value is Idle.






