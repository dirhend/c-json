Common informations about Backup Restore Service:

What is the IBExpertBackupRestore Scheduler?

The IBExpertBackupRestore Scheduler service is a comprehensive 
utility providing automatic backup and restore facilities for 
Firebird and InterBase databases, including backup file 
compression and even an option to automatically mail backup/restore 
log files. This service is part of the HK-Software IBExpert 
Developer Studio for Firebird and InterBase database 
development and administration.

See also http://www.ibexpert.net/ibe

The IBExpert Team
www.ibexpert.com

