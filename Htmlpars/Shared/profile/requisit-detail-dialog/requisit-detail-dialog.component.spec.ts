import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitDetailDialogComponent } from './requisit-detail-dialog.component';


describe('RequisitDetailDialogComponent', () => {
  let component: RequisitDetailDialogComponent;
  let fixture: ComponentFixture<RequisitDetailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisitDetailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
