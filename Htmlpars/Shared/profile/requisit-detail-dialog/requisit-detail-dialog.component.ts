import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { ClientRequisit } from 'app/main/shared/models/client-requisit.model';
import { DadataBankInfo, DadataClientInfo, DadataUserInfo } from 'app/main/shared/models/dadata.model';
import { Employee } from 'app/main/shared/models/employee.model';
import { MyValidator } from 'app/main/shared/validators/my-validator';
import * as _ from 'lodash';

import { EmployeeDetailDialogComponent } from '../employee-detail-dialog/employee-detail-dialog.component';
import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { RequisitDetailService } from './requisit-detail-dialog.service';

declare let $: any;

@Component({
  selector: 'app-requisit-detail-dialog',
  templateUrl: './requisit-detail-dialog.component.html',
  styleUrls: ['./requisit-detail-dialog.component.scss']
})
export class RequisitDetailDialogComponent implements OnInit, AfterViewInit {

  editItem: ClientRequisit;

  editForm: FormGroup;
  editFormErrors: any;

  EmployeeDetailDialogRef: MatDialogRef<EmployeeDetailDialogComponent>;

  constructor(
    private formBuilder: FormBuilder,
    public translationLoader: FuseTranslationLoaderService,
    public dialogRef: MatDialogRef<RequisitDetailDialogComponent>,
    public dialog: MatDialog,
    public requisitService: RequisitDetailService
  ) {
    this.translationLoader.loadTranslations(english, russian);

    this.editFormErrors = {
      name: {},
      inn: {},
      kpp: {},
      officialAddress: {},
      postAddress: {},
      phone: {},
      ceoname: {},
      accountantname: {},
      bik: {},
      bankname: {},
      bankaccount: {},
      account: {}
    };
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      name: ['', Validators.required],
      inn: ['', [Validators.required, MyValidator.minLength(10), MyValidator.maxLength(12)]],
      kpp: [''],
      officialAddress: ['', Validators.required],
      postAddress: [''],
      phone: [''],
      ceoname: ['', Validators.required],
      accountantname: ['', Validators.required],
      bik: ['', [Validators.required, MyValidator.minLength(9)]],
      bankname: ['', Validators.required],
      bankaccount: ['', [Validators.required, MyValidator.minLength(20)]],
      account: ['', [Validators.required, MyValidator.minLength(20)]]
    });

    this.editForm.valueChanges.subscribe(() => {
      this.onFormValuesChanged();
    });

    if (this.editItem.ceo.surname !== '') {
      this.editForm.controls.ceoname.setValue(this.editItem.ceo.surname + ' ' + this.editItem.ceo.name + ' ' + this.editItem.ceo.patronymic);
    }

    if (this.editItem.accountant.surname !== '') {
      this.editForm.controls.accountantname.setValue(this.editItem.accountant.surname + ' ' + this.editItem.accountant.name + ' ' + this.editItem.accountant.patronymic);
    }

    if (this.editItem.bank.name !== '') {
      this.editForm.controls.bankname.setValue(this.editItem.bank.name);
      this.editForm.controls.bik.setValue(this.editItem.bank.bik);
      this.editForm.controls.bankaccount.setValue(this.editItem.bank.account);
    }
    this.editForm.controls.account.setValue(this.editItem.bankAccount);

    this.fillForm();
  }

  ngAfterViewInit() {
  }

  private removeDistricts(suggestions: any) {
    return suggestions.filter((suggestion: any) =>
      suggestion.data.city_district === null
    );
  }

  private fillForm(): void {
    if (!this.editItem) {
      this.editItem = new ClientRequisit();
    }

    this.editItem = _.cloneDeep(this.editItem);
    this.editForm.patchValue(this.editItem);
  }

  openEditEmployeeDialog(employee: Employee, callbackSuccess?: Function) {
    this.EmployeeDetailDialogRef = this.dialog.open(EmployeeDetailDialogComponent, {
      disableClose: false,
      minWidth: '700px',
      minHeight: '600px',
      maxHeight: '1000px'
    });

    this.EmployeeDetailDialogRef.componentInstance.editManager = employee;

    this.EmployeeDetailDialogRef.afterClosed().subscribe((result: Employee | undefined) => {
      if (result) {
        if (callbackSuccess) { callbackSuccess(result); }
      }
      this.EmployeeDetailDialogRef = null;
    });
  }

  onFormValuesChanged() {
    for (const field in this.editFormErrors) {
      if (!this.editFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.editFormErrors[field] = {};

      // Get the control
      const control = this.editForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.editFormErrors[field] = control.errors;
      }
    }
  }

  editCeoClick() {
    this.openEditEmployeeDialog(this.editItem.ceo, (result: Employee) => {
      this.editItem.ceo = result;
      this.editForm.controls.ceoname.setValue(result.surname + ' ' + result.name + ' ' + result.patronymic);
    });
  }

  editAccountantClick() {
    this.openEditEmployeeDialog(this.editItem.accountant, (result: Employee) => {
      this.editItem.accountant = result;
      this.editForm.controls.accountantname.setValue(result.surname + ' ' + result.name + ' ' + result.patronymic);
    });
  }

  closeDialogAndSaveData() {

    this.editItem.name = this.editForm.controls.name.value;
    this.editItem.inn = this.editForm.controls.inn.value;
    this.editItem.kpp = this.editForm.controls.kpp.value;
    this.editItem.officialAddress = this.editForm.controls.officialAddress.value;
    this.editItem.postAddress = this.editForm.controls.postAddress.value;
    this.editItem.phone = this.editForm.controls.phone.value;

    this.editItem.bank.name = this.editForm.controls.bankname.value;
    this.editItem.bank.bik = this.editForm.controls.bik.value;
    this.editItem.bank.account = this.editForm.controls.bankaccount.value;
    this.editItem.bankAccount = this.editForm.controls.account.value;

    this.dialogRef.close(this.editItem);
  }

  private onLoadingFirmInfo(data: any) {
    const clientInfo: DadataClientInfo = data.client;
    const userInfo: DadataUserInfo = data.ceo;

    this.editForm.controls.name.setValue(clientInfo.name.short_with_opf);
    this.editForm.controls.ceoname.setValue(clientInfo.management.name);
    this.editForm.controls.kpp.setValue(clientInfo.kpp);
    this.editForm.controls.officialAddress.setValue(clientInfo.address.value);

    this.editItem.ceo.surname = userInfo.surname;
    this.editItem.ceo.name = userInfo.name;
    this.editItem.ceo.patronymic = userInfo.patronymic;
    this.editItem.ceo.post = clientInfo.management.post;
  }

  private onLoadingBankInfo(data: DadataBankInfo) {
    this.editForm.controls.bankname.setValue(data.name.payment);
    this.editForm.controls.bankaccount.setValue(data.correspondent_account);
  }

  findFirmByInn() {
    this.requisitService.getFirmInfoByInn(this.editForm.controls.inn.value)
      .subscribe(data => this.onLoadingFirmInfo(data));
  }

  findBankByBik() {
    this.requisitService.getBankInfoByBik(this.editForm.controls.bik.value)
      .subscribe(data => this.onLoadingBankInfo(data));
  }

  numberKeyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    const inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode !== 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
}
