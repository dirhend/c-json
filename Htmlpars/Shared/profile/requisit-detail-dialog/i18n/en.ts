export const locale = {
    lang: 'en',
    data: {
        'REQUISIT_CARD': {
            'TITLE': 'Реквизиты компании',
            'OK': 'ОК',
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'NAME': 'Название',
                'INN': 'ИНН',
                'OFFICIAL_ADDRESS': 'Официальный адрес',
                'POST_ADDRESS': 'Почтовый адрес',
                'PHONE': 'Телефон',
                'KPP': 'КПП',
                'LOAD_BY_INN': 'Заполнить данные по ИНН',
                'LOAD_BY_BIK': 'Заполнить данные по БИК',
                'CEO': 'Руководитель',
                'ACCOUNTANT': 'Бухгалтер',
                'MAIN_INFO_TAB': 'Основная информация',
                'BANK_INFO_TAB': 'Банковские реквизиты',
                'CONTACT_INFO_TAB': 'Контактная информация',
                'ACCOUNT': 'Расчетный счет',
                'BANK': {
                    'BIK': 'БИК',
                    'NAME': 'Наименование банка',
                    'ACCOUNT': 'Корреспондентский счет',
                },
                'ERROR': {
                    'NAMEEMPTY': 'Название не задано',
                    'INNEEMPTY': 'ИНН не задан',
                    'INNINCORRECT': 'Некорректный ИНН',
                    'ACCOUNTEEMPTY': 'Расчетный счет не задан',
                    'OFFICIAL_ADDRESSEMPTY': 'Официальный адрес не задан',
                    'ACCOUNTINCORRECT': 'Некорректный расчетный счет',
                    'BANK': {
                        'NAMEEEMPTY': 'Наименование банка не задан',
                        'ACCOUNTEMPTY': 'Корреспондентский счет не задан',
                        'BIKEEMPTY': 'БИК не задан',
                        'BIKINCORRECT': 'Некорректный БИК',
                        'ACCOUNTINCORRECT': 'Некорректный корреспондентский счет'
                    }
                }
            }
        }
    }
};
