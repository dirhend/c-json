import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ProfileService } from './profile.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateManagerService } from '../../../shared/services/translate-manager.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { ApiLinks } from '../../../../app.api-links';
import { LocalStorageManager } from 'app/main/shared/helpers/local-storage-manager.helper';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class ProfileComponent implements OnInit {

  get client() {
    return LocalStorageManager.client;
  }

  constructor(
    public profileService: ProfileService,
    public translationLoader: FuseTranslationLoaderService,
    private translateManagerService: TranslateManagerService
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  ngOnInit() {
  }

  getAvatarName() {
    const defaultAvatar = 'assets/images/avatars/profile.jpg';
    return `${this.client ? (this.client.avatarName ? ApiLinks.avatarView(this.client.id) : defaultAvatar) : defaultAvatar}`;
  }

  uploadAvatar(file: File) {

    const input = new FormData();
    input.set('file', file);

    this.profileService.uploadAvatar(input)
      .then((avatarName: string) => {
        const client = LocalStorageManager.client;
        client.avatarName = avatarName;
        LocalStorageManager.client = client;

        this.translateManagerService.openSnackBarTranslated('PROFILE.SNACKBAR.AVATARUPLOADED', 'OK');
      })
      .catch(() => {
        this.translateManagerService.openSnackBarTranslated('PROFILE.SNACKBAR.AVATARUPLOADFAIL', 'OK');
      });
  }

  deleteAvatar() {
    this.profileService.deleteAvatar()
      .then(() => {
        const client = LocalStorageManager.client;
        client.avatarName = '';
        LocalStorageManager.client = client;

        this.translateManagerService.openSnackBarTranslated('PROFILE.SNACKBAR.AVATARUPDELETED', 'OK');
      })
      .catch(() => {
        this.translateManagerService.openSnackBarTranslated('PROFILE.SNACKBAR.AVATARDELETEFAIL', 'OK');
      });
  }

}
