export const locale = {
    lang: 'en',
    data: {
        'PROFILE': {
            'ADDAVATAR': 'Загрузить фотографию',
            'DELETEAVATAR': 'Удалить фотографию',
            'SNACKBAR': {
                'AVATARUPLOADED': 'Фотография загружена',
                'AVATARUPLOADFAIL': 'Не удалось загрузить фотографию',
                'AVATARUPDELETED': 'Фотография удалена',
                'AVATARDELETEFAIL': 'Не удалось удалить фотографию'
            }
        }
    }
};
