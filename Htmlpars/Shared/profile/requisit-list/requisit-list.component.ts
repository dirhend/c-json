import 'rxjs/add/observable/merge';

import { SelectionModel } from '@angular/cdk/collections';
import { DataSource } from '@angular/cdk/table';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { TranslateService } from '@ngx-translate/core';
import { RequisitListService } from 'app/main/content/client/profile/requisit-list/requisit-list.service';
import { ClientRequisit } from 'app/main/shared/models/client-requisit.model';
import { TranslateManagerService } from 'app/main/shared/services/translate-manager.service';
import { Observable } from 'rxjs';

import { ProfileService } from '../profile.service';
import { RequisitDetailDialogComponent } from '../requisit-detail-dialog/requisit-detail-dialog.component';
import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Component({
  selector: 'app-requisit-list',
  templateUrl: './requisit-list.component.html',
  styleUrls: ['./requisit-list.component.scss']
})
export class RequisitListComponent implements OnInit {

  dataSource: RequisitDataSource | null;
  displayedColumns = ['iscurrent', 'name', 'edit', 'delete'];
  selection = new SelectionModel<ClientRequisit>(false, []);

  DetailDialogRef: MatDialogRef<RequisitDetailDialogComponent>;
  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

  constructor(
    public profileService: ProfileService,
    public dialog: MatDialog,
    private translate: TranslateService,
    public translationLoader: FuseTranslationLoaderService,
    private translateManagerService: TranslateManagerService,
    public requisitListService: RequisitListService,
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  isSelectedRow(row: ClientRequisit) {
    return (row.id === this.requisitListService.defaultRequisitId);
  }

  changeSelection(row: any) {
    if (!this.selection.isSelected(row)) {
      this.selection.toggle(row);
      this.requisitListService.setDefaultRequest(this.selection.selected[0]);
    }
  }

  openAddRequisitDialog(callbackSuccess?: Function) {
    this.DetailDialogRef = this.dialog.open(RequisitDetailDialogComponent, {
      disableClose: false,
      minWidth: '700px',
      minHeight: '600px',
      maxHeight: '1000px'
    });

    this.DetailDialogRef.componentInstance.editItem = new ClientRequisit();

    this.DetailDialogRef.afterClosed().subscribe((result: ClientRequisit | undefined) => {

      if (result) {
        if (callbackSuccess) { callbackSuccess(result); }
      }
      this.DetailDialogRef = null;
    });
  }

  openEditRequisitDialog(requisit: ClientRequisit, callbackSuccess?: Function) {
    this.DetailDialogRef = this.dialog.open(RequisitDetailDialogComponent, {
      disableClose: false,
      minWidth: '700px',
      minHeight: '600px',
      maxHeight: '1000px'
    });

    this.DetailDialogRef.componentInstance.editItem = requisit;

    this.DetailDialogRef.afterClosed().subscribe((result: ClientRequisit | undefined) => {
      if (result) {
        if (callbackSuccess) { callbackSuccess(result); }
      }
      this.DetailDialogRef = null;
    });
  }

  addRequisit() {
    this.openAddRequisitDialog((result: ClientRequisit) => {

      this.requisitListService.addRequisit(result)
        .then(() => {
          this.translateManagerService.openSnackBarTranslated('REQUISIT_LIST.SNACKBAR.REQUISIT_ADDED', 'ОК');
        })
        .catch((err: HttpErrorResponse) => {
          if (err.status === 403) {
            this.translateManagerService.openSnackBarTranslated('REQUISIT_LIST.SNACKBAR.REQUISIT_EXIST', 'ОК');
          }
          else {
            this.translateManagerService.openSnackBarTranslated('REQUISIT_LIST.SNACKBAR.REQUISIT_ADDFAIL', 'ОК');
          }
        });

    });
  }

  editRequisit(requisit: ClientRequisit) {    
    this.openEditRequisitDialog(requisit, (result: ClientRequisit) => {
      this.requisitListService.editRequisit(result)
        .then(() => {
          for (const key of Object.keys(result)) {
            requisit[key] = result[key];
          }

          this.translateManagerService.openSnackBarTranslated('REQUISIT_LIST.SNACKBAR.REQUIESTSEDITED', 'ОК');
        })
        .catch((err: HttpErrorResponse) => {
          if (err.status === 403) {
            this.translateManagerService.openSnackBarTranslated('REQUISIT_LIST.SNACKBAR.REQUISIT_EXIST', 'ОК');
          }
        });
    });
  }

  deleteRequisit(requisit: ClientRequisit) {
    this.confirmDialogRef = this.dialog.open(FuseConfirmDialogComponent, {
      disableClose: false
    });

    this.translate.get('REQUISIT_LIST.SNACKBAR.REQUISITDELETE', { value: requisit.name }).subscribe((res: string) => {
      this.confirmDialogRef.componentInstance.confirmMessage = res;

      this.confirmDialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.requisitListService.deleteRequisit(requisit)
            .then(() => {
              this.translateManagerService.openSnackBarTranslated('REQUISIT_LIST.SNACKBAR.REQUISITDELETED', 'ОК');
            })
            .catch((err: HttpErrorResponse) => {
              if (err.status === 403) {
                this.translateManagerService.openSnackBarTranslated('REQUISIT_LIST.SNACKBAR.REQUISIT_IS_CURRENT', 'ОК');
              }
            });
        }
        this.confirmDialogRef = null;
      });
    });
  }

  ngOnInit() {
    this.dataSource = new RequisitDataSource(this.requisitListService);

    this.requisitListService.getRequisitsList();
  }
}

export class RequisitDataSource extends DataSource<any>
{
  constructor(
    private requisitListService: RequisitListService
  ) {
    super();
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {

    const displayDataChanges = [
      this.requisitListService.onRequisitsChanged
    ];
    return Observable.merge(...displayDataChanges).map(() => {

      const data: ClientRequisit[] = this.requisitListService.requisits;
      return data;
    });

  }

  disconnect() {
  }
}
