import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequisitListComponent } from './requisit-list.component';

describe('RequisitListComponent', () => {
  let component: RequisitListComponent;
  let fixture: ComponentFixture<RequisitListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequisitListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequisitListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
