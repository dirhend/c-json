import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpBackend, HttpHeaders } from '@angular/common/http';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { ClientRequisit } from 'app/main/shared/models/client-requisit.model';
import { ApiLinks } from 'app/app.api-links';


@Injectable()
export class RequisitListService {
  requisits: ClientRequisit[];
  onRequisitsChanged: BehaviorSubject<any> = new BehaviorSubject({});
  requisitsTotalCount = 0;
  defaultRequisitId = 0;

  private readonly urlRequisits: string = ApiLinks.requisits;
  
  private httpLocal: HttpClient;

  constructor(
    private handler: HttpBackend,
    private http: HttpClient
  ) {
    this.httpLocal = new HttpClient(handler);
  }

  /**
  * Resolve
  * @param {ActivatedRouteSnapshot} route
  * @param {RouterStateSnapshot} state
  * @returns {Observable<any> | Promise<any> | any}
  */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {

    return new Promise((resolve, reject) => {

      Promise.all([
        this.getRequisitsList()
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }

  getRequisitsList(): Promise<any> {
    const paramsHttp: HttpParams = new HttpParams();

    return new Promise((resolve, reject) => {
      this.http.get(this.urlRequisits, { params: paramsHttp })
        .map((res: {
          totalCount: number,
          requestsDto: ClientRequisit[],
          defaultId: number
        }) => {
          this.requisitsTotalCount = res.totalCount;
          this.defaultRequisitId = res.defaultId;
          return res.requestsDto;
        })
        .subscribe((response: any) => {
          this.requisits = response;
          this.onRequisitsChanged.next(this.requisits);
          resolve(response);
        }, reject);
    });
  }

  addRequisit(item: ClientRequisit): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.urlRequisits, item)
        .subscribe((response: ClientRequisit) => {
          this.requisits.push(response);
          this.onRequisitsChanged.next(this.requisits);

          this.requisitsTotalCount++;

          resolve(response);
        }, reject);
    });
  }

  editRequisit(item: ClientRequisit): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.urlRequisits}/${item.id}`, item)
        .subscribe(() => {
          this.updateRequisit(item);
          this.onRequisitsChanged.next(this.requisits);

          resolve();
        }, reject);
    });
  }

  deleteRequisit(item: ClientRequisit): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.urlRequisits}/${item.id}`)
        .subscribe(() => {
          this.requisits.splice(this.requisits.findIndex(s => s.id === item.id), 1);
          this.onRequisitsChanged.next(this.requisits);

          this.requisitsTotalCount--;

          resolve();
        }, reject);
    });
  }

  setDefaultRequest(item: ClientRequisit): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.urlRequisits}/${item.id}/SetDefault`, {})
        .subscribe(() => {
          this.defaultRequisitId = item.id;          
          this.onRequisitsChanged.next(this.requisits);

          resolve();
        }, reject);
    });
  }

  private updateRequisit(item: ClientRequisit) {
    const requisit = this.requisits.find(s => s.id === item.id);

    requisit.name = item.name;
    requisit.inn = item.inn;
    requisit.officialAddress = item.officialAddress;
    requisit.postAddress = item.postAddress;
  }
}
