export const locale = {
    lang: 'en',
    data: {
        'REQUISIT_LIST': {
            'TITLE': 'Реквизиты компании',
            'NEW_REQUISIT': 'Добавить реквизиты',
            'TABLE': {
                'IS_CURRENT': 'Тек.',
                'NAME': 'Название фирмы'
            },
            'SNACKBAR': {
                'REQUISIT_ADDED': 'Реквизиты были успешно добавлены',     
                'REQUISIT_EXIST': 'Реквизиты для указанной фирмы уже существует',
                'REQUISIT_ADDFAIL': 'Ну удалось добавить реквизиты',
                'REQUIESTSEDITED': 'Реквизиты были обновлены',
                'REQUISITDELETE': 'Удалить реквизиты {{value}}?',
                'REQUISITDELETED': 'Реквизиты были удалены',
                'REQUISIT_IS_CURRENT': 'Нельзя удалить текущие реквизиты'
                }
        }
    }
};
