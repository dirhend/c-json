export const locale = {
    lang: 'en',
    data: {
        'COMMON': {
            'TITLE': 'Common',
            'SAVE': 'SAVE',
            'FORM': {
                'GENERATEGUID': 'Generate new access key',
                'GUID': 'Ключ доступа',
                'COPYGUIDTOCLIPBOARD': 'Copy to clipboard',
                'COMPANY': 'Company name',
                'SURNAME': 'Surname',
                'NAME': 'Name',
                'PATRONYMIC': 'Patronymic',
                'PHONE': 'Mobile number',   
                'COUNTRY': 'Страна',     
                'CURRENCY': 'Валюта',    
                'NOTIFY_IN_SUPPORT': 'Уведомлять при ответе на обращения',        
                'ERROR': {
                    'NAMEEMPTY': 'Name is required',
                    'SURNAMEEMPTY': 'Surname is required',
                    'COUNTRYEMPTY': 'Страна не выбрана',
                    'CURRENCYEMPTY': 'Валюта не выбрана'
                }
            },
            'SNACKBAR': {
                'SUCCSAVE': 'Changes are saved',
                'ERRSAVE': 'Failed to save changes',
                'COPYIDTOCLIPBOARD': 'Ключ доступа copied to the clipboard'
            }
        }
    }
};
