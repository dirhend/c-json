import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateGuidDialogComponent } from './generate-guid-dialog.component';

describe('GenerateGuidDialogComponent', () => {
  let component: GenerateGuidDialogComponent;
  let fixture: ComponentFixture<GenerateGuidDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateGuidDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateGuidDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
