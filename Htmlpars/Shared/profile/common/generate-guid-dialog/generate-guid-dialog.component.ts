import { Component, OnInit } from '@angular/core';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { MatDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { ProfileService } from '../../profile.service';
import { TranslateManagerService } from 'app/main/shared/services/translate-manager.service';

@Component({
  selector: 'app-generate-guid-dialog',
  templateUrl: './generate-guid-dialog.component.html',
  styleUrls: ['./generate-guid-dialog.component.scss']
})
export class GenerateGuidDialogComponent implements OnInit {

  public hide = true;
  public password: FormControl = new FormControl('', [Validators.required, Validators.maxLength(100)]);

  constructor(
    public translationLoader: FuseTranslationLoaderService,
    public dialogRef: MatDialogRef<GenerateGuidDialogComponent>,
    public profileService: ProfileService,
    private translateManagerService: TranslateManagerService
  ) {
    this.translationLoader.loadTranslations(english, russian);    
  }

  ngOnInit() {
  }

  generateGuid() {

    this.profileService.getNewGUID(this.password.value)
    .then(() => {
      this.translateManagerService.openSnackBarTranslated('GENGUIDDIALOG.SNACKBAR.NEWGUIDGENERATED', 'OK');
      this.dialogRef.close();
    })
    .catch(() => {
      this.translateManagerService.openSnackBarTranslated('GENGUIDDIALOG.SNACKBAR.NEWGUIDGENERATEFAIL', 'OK');
    });
  }

  getErrorMessage() {
    const mess = this.password.hasError('required') ? 'GENGUIDDIALOG.PASSWORD.ERROR.REQUIRED' :
      this.password.hasError('maxlength') ? 'GENGUIDDIALOG.PASSWORD.ERROR.MAXLEN' : '';

    return mess;
  }
}
