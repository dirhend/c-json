import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { ApiLinks } from 'app/app.api-links';
import { LocalStorageManager } from 'app/main/shared/helpers/local-storage-manager.helper';
import { LocalizationHelper } from 'app/main/shared/helpers/localization.helper';
import { Country } from 'app/main/shared/models/country.model';
import { Currency } from 'app/main/shared/models/currency.model';
import { TranslateManagerService } from 'app/main/shared/services/translate-manager.service';
import { NgxLocalizedNumbersService } from 'ngx-localized-numbers';

import { ProfileService } from '../profile.service';
import { GenerateGuidDialogComponent } from './generate-guid-dialog/generate-guid-dialog.component';
import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

declare let $: any;
@Component({
  selector: 'app-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.scss']
})
export class CommonComponent implements OnInit, AfterViewInit {

  profileForm: FormGroup;
  profileFormErrors: any;
  showApiKeyPerm = false;

  generateGUIDDialogRef: MatDialogRef<GenerateGuidDialogComponent>;

  @ViewChild('guidInp') guidInp: ElementRef;
  @ViewChild('country') country: ElementRef;
  selectedCountry: Country;
  @ViewChild('currency') currency: ElementRef;
  selectedCurrency: Currency;

  constructor(
    private formBuilder: FormBuilder,
    public profileService: ProfileService,
    public translationLoader: FuseTranslationLoaderService,
    private translateManagerService: TranslateManagerService,
    public dialog: MatDialog,
    private localizedNumbersService: NgxLocalizedNumbersService,
  ) {
    this.translationLoader.loadTranslations(english, russian);

    this.profileFormErrors = {
      country: {},
      currency: {},
      nameFirm: {},
      personSurname: {},
      personName: {},
      personPatronymic: {},
      personPhone: {},
      notifyInSupport: {}
    };

    this.showApiKeyPerm = LocalStorageManager.client.permissions.apiKey;
  }

  ngAfterViewInit() {

    $(this.country.nativeElement)
      .suggestions({
        token: ApiLinks.dadataToken,
        type: 'COUNTRY',
        count: 5,
        hint: false,
        minChars: 2,
        bounds: 'country',
        onSelect: (suggestions: any) => { this.setSelectedCountry(suggestions); }
      });


    $(this.currency.nativeElement)
      .suggestions({
        token: ApiLinks.dadataToken,
        type: 'CURRENCY',
        count: 5,
        hint: false,
        minChars: 2,
        bounds: 'currency',
        onSelect: (suggestions: any) => { this.setSelectedCurrency(suggestions); }
      });
  }

  private setSelectedCurrency(country: any): void {
    this.selectedCurrency = new Currency();
    this.selectedCurrency.id = 0;
    this.selectedCurrency.code = country.data.code;
    this.selectedCurrency.strCode = country.data.strcode;
    this.selectedCurrency.name = country.data.name;
  }

  private setSelectedCountry(country: any): void {
    this.selectedCountry = new Country();
    this.selectedCountry.id = 0;
    this.selectedCountry.code = country.data.code;
    this.selectedCountry.alfa2 = country.data.alfa2;
    this.selectedCountry.alfa3 = country.data.alfa3;
    this.selectedCountry.nameShort = country.data.name_short;
  }

  ngOnInit() {
    this.profileForm = this.formBuilder.group({
      nameFirm: ['', [Validators.maxLength(50)]],
      personSurname: ['', [Validators.required, Validators.maxLength(50)]],
      personName: ['', [Validators.required, Validators.maxLength(50)]],
      personPatronymic: ['', Validators.maxLength(50)],
      personPhone: ['', Validators.maxLength(50)],
      country: ['', Validators.required],
      currency: ['', Validators.required],
      notifyInSupport: ['']
    });

    this.profileForm.patchValue(this.profileService.client);
    this.profileForm.controls.country.setValue(this.profileService.client.country.nameShort);
    this.profileForm.controls.currency.setValue(this.profileService.client.currency.name);
    this.profileForm.valueChanges.subscribe(() => {
      this.onProfileFormValuesChanged();
    });
  }

  onProfileFormValuesChanged() {
    for (const field in this.profileFormErrors) {
      if (!this.profileFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.profileFormErrors[field] = {};

      // Get the control
      const control = this.profileForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.profileFormErrors[field] = control.errors;
      }
    }
  }

  onSubmitProfileForm() {
    if (this.profileForm.valid) {
      this.clientSetValue();
      this.profileService.saveClient(this.profileService.client)
        .then((data) => {
          this.translateManagerService.openSnackBarTranslated('COMMON.SNACKBAR.SUCCSAVE', 'ОК');

          this.setNumberFormat(this.profileService.client.currency.strCode);
        })
        .catch((error) => {
          this.translateManagerService.openSnackBarTranslated('COMMON.SNACKBAR.ERRSAVE', 'ОК');
        });
    }
  }

  private setNumberFormat(currencyStrCode: string) {
    const localizedCode = LocalizationHelper.getLocalizedNumberCodeByISOCode(currencyStrCode);
    this.localizedNumbersService.setLocale(localizedCode);
  }

  private clientSetValue(): void {
    this.profileService.client.nameFirm = this.profileForm.value['nameFirm'];
    this.profileService.client.personSurname = this.profileForm.value['personSurname'];
    this.profileService.client.personPatronymic = this.profileForm.value['personPatronymic'];
    this.profileService.client.personPhone = (this.profileForm.controls.personPhone.value
      ? '+' : '')
      + (this.profileForm.controls.personPhone.value as string).replace(/\+/g, '');
    this.profileService.client.notifyInSupport = this.profileForm.value['notifyInSupport'];

    if (this.selectedCountry) {
      this.profileService.client.country = this.selectedCountry;
    }

    if (this.selectedCurrency) {
      this.profileService.client.currency = this.selectedCurrency;
    }
  }

  copyToClipBoard(text: string) {

    /* Select the text field */
    this.guidInp.nativeElement.select();

    /* Copy the text inside the text field */
    document.execCommand('Copy');

    /* Alert the copied text */
    this.translateManagerService.openSnackBarTranslated('COMMON.SNACKBAR.COPIEDTOCLIPBOARD', 'OK');
  }

  openGenerateGUIDDialog() {
    this.generateGUIDDialogRef = this.dialog.open(GenerateGuidDialogComponent, {
      disableClose: false,
      minWidth: '230px',
      maxWidth: '500px',
      minHeight: '600px',
      maxHeight: '1000px'
    });

    this.generateGUIDDialogRef.afterClosed().subscribe(() => {
      this.generateGUIDDialogRef = null;
    });
  }
}
