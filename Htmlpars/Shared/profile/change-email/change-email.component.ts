import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from '../profile.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { TranslateManagerService } from 'app/main/shared/services/translate-manager.service';
import { MyValidator } from 'app/main/shared/validators/my-validator';

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent implements OnInit {

  changeEmailForm: FormGroup;
  changeEmailFormErrors: any;

  constructor(
    private formBuilder: FormBuilder,
    public profileService: ProfileService,
    public translationLoader: FuseTranslationLoaderService,
    private translateManagerService: TranslateManagerService
  ) { 
    this.translationLoader.loadTranslations(english, russian);

    this.changeEmailFormErrors = {
      oldPassword: {},
      newPassword: {}
    };
  }

  ngOnInit() {
    this.changeEmailForm = this.formBuilder.group({
      newEmail: ['', [Validators.required, MyValidator.email]]
    });

    this.changeEmailForm.valueChanges.subscribe(() => {
      this.onChangeEmailFormValuesChanged();
    });
  }

  onChangeEmailFormValuesChanged() {
    for (const field in this.changeEmailFormErrors) {
      if (!this.changeEmailFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.changeEmailFormErrors[field] = {};

      // Get the control
      const control = this.changeEmailForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.changeEmailFormErrors[field] = control.errors;
      }
    }
  }

  onSubmitChangeEmailForm() {
    if (this.changeEmailForm.valid) {

      this.profileService.sendChangeEmailMess(this.changeEmailForm.controls.newEmail.value)
        .then((data) => {
          this.translateManagerService.openSnackBarTranslated('CHANGEEMAIL.SNACKBAR.EMAILSENDED', 'ОК');
        })
        .catch((error) => {
          this.translateManagerService.openSnackBarTranslated('CHANGEEMAIL.SNACKBAR.ERREMAILSEND', 'ОК');
        });
    }
  }
}
