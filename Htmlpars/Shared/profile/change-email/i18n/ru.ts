export const locale = {
    lang: 'ru',
    data: {
        'CHANGEEMAIL': {
            'TITLE': 'Изменение Email',
            'CHANGE': 'ИЗМЕНИТЬ',
            'FORM': {
                'NEWEMAIL': 'Новый Email',                  
                'ERROR': {
                }
            },
            'SNACKBAR': {
                'EMAILSENDED': 'Письмо для подтверждения Email отправлено',
                'ERREMAILSEND': 'Не удалось применить изменения'
            }
        }
    }
};
