export const locale = {
    lang: 'en',
    data: {
        'ADDMANAGERDIALOG': {
            'TITLE': 'Карточка сотрудника',
            'CREATE': 'СОЗДАТЬ',
            'OK': 'OK',
            'CANCEL': 'ОТМЕНА',
            'FORM': {
                'SURNAME': 'Фамилия',
                'NAME': 'Имя',
                'PATRONYMIC': 'Отчество',
                'POST': 'Должность',
                'ERROR': {
                    'SURNAMEEMPTY': 'Поле Фамилия не должно быть пустым',
                    'NAMEEMPTY': 'Поле Имя не должно быть пустым',
                    'PATRONYMICEMPTY': 'Поле Отчество не должно быть пустым',
                    'POSTCEMPTY': 'Поле Должность не должно быть пустым'
                }
            },
            'SNACKBAR': {
                'MANAGEREDITED': 'Изменения сохранены',
                'MANAGEREDITFAIL': 'Не удалось сохранить изменения',                
                'INVALIDDATA': 'Введены некорректные данные',
            }
        }
    }
};