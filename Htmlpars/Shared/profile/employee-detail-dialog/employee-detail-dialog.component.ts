import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { Employee } from 'app/main/shared/models/employee.model';
import * as _ from 'lodash';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';


@Component({
  selector: 'app-employee-detail-dialog',
  templateUrl: './employee-detail-dialog.component.html',
  styleUrls: ['./employee-detail-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EmployeeDetailDialogComponent implements OnInit {

  managerForm: FormGroup;
  managerFormErrors: any;

  manager: Employee = undefined;

  @Input() set editManager(val: Employee) {
    this.manager = _.cloneDeep(val);
  }

  constructor(
    public translationLoader: FuseTranslationLoaderService,
    public dialogRef: MatDialogRef<EmployeeDetailDialogComponent>,
    private formBuilder: FormBuilder,
  ) {
    this.translationLoader.loadTranslations(english, russian);

    this.managerFormErrors = {
      surname: {},
      name: {},
      patronymic: {},
      post: {}
    };
  }

  ngOnInit() {
    this.managerForm = this.formBuilder.group({
      surname:
        [this.manager ? this.manager.surname : '', Validators.required],
      name:
        [this.manager ? this.manager.name : '', Validators.required],
      patronymic:
        [this.manager ? this.manager.patronymic : '', Validators.required],
      post:
        [this.manager ? this.manager.post : '', []]
    });

    this.managerForm.valueChanges.subscribe(() => {
      this.onRegisterFormValuesChanged();
    });
  }

  onRegisterFormValuesChanged() {
    for (const field in this.managerFormErrors) {
      if (!this.managerFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.managerFormErrors[field] = {};

      // Get the control
      const control = this.managerForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.managerFormErrors[field] = control.errors;
      }
    }
  }

  onSubmit() {
    if (this.managerForm.valid) {

      const manager: Employee = this.manager || new Employee();
      manager.surname = this.managerForm.controls.surname.value;
      manager.name = this.managerForm.controls.name.value;
      manager.patronymic = this.managerForm.controls.patronymic.value;
      manager.post = this.managerForm.controls.post.value;

      this.dialogRef.close(manager);
    }
  }

}
