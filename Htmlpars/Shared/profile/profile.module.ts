import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { ProfileService } from './profile.service';
import { ProfileRoutingModule } from './profile-routing.module';
import { CommonComponent } from './common/common.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ChangeEmailComponent } from './change-email/change-email.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatFormFieldModule, MatInputModule, MatButtonModule, MatIconModule, MatDialogModule, 
  MatTooltipModule, MatMenuModule, MatTableModule, MatCheckboxModule, MatRadioModule, MatTabsModule, MatToolbarModule, MatSelectModule } from '@angular/material';
import { GenerateGuidDialogComponent } from './common/generate-guid-dialog/generate-guid-dialog.component';
import { TranslateManagerService } from 'app/main/shared/services/translate-manager.service';
import { RequisitListComponent } from './requisit-list/requisit-list.component';
import { CdkTableModule } from '@angular/cdk/table';
import { RequisitDetailDialogComponent } from './requisit-detail-dialog/requisit-detail-dialog.component';
import { RequisitListService } from 'app/main/content/client/profile/requisit-list/requisit-list.service';
import { FormsModule } from '@angular/forms';
import { EmployeeDetailDialogComponent } from './employee-detail-dialog/employee-detail-dialog.component';
import { RequisitDetailService } from 'app/main/content/client/profile/requisit-detail-dialog/requisit-detail-dialog.service';
import { SharedModule } from 'app/main/shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    CdkTableModule,
    MatDialogModule,
    MatTooltipModule,
    MatMenuModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTabsModule,
    MatToolbarModule,
    MatSelectModule,

    CommonModule,
    NgxMaskModule.forChild(),
    FuseSharedModule,
    TranslateModule,
    ProfileRoutingModule,
    FormsModule,
    SharedModule,
  ],
  declarations: [
    ProfileComponent,
    CommonComponent,
    ChangePasswordComponent,
    ChangeEmailComponent,
    GenerateGuidDialogComponent,
    RequisitListComponent,
    RequisitDetailDialogComponent,
    EmployeeDetailDialogComponent,
  ],
  providers: [
    ProfileService,
    TranslateManagerService,
    RequisitListService,
    RequisitDetailService
  ],
  entryComponents: [
    GenerateGuidDialogComponent,
    RequisitDetailDialogComponent,
    EmployeeDetailDialogComponent
  ]
})
export class ProfileModule { }
