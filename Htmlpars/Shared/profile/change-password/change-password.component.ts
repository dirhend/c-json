import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from '../profile.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';
import { TranslateManagerService } from 'app/main/shared/services/translate-manager.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  changePasswordFormErrors: any;

  constructor(
    private formBuilder: FormBuilder,
    public profileService: ProfileService,
    public translationLoader: FuseTranslationLoaderService,
    private translateManagerService: TranslateManagerService
  ) {
    this.translationLoader.loadTranslations(english, russian);

    this.changePasswordFormErrors = {
      oldPassword: {},
      newPassword: {}
    };
  }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', [Validators.required, Validators.maxLength(50), Validators.minLength(6)]],
      newPassword: ['', [Validators.required, Validators.maxLength(50), Validators.minLength(6)]]
    });

    this.changePasswordForm.valueChanges.subscribe(() => {
      this.onChangePasswordFormValuesChanged();
    });
  }

  onChangePasswordFormValuesChanged() {
    for (const field in this.changePasswordFormErrors) {
      if (!this.changePasswordFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.changePasswordFormErrors[field] = {};

      // Get the control
      const control = this.changePasswordForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.changePasswordFormErrors[field] = control.errors;
      }
    }
  }

  onSubmitChangePasswordForm() {
    if (this.changePasswordForm.valid) {

      this.profileService.changePassword(this.changePasswordForm.controls.oldPassword.value, this.changePasswordForm.controls.newPassword.value)
        .then((data) => {
          this.translateManagerService.openSnackBarTranslated('CHANGEPASS.SNACKBAR.SUCCCHANGE', 'ОК');
        })
        .catch((error) => {
          this.translateManagerService.openSnackBarTranslated('CHANGEPASS.SNACKBAR.ERRCHANGE', 'ОК');
        });
    }
  }
}
