export const locale = {
    lang: 'en',
    data: {
        'CHANGEPASS': {
                'TITLE': 'Password change',
                'SAVE': 'SAVE',
                'FORM': {
                    'OLDPASS': 'Old password',
                    'NEWPASS': 'New password',                  
                    'ERROR': {
                    }
                },
                'SNACKBAR': {
                    'SUCCCHANGE': 'The changes are saved',
                    'ERRCHANGE': 'Failed to save changes'
                }
        }
    }
};
