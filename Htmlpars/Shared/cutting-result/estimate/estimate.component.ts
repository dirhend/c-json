import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { OrderDetailService } from '@cutting/main/content/order/order-detail/order-detail.service';
import { fuseAnimations } from '@fuse-cut/animations';
import { FuseTranslationLoaderService } from '@fuse-cut/services/translation-loader.service';
import { CuttingPanelListService, Estimate, OrderStatusAction } from 'cloud-shared-lib';
import { NgxLocalizedNumbersService } from 'ngx-localized-numbers';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Component({
  selector: 'app-estimate',
  templateUrl: './estimate.component.html',
  styleUrls: ['./estimate.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class EstimateComponent implements OnInit {
  selectedMaterialId = 0;

  estimate: Estimate = new Estimate();

  get confirmedOrder(): boolean {
    return (
      this.orderDetailService.onOrderChanged.value.statusAction !== OrderStatusAction.Create &&
      this.orderDetailService.onOrderChanged.value.statusAction !== OrderStatusAction.CuttingCompleted &&
      this.orderDetailService.onOrderChanged.value.statusAction !== OrderStatusAction.Ordering
    );
  }

  get singleEstimateList(): boolean {
    return this.cuttingPanelListService.onCuttingOptionsChanged.value.specificationSettings.singleEstimateList;
  }

  get priceOrderWithDiscount(): number {
    return !this.estimate
      ? 0
      : this.estimate.PriceOrder -
          ((this.estimate.PriceMaterial * this.materialDiscountPercentage) / 100 + (this.estimate.PriceOperation * this.operationsDiscountPercentage) / 100);
  }

  @Input() materialDiscountPercentage = 0;
  @Input() operationsDiscountPercentage = 0;
  @Input() set materialId(value: number) {
    this.selectedMaterialId = value;
    this.getEstimate(value);
  }

  constructor(
    private orderDetailService: OrderDetailService,
    private cuttingPanelListService: CuttingPanelListService,
    private localizedNumbersService: NgxLocalizedNumbersService,
    private translationLoader: FuseTranslationLoaderService
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  ngOnInit() {
    if (this.selectedMaterialId > 0) {
      this.orderDetailService.onEstimateChanged.subscribe((response: Estimate) => {
        this.estimate = response;
      });
    } else {
      this.orderDetailService.onEstimateOrderChanged.subscribe((response: Estimate) => {
        this.estimate = response;
      });
    }
  }

  getEstimate(materialId: number) {
    if (materialId > 0) {
      this.orderDetailService.getEstimate(materialId);
    } else {
      this.orderDetailService.getOrderEstimate();
    }
  }
}
