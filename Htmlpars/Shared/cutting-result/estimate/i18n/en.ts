export const locale = {
  lang: 'en',
  data: {
    CUT_RESULT_ESTIMATE: {
      APPROXIMATE_CUT_MATERIAL_COST: 'Ориентировочная стоимость раскроя материала:',
      APPROXIMATE_ORDER_COST: 'Ориентировочная стоимость заказа:',
      CUT_MATERIAL_COST: 'Стоимость раскроя материала:',
      ORDER_COST: 'Стоимость заказа:',
      CUT_MATERIAL_COST_WITHOUT_DISCOUNT: 'Стоимость раскроя материала без скидки:',
      ORDER_COST_WITHOUT_DISCOUNT: 'Стоимость заказа без скидки:',
      DISCOUNT: 'Скидка',
      TOTAL: 'Итого',
      COST_WITH_DISCOUNT: 'Стоимость со скидкой',
      MATERIALS_TITLE: 'Материалы',
      NAME: 'Название',
      COUNT: 'Количество',
      MEASURE: 'Ед. измерения',
      WORKS_TITLE: 'Работы',
      CUTTING_AMOUNT_WARNING: 'Конечная стоимость раскроя будет указана после проверки администратором'
    }
  }
};
