import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuttingMapsComponent } from './cutting-maps.component';

describe('CuttingMapsComponent', () => {
  let component: CuttingMapsComponent;
  let fixture: ComponentFixture<CuttingMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuttingMapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuttingMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
