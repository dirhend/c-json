import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FuseTranslationLoaderService } from '@fuse-cut/services/translation-loader.service';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Component({
  selector: 'app-map-viewer-dialog',
  templateUrl: './map-viewer-dialog.component.html',
  styleUrls: ['./map-viewer-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MapViewerDialogComponent implements OnInit {

  mapSvg = '';

  constructor(
    public dialogRef: MatDialogRef<MapViewerDialogComponent>,
    private translationLoader: FuseTranslationLoaderService,
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  ngOnInit() {
  }

}
