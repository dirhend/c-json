import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapViewerDialogComponent } from './map-viewer-dialog.component';

describe('MapViewerDialogComponent', () => {
  let component: MapViewerDialogComponent;
  let fixture: ComponentFixture<MapViewerDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapViewerDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapViewerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
