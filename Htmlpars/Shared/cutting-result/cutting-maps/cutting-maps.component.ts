import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { MapViewerDialogComponent } from './map-viewer-dialog/map-viewer-dialog.component';
import { OrderDetailService } from '@cutting/main/content/order/order-detail/order-detail.service';

@Component({
  selector: 'app-cutting-maps',
  templateUrl: './cutting-maps.component.html',
  styleUrls: ['./cutting-maps.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CuttingMapsComponent implements OnInit {

  maps: string[] = [];
  selectedMaterialId = 0;

  mapViewerDialogRef: MatDialogRef<MapViewerDialogComponent>;

  @Input() set materialId(value: number) {
    this.selectedMaterialId = value;
    this.getMaps(value);
  }

  constructor(
    public dialog: MatDialog,
    private orderDetailService: OrderDetailService
  ) {
    this.orderDetailService.onCuttingMapsChanged
      .subscribe((response: string[]) => {
        this.maps = response;
      });

    // this.orderDetailService.onMaterialsChanged
    //   .subscribe((response: Material[]) => {
    //     this.materials = response;
    //     this.selectedMaterialId = this.materials[0] ? this.materials[0].id : 0;
    //   });
  }

  getMaps(materialId: number) {
    this.orderDetailService.getCuttingMaps(materialId);
  }

  openMapViewerDialog(mapSvg: string) {
    this.mapViewerDialogRef = this.dialog.open(MapViewerDialogComponent, {
      disableClose: false,
      panelClass: 'map-viewer-dialog',
      width: '800px'
    });

    this.mapViewerDialogRef.componentInstance.mapSvg = mapSvg;

    this.mapViewerDialogRef.afterClosed().subscribe(() => {

      this.mapViewerDialogRef = null;
    });
  }

  ngOnInit() {
  }

}
