import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { fuseAnimations } from '@fuse-cut/animations';
import { FuseTranslationLoaderService } from '@fuse-cut/services/translation-loader.service';
import { CuttingOffcut } from 'cloud-shared-lib';
import { NgxLocalizedNumbersService } from 'ngx-localized-numbers';

import { OrderDetailService } from '../../order-detail.service';
import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Component({
  selector: 'app-used-client-offcuts',
  templateUrl: './used-client-offcuts.component.html',
  styleUrls: ['./used-client-offcuts.component.scss'],
  animations: fuseAnimations
})
export class UsedClientOffcutsComponent implements OnInit {
  displayedColumns = ['height', 'width', 'count', 'area'];

  dataSource: MatTableDataSource<CuttingOffcut> | null;

  @Input() set materialId(value: number) {
    this.getOffcuts(value);
  }

  constructor(
    private orderDetailService: OrderDetailService,
    private localizedNumbersService: NgxLocalizedNumbersService,
    private translationLoader: FuseTranslationLoaderService
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  getOffcuts(materialId: number): void {
    this.orderDetailService.getUsedClientOffcuts(materialId);
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = [];

    this.orderDetailService.onUsedClientOffcutsChanged.subscribe((response: CuttingOffcut[]) => {
      this.dataSource.data = response;
    });
  }
}
