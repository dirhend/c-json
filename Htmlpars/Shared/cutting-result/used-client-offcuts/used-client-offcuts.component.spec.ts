import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsedClientOffcutsComponent } from './used-client-offcuts.component';

describe('UsedClientOffcutsComponent', () => {
  let component: UsedClientOffcutsComponent;
  let fixture: ComponentFixture<UsedClientOffcutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsedClientOffcutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsedClientOffcutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
