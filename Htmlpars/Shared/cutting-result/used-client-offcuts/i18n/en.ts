export const locale = {
    lang: 'en',
    data: {
        'USED_CLIENT_OFFCUTS': {
            'HEIGHT': 'Длина (мм)',
            'WIDTH': 'Ширина (мм)',
            'COUNT': 'Количество',
            'SQUARE': 'Площадь (кв.м.)'
        }
    }
};
