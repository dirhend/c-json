import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ApiLinks } from '@cutting/app.api-links';
import { OrderDetailService } from '@cutting/main/content/order/order-detail/order-detail.service';
import { FuseTranslationLoaderService } from '@fuse-cut/services/translation-loader.service';
import {
  customResponse,
  CuttingCuttedMaterial,
  CuttingMaterial,
  CuttingPanel,
  CuttingPanelListService,
  ICustomResponce,
} from 'cloud-shared-lib';
import { iif, Observable, of } from 'rxjs';
import { concatMap, filter, map, tap } from 'rxjs/operators';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Component({
  selector: 'app-cutting-result',
  templateUrl: './cutting-result.component.html',
  styleUrls: ['./cutting-result.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CuttingResultComponent implements OnInit {
  selectedMaterialId = 0;
  clientShowCuttingMaps: boolean | null = false;
  commonShowCuttingMaps = false;
  data$: Observable<ICustomResponce<CuttingCuttedMaterial[]>>;
  orderMaterials: CuttingMaterial[] = [];

  get showCuttingMaps() {
    return this.clientShowCuttingMaps === null ? this.commonShowCuttingMaps : this.clientShowCuttingMaps;
  }
  get linkToPanelsSchema(): Observable<string> {
    return this.orderDetailService.onOrderChanged.pipe(
      filter(o => o),
      map(o => ApiLinks.panelSchema(o.id))
    );
  }

  // FIXME: crutch for focus first tab
  @Input() showSubTabs = false;

  @Input() materialDiscountPercentage = 0;
  @Input() operationsDiscountPercentage = 0;

  constructor(
    private orderDetailService: OrderDetailService, //
    private cuttingPanelListService: CuttingPanelListService,
    private translationLoader: FuseTranslationLoaderService
  ) {
    this.translationLoader.loadTranslations(english, russian);

    this.cuttingPanelListService.onCuttingOptionsChanged.subscribe(response => (this.commonShowCuttingMaps = response.showCuttingMaps));

    this.cuttingPanelListService.onClientOptionsChanged.subscribe(response => (this.clientShowCuttingMaps = response.showCuttingMaps));

    this.orderDetailService.onMaterialsChanged.subscribe(m => (this.orderMaterials = m));

    this.data$ = this.getData();
  }

  ngOnInit() {}

  getSelectedPanels(selectedCuttedMaterialInMaterialBaseId: number): CuttingPanel[] {
    const mIndex = this.orderMaterials.findIndex(m => m.inMaterialBaseId === selectedCuttedMaterialInMaterialBaseId);
    return mIndex < 0 ? [] : this.orderMaterials[mIndex].panels;
  }

  getData(): Observable<ICustomResponce<CuttingCuttedMaterial[]>> {
    return this.orderDetailService.onEndCutting.pipe(
      concatMap(cr => iif(() => cr === 0, this.orderDetailService.getCuttedMaterials(), of([]))),
      customResponse<CuttingCuttedMaterial[]>()
    );
  }
}
