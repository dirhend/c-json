export const locale = {
  lang: 'ru',
  data: {
    CUT_RESULT: {
      ESTIMATE: 'Смета',
      OFFCUTS: 'Обрезки',
      CUTTING_MAPS: 'Карты раскроя',
      PANELS_SCHEMA: 'Чертежи панелей'
    }
  }
};
