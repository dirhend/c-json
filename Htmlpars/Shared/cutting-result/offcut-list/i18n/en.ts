export const locale = {
    lang: 'en',
    data: {
  "OFFCUT_LIST": {
    "TAKE_AWAY": "Забрать",
    "HEIGHT": "Длина (мм)",
    "WIDTH": "Ширина (мм)",
    "COUNT": "Количество",
    "SQUARE": "Площадь (кв.м.)"
  }
}
};
