export const locale = {
    lang: 'ru',
    data: {
  "OFFCUT_LIST": {
    "TAKE_AWAY": "Забрать",
    "HEIGHT": "Длина (мм)",
    "WIDTH": "Ширина (мм)",
    "COUNT": "Количество",
    "SQUARE": "Площадь (кв.м.)"
  }
}
};
