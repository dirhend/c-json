import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffcutListComponent } from './offcut-list.component';

describe('OffcutListComponent', () => {
  let component: OffcutListComponent;
  let fixture: ComponentFixture<OffcutListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffcutListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffcutListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
