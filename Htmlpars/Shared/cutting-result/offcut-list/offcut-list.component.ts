import { SelectionModel } from '@angular/cdk/collections';
import { Component, Input, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { OrderDetailService } from '@cutting/main/content/order/order-detail/order-detail.service';
import { fuseAnimations } from '@fuse-cut/animations';
import { FuseTranslationLoaderService } from '@fuse-cut/services/translation-loader.service';
import { CuttingOffcut } from 'cloud-shared-lib';
import { NgxLocalizedNumbersService } from 'ngx-localized-numbers';

import { locale as english } from './i18n/en';
import { locale as russian } from './i18n/ru';

@Component({
  selector: 'app-offcut-list',
  templateUrl: './offcut-list.component.html',
  styleUrls: ['./offcut-list.component.scss'],
  animations: fuseAnimations
})
export class OffcutListComponent implements OnInit {
  selectedMaterialId = 0;

  displayedColumns = ['takeAway', 'height', 'width', 'count', 'area'];

  dataSource: MatTableDataSource<CuttingOffcut> | null;
  selection = new SelectionModel<CuttingOffcut>(true, []);

  @Input() set materialId(value: number) {
    this.selectedMaterialId = value;
    this.getOffcuts(value);
  }

  constructor(
    private orderDetailService: OrderDetailService,
    private localizedNumbersService: NgxLocalizedNumbersService,
    private translationLoader: FuseTranslationLoaderService
  ) {
    this.translationLoader.loadTranslations(english, russian);
  }

  getOffcuts(materialId: number): void {
    this.orderDetailService.getOffcuts(materialId);
  }

  toggleTakeAwayOffcut(item: CuttingOffcut): void {
    this.selection.toggle(item);
    item.takeAway = this.selection.isSelected(item);
    this.orderDetailService.toggleTakeAwayOffcut(this.selectedMaterialId, item);
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = [];

    this.orderDetailService.onOffcutsChanged.subscribe((response: CuttingOffcut[]) => {
      this.dataSource.data = response;
      this.selection.clear();
      this.selection.select(...this.dataSource.data.filter(o => o.takeAway));
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.dataSource.data.filter(o => o.takeAway).forEach(row => this.toggleTakeAwayOffcut(row))
      : this.dataSource.data.filter(o => !o.takeAway).forEach(row => this.toggleTakeAwayOffcut(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: CuttingOffcut): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row`;
  }
}
