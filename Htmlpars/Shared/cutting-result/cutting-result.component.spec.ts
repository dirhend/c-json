import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CuttingResultComponent } from './cutting-result.component';

describe('CuttingResultComponent', () => {
  let component: CuttingResultComponent;
  let fixture: ComponentFixture<CuttingResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CuttingResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CuttingResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
