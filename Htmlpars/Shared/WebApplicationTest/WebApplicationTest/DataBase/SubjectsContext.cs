﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class SubjectsContext :DbContext
    {
        public DbSet<Subject> Subjects { get; set; }

        public SubjectsContext(DbContextOptions<SubjectsContext> options)
            : base(options)
        {
        }
    }
}
