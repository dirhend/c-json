﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebirdSql.EntityFrameworkCore.Firebird.Extensions;
using Microsoft.EntityFrameworkCore;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class GroupStudentsContext : DbContext
    {
        public DbSet<GroupStudent> GroupStudents { get; set; }

        public GroupStudentsContext(DbContextOptions<GroupStudentsContext> options)
            : base(options)
        {
        }
    }
}
