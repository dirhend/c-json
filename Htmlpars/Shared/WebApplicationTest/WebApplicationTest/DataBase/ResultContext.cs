﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class ResultContext : DbContext
    {
        public DbSet<Result> Result { get; set; }

        public ResultContext(DbContextOptions<ResultContext> options)
            : base(options)
        {
        }
    }
}
