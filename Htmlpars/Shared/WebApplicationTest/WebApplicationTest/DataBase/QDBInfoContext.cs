﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class QDBInfoContext : DbContext 
    {
        public DbSet<QDBInfo> QDBInfo { get; set; }

        public QDBInfoContext(DbContextOptions<QDBInfoContext> options)
            : base(options)
        {
        }
    }
}
