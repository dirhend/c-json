﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class SectionContext : DbContext
    {
        public DbSet<Section> Section { get; set; }

        public SectionContext(DbContextOptions<SectionContext> options)
            : base(options)
        {
        }
    }
}
