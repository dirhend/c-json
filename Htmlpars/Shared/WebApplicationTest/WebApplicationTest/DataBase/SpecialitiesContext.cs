﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class SpecialitiesContext : DbContext
    {
        public DbSet<Specialities> Specialities { get; set; }

        public SpecialitiesContext(DbContextOptions<SpecialitiesContext> options)
            : base(options)
        {
        }
    }
}
