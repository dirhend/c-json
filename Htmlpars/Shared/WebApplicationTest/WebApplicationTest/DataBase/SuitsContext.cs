﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class SuitsContext : DbContext
    {
        public DbSet<Suits> Suits { get; set; }

        public SuitsContext(DbContextOptions<SuitsContext> options)
            : base(options)
        {
        }
    }
}
