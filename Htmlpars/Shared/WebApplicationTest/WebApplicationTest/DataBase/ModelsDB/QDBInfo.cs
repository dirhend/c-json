﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("QDBINFO")]
    public class QDBInfo
    {
        [Key]
        [Column("NAME")]
        [MaxLength(20)]
        public string Name { get; set; }

        [Column("VALUE")]
        [MaxLength(255)]
        public string Value { get; set; }
    }
}
