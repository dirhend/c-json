﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("MESSAGES")]
    public class Messages
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("USERFROM_ID")]
        public int UserFromID { get; set; }

        [Column("USERTO_ID")]
        public int UserToID { get; set; }

        [Column("MESSAGETEXT")]
        [MaxLength(1024)]
        public string MessageText { get; set; }

        public User UserFrom { get; set; }
        public User UserTo { get; set; }
    }
}
