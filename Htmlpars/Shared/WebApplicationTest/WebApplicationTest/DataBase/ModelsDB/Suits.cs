﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("SUITS")]
    public class Suits
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("NAME")]
        [MaxLength(255)]
        public string Name { get; set; }

        [Column("TESTTIME")]
        public DateTime TestTime { get; set; }
        
        [Column("MARK3")]
        public int MarkOn3 { get; set; }

        [Column("MARK4")]
        public int MarkOn4 { get; set; }

        [Column("MARK5")]
        public int MarkOn5 { get; set; }

        [Column("SPECIALITY_ID")]
        public int SpecialitID { get; set; }
    }
}
