﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("USERS")]
    public class User
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("NAME")]
        [MaxLength(1024)]
        public string Name { get; set; }

        [Column("LOGIN")]
        [MaxLength(20)]
        public string Login { get; set; }

        [Column("PASSWORD")]
        [MaxLength(20)]
        public string Password { get; set; }

        [Column("USERTYPE")]
        public int UserType { get; set; }

        public List<User> Users { get; set; }
        public User()
        {
            Users = new List<User>();
        }
    }
}
