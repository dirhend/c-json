﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("SPECIALITIES")]
    public class Specialities
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("NAME")]
        [MaxLength(64)]
        public string Name { get; set; }
    }
}
