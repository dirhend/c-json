﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("RESULTS")]
    public class Result
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("SUIT_ID")]
        public int SuitID { get; set; }

        [Column("CLASS_ID")]
        public int ClassID { get; set; }

        [Column("STUDENT")]
        [MaxLength(64)]
        public string Student { get; set; }

        [Column("COMPNAME")]
        [MaxLength(64)]
        public string CompName { get; set; }

        [Column("STARTTIME")]
        public DateTime StartTime { get; set; }

        [Column("ENDTIME")]
        public DateTime? EndTime{ get; set; }

        [Column("FULLQCOUNT")]
        public int FullQcount { get; set; }

        [Column("CORRECTQCOUNT")]
        public int CorrectQcount{ get; set; }

        [Column("MARK")]
        public int Mark { get; set; }

        [Column("DELETED")]
        public int Deleted { get; set; }

        [Column("ADATA")]
        [MaxLength(4096)]
        public byte? AData { get; set; }

        [Column("ANSWEREDQCOUNT")]
        public int AnsverQcount { get; set; }

        [Column("RIGHTPERCENTAGE")]
        public int? RightPercentage { get; set; }
    }
}
