﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("QUESTIONS")]
    public class Question
    {
        [Key]
        [Column("QID")]
        public int QId { get; set; }

        [Column("SECTION_ID")]
        public int SectionID { get; set; }

        [Column("QUESTION_NAME")]
        [MaxLength(255)]
        public string QuestionName { get; set; }

        [Column("QUESTION_DATA")]
        [MaxLength(512)]
        public object QuestionData { get; set; }

        [Column("RATE")]
        public int Rate { get; set; }

        [Column("ANSWER_DATA")]
        [MaxLength(512)]
        public object AnswerData { get; set; }

        [Column("QUESTION_CRC")]
        public int QuestionCRC { get; set; }

        [Column("EDIT_TIME")]
        public DateTime EditTime { get; set; }

        [Column("AUTHOR")]
        public int Author { get; set; }
    }
}
