﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("USERRIGHTS")]
    public class UserRights
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("USER_ID")]
        public int User_id { get; set; }

        [Column("SUBJ_ID")]
        public int Subject_id { get; set; }

        [Column("ACCESS")]
        public int Access { get; set; }
    }
}
