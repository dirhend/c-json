﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("SUBJECTS")]
    public class Subject
    {
        [Key]
        [Column("SUBJ_ID")]
        public int SubjectID { get; set; }

        [Column("SUBJ_NAME")]
        [MaxLength(256)]
        public string SubjectName { get; set; }
    }
}
