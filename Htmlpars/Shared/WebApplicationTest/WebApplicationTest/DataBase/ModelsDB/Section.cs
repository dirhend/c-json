﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("SECTIONS")]
    public class Section
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("SUBJ_ID")]
        public int SubjectID { get; set; }

        [Column("NAME")]
        public string Name { get; set; }
    }
}
