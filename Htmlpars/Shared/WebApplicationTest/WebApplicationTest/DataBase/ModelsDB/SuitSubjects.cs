﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDB
{
    [Table("SUITSUBJECTS")]
    public class SuitSubjects
    {
        [Key]
        [Column("ID")]
        public int ID { get; set; }

        [Column("SUIT_ID")]
        public int SuitID { get; set; }

        [Column("SECT_ID")]
        public int SctID { get; set; }

        [Column("QNUM")]
        public int Qnum { get; set; }
    }
}
