﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class SuitSubjectsContext : DbContext
    {
        public DbSet<SuitSubjects> SuitSubjects { get; set; }

        public SuitSubjectsContext(DbContextOptions<SuitSubjectsContext> options)
            : base(options)
        {
        }
    }
}
