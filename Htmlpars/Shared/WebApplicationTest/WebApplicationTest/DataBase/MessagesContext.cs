﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class MessagesContext : DbContext
    {
        public DbSet<Messages> Messages { get; set; }

        public MessagesContext(DbContextOptions<MessagesContext> options)
            : base(options)
        {
        }
    }
}
