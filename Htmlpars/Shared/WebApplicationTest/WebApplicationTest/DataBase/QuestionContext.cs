﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class QuestionContext : DbContext
    {
        public DbSet<Question> Question { get; set; }

        public QuestionContext(DbContextOptions<QuestionContext> options)
            : base(options)
        {
        }
    }
}
