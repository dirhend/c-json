﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDTO
{
    public class QuestionDTO
    {
        public int QId { get; set; }
        public int SectionID { get; set; }
        public string QuestionName { get; set; }
        public object QuestionData { get; set; }
        public int Rate { get; set; }
        public object AnswerData { get; set; }
        public int QuestionCRC { get; set; }
        public DateTime EditTime { get; set; }
        public int Author { get; set; }
    }
}
