﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDTO
{
    public class ResultDTO
    {
        public int ID { get; set; }
        public int SuitID { get; set; }
        public int ClassID { get; set; }
        public string Student { get; set; }
        public string CompName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int FullQcount { get; set; }
        public int CorrectQcount { get; set; }
        public int Mark { get; set; }
        public int Deleted { get; set; }
        public byte? AData { get; set; }
        public int AnsverQcount { get; set; }
        public int? RightPercentage { get; set; }
    }
}
