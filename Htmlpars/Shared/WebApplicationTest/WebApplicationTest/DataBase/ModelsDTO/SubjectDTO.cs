﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDTO
{
    public class SubjectDTO
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
    }
}
