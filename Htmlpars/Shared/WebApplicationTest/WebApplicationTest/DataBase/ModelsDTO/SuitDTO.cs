﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDTO
{
    public class SuitDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime TestTime { get; set; }
        public int MarkOn3 { get; set; }
        public int MarkOn4 { get; set; }
        public int MarkOn5 { get; set; }
        public int SpecialitID { get; set; }
    }
}
