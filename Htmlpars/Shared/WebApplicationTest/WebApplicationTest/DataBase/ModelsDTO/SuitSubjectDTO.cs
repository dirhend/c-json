﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDTO
{
    public class SuitSubjectDTO
    {
        public int ID { get; set; }
        public int SuitID { get; set; }
        public int SctID { get; set; }
        public int Qnum { get; set; }
    }
}
