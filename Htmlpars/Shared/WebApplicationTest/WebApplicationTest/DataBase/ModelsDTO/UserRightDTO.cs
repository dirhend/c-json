﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDTO
{
    public class UserRightDTO
    {
        public int ID { get; set; }
        public int User_id { get; set; }
        public int Subject_id { get; set; }
        public int Access { get; set; }
    }
}
