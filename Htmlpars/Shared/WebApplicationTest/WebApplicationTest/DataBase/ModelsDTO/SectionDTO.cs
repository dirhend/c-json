﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDTO
{
    public class SectionDTO
    {
        public int ID { get; set; }
        public int SubjectID { get; set; }
        public string Name { get; set; }
    }
}
