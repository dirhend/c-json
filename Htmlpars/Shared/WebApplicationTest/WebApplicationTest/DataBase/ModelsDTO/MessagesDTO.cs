﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationTest.DataBase.ModelsDTO
{
    public class MessagesDTO
    {
        public int ID { get; set; }
        public int UserFromID { get; set; }
        public int UserToID { get; set; }
        public string MessageText { get; set; }
    }
}
