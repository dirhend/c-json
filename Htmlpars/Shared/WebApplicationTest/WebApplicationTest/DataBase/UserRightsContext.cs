﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase.ModelsDB;

namespace WebApplicationTest.DataBase
{
    public class UserRightsContext : DbContext
    {
        public DbSet<UserRights> UserRights { get; set; }

        public UserRightsContext(DbContextOptions<UserRightsContext> options)
            : base(options)
        {
        }
    }
}
