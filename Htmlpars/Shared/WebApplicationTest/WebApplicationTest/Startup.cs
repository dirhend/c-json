﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirebirdSql.EntityFrameworkCore.Firebird.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebApplicationTest.DataBase;

namespace WebApplicationTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("Develop", builder =>
            {
                builder
                    .WithOrigins("http://localhost:8080", "http://localhost:4200")
                    .AllowAnyMethod()
                    .AllowCredentials()
                    .AllowAnyHeader()
                    .WithExposedHeaders("Content-Disposition");
            }));

            string ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddMvc();
            services.AddEntityFrameworkFirebird().AddDbContext<UsersContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<GroupStudentsContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<UserRightsContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<SuitSubjectsContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<SuitsContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<SubjectsContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<SpecialitiesContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<SectionContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<ResultContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<QuestionContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<QDBInfoContext>(option => option.UseFirebird(ConnectionString));
            services.AddEntityFrameworkFirebird().AddDbContext<MessagesContext>(option => option.UseFirebird(ConnectionString));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("Develop");
            app.UseMvc();
        }
    }
}
