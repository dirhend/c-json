﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class SpecialitiesController : Controller
    {
        private readonly SpecialitiesContext _context;

        public SpecialitiesController(SpecialitiesContext context)
        {
            _context = context;
        }

        // GET api/Specialities
        [HttpGet]
        public IActionResult GetSpecialities()
        {
            var Speciality = _context.Specialities
                .Select(t => new SpecialityDTO()
                {
                    ID=t.ID,
                    Name=t.Name
                })
                .ToList();

            return Ok(Speciality);
        }

        // POST api/Specialities
        [HttpPost]
        public IActionResult AddSpeciality([FromBody] SpecialityDTO SpecialityAddDTO)
        {
            var NewSpeciality = new Specialities
            {
                ID=SpecialityAddDTO.ID,
                Name=SpecialityAddDTO.Name
            };
            _context.Specialities.Add(NewSpeciality);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/Specialities/5
        [HttpPut("{id}")]
        public IActionResult UpdateSpeciality(int id, [FromBody] SpecialityDTO SpecialityUpdateDTO)
        {
            var Speciality = _context.Specialities.Find(id);
            if (Speciality == null)
            {
                return BadRequest();
            }

            Speciality.Name = SpecialityUpdateDTO.Name;

            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/Specialities/5
        [HttpDelete("{id}")]
        public IActionResult DropSpeciality(int id)
        {
            var Speciality = _context.Specialities.Find(id);
            if (Speciality == null)
            {
                return BadRequest();
            }

            _context.Specialities.Remove(Speciality);
            _context.SaveChanges();
            return Ok();
        }

    }
}
