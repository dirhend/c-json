﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class QuestionController : Controller
    {
        private readonly QuestionContext _context;

        public QuestionController(QuestionContext context)
        {
            _context = context;
        }

        // GET api/Question
        [HttpGet]
        public IActionResult GetQuestion()
        {
            var Question = _context.Question
                .Select(t => new QuestionDTO()
                {
                    QId=t.QId,
                    SectionID=t.SectionID,
                    QuestionName=t.QuestionName,
                    //QuestionData=t.QuestionData,
                    Rate=t.Rate,
                    //AnswerData=t.AnswerData,
                    QuestionCRC=t.QuestionCRC,
                    EditTime=t.EditTime,
                    Author=t.Author
                })
                .ToList();

            return Ok(Question);
        }
        
        // POST api/Question
        [HttpPost]
        public IActionResult AddQuestion([FromBody] QuestionDTO QuestionAddDTO)
        {
            var NewQuestion = new Question
            {
                QId = QuestionAddDTO.QId,
                SectionID = QuestionAddDTO.SectionID,
                QuestionName = QuestionAddDTO.QuestionName,
                //QuestionData=QuestionAddDTO.QuestionData,
                Rate = QuestionAddDTO.Rate,
                //AnswerData=t.AnswerData,
                QuestionCRC = QuestionAddDTO.QuestionCRC,
                EditTime = QuestionAddDTO.EditTime,
                Author = QuestionAddDTO.Author
            };
            _context.Question.Add(NewQuestion);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/Question/5
        [HttpPut("{id}")]
        public IActionResult UpdateQuestion(int id, [FromBody] QuestionDTO QuestionUpdateDTO)
        {
            var Question = _context.Question.Find(id);
            if (Question == null)
            {
                return BadRequest();
            }

            Question.SectionID = QuestionUpdateDTO.SectionID;
            Question.QuestionName = QuestionUpdateDTO.QuestionName;
            //Question.QuestionData = QuestionUpdateDTO.QuestionData;
            Question.Rate = QuestionUpdateDTO.Rate;
            //Question.AnswerData = QuestionUpdateDTO.AnswerData;
            Question.QuestionCRC = QuestionUpdateDTO.QuestionCRC;
            Question.EditTime = QuestionUpdateDTO.EditTime;
            Question.Author = QuestionUpdateDTO.Author;


            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/Question/5
        [HttpDelete("{id}")]
        public IActionResult DropQuestion(int id)
        {
            var Question = _context.Question.Find(id);
            if (Question == null)
            {
                return BadRequest();
            }

            _context.Question.Remove(Question);
            _context.SaveChanges();
            return Ok();
        }
    }
}
