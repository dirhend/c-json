﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;
using WebApplicationTest.Pagination;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private readonly UsersContext _context;

        public UsersController(UsersContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(int page = 1)
        {
            int pageSize = 3;   // количество элементов на странице

            IQueryable<User> source = _context.Users.Include(x => x.Users);
            var count = await source.CountAsync();
            var items = await source.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            UserViewModel viewModel = new UserViewModel
            {
                PageViewModel = pageViewModel,
                Users = items
            };
            return View(viewModel);
        }

        // GET api/users
        [HttpGet]
        public IActionResult GetUsers()
        {
            var users = _context.Users
                .Select(t => new UserDTO()
                {
                    ID = t.ID,
                    Surname = t.Name.Substring(0, t.Name.IndexOf(' ')),
                    Name = t.Name.Substring(t.Name.IndexOf(' ') + 1),
                    Login = t.Login,
                    Password = t.Password,
                    UserType = t.UserType
                })
                .ToList();

            return Ok(users); 
        }
        
        // POST api/users
        [HttpPost]
        public IActionResult AddUser([FromBody] UserDTO userAddDTO)
        {
             var NewUser = new User
            {
                ID = userAddDTO.ID,
                Name = userAddDTO.Name,
                Login = userAddDTO.Login,
                Password = userAddDTO.Password,
                UserType = userAddDTO.UserType
            };
            _context.Users.Add(NewUser);
            _context.SaveChanges();
            return Ok();
            
        }

        // PUT api/users/5
        [HttpPut("{id}")]
        public IActionResult UpdateUser (int id, [FromBody] UserDTO UserUpdateDTO)
        {
            var user = _context.Users.Find(id);
            if (user == null)
            {
                return BadRequest();
            }

            user.Name = UserUpdateDTO.Name;
            user.Login = UserUpdateDTO.Login;
            user.Password = UserUpdateDTO.Password;
            user.UserType = UserUpdateDTO.UserType;
           
            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public IActionResult DropUser(int id)
        {
            var user = _context.Users.Find(id);
            if (user == null)
            {
                return BadRequest();
            }

            _context.Users.Remove(user);
            _context.SaveChanges();
            return Ok();
        }
    }
}
