﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class QDBInfoController : Controller
    {
        private readonly QDBInfoContext _context;

        public QDBInfoController(QDBInfoContext context)
        {
            _context = context;
        }

        // GET api/QDBInfo
        [HttpGet]
        public IActionResult GetQDBInfo()
        {
            var QDBInfo = _context.QDBInfo
                .Select(t => new QDBInfoDTO()
                {
                    Name=t.Name,
                    Value=t.Value
                })
                .ToList();

            return Ok(QDBInfo);
        }

        // POST api/QDBInfo
        [HttpPost]
        public IActionResult AddQDBInfo([FromBody] QDBInfoDTO QDBInfoAddDTO)
        {
            var NewQDBInfo = new QDBInfo
            {
                Name = QDBInfoAddDTO.Name,
                Value = QDBInfoAddDTO.Value
            };
            _context.QDBInfo.Add(NewQDBInfo);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/QDBInfo/5
        [HttpPut("{id}")]
        public IActionResult UpdateUser(string id, [FromBody] QDBInfoDTO QDBInfoUpdateDTO)
        {
            var QDBInfo = _context.QDBInfo.Find(id);
            if (QDBInfo == null)
            {
                return BadRequest();
            }

            QDBInfo.Name = QDBInfoUpdateDTO.Name;
            QDBInfo.Value = QDBInfoUpdateDTO.Value;

            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/QDBInfo/5
        [HttpDelete("{id}")]
        public IActionResult DropQDBInfo(string id)
        {
            var QDBInfo = _context.QDBInfo.Find(id);
            if (QDBInfo == null)
            {
                return BadRequest();
            }

            _context.QDBInfo.Remove(QDBInfo);
            _context.SaveChanges();
            return Ok();
        }
    }
}
