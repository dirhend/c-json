﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class SuitSubjectsController : Controller
    {

        private readonly SuitSubjectsContext _context;

        public SuitSubjectsController(SuitSubjectsContext context)
        {
            _context = context;
        }

        // GET api/SuitSubjects
        [HttpGet]
        public IActionResult GetSuitSubjects()
        {
            var SuitSubjects = _context.SuitSubjects
                .Select(t => new SuitSubjectDTO()
                {
                    ID = t.ID,
                    SuitID = t.SuitID,
                    SctID = t.SctID,
                    Qnum = t.Qnum

                })
                .ToList();

            return Ok(SuitSubjects);
        }

        // POST api/SuitSubjects
        [HttpPost]
        public IActionResult AddNewSuitSubject([FromBody] SuitSubjectDTO SuitSubjectAddDTO)
        {
            var NewSuitSubject = new SuitSubjects
            {
                ID = SuitSubjectAddDTO.ID,
                SuitID = SuitSubjectAddDTO.SuitID,
                SctID = SuitSubjectAddDTO.SctID,
                Qnum = SuitSubjectAddDTO.Qnum
            };
            _context.SuitSubjects.Add(NewSuitSubject);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/SuitSubjects/5
        [HttpPut("{id}")]
        public IActionResult UpdateSuitSubject(int id, [FromBody] SuitSubjectDTO SuitSubjectUpdateDTO)
        {
            var SuitSubject = _context.SuitSubjects.Find(id);
            if (SuitSubject == null)
            {
                return BadRequest();
            }

            SuitSubject.SuitID = SuitSubjectUpdateDTO.SuitID;
            SuitSubject.SctID = SuitSubjectUpdateDTO.SctID;
            SuitSubject.Qnum = SuitSubjectUpdateDTO.Qnum;

            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/SuitSubjects/5
        [HttpDelete("{id}")]
        public IActionResult DropSuitSubject(int id)
        {
            var SuitSubject = _context.SuitSubjects.Find(id);
            if (SuitSubject == null)
            {
                return BadRequest();
            }

            _context.SuitSubjects.Remove(SuitSubject);
            _context.SaveChanges();
            return Ok();
        }
    }
}
