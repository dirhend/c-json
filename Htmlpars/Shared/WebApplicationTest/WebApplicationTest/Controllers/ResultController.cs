﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class ResultController : Controller
    {
        private readonly ResultContext _context;

        public ResultController(ResultContext context)
        {
            _context = context;
        }

        // GET api/Result
        [HttpGet]
        public IActionResult GetResult()
        {
            var Result = _context.Result
                .Select(t => new ResultDTO()
                {
                    ID=t.ID,
                    SuitID=t.SuitID,
                    ClassID = t.ClassID,
                    Student = t.Student,
                    CompName = t.CompName,
                    StartTime = t.StartTime,
                    EndTime = t.EndTime,
                    FullQcount = t.FullQcount,
                    CorrectQcount = t.CorrectQcount,
                    Mark = t.Mark,
                    Deleted = t.Deleted,
                    //AData = t.AData,
                    AnsverQcount = t.AnsverQcount,
                    RightPercentage = t.RightPercentage
                })
                .ToList();

            return Ok(Result);
        }
        
        // POST api/Result
        [HttpPost]
        public IActionResult AddResult([FromBody] ResultDTO ResultAddDTO)
        {
            var NewResult = new Result
            {
                ID = ResultAddDTO.ID,
                SuitID = ResultAddDTO.SuitID,
                ClassID = ResultAddDTO.ClassID,
                Student = ResultAddDTO.Student,
                CompName = ResultAddDTO.CompName,
                StartTime = ResultAddDTO.StartTime,
                EndTime = ResultAddDTO.EndTime,
                FullQcount = ResultAddDTO.FullQcount,
                CorrectQcount = ResultAddDTO.CorrectQcount,
                Mark = ResultAddDTO.Mark,
                Deleted = ResultAddDTO.Deleted,
                //AData = ResultAddDTO.AData,
                AnsverQcount = ResultAddDTO.AnsverQcount,
                RightPercentage = ResultAddDTO.RightPercentage
            };
            _context.Result.Add(NewResult);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/Result/5
        [HttpPut("{id}")]
        public IActionResult UpdateResult(int id, [FromBody] ResultDTO ResultUpdateDTO)
        {
            var Result = _context.Result.Find(id);
            if (Result == null)
            {
                return BadRequest();
            }

            Result.SuitID = ResultUpdateDTO.SuitID;
            Result.ClassID = ResultUpdateDTO.ClassID;
            Result.Student = ResultUpdateDTO.Student;
            Result.CompName = ResultUpdateDTO.CompName;
            Result.StartTime = ResultUpdateDTO.StartTime;
            Result.EndTime = ResultUpdateDTO.EndTime;
            Result.FullQcount = ResultUpdateDTO.FullQcount;
            Result.CorrectQcount = ResultUpdateDTO.CorrectQcount;
            Result.Mark = ResultUpdateDTO.Mark;
            Result.Deleted = ResultUpdateDTO.Deleted;
            //Result.AData = ResultUpdateDTO.AData;
            Result.AnsverQcount = ResultUpdateDTO.AnsverQcount;
            Result.RightPercentage = ResultUpdateDTO.RightPercentage;

            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/Result/5
        [HttpDelete("{id}")]
        public IActionResult DropResult(int id)
        {
            var Result = _context.Result.Find(id);
            if (Result == null)
            {
                return BadRequest();
            }

            _context.Result.Remove(Result);
            _context.SaveChanges();
            return Ok();
        }
    }
}
