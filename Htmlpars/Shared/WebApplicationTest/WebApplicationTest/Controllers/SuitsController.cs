﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class SuitsController : Controller
    {
        private readonly SuitsContext _context;

        public SuitsController(SuitsContext context)
        {
            _context = context;
        }

        // GET api/Suits
        [HttpGet]
        public IActionResult GetSuits()
        {
            var Suits = _context.Suits
                .Select(t => new SuitDTO()
                {
                    ID = t.ID,
                    Name = t.Name,
                    TestTime = t.TestTime,
                    MarkOn3 = t.MarkOn3,
                    MarkOn4 = t.MarkOn4,
                    MarkOn5 = t.MarkOn5,
                    SpecialitID = t.SpecialitID
                })
                .ToList();

            return Ok(Suits);
        }

        // POST api/Suits
        [HttpPost]
        public IActionResult AddSuits([FromBody] SuitDTO SuitAddDTO)
        {
            var NewSuit = new Suits
            {
                ID = SuitAddDTO.ID,
                Name = SuitAddDTO.Name,
                TestTime = SuitAddDTO.TestTime,
                MarkOn3 = SuitAddDTO.MarkOn3,
                MarkOn4 = SuitAddDTO.MarkOn4,
                MarkOn5 = SuitAddDTO.MarkOn5,
                SpecialitID = SuitAddDTO.SpecialitID
            };
            _context.Suits.Add(NewSuit);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/Suits/5
        [HttpPut("{id}")]
        public IActionResult UpdateSuit(int id, [FromBody] SuitDTO SuitUpdateDTO)
        {
            var Suit = _context.Suits.Find(id);
            if (Suit == null)
            {
                return BadRequest();
            }

            Suit.Name = SuitUpdateDTO.Name;
            Suit.TestTime = SuitUpdateDTO.TestTime;
            Suit.MarkOn3 = SuitUpdateDTO.MarkOn3;
            Suit.MarkOn4 = SuitUpdateDTO.MarkOn4;
            Suit.MarkOn5 = SuitUpdateDTO.MarkOn5;
            Suit.SpecialitID = SuitUpdateDTO.SpecialitID;


            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/Suits/5
        [HttpDelete("{id}")]
        public IActionResult DropSuit(int id)
        {
            var Suit = _context.Suits.Find(id);
            if (Suit == null)
            {
                return BadRequest();
            }

            _context.Suits.Remove(Suit);
            _context.SaveChanges();
            return Ok();
        }
    }
}
