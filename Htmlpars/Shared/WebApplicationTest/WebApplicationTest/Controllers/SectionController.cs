﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class SectionController : Controller
    {
        private readonly SectionContext _context;

        public SectionController(SectionContext context)
        {
            _context = context;
        }

        // GET api/Section
        [HttpGet]
        public IActionResult GetSection()
        {
            var Section = _context.Section
                .Select(t => new SectionDTO()
                {
                    ID=t.ID,
                    SubjectID=t.SubjectID,
                    Name=t.Name
                })
                .ToList();

            return Ok(Section);
        }

        // POST api/Section
        [HttpPost]
        public IActionResult AddSection([FromBody] SectionDTO SectionAddDTO)
        {
            var NewSection = new Section
            {
                ID= SectionAddDTO.ID,
                SubjectID=SectionAddDTO.SubjectID,
                Name=SectionAddDTO.Name
            };
            _context.Section.Add(NewSection);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/Section/5
        [HttpPut("{id}")]
        public IActionResult UpdateSection(int id, [FromBody] SectionDTO SectionUpdateDTO)
        {
            var Section = _context.Section.Find(id);
            if (Section == null)
            {
                return BadRequest();
            }

            Section.Name = SectionUpdateDTO.Name;
            Section.Name = SectionUpdateDTO.Name;

            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/Section/5
        [HttpDelete("{id}")]
        public IActionResult DropSection(int id)
        {
            var Section = _context.Section.Find(id);
            if (Section == null)
            {
                return BadRequest();
            }

            _context.Section.Remove(Section);
            _context.SaveChanges();
            return Ok();
        }
    }
}
