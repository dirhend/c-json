﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class UserRightsController : Controller
    {
        private readonly UserRightsContext _context;

        public UserRightsController(UserRightsContext context)
        {
            _context = context;
        }

        // GET api/UserRights
        [HttpGet]
        public IActionResult GetRights()
        {
            var Rights = _context.UserRights
                .Select(t => new UserRightDTO()
                {
                    ID = t.ID,
                    User_id = t.User_id,
                    Subject_id=t.Subject_id,
                    Access=t.Access
                })
                .ToList();

            return Ok(Rights);
        }

        // POST api/UserRights
        [HttpPost]
        public IActionResult AddRight([FromBody] UserRightDTO RightAddDTO)
        {
            var NewRight = new UserRights
            {
                ID = RightAddDTO.ID,
                User_id = RightAddDTO.User_id,
                Subject_id = RightAddDTO.Subject_id,
                Access = RightAddDTO.Access
            };
            _context.UserRights.Add(NewRight);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/UserRights/5
        [HttpPut("{id}")]
        public IActionResult UpdateUser(int id, [FromBody] UserRightDTO RightUpdateDTO)
        {
            var Right = _context.UserRights.Find(id);
            if (Right == null)
            {
                return BadRequest();
            }

            Right.User_id = RightUpdateDTO.User_id;
            Right.Subject_id = RightUpdateDTO.Subject_id;
            Right.Access = RightUpdateDTO.Access;

            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/UserRights/5
        [HttpDelete("{id}")]
        public IActionResult DropRight(int id)
        {
            var Right = _context.UserRights.Find(id);
            if (Right == null)
            {
                return BadRequest();
            }

            _context.UserRights.Remove(Right);
            _context.SaveChanges();
            return Ok();
        }
    }
}
