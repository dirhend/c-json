﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : Controller
    {
        private readonly MessagesContext _context;

        public MessagesController(MessagesContext context)
        {
            _context = context;
        }

        // GET api/Messages
        [HttpGet]
        public IActionResult GetMessages()
        {
            var Messages = _context.Messages
                .Select(t => new MessagesDTO()
                {
                    ID = t.ID,
                    UserFromID = t.UserFromID,
                    UserToID = t.UserToID,
                    MessageText = t.MessageText
                })
                .ToList();

            return Ok(Messages);
        }

        // POST api/Messages
        [HttpPost]
        public IActionResult AddMessage([FromBody] MessagesDTO MessageAddDTO)
        {
            var NewMessage = new Messages
            {
                ID = MessageAddDTO.ID,
                UserFromID= MessageAddDTO.UserFromID,
                UserToID= MessageAddDTO.UserToID,
                MessageText= MessageAddDTO.MessageText
            };
            _context.Messages.Add(NewMessage);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/Messages/5
        [HttpPut("{id}")]
        public IActionResult UpdateMessage(int id, [FromBody] MessagesDTO MessageUpdateDTO)
        {
            var Message = _context.Messages.Find(id);
            if (Message == null)
            {
                return BadRequest();
            }

            Message.UserFromID = MessageUpdateDTO.UserFromID;
            Message.UserToID = MessageUpdateDTO.UserToID;
            Message.MessageText = MessageUpdateDTO.MessageText;

            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/Messages/5
        [HttpDelete("{id}")]
        public IActionResult DropMessage(int id)
        {
            var Message = _context.Messages.Find(id);
            if (Message == null)
            {
                return BadRequest();
            }

            _context.Messages.Remove(Message);
            _context.SaveChanges();
            return Ok();
        }
    }
}
