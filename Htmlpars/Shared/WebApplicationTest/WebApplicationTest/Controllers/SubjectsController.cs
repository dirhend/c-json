﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class SubjectsController : Controller
    {
        private readonly SubjectsContext _context;

        public SubjectsController(SubjectsContext context)
        {
            _context = context;
        }

        // GET api/Subjects
        [HttpGet]
        public IActionResult GetSubjects()
        {
            var Subjects = _context.Subjects
                .Select(t => new SubjectDTO()
                {
                    SubjectID = t.SubjectID,
                    SubjectName = t.SubjectName
                })
                .ToList();

            return Ok(Subjects);
        }

        // POST api/Subjects
        [HttpPost]
        public IActionResult AddNewSubject([FromBody] SubjectDTO SubjectAddDTO)
        {
            var NewSubject = new Subject
            {
                SubjectID=SubjectAddDTO.SubjectID,
                SubjectName=SubjectAddDTO.SubjectName   
            };
            _context.Subjects.Add(NewSubject);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/Subjects/5
        [HttpPut("{id}")]
        public IActionResult UpdateSubject(int id, [FromBody] SubjectDTO SubjectUpdateDTO)
        {
            var Subject = _context.Subjects.Find(id);
            if (Subject == null)
            {
                return BadRequest();
            }

            Subject.SubjectName = SubjectUpdateDTO.SubjectName;

            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/Subjects/5
        [HttpDelete("{id}")]
        public IActionResult DropSubject(int id)
        {
            var Subject = _context.Subjects.Find(id);
            if (Subject == null)
            {
                return BadRequest();
            }

            _context.Subjects.Remove(Subject);
            _context.SaveChanges();
            return Ok();
        }
    }
}
