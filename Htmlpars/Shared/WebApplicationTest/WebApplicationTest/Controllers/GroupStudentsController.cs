﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplicationTest.DataBase;
using WebApplicationTest.DataBase.ModelsDB;
using WebApplicationTest.DataBase.ModelsDTO;

namespace WebApplicationTest.Controllers
{
    [Route("api/[controller]")]
    public class GroupStudentsController : Controller
    {
        private readonly GroupStudentsContext _context;

        public GroupStudentsController(GroupStudentsContext context)
        {
            _context = context;
        }

        // GET api/GroupStudents
        [HttpGet]
        public IActionResult GetGroupStudents()
        {
            var GroupStudents = _context.GroupStudents
                .Select(t => new GroupStudentDTO()
                {
                    ID = t.ID,
                    Name = t.Name,
                    SpecID = t.SpecID
                })
                .ToList();

            return Ok(GroupStudents);
        }

        // POST api/GroupStudents
        [HttpPost]
        public IActionResult AddGroupStudent ([FromBody] GroupStudentDTO GroupAddDTO)
        {
            var GroupStudent = new GroupStudent
            {
                ID = GroupAddDTO.ID,
                Name = GroupAddDTO.Name,
                SpecID = GroupAddDTO.SpecID
            };
            _context.GroupStudents.Add(GroupStudent);
            _context.SaveChanges();
            return Ok();

        }

        // PUT api/GroupStudents/5
        [HttpPut("{id}")]
        public IActionResult UpdateGroupStudents (int id, [FromBody] GroupStudentDTO GroupStudentsUpdateDTO)
        {
            var GroupStudent = _context.GroupStudents.Find(id);
            if (GroupStudent == null)
            {
                return BadRequest();
            }

            GroupStudent.Name = GroupStudentsUpdateDTO.Name;
            GroupStudent.SpecID = GroupStudentsUpdateDTO.SpecID;

            _context.SaveChanges();
            return Ok();
        }

        // DELETE api/GroupStudents/5
        [HttpDelete("{id}")]
        public IActionResult DropGroupStudents(int id)
        {
            var GroupStudents = _context.GroupStudents.Find(id);
            if (GroupStudents == null)
            {
                return BadRequest();
            }

            _context.GroupStudents.Remove(GroupStudents);
            _context.SaveChanges();
            return Ok();
        }
    }
}
