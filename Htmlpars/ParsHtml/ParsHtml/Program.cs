﻿using System;
using System.Collections.Generic;

namespace ParsHtml
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Result> results = new List<Result>();
            Console.WriteLine("введите путь до каталога");
            string Path = Console.ReadLine();
            SearchandRead.Search(Path, results);
            Result.Output(results);
            Console.ReadKey();
        }
    }
}
