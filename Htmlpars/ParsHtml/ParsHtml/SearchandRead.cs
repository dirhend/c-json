﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ParsHtml
{
    class SearchandRead
    {
        static public void Search(string Path, List<Result> result)
        {
            if (!Directory.Exists(Path))
                return;
            string[] dirs = Directory.GetDirectories(Path);

            foreach (string dir in dirs)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(dir);

                string[] files = Directory.GetFiles(dir);
                foreach (var file in files)
                {
                    DirectoryInfo dirInfofile = new DirectoryInfo(file);
                    if (dirInfofile.Extension == ".html")
                    {
                        FileRead(file, result);
                    }
                }
                if (Directory.EnumerateDirectories(dir) != null)
                    Search(dir, result);
            }
        }
        public static void FileRead(string path, List<Result> result)
        {
            List<string> tags = new List<string>();
            int countTranslate = 0;
            string htmlfile = "";
            using (StreamReader sr = new StreamReader(path, Encoding.Default))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    htmlfile += line;
                    string[] words = line.Split(new char[] { ' ' });
                    foreach (var word in words)
                    {
                        word.Trim();
                        if (word == "translate")
                        {
                            countTranslate++;
                            string сutstring = htmlfile.Substring(htmlfile.LastIndexOf("<"), htmlfile.Length - htmlfile.LastIndexOf("<"));
                            string[] wordscutstring = сutstring.Split(new char[] { ' ' });
                            tags.Add(wordscutstring[0].Trim(new char[] { '<', '>', ' ', '/' }));
                        }
                    }
                }
            }
            FileInfo fileInf = new FileInfo(path);
            result.Add(new Result()
            {
                count = countTranslate,
                filename = fileInf.Name,
                tags = new List<string>(tags)
            });
            tags.Clear();
        }
    }
}
