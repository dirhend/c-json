﻿using System.IO;
using System.Collections.Generic;
using System.Text;

namespace ParsHtml
{
    class Result
    {
        public List<string> tags = new List<string>();
        public int count;
        public string filename;
        public static void Output(List<Result> results)
        {
            using (File.Create(($@"Debug\..\..\..\..\..\allresult.txt"))) ;
            using (StreamWriter sw = new StreamWriter($@"Debug\..\..\..\..\..\allresult.txt", false, Encoding.Default))
            {
                foreach (var result in results)
                {
                    sw.WriteLine("\nИмя файла : " + result.filename + "\n Количество тегов : " + result.count);
                    foreach (var tag in result.tags)
                    {
                        sw.WriteLine(tag);
                    }
                }
            }
        }
    }
}
